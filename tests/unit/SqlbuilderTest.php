<?php

namespace unit;

use Codeception\Test\Unit;
use Exception;
use tests\unit\models\Dhcpdomain;
use tests\unit\models\Dhcphost;
use tests\unit\models\Dhcpoptiontype;
use tests\unit\models\Org;
use tests\unit\models\Person;
use uhi67\uxapp\DBX;
use uhi67\uxapp\Query;
use uhi67\uxapp\UXApp;
use uhi67\uxapp\UXAppException;
use UnitTester;

require_once __DIR__ ."/models/Dhcphost.php";
require_once __DIR__ ."/models/Dhcpdomain.php";
require_once __DIR__ ."/models/Dhcpoption.php";
require_once __DIR__ ."/models/Dhcpoptionname.php";
require_once __DIR__ ."/models/Dhcpoptiontype.php";
require_once __DIR__ ."/models/Org.php";
require_once __DIR__ ."/models/Person.php";
/**
 * sql builder is part of DBX
 */
class SqlbuilderTest extends Unit {
    /** @var DBX $db */
    public $db;

    /**
     * @var UnitTester
     */
    protected $tester;

    /** @noinspection PhpMethodNamingConventionInspection */

    protected function _before() {
        UXApp::$app->setLocale('hu-HU');
        $this->db = UXApp::$app->db;
    }

    /**
     * @dataProvider provIsOperator
     *
     * @param string $op
     * @param bool|integer $expected
     *
     * @return void
     */
    function testIsOperator($op, $expected) {
        $result = $this->db->isOperator($op);
        $this->assertSame($expected, $result);
    }
    function provIsOperator() {
        return [
            // test case data
            ['', false],
            ['xxxx', false],
            ['true', 0],
            ['TRUE', 0],
            ['not', 1],
            ['or', 4],
            ['-', 2],
        ];
    }

    /**
     * @dataProvider provOperatorPrecedence
     *
     * @param mixed $op
     * @param mixed $expected
     * @return void
     */
    function testOperatorPrecedence($op, $expected) {
        $result = $this->db->operatorPrecedence($op);
        $this->assertSame($expected, $result);
    }
    function provOperatorPrecedence() {
        return [
            ['', false],
            ['xxxx', false],
            ['true', 1],
            ['FALSE', 2],
            ['-', 10],
            ['not', 34],
            ['or', 36],
        ];
    }

    /**
     * @dataProvider provPreprocessAssociative
     *
     * @param array $expected
     * @param array $list
     *
     * @return void
     */
    function testPreprocessAssociative($expected, $list) {
        $result = $this->db->preprocessAssociative($list);
        $this->assertEquals($expected, $result);
    }
    function provPreprocessAssociative() {
        return [
            [
                [],
                []
            ],
            [
                [1,2,3],
                [1,2,3]
            ],
            [
                ['apple', ['=', 'p', 'peach']],
                ['apple', 'p'=>'peach'],
            ],
            [
                ['apple', 'bbb', ['=', 'p', 'peach']],
                ['apple', 'p'=>'peach', 'bbb'],
            ],
            [
                [['=', 'a', 'apple'], ['=', 'p', 'peach']],
                ['a'=>'apple', 'p'=>'peach'],
            ],
        ];
    }

    /**
     * @dataProvider provQuoteName
     *
     * @param string $name
     * @param string $expected
     *
     * @return void
     * @throws UXAppException
     */
    function testQuoteName($name, $expected) {
        $result = $this->db->quoteName($name);
        if($this->db->type=='MY') $expected = str_replace('"', '`', $expected);
        $this->assertEquals($expected, $result);
    }
    function provQuoteName() {
        return [
            ['field', '"field"'],
            ['table.field', '"table"."field"'],
        ];
    }

    /**
     * @throws UXAppException
     */
    function testBuildExpression1() {
        $this->assertEquals('2', $this->db->buildExpression(2));
        $this->assertEquals("'literal'", $this->db->buildExpression("'literal'"));
    }

    /**
     * @dataProvider provBuildFieldNameValue
     *
     * @param string $name
     * @param string $prefix
     * @param string $expected
     *
     * @return void
     * @throws UXAppException
     */
    function testBuildFieldNameValue($name, $prefix, $expected) {
        $result = $this->db->buildFieldNameValue($name, $prefix);
        if($this->db->type=='MY') $expected = str_replace('"', '`', $expected);
        $this->assertEquals($expected, $result);
    }
    function provBuildFieldNameValue() {
        return [
            ['field', null, '"field"'],
            ['table.field', null, '"table"."field"'],
            ['field', 'table', '"table"."field"'],
            ["'literal'", null, "'literal'"],
            ["'literal'", 'a', "'literal'"],
            ["(expression)", null, "(expression)"],
            ['$param1', null, '$param1'],
            ['$2', null, '$2'],
            [['host()', 'range'], 'ip', 'host("ip"."range")']
        ];
    }

    /**
     * @dataProvider provBuildFieldname
     *
     * @param string $name
     * @param string $prefix
     * @param string $output
     * @param string $expected
     *
     * @return void
     * @throws UXAppException
     */
    function testBuildFieldname($name, $prefix, $output, $expected) {
        $result = $this->db->buildFieldName($name, $prefix, $output);
        if($this->db->type=='MY') $expected = str_replace('"', '`', $expected);
        $this->assertEquals($expected, $result);
    }
    function provBuildFieldname() {
        return [
            ['field', null, null, '"field"'],
            ['table.field', null, null, '"table"."field"'],
            ['field', 'table', null, '"table"."field"'],
            ['field', 'table', 'f', '"table"."field" AS "f"'],
            ["'literal'", null, 'b', "'literal' AS \"b\""],
            ["'literal'", 'table1', 'b', "'literal' AS \"b\""],
            ["(expression)", 'a', null, "(expression)"],
            ['$param1', null, null, '$param1'],
            ['$2', null, 'e', '$2 AS "e"'],
            [['host()', 'range'], 'ip', 'f', 'host("ip"."range") AS "f"']
        ];
    }

    /**
     * @dataProvider provBuildFieldnames
     *
     * @param $models
     * @param array $fields
     * @param $expected_number
     * @param $expected_sql
     *
     * @return void
     * @throws UXAppException
     */
    function testBuildFieldnames($models, $fields, $expected_number, $expected_sql) {
        $fields = $this->tester->evaluateItems($fields, $this);
        $result = $this->db->buildFieldNames($models, $fields, $number);
        $this->assertEquals($expected_number, $number);
        if($this->db->type=='MY') $expected_sql = str_replace('"', '`', $expected_sql);
        $this->assertEquals($expected_sql, $result);
    }
    function provBuildFieldnames() {
        return [
            [[Org::class], ['name'], 1, '"name"'],
            [[Org::class, 'd'=>Person::class], ['org.name', 'deleter_name'=>'d.name'], 2, '"org"."name", "d"."name" AS "deleter_name"'],
            [[Org::class], ['name', 'type'=>'(\'type1\')'], 2, '"name", (\'type1\') AS "type"'],
            [
                [Dhcpdomain::class],
                [
                    'ip' => ['host()', ['coalesce()', ['-', 'minip', 1], 'r.range']],
                    'netclass'=>'#eval:return $scope->db->literal(\'range_end\');',
                ],
                2,
                'host(coalesce("minip" - 1, "r"."range")) AS "ip", \'range_end\' AS "netclass"'
            ],
            [
                [Dhcpdomain::class, 'p'=>Dhcpdomain::class],
                [
                    'ip' => ['host()', ['coalesce()', ['-', 'minip', 1], 'r.range']],
                    'netclass'=>'#eval:return $scope->db->literal(\'range_end\');',
                ],
                2,
                'host(coalesce("dhcpdomain"."minip" - 1, "r"."range")) AS "ip", \'range_end\' AS "netclass"'
            ],
            // Automatic output alias
            [
                [Person::class], ['name', 'unit1.name'], 2, '"name", "unit1"."name" AS "unit1_name"'
            ]
        ];
    }

    /**
     * @dataProvider provBuildExpression
     *
     * @param $expected
     * @param $expression
     * @param null $alias
     *
     * @return void
     * @throws UXAppException
     */
    function testBuildExpression($expected, $expression, $alias=null) {
        $result = $this->db->buildExpression($expression, $alias);
        #$this->compare($expected, $result);
        if($this->db->type=='MY') {
            $expected = str_replace('"', '`', $expected);
            $expected = str_replace('~*', 'RLIKE', $expected);
        }
        $this->assertEquals($expected, $result);
    }

    /**
     * @return array
     */
    function provBuildExpression() {
        return [
            // expected, expression, alias
            ['"field"', 'field', null],
            ['(true)', '(true)', null],
            ['TRUE', ['true'], null],
            ['$1', '$1', null],
            ['$p', '$p', null],
            ['TRUE', []],
            ['"alias"."field"', 'field', 'alias'],
            ['"id" = \'alma\'',
                ['id' => "'alma'"], null],
            ['"id" = 23',
                ['=', 'id', 23], null],
            ['"id" = 23 AND "x" = 4',
                ['id' => 23, 'x' => 4], null],
            10 => ['"x" = 4 AND "id" = 23',
                ['and', 'id' => 23, ['=', 'x', 4]], null],
            ['((x=5) OR "x" = 4) AND "id" = 23',
                ['and', 'id' => 23, ['or', 'x' => 4, '(x=5)']], null],
            ['"id" IS NULL',
                ['is null', 'id'], null],
            ['"id" IS NULL OR "parent" IS NOT NULL',
                ['or', ['is null', 'id'], ['is not null', 'parent']], null],
            ['"id" IS NULL OR "parent" IS NOT NULL AND (y>3)',
                ['or', ['is null', 'id'], ['and', ['is not null', 'parent'], '(y>3)']], null],
            ['"a" BETWEEN 1 AND 2',
                ['between', 'a', 1, 2], null],
            ['"a" = 1 AND "b" = 2 AND "c" = 3',
                ['a' => 1, 'b' => 2, 'c' => 3], null],
            ['"a" IN (1, 2, 3)',
                ['in', 'a', [1, 2, 3]], null],
            ['"a" IN (select id from datatype)',
                ['in', 'a', new Query(['sql' => /** @lang text */ 'select id from datatype', 'db' => $this])], null],
            ['"t1"."a" ~* $1 AND "t2"."b"',
                ['and', ['rlike', 'a', '$1'], 't2.b'], 't1'],
            ['samlres_ancestor("a", $1)',
                ['samlres_ancestor()', 'a', '$1'], null],
            ['"id" = 0',
                ['id' => 0], null],
            ['"id" != $1 AND samlres_ancestor("id", $1) AND "restype" NOT IN (0, 1)', ['and',
                ['!=', 'id', '$1'],
                ['samlres_ancestor()', 'id', '$1'],
                ['not in', 'restype', [0, 1]]
            ], null],
            ['TRUE', ['AND'], null],
            ['FALSE', ['OR'], null],
            ["''", ['||'], null],
            ["2 * 3", ['*', 2, 3], null],
            [1, ['*'], null],
            ['"dataset" NOT IN ("2", 3) AND "samlres" = 156', ['samlres' => 156, ['not in', 'dataset', ['2', 3]]], null],
            #array('"gy" IN (SELECT "id" FROM "dom")', array('in', 'gy', Query::createSelect(array('from'=>'Dom', 'fields'=>array('id'), 'connection'=>$this))), null),
            // expected, expression, alias
            ['$1', '$1', 'table1'],
            ['"table1"."field"', 'field', 'table1'],
            ['(true)', '(true)', 'table1'],
            ['TRUE', ['true'], 'table1'],
            ["'literal'", "'literal'", null],
            ["'literal'", "'literal'", 'table1'],
            ['CASE WHEN "alma" = 23 THEN "alma" + 23 WHEN "alma" != 10 THEN "alma" - 10 ELSE "alma" END', ['case',
                [['=', 'alma', 23], ['!=', 'alma', 10]], // conditions
                [['+', 'alma', 23], ['-', 'alma', 10], 'alma'], //values
            ], null],
            ['kalap(23) :: bit(16)', ['::', ['kalap()', 23], ['bit()', 16]], null],
            ['(SELECT "a" FROM "aa")', new Query(['type' => 'select', 'fields' => ['a'], 'from' => 'aa']), null],
            ['"a" = (SELECT "a" FROM "aa")', ['=', 'a', new Query(['type' => 'select', 'fields' => 'a', 'from' => 'aa'])], null],
            ['"id" IS NULL', ['id' => null], null],
            ['"id" IS NULL', ['id' => []], null],
            ['"id" IN (1, 2, 3)', ['id' => [1, 2, 3]], null],
            ['"userid" = \'test\'', ['userid' => "'test'"]],
            ['NOT "userid" = \'test\'', ['not', ['=', 'userid', "'test'"]]],
            ['NOT ("userid" = \'test\')', ['not', [['userid' => "'test'"]]]],
            ['"userid" = \'test\' AND true', ['AND', ['userid' => "'test'"], true]],
            ['NOT ("a" = \'b\' AND "c" = \'d\') AND "userid" = \'test\'', ["and", "userid"=>"'test'", ["not",[["a"=>"'b'", "c"=>"'d'"]]]]],
        ];
    }

    /**
     * @dataProvider provBuildExpressionList
     *
     * @param string $expected
     * @param array $expressionlist
     * @param string $alias
     *
     * @return void
     * @throws UXAppException
     */
    function testBuildExpressionList($expected, $expressionlist, $alias) {
        $result = $this->db->buildExpressionList($expressionlist, $alias);
        #$this->compare($expected, $result);
        if($this->db->type=='MY') $expected = str_replace('"', '`', $expected);
        $this->assertEquals($expected, $result);
    }
    function provBuildExpressionList() {
        return [
            // $expected, $expressionlist, $alias
            ["2, 3", [2,3], null],
            ['"b"."alma", "a"."alma"', ['alma', 'a.alma'], 'b'],
        ];
    }


    /**
     * @dataProvider provBuildJoin
     *
     * @param $expected
     * @param $model
     * @param $mainAlias
     * @param $foreignmodel
     * @param $alias
     * @param $type
     * @param $conditions
     *
     * @return void
     * @throws Exception
     */
    function testBuildJoin($expected, $model, $mainAlias, $foreignmodel, $alias, $type, $conditions) {
        $result = $this->db->buildJoin($model, $mainAlias , $foreignmodel, $alias, $type, $conditions);
        if($this->db->type=='MY') $expected = str_replace('"', '`', $expected);
        $this->assertEquals($expected, $result);
    }
    function provBuildJoin() {
        return [
            // $expected, $model, $mainAlias, $foreignmodel, $alias, $type, $conditions
            [' LEFT JOIN "person" "c" ON "dhcpdomain"."creator" = "c"."id"',
                Dhcpdomain::class, '', Person::class, 'c', 'LEFT', ['creator'=>'id']],
            [' LEFT JOIN "person" ON "d"."creator" = "person"."id"',
                Dhcpdomain::class, 'd', Person::class, '', 'LEFT', ['creator'=>'id']],
            [' LEFT JOIN "person" ON "x"."creator" = "person"."id"',
                Dhcpdomain::class, 'd', Person::class, '', 'LEFT', ['x.creator'=>'id']],
        ];
    }


    /**
     * @dataProvider provBuildJoins
     *
     * @param string expected
     * @param string|array $model
     * @param string $mainAlias
     * @param array $joins
     *
     * @throws UXAppException
     */
    function testBuildJoins($expected, $model, $mainAlias, $joins) {
        $result = $this->db->buildJoins($model, $mainAlias , $joins);
        if($this->db->type=='MY') $expected = str_replace('"', '`', $expected);
        $this->assertEquals($expected, $result);
    }
    function provBuildJoins() {
        return [ // $expected, $model, $mainAlias, $joins
            [' LEFT JOIN "person" "c" ON "dhcpdomain"."creator" = "c"."id"',
                Dhcpdomain::class, '', [
                    'c' => 'creator1'
            ]],
            [' LEFT JOIN "person" "c" ON "d"."creator" = "c"."id"',
                Dhcpdomain::class, 'd', [
                'c' => 'creator1'
            ]],
            [' LEFT JOIN "person" "c" ON "d"."creator" = "c"."id" LEFT JOIN "org" "u" ON "c"."unit" = "u"."id"',
                Dhcpdomain::class, 'd', [
                'c' => 'creator1',
                'u' => 'creator1.unit1'
            ]],
        ];
    }

    /**
     * @dataProvider provBuildGroupby
     *
     * @param mixed $expected
     * @param mixed $alias
     * @param $groupby
     *
     * @return void
     * @throws UXAppException
     */
    function testBuildGroupby($expected, $alias, $groupby) {
        if($expected) $expected = ' '.$expected;

        $result = $this->db->buildGroupby($groupby, $alias);
        if($this->db->type=='MY') $expected = str_replace('"', '`', $expected);
        $this->assertEquals($expected, $result);
    }
    function provBuildGroupby() {
        return [
            // $expected, $alias, $groupby
            ['GROUP BY "id"', null, ['id']],
            ['GROUP BY "d"."name"', 'c', ['d.name']],
            ['GROUP BY "d"."name"', null, ['d.name']],
            ['GROUP BY "c"."name"', 'c', ['name']],
        ];
    }

    /**
     * @dataProvider provBuildOrders
     *
     * @param mixed $expected
     * @param mixed $alias
     * @param mixed $orders
     *
     * @return void
     * @throws UXAppException
     */
    function testBuildOrders($expected, $alias, $orders) {
        if($expected) $expected = ' '.$expected;

        $result = $this->db->buildOrders($orders, $alias);
        if($this->db->type=='MY') $expected = str_replace('"', '`', $expected);
        if(!$this->db->supportsOrderNullsLast()) $expected = preg_replace('~(([^\s]+)( DESC)?) NULLS LAST~', 'ISNULL($2), $1', $expected);
        $this->assertEquals($expected, $result);
    }
    function provBuildOrders() {
        return [
            // $expected, $alias, $orders
            ['ORDER BY "id"', null, ['id']],
            ['ORDER BY "id" DESC', null, [['id', DBX::ORDER_DESC]]],
            ['ORDER BY "id" DESC NULLS LAST', null, [['id', DBX::ORDER_DESC, DBX::NULLS_LAST]]],
            ['ORDER BY "c"."id" DESC NULLS LAST, "b"."name"', 'c', [['id', DBX::ORDER_DESC, DBX::NULLS_LAST], 'b.name']],
            ['ORDER BY "name" = $1', null, [[['=', 'name', '$1']]]],
            ['ORDER BY "c"."name" = "d"."name"', 'c', [[['=', 'name', 'd.name']]]],
            ['ORDER BY "m"."id"', null, 'm.id'],
            ['ORDER BY "m"."id" desc', null, 'm.id desc'],
            ['ORDER BY m.id desc', null, '(m.id desc)'],
        ];
    }

    /**
     * @dataProvider provBuildValues
     *
     * @param mixed $expected
     * @param mixed $values
     * @param mixed $number
     *
     * @return void
     * @throws UXAppException
     */
    function testBuildValues($expected, $values, $number) {
        $values = $this->tester->evaluateItems($values, $this);

        $result = $this->db->buildValues($values, $number);
        #$this->compare($expected, $result);
        if($this->db->type=='MY') $expected = str_replace('"', '`', $expected);
        $this->assertEquals($expected, $result);
    }
    function provBuildValues() {
        /** @noinspection SqlResolve */
        return [
            // test case data: $expected, $values, $number
            ["VALUES ('alma', 'körte')", [['alma', 'körte']], 2],
            ["VALUES ('alma', NULL)", [['alma']], 2],
            ["VALUES ('alma', NULL),\n('banán', 'eper')", [['alma'], ['banán', 'eper']], 2],
            [/** @lang text */"select id, null from datatype", /** @lang text */'select id, null from datatype', 2],
            ['SELECT "id", "name" FROM "dhcpoptiontype" WHERE "id" = 1',
                [[Query::class, 'createselect'], Dhcpoptiontype::class, ['id', 'name'], null, ['id'=>1], null, null, null, null, '#eval:return $scope->db;'],
                2],
        ];
    }

    /**
     * @throws UXAppException
     */
    function testBuildFieldNames2() {
        $fieldNames = $this->db->buildFieldNames(['tests\unit\models\Dhcphost'], ['id', 'org_name'=>'org1.name', 'right'=>'(effective_right(99, 21, dom.id))'], $number);
        $expected = '"id", "org1"."name" AS "org_name", (effective_right(99, 21, dom.id)) AS "right"';
        if($this->db->type=='MY') $expected = str_replace('"', '`', $expected);
        $this->assertEquals($expected, $fieldNames);
    }

    /**
     * @throws Exception
     */
    function testBuildTableNames() {
        $tableNames = $this->db->buildTableNames(['tests\unit\models\Dhcphost']);
        $expected = '"dhcphost"';
        if($this->db->type=='MY') $expected = str_replace('"', '`', $expected);
        $this->assertEquals($expected, $tableNames);
    }

    /**
     * @dataProvider provFinalSQL
     *
     * @param string $expected
     * @param array $fields
     * @param array $joins
     * @param array $condition
     * @param array $params
     *
     * @return void
     * @throws UXAppException
     */
    function testFinalSQL($expected, $fields, $joins, $condition, $params) {
        $query = Dhcphost::createSelect($fields, $joins, $condition, $params, $this->db);
        $this->assertEquals('tests\unit\models\Dhcphost', $query->modelname);
        $this->assertEquals(['tests\unit\models\Dhcphost'], $query->from);
        if($this->db->type=='MY') $expected = str_replace('"', '`', $expected);
        $this->assertEquals($expected, $query->finalSQL);
    }
    function provFinalSQL() {
        return [
            // test case data: $expected, $fields, $joins, $condition, $params
            [
                'SELECT "dhcphost"."id", "org1"."name" AS "org_name", (effective_right(99, 21, dom.id)) AS "right" '.
                'FROM "dhcphost" LEFT JOIN "org" "org1" ON "dhcphost"."org" = "org1"."id" LEFT JOIN "dhcpdomain" "dom" ON "dhcphost"."dhcpdomain" = "dom"."id" '.
                'WHERE "dhcpdomain"."deleted" IS NULL',
                ['id', 'org_name'=>'org1.name', 'right'=>'(effective_right(99, $1, dom.id))'],
                ['org1', 'dom'=> [Dhcpdomain::class, 'LEFT', 'dhcpdomain'=>'id']],
                ['and', ['is null', 'dhcpdomain.deleted']],
                [21],
            ],
            [
                'SELECT "dhcphost"."id" '.
                'FROM "dhcphost" LEFT JOIN "org" "org1" ON "dhcphost"."org" = "org1"."id" LEFT JOIN "dhcpdomain" "dom" ON "dhcphost"."dhcpdomain" = "dom"."id" '.
                'WHERE "dhcpdomain"."deleted" IS NULL',
				['id'],
                ['org1', 'dom'=> [Dhcpdomain::class, 'LEFT', 'dhcpdomain'=>'id']],
                ['and', ['is null', 'dhcpdomain.deleted']],
                [21]
            ],
        ];
    }
}
