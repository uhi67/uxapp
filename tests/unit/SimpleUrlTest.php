<?php /** @noinspection PhpFullyQualifiedNameUsageInspection */

/** @noinspection PhpIllegalPsrClassPathInspection */

namespace tests\unit;

use uhi67\uxapp\SimpleUrlFormatter;
use Codeception\Test\Unit;
use uhi67\uxapp\UXApp;
use uhi67\uxapp\UXAppException;

/**
 * Test of codeception interface
 */
class SimpleUrlTest extends Unit {
    /**
     * @var \UnitTester
     */
    protected $tester;

    /** @var UXApp $app */
    public $app;

    /** @noinspection PhpMethodNamingConventionInspection */

    protected function _before() {
        UXApp::$app->setLocale('hu-HU');
        $this->app = UXApp::$app;
    }

	/**
	 * @dataProvider dataCreateUrl
	 * @throws UXAppException
	 */
	function testCanonize(array $to, array $canon, string $url, string $compressed) {
		/** @var SimpleUrlFormatter $urlFormatter */
		$urlFormatter = $this->app->urlFormatter;
		$this->assertEquals($canon, $urlFormatter->canonize($to), "Canonize ".json_encode($to, true));
	}

    /**
     * @dataProvider dataCreateUrl
     *
     * @param array $to
     * @param array $canon
     * @param string $url
     * @param string $compressed
     *
     * @throws \uhi67\uxapp\UXAppException
     * @throws \Exception
     */
	function testCreateUrl(array $to, array $canon, string $url, string $compressed) {
        /** @var SimpleUrlFormatter $urlFormatter */
        $urlFormatter = $this->app->urlFormatter;
		$this->assertInstanceOf(SimpleUrlFormatter::class, $urlFormatter);
        $urlFormatter->compress = false;

        $this->assertEquals($url, $urlFormatter->createUrl($to));

        // Parsing url
        $this->assertEquals($canon, $urlFormatter->parseUrl($url));

        // Compressing
        $urlFormatter->compress = true;
        $this->assertEquals($compressed, $urlFormatter->createUrl($to));
    }

	function dataCreateUrl(): array {
		return [ // $to, $canon, $url, $compressed
            [['start'], ['page'=>'start'], '/index.php?page=start', '/start'],
            [['page'=>'start'], ['page'=>'start'], '/index.php?page=start', '/start'],
			[['main', 'page'=>'start'], ['page'=>'start', 'act'=>'main'], '/index.php?act=main&page=start', '/start/main'],
            [
                ['article', 'update', '#'=>'here'],
                ['page'=>'article', 'act'=>'update', '#'=>'here'],
                '/index.php?page=article&act=update#here',
                '/article/update#here'
            ],
            [
                ['article', 'update', 23, 'up', '#'=>'here'],
				['page'=>'article', 'act'=>'update', 'id'=>23, 'path'=>'up', '#'=>'here'],
                '/index.php?page=article&act=update&id=23&path=up#here',
                '/article/update/23/up#here'
            ],
            [
                ['article', 'update', 23, 'up', 2, '#'=>'here'],
				['page'=>'article', 'act'=>'update', 'id'=>23, 'path'=>'up/2', '#'=>'here'],
                '/index.php?page=article&act=update&id=23&path=up%2F2#here',
                '/article/update/23/up/2#here'
            ],
            [
                ['article', 23, 'up', 2, '#'=>'here'],
				['page'=>'article', 'id'=>23, 'path'=>'up/2', '#'=>'here'],
                '/index.php?page=article&id=23&path=up%2F2#here',
                '/article/23/up/2#here'
            ],
            // module url
            [
                ['module'=>'mod', 'article', 'update', 23, 'up', 2, '#'=>'here'],
				['module'=>'mod', 'page'=>'article', 'act'=>'update', 'id'=>23, 'path'=>'up/2', '#'=>'here'],
				'/index.php?page=article&act=update&id=23&module=mod&path=up%2F2#here',
                '/module/mod/article/update/23/up/2#here'
            ],
        ];
    }
}
