<?php
/** @noinspection PhpIllegalPsrClassPathInspection */

namespace unit;

use uhi67\uxapp\BaseUrlFormatter;

class BaseUrlFormatterTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
	/** @var BaseUrlFormatter $urlFormatter */
	private $urlFormatter;
    
    protected function _before()
    {
		$this->urlFormatter = new \uhi67\uxapp\BaseUrlFormatter();
    }

    protected function _after()
    {
    }

    // tests

	/**
	 * @dataProvider provCanonize
	 * @param $expected
	 * @param $to
	 * @return void
	 * @throws \uhi67\uxapp\UXAppException
	 */
	public function testCanonize($expected, $to) {
		$this->assertEquals($expected, $this->urlFormatter->canonize($to));
	}
	public function provCanonize() {
		return [
			[[], []],
			[['page'=>'page', 'act'=>'act', 'id'=>23, 'p2'=>45], ['page', 'act', 'id'=>23, 'p2'=>45]],
			[['page'=>'page', 'act'=>'act', 'id'=>23, 'p2'=>46], ['page', 'act', 23, 'p2'=>46]],
			[['page'=>'page', 'act'=>'act', 'id'=>23, 'p2'=>47],  ['page'=>'page', 'act'=>'act', 'id'=>23, 'p2'=>47]],
		];
	}

	/**
	 * @dataProvider provCreateUrl
	 * @param string $expected
	 * @param array $to
	 * @param bool $abs
	 * @return void
	 * @throws \uhi67\uxapp\UXAppException
	 */
    public function testCreateUrl(string $expected, array $to, bool $abs=false)
    {
	    $this->assertEquals($expected, $this->urlFormatter->createUrl($to, $abs));
    }
	function provCreateUrl() {
		return [
			['', [], false],
			['/index.php?page=page&act=act&id=23&p2=45', ['page', 'act', 'id'=>23, 'p2'=>45]],
			['/index.php?page=page&act=act&id=23&p2=46', ['page', 'act', 23, 'p2'=>46]],
			['/index.php?page=page&act=act&id=23&p2=47', ['page'=>'page', 'act'=>'act', 'id'=>23, 'p2'=>47]],
			['/index.php?module=uhi67%2Fuxapp-debug&page=trace&session=15ileqsn6l9dneqbfdddg7m42c', ['module'=>'uhi67/uxapp-debug', 'page'=>'trace', 'session'=>'15ileqsn6l9dneqbfdddg7m42c']],
		];
	}
}
