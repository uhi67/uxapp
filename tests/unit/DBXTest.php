<?php

namespace unit;

use Codeception\Test\Unit;
use uhi67\uxapp\DBX;
use uhi67\uxapp\UXApp;
use uhi67\uxapp\UXAppException;
use UnitTester;

/**
 * sql builder is part of DBX
 */
class DBXTest extends Unit {
	/** @var DBX $db */
	public $db;

	/**
	 * @var UnitTester
	 */
	protected $tester;

	/** @noinspection PhpMethodNamingConventionInspection */

	protected function _before() {
		UXApp::$app->setLocale('hu-HU');
		$this->db = UXApp::$app->db;
	}

	/**
	 * @throws UXAppException
	 */
	function testDropTable() {
		if($this->db->tableExists('droptest')) {
			$this->assertTrue(!!$this->db->dropTable('droptest'));
		}
		$this->assertTrue(!!$this->db->query('CREATE TABLE droptest (id serial, name text)'));
		$tables = $this->db->getTables();
		$this->assertIsArray($tables);
		$this->assertContains('droptest', $tables);

		// Sequence test
		$sequences = $this->db->getSequences();
		if($this->db->type == 'PG') {
			$this->assertContains('droptest_id_seq', $sequences);
		} else {
			$this->assertEmpty($sequences);
		}
		//$this->expectException(UXAppException::class);
		//$this->assertTrue(!!$this->db->dropSequence('droptest_id_seq'));

		$this->assertTrue(!!$this->db->dropTable('droptest'));

		// Sequence is dropped automatically
		$sequences = $this->db->getSequences();
		$this->assertNotContains('droptest_id_seq', $sequences);
	}

	/**
	 * @throws UXAppException
	 */
	function testDropRoutine() {
		$this->assertTrue(!!$this->db->dropRoutine('functionTest()'));
		$sql = $this->db->type == 'PG' ? /** @lang SQL */'CREATE FUNCTION functionTest() RETURNS integer LANGUAGE plpgsql AS $$ BEGIN RETURN 13; END; $$'
			: /** @lang */'CREATE FUNCTION functionTest() RETURNS integer RETURN 13;';
		// Notice: In MySQL, you may have to enable function creation with "SET GLOBAL log_bin_trust_function_creators = 1;"
		$this->assertTrue(!!$this->db->execute($sql));
		$routines = $this->db->getRoutines();
		$this->assertIsArray($routines);
		$this->assertGreaterThan(0, count($routines));
		// PG: name converted to lowercase; MY: no ()
		$name = $this->db->type=='PG' ? 'FUNCTION functiontest()' : 'FUNCTION functionTest';
		$this->assertContains($name, $routines);
		$this->assertEquals(13, $this->db->selectvalue('SELECT functionTest()'));
		$this->assertTrue(!!$this->db->dropRoutine('functionTest()'));
	}

	function testDropForeignKeys() {
		if($this->db->type=='PG') {
			$this->assertTrue(!!$this->db->query('CREATE TABLE fktest1 (id serial primary key, name text)'));
			/** @noinspection SqlResolve */
			$this->assertTrue(!!$this->db->query('CREATE TABLE fktest2 (id serial primary key, ref int references fktest1, name text)'));
		} else {
			$this->assertTrue(!!$this->db->query(/** @lang */'CREATE TABLE fktest1 (id int auto_increment primary key, name text)'));
			/** @noinspection SqlResolve */
			$this->assertTrue(!!$this->db->query(/** @lang */'CREATE TABLE fktest2 (
					id int auto_increment primary key, 
					ref int, 
					name text,
            		constraint fktest2_ref_fkey foreign key (ref) references fktest1(id)  -- auto-name would be fktest2_ibfk_1
                )
    		'));
		}

		$fkeys = $this->db->getForeignKeys('fktest2');
		$this->assertIsArray($fkeys);
		/** @noinspection PhpParamsInspection */
		$this->assertCount(1, $fkeys);
		$this->assertArrayHasKey('fktest2_ref_fkey', $fkeys);

		$refs = $this->db->getReferrerKeys('fktest1');
		$this->assertArrayHasKey('fktest2_ref_fkey', $refs);
		$refs = $this->db->getReferrerKeys('fktest2');
		$this->assertIsArray($refs);
		$this->assertArrayNotHasKey('fktest2_ref_fkey', $refs);

		$this->db->dropForeignKey('fktest2_ref_fkey', 'fktest2');

		$refs = $this->db->getReferrerKeys('fktest1');
		$this->assertArrayNotHasKey('fktest2_ref_fkey', $refs);
		$fkeys = $this->db->getForeignKeys('fktest2');
		$this->assertArrayNotHasKey('fktest2_ref_fkey', $fkeys);

		$this->assertTrue(!!$this->db->dropTable('fktest1'));
		$this->assertTrue(!!$this->db->dropTable('fktest2'));
	}
}