<?php

require __DIR__.'/models/City.php';

class ArrayDataSourceTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    public $dataSource;

    protected function _before()
    {

    }

    protected function _after()
    {
    }

    // tests
    public function testArrayData()
    {
        $data = [];
        for($i=1;$i<101;$i++) $data[] = [$i, 'Example '.$i];
        $dataSource = new \uhi67\uxapp\ArrayDataSource(['data'=>$data]);
        $this->assertEquals(100, $dataSource->count);
        $this->assertNull($dataSource->modelClass);
        $this->assertEquals([[10, 'Example 10'], [11, 'Example 11']], $dataSource->fetch(9,2));
        $dataSource->pattern = '/e .9/';
        $this->assertEquals([[69, 'Example 69'], [79, 'Example 79']], $dataSource->fetch(5,2));
        $dataSource->setPattern('9', ['fieldName'=>1]);
        $this->assertEquals([[59, 'Example 59'], [69, 'Example 69']], $dataSource->fetch(5,2));
    }

    public function testModelData()
    {
        $data = [];
        for($i=1;$i<101;$i++) $data[] = new \tests\unit\models\City(['id'=>$i, 'city'=>'Example '.$i]);
        $dataSource = new \uhi67\uxapp\ArrayDataSource(['data'=>$data]);
        $this->assertEquals(100, $dataSource->count);
        $this->assertEquals(\tests\unit\models\City::class, $dataSource->modelClass);
        /** @var \tests\unit\models\City[] $d1 */
        $d1 = $dataSource->fetch(9,2);
        $this->assertEquals('Example 11', $d1[1]->city);
        $dataSource->pattern = '/e .9/';
        $d2 = $dataSource->fetch(5,2);
        $this->assertEquals('Example 79', $d2[1]->city);
        $dataSource->setPattern('9', ['fieldName'=>'city']);
        $d3 = $dataSource->fetch(5,2);
        $this->assertEquals('Example 69', $d3[1]->city);
    }
}