<?php

use uhi67\uxapp\UXApp;

class L10nFileTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

	/** @var UXApp $app */
	public $app;

    protected function _before()
    {
	    UXApp::$app->setLocale('hu-HU');
	    $this->app = UXApp::$app;
	}

    protected function _after()
    {
    }

    // tests
    public function testL()
    {
	    $this->assertInstanceOf(\uhi67\uxapp\L10nFile::class, UXApp::$app->l10n);
	    $this->assertEquals(dirname(__DIR__).'/_data/def/translations/cat1', UXApp::$app->l10n->directory('cat1'));
	    $this->assertEquals(dirname(__DIR__).'/_data/def/translations/app', UXApp::$app->l10n->directory('app'));
	    $this->assertEquals(dirname(__DIR__,2).'/def/translations', UXApp::$app->l10n->directory('uxapp'));
	    $this->assertEquals('hu', UXApp::$app->locale);
	    $this->assertEquals('hu', UXApp::$app->l10n->lang);
	    $this->assertEquals(dirname(__DIR__).'/_data/def/translations', UXApp::$app->l10n->dir);
	    $this->assertEquals('Fordítás', UXApp::la('app', 'Translation', [], 'hu'));
	    $this->assertEquals('Fordítás', UXApp::la('app', 'Translation'));
	    $this->assertEquals('any***', UXApp::la('uxapp/other', 'any'));



		$this->assertEquals('kötelező', UXApp::la('uxapp', 'is mandatory'));
	    $this->assertEquals('any other non-existing*', UXApp::la('uxapp', 'any other non-existing'));
	    $this->assertEquals('Lokalizáció', UXApp::la('cat1', 'Localization'));
    }
}