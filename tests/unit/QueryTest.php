<?php
namespace unit;

use Codeception\Test\Unit;
use Exception;
use ReflectionException;
use tests\unit\models\Dhcpoptiontype;
use tests\unit\models\Org;
use tests\unit\models\Person;
use uhi67\uxapp\BaseModel;
use uhi67\uxapp\DBX;
use uhi67\uxapp\Query;
use uhi67\uxapp\UXApp;
use uhi67\uxapp\UXAppException;
use uhi67\uxml\UXMLDoc;
use UnitTester;

class QueryTest extends Unit {
    /** @var DBX $db */
    public $db;

    /**
     * @var UnitTester
     */
    protected $tester;

    /** @noinspection PhpMethodNamingConventionInspection */
    /**
     * @throws Exception
     */
    protected function _before() {
        UXApp::$app->setLocale('hu-HU');
        $this->db = UXApp::$app->db;
    }

    /**
     * @throws ReflectionException
     * @throws UXAppException
     */
    function testSelect() {
        $query = new Query([
            'type' => 'select',
            'fields' => ['id', 'id2' => '(id+1)'],
            'from' => 'table1',
            'where' => [['is not null', 'id'], ['name' => '$1']],
            'params' => ['name1']
        ]);
        /** @noinspection SqlResolve */
        $expected = 'SELECT "id", (id+1) AS "id2" FROM "table1" WHERE "id" IS NOT NULL AND "name" = \'name1\'';
        $this->assertEquals($expected, $query->finalSQL);

        // Query without from
        $query = Query::createSelect()->select(['now()'])->setDb($this->db);
        $this->assertEquals(date('Y-m-d'), substr($query->scalar(),0,10));
    }

    /**
     * @throws UXAppException
     */
    function testAndWhere() {
        $query = Query::createSelect()
            ->select(['id', 'id2' => '(id+1)'])
            ->from('table1')
            ->where(['is not null', 'id'])
            ->andWhere(['name' => '$1'])
            ->bind(['name1']);
        /** @noinspection SqlResolve */
        $expected = 'SELECT "id", (id+1) AS "id2" FROM "table1" WHERE "id" IS NOT NULL AND "name" = \'name1\'';
        $this->assertEquals($expected, $query->finalSQL);
    }

    /**
     * @dataProvider provFirst
     * (id, name, parent, orgid, descr, type, deleted, created, deleter) VALUES
     * (0, NULL, NULL, -1, NULL, 1, NULL, now(), NULL),
     * (1, 'Kukutyin Zabhegyező Vállalat', 0, 0, NULL, 0, NULL, now(), NULL),
     *
     * @throws UXAppException
     * @throws ReflectionException
     */
    function testFirst($expected, $condition) {
        $query = Org::createSelect(['id', 'name'], null, $condition, null, $this->db);
        $this->assertEquals($expected, $query->first()); // default is array mode
    }

    function provFirst() {
        return [
            [['id' => 1, 'name' => 'Kukutyin Zabhegyező Vállalat'], ['id' => 1]],
            [false, ['id' => -1]]
        ];
    }

    /**
     * @dataProvider provFirst1
     * (id, name, parent, orgid, descr, type, deleted, created, deleter) VALUES
     * (0, NULL, NULL, -1, NULL, 1, NULL, now(), NULL),
     * (1, 'Kukutyin Zabhegyező Vállalat', 0, 0, NULL, 0, NULL, now(), NULL),
     *
     * @throws UXAppException
     * @throws ReflectionException
     */
    function testFirst1($expected, $condition) {
        $query = Org::createSelect(['id', 'name'], null, $condition);
        $result = $query->first(true);
        if($result instanceof BaseModel) $result = array_merge(['class' => get_class($result)], array_filter($result->toArray()));
        $this->assertEquals($expected, $result); // default is array mode
    }

    function provFirst1() {
        return [
            [['class' => Org::class, 'id' => 1, 'name' => 'Kukutyin Zabhegyező Vállalat'], ['id' => 1]],
            [null, ['id' => -1]]
        ];
    }

    /**
     * person.unit references to unit.id
     *
     * @throws UXAppException
     * @throws ReflectionException
     */
    function testGetRefererQuery() {
        $org = Org::first(2);
        $this->assertEquals('Zabhegyező főosztály', $org->name);
        $query = $org->getRefererQuery(Person::class, 'unit1')->select(['id', 'name']);
        /** @noinspection SqlResolve */
        $this->assertEquals('SELECT "id", "name" FROM "person" WHERE "unit" = 2', $query->finalSQL);
    }

    /**
     * @throws UXAppException
     */
    function testDistinct() {
        $query = Query::createSelect()
            ->select(['id', 'id2' => '(id+1)'])
            ->distinct()
            ->from('table1')
            ->where(['is not null', 'id'])
            ->andWhere(['name' => '$1'])
            ->bind(['name1']);
        /** @noinspection SqlResolve */
        $expected = 'SELECT DISTINCT "id", (id+1) AS "id2" FROM "table1" WHERE "id" IS NOT NULL AND "name" = \'name1\'';
        $this->assertEquals($expected, $query->finalSQL);
    }

    /**
     * @throws UXAppException
     */
    function testCreateNodesQuery() {
        $query = Query::createSelect()
            ->select(['id', 'name'])
            ->from(Dhcpoptiontype::class)
            ->where(['<', 'id', 3])
            ->setDb($this->db);
        $doc = new UXMLDoc();
        $this->assertEquals(Dhcpoptiontype::class, $query->modelname);
        Dhcpoptiontype::createNodesQuery($doc->documentElement, $query);
        //$this->assertEquals('', $doc->saveXML());
        $this->tester->assertXmlContains($doc->documentElement, '//dhcpoptiontype[@id=1 and @name=\'boolean\']');
    }

    function testShortQuery() {
        $query = Query::createSelect(null, ['value'=>["now()"]])->setDb($this->db);
        $this->assertNull($query->from);
        $expected = 'SELECT now() AS "value"';
        $this->assertEquals($expected, $query->finalSQL);
    }

    /**
     * @throws UXAppException
     */
    function testMap() {
        $query = Person::createSelect()->where(['is not null', 'unit'])->setDb($this->db);
        $result = $query->map('uid', function($model) {
            /** @var Person $model */
            return $model->unit1->name;
        });
        $this->assertCount(5, $result);
        $this->assertEquals('Kukutyin Zabhegyező Vállalat', $result['admin']);

        $query = Person::createSelect()->setDb($this->db);
        $result = $query->map('email', function($model) {
            /** @var Person $model */
            return $model->creator ? $model->creator1->name : null;
        }, true, true);
        $this->assertCount(2, $result);
        $this->assertEquals('Adminisztrátor', $result['test17@pte.hu']);
    }
}
