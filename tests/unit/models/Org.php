<?php
namespace tests\unit\models;

use DateTime;
use uhi67\uxapp\Model;

/**
 * Model for org table
 *
 * @property int $id
 * @property int $parent
 * @property string $name
 * @property int $orgid
 * @property string $descr
 * @property int $type
 * @property DateTime $created
 * @property int $deleter
 * @property DateTime $deleted
 */
class Org extends Model {
	public static function foreignKeys() {
		return [
			// name => array(modelname, foreign_id=>reference_field)
			'parent1' => [Org::class, 'parent'=>'id'],
			'deleter1' => [Person::class, 'deleter'=>'id'],
		];
	}
}
