<?php
namespace tests\unit\models;

use DateTime;
use uhi67\uxapp\Inet;
use uhi67\uxapp\Model;
use uhi67\uxapp\UXAppException;

/**
 * Model for dhcpdomain table
 *
 * @property int $id
 * @property int $parent -- szülő domain
 * @property string $name
 * @property string $version
 * @property string $descr
 * @property int $creator references Person
 * @property DateTime $created
 * @property int $deleter references Person
 * @property DateTime $deleted
 * @property boolean $active
 * @property int $modifier references Person
 * @property DateTime $modified
 * @property int $range references Iprange -- ip tartomány, melyből az automatikus kiosztás történik
 * @property int $admin references Person
 * @property int $org  references Org
 * @property int $server references Host
 * @property Inet $minip -- A legkisebb automatikusan kiadható ip a tartományból
 */
class Dhcpdomain extends Model {
	public static function foreignKeys() {
		return [
			// name => array(modelname, foreign_id=>reference_field)
			#'server1' => [Host::class, 'dns'=>'id'],
			'org1' => [Org::class, 'org'=>'id'],
			'parent1' => [Dhcpdomain::class, 'parent'=>'id'],
			'admin1' => [Person::class, 'admin'=>'id'],
			'modifier1' => [Person::class, 'modifier'=>'id'],
			'creator1' => [Person::class, 'creator'=>'id'],
			'deleter1' => [Person::class, 'deleter'=>'id'],
			#'range1' => [Iprange::class, 'range'=>'id'],
		];
	}

	/**
	 * @inheritdoc
	 * @return array
	 * @see validate()
	 */
	public static function rules() {
		return [
			'name' => ['string', 'mandatory', ['uniqueifnull', 'deleted'], ['length', 5,64]],
			'active' => ['boolean'],
			'org'=> ['org'],
			'created' => ['DateTime', 'not null'],
			'deleted' => ['DateTime'],
			'modified' => ['DateTime'],
			'minip' => ['Inet', 'hostonly'],
		];
	}

	/**
	 * A mezőkhöz tartozó címkék és egyben az alapértelmezett mezősorrend.
	 * @return array
	 */
	public static function attributeLabels(): array {
		return [
			'id' => 'Azonosító',
			'name' => 'Tartománynév',
			'parent' => 'Szülő',
			'org' => 'Tulajdonos egység',
			'active' => 'Aktív',
			'descr' => 'Leírás',

			'creator' => 'Létrehozta',
			'created' => 'Létrehozás időpontja',
			'deleter' => 'Törölte',
			'deleted' => 'Törlés időpontja',
			'modifier' => 'Módosította',
			'modified' => 'Módosítás időpontja',
		];
	}

	/**
	 * A mezőhöz rendelt magyarázatok. Ha több van, akkor az alábbiak lehetnek:
	 * - hint: egér föléhúzásra
	 * - comment: rövid, mindig látszik a mező után
	 * - help: hosszú, [?] alatt (több is lehet, bekezdések) (null: nincs [?])
	 * - descr: közepes, következő sorban jelenik meg (több is lehet, sorok)
	 * - tooltip: tutorial, sárgában
	 * @return array
	 */
	public static function attributeHints(): array {
		return [
			'active' => ['comment'=>'Nem aktív domain erőforrásai nem kerülnek bele a DNS zónafájlba.'],
			'version' => '',
			'descr' => ['descr'=>'Kötetlen leírás, zónafájlba nem kerül bele'],
			'modifier' => 'Utoljára módosította',
			'modified' => 'Utolsó módosítás időpontja',
		];
	}

	/**
	 * Importált szervezet mentése az ellenőrzés során
	 *
	 * @param string $field -- org mező, melyhez importáljuk az összetett (org,orgid) értéket
	 * @return boolean
	 * @throws UXAppException
	 */
	function orgValidate($field) {
		$value = $this->getAttribute($field);
		if($value===null) return true;
		if(is_array($value)) return $this->setError($field, 'Szervezeti egység importálása a tesztben nem implementált.');
		return true;
	}

	/**
	 * @param $field
	 * @return bool|false
	 * @throws UXAppException
	 */
	function hostonlyValidate($field) {
		$value = $this->getAttribute($field);
		if(!is_object($value) || !($value instanceof Inet)) return $this->setError($field, 'Nem IP cím');
		if(!$value->isHost()) return $this->setError($field, 'Nem host IP cím');
		return true;
	}
}
