<?php
namespace tests\unit\models;

use uhi67\uxapp\Model;
/**
 * Model for systext table
 *
 * @package UXApp test
 * @author Peter Uherkovich
 * @copyright 2018
 *
 * @property int $id
 * @property int $source
 * @property string $lang -- varchar(2)
 * @property string $name -- reference name for long system texts
 * @property string $value
 */
class Systext extends Model {
	public static function foreignKeys() {
		return [
			// name => array(modelname, foreign_id=>reference_field)
			'lang1' => [Language::class, 'lang'=>'id'],
			'source1' => [Systext::class, 'source'=>'id'],
		];
	}

	/**
	 * Must returns validation rules for fields
	 * Models are validated against rules before saving to database
	 *
	 *    - array(rulename, arguments, ...) -- global rule, e.g. unique for multiple fields
	 *    - fieldname => array(rule, rule...) -- rule list for single field
	 *
	 * Typename is a php typename or classname or null if no check
	 * Pattern is a RE pattern or array of patterns or null if no check
	 *
	 * Global rules:
	 *
	 *    - 'unique' fieldnamelist
	 *    - custom arglist -- any custom name refers to validateCustom(array $arglist) method
	 *
	 * Field rules:
	 *
	 * 	  - 'unique' -- field is unique without condition
	 *    - 'not null' -- field is not null
	 *    - 'mandatory' -- field is not null and not empty string
	 *    - typenames (boolean, int, float, number, string, xml, time, date, datetime, )
	 * 	  - ['length', min, max]
	 *    - ['pattern', pattern(s)]
	 * 	  - ['type', typename]
	 *    - ['between', lower, upper] -- valid value between (inluding) limits
	 *    - [custom, arglist] -- any custom name refers to validateCustom($fieldname, array $arglist) method.
	 *
	 * @return array
	 * @see validate()
	 */
	public static function rules() {
		return [
			'lang' => [['length', 2, 2]],
		];
	}

}
