<?php
namespace tests\unit\models;

use uhi67\uxapp\Model;

/**
 * Model for language table
 *
 * @package rr.dev
 * @author Peter Uherkovich
 * @copyright 2017
 *
 * @property string $id -- varchar(2)
 * @property string $tla -- varchar(3)
 * @property string $name
 */
class Language extends Model {
}
