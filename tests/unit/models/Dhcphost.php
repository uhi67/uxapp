<?php
namespace tests\unit\models;

use DateTime;
use uhi67\uxapp\Inet;
use uhi67\uxapp\Macaddr;
use uhi67\uxapp\Model;
use uhi67\uxapp\UXAppException;

/**
 * Model for dhcphost table
 *
 * @package rr.dev
 * @author Peter Uherkovich
 * @copyright 2017
 *
 * @property int $id
 * @property int $dhcpdomain
 * @property string $name
 * @property Macaddr $macaddr
 * @property string $vendor
 * @property string $descr
 * @property boolean $active
 * @property boolean $reserved
 * @property Inet $ip
 * @property Inet $ipend references Iprange
 * @property int $org  references Org
 * @property int $owner references Person
 * @property int $notify references Person
 * @property int $creator references Person
 * @property DateTime $created
 * @property int $deleter references Person
 * @property DateTime $deleted
 * @property int $modifier references Person
 * @property DateTime $modified
 * @property DateTime $expires
 * @property DateTime $lastuse
 */
class Dhcphost extends Model {

	public static function foreignKeys() {
		return [
			// name => array(modelname, foreign_id=>reference_field)
			'dhcpdomain1' => [Dhcpdomain::class, 'dhcpdomain'=>'id'],
			'org1' => [Org::class, 'org'=>'id'],
			'owner1' => [Person::class, 'owner'=>'id'],
			'notify1' => [Person::class, 'notify'=>'id'],
			'creator1' => [Person::class, 'creator'=>'id'],
			'modifier1' => [Person::class, 'modifier'=>'id'],
			'deleter1' => [Person::class, 'deleter'=>'id'],
		];
	}

	/**
	 * @inheritdoc
	 * @return array
	 * @see validate()
	 */
	public static function rules() {
		return [
			['unique if null', ['deleted', 'mac', 'dhcpdomain']],
			'name' => ['string', 'mandatory', ['unique if null', 'deleted'], ['length', 5,64]],
			'active' => ['boolean'],
			'org'=> ['references'],
			'created' => [DateTime::class, 'not null'],
			'deleted' => [DateTime::class],
			'modified' => [DateTime::class],
			'mac' => [
				Macaddr::class,
				['if', 'reserved', false, 'mandatory'],
			],
			'ip' => [Inet::class, 'hostonly', 'mandatory'],
			'ipend' => [Inet::class, 'hostonly'],
		];
	}

	/**
	 * A mezőkhöz tartozó címkék és egyben az alapértelmezett mezősorrend.
	 * @return array
	 */
	public static function attributeLabels() {
		return [
			'id' => 'Azonosító',
			'name' => 'Név',
			'dhcpdomain' => 'Tartomány',
			'org' => 'Tulajdonos egység',
			'active' => 'Aktív',
			'descr' => 'Leírás',
			'mac' => 'MAC cím',
			'expires' => 'Lejár',
			'lastuse' => 'Legutóbbi használat',
			'reserved' => 'Csak foglalás',
			'notify' => 'Értesítendő',
			'owner' => 'Végfelhasználó',
			'vendor' => 'Gyártó',
			'ip' => 'IP cím',
			'ipend' => '-ig',

			'creator' => 'Létrehozta',
			'created' => 'Létrehozás időpontja',
			'deleter' => 'Törölte',
			'deleted' => 'Törlés időpontja',
			'modifier' => 'Módosította',
			'modified' => 'Módosítás időpontja',
		];
	}

	/**
	 * A mezőhöz rendelt magyarázatok. Ha több van, akkor az alábbiak lehetnek:
	 * - hint: egér föléhúzásra
	 * - comment: rövid, mindig látszik a mező után
	 * - help: hosszú, [?] alatt (több is lehet, bekezdések) (null: nincs [?])
	 * - descr: közepes, következő sorban jelenik meg (több is lehet, sorok)
	 * - tooltip: tutorial, sárgában
	 * @return array
	 */
	public static function attributeHints() {
		return [
			'active' => ['comment'=>'Nem aktív bejegyzés nem kerül bele a konfigurációba.'],
			'descr' => ['descr'=>'Kötetlen leírás, zónafájlba nem kerül bele'],
			'modifier' => 'Utoljára módosította',
			'modified' => 'Utolsó módosítás időpontja',
			'mac' => '12 hexa számjegy bármilyen szeparátorral vagy anélkül',
		];
	}

	/**
	 * Checks Inet data if it is a host address only
	 *
	 * @param string $field
	 *
	 * @return boolean
	 * @throws UXAppException
	 */
	function hostonlyValidate($field) {
		$value = $this->getAttribute($field);
		if($value === null) return true;
		if(!is_object($value) || !($value instanceof Inet)) return $this->setError($field, 'Nem IP cím');
		if(!$value->isHost()) return $this->setError($field, 'is invalid host address');
		return true;
	}

}
