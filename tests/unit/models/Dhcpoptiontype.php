<?php
namespace tests\unit\models;

use uhi67\uxapp\Model;

/**
 * Dhcpoptiontype model
 *
 * @package rr.dev
 * @author Peter Uherkovich
 * @copyright 2018
 *
 * @property int $id
 * @property string $name
 * @property string $descr
 */
class Dhcpoptiontype extends Model {
    /**
     * Must returns the attribute labels.
     * Attribute labels are mainly used for display purpose
     * Order of labels is the default order of fields.
     * @return array attribute labels (name => label)
     */
	public static function attributeLabels(): array {
		return [
			'id' => 'Az.',
			'name' => 'Név',
			'descr' => 'Leírás',
		];
	}

	/**
	 * Returns validation rules for fields
	 * Models are validated against rules before saving to database
	 *
	 *  - rule -- numeric indexed rules are global rule, e.g. unique for multiple fields
	 *  - fieldname => array(rule, rule...) -- fieldname indexed rules are for single field
	 *
	 * Rule:
	 *
	 *	- rulename
	 *  - array(rulename, arguments, ...)
	 *
	 * Rulename always refers to method `rulenameValidate` and will be added to the field class as `rule-rulename`
	 * Arguments will be added to field as data-rulename-data (always an array, for all remaining arguments)
	 * Associative arguments 'key'=>value will be used in php validator method as normal argument,
	 * but in forms will be added as data-rulename-key and excluded from data-rulename.
	 *
	 * Global rules:
	 *
	 *    - 'unique' fieldnamelist
	 *    - custom arglist -- any custom name refers to validateCustom(array $arglist) method
	 *
	 * Field rules:
	 *
	 * 	  - 'unique' -- field is unique without condition
	 *    - 'not null' -- field is not null
	 *    - 'mandatory' -- field is not null and not empty string
	 *    - typenames (boolean, int, float, number, string, xml, time, date, datetime, )
	 * 	  - ['length', min, max]
	 *    - ['pattern', pattern(s)] -- valid if at least one of RE patterns is valid (second level: all of them)
	 * 	  - ['type', typename]
	 *    - ['between', lower, upper] -- valid value between (inluding) limits
	 *    - [custom, arglist] -- any custom name refers to validateCustom($fieldname, array $arglist) method.
	 *
	 * `rulenameValidate` functions:
	 *
	 * 	- must accept parameters (fieldname, arg1, arg2, ...) where fieldname is null on global calls.
	 *  - first element of args is custom message or null if default should be used.
	 *  - $1 is the place of the fieldname in the message
	 *  - other elments of args are rule data, e.g. RegExp pattern for pattern rule.
	 *  - must return boolean
	 *  - may set error text of the invalid field on the form by setError(fieldname, message)
	 *
	 * @return array
	 * @see validate()
	 */
	public static function rules(): array {
		return [
			'name' => ['mandatory', 'string', ['length', 2, 20]],
		];
	}

}
