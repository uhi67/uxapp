<?php
namespace tests\unit\models;

use DateTime;
use uhi67\uxapp\Model;
/**
 * Person
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $title
 * @property int $unit
 * @property string $descr
 * @property string $dn
 * @property DateTime $created
 * @property int $loggedin
 * @property DateTime $lastclick
 * @property DateTime $lastlogin
 * @property string $uid
 * @property string $org
 * @property string $idp
 * @property int $enabled
 * @property int $type -- 1=TYPE_PERSON, 2=TYPE_GROUP
 * @property int $logins
 * @property DateTime $deleted
 * @property string $tel
 * @property int $pagelen
 * @property int $creator
 * @property string $tooltip
 *
 * @property-read Org $unit1
 * @property-read Person $creator1
 */
class Person extends Model {
	const TYPE_PERSON = 1;
	const TYPE_GROUP = 2;

	public static function foreignKeys() {
		return [
			// name => array(modelname, foreign_id=>reference_field)
			'creator1' => [Person::class, 'creator'=>'id'],
			'unit1' => [Org::class, 'unit'=>'id'],
		];
	}

	public static function rules() {
		return [
			['unique', ['name', 'unit']],
			['unique', ['uid', 'idp']],
			'name' => ['mandatory'],
			'email' => ['unique', 'email'],
			#'modified' => array('DateTime'),
			'created' => [DateTime::class],
			'deleted' => [DateTime::class],
			'pagelen' => ['integer'],
			'idp' => ['url'],
			'creator' => ['references'],
			'unit' => [['references', 'unit1'], ['references', [Org::class, 'id']]], // Redundant for testing
		];
	}

	public static function attributeLabels() {
		return [
			'name' => 'Név',
			'unit' => 'Egység',
		];
	}

}
