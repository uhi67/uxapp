<?php
namespace tests\unit\models;
/**
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 *
 */

use uhi67\uxapp\BaseModel;

class City extends BaseModel {
    public $id;
    public $city;
}
