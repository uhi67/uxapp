<?php
/** @noinspection PhpIllegalPsrClassPathInspection */

namespace app\vendor\uhi67\uxapp\tests\unit;

use Codeception\Test\Unit;
use Exception;
use uhi67\uxapp\UrlFormatter;
use uhi67\uxapp\UXApp;
use uhi67\uxapp\UXAppException;
use UnitTester;

class UrlFormatterTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;
    /** @var UrlFormatter $urlFormatter */
    private $urlFormatter;
    
    protected function _before()
    {
        $this->urlFormatter = new UrlFormatter([
            'urlFormat' => [
                [['module'=>'[\w-]+\/[\w-]+', 'page'=>'[\w-]+'], 'module/{$module}/{$page}'],
                [['page'=>'[\w-]+', 'act'=>'default', 'id'=>'\d+'], '{$page}/{$id}'],
                [['page'=>'[\w-]+', 'act'=>'[\w-]+', 'id'=>'\d+'], '{$page}/{$act}/{$id}'],
                [['page'=>'[\w-]+', 'act'=>'[\w-]+'], '{$page}/{$act}'],
                [['page'=>'[\w-]+', 'act'=>'default'], '{$page}'],
            ],
            'compress' => true,
        ]);
    }

    protected function _after()
    {
    }

    // tests
    public function testCreate() {
        $this->assertEquals('', UXApp::$app->baseurl);
        $this->assertEquals('/index.php', $this->urlFormatter->script);
        $this->assertEquals('', $this->urlFormatter->baseUrl);
    }

    /**
     * @dataProvider provCanonize
     * @param array $expected
     * @param array $to
     * @return void
     * @throws UXAppException
     */
    public function testCanonize(array $expected, array $to)
    {
        $this->assertEquals($expected, $this->urlFormatter->canonize($to));
    }
    function provCanonize(): array {
        return [
            [[], []],
            [[], ['']],
            [['page'=>'page', 'act'=>'act', 'id'=>23, 'p2'=>45], ['page', 'act', 'id'=>23, 'p2'=>45]],
            [['page'=>'page', 'act'=>'act', 'id'=>23, 'p2'=>46], ['page', 'act', 23, 'p2'=>46]],
            [['page'=>'page', 'act'=>'act', 'id'=>23, 'p2'=>47], ['page'=>'page', 'act'=>'act', 'id'=>23, 'p2'=>47]],
            [['module'=>'uhi67/uxapp-debug', 'page'=>'trace', 'session'=>'15ileqsn6l9dneqbfdddg7m42c'], ['module'=>'uhi67/uxapp-debug', 'page'=>'trace', 'session'=>'15ileqsn6l9dneqbfdddg7m42c']],
        ];
    }

    /**
     * @dataProvider provCreateUrl
     * @param string $expected
     * @param array $to
     * @param bool $abs
     * @return void
     * @throws UXAppException
     */
    public function testCreateUrl(string $expected, array $to, bool $abs=false)
    {
        $this->assertEquals($expected, $this->urlFormatter->createUrl($to, $abs), json_encode($to));
    }
    function provCreateUrl(): array {
        return [
            ['/', [], false],
            ['/', [''], false],
            ['page/act/23?p2=45', ['page', 'act', 'id'=>23, 'p2'=>45]],
            ['page/act/23?p2=46', ['page', 'act', 23, 'p2'=>46]],
            ['page/act/23?p2=47', ['page'=>'page', 'act'=>'act', 'id'=>23, 'p2'=>47]],
            ['module/uhi67/uxapp-debug/trace?session=15ileqsn6l9dneqbfdddg7m42c', ['module'=>'uhi67/uxapp-debug', 'page'=>'trace', 'session'=>'15ileqsn6l9dneqbfdddg7m42c']],
            ['/module/uhi67/uxapp-debug/trace?session=15ileqsn6l9dneqbfdddg7m42c', ['module'=>'uhi67/uxapp-debug', 'page'=>'trace', 'session'=>'15ileqsn6l9dneqbfdddg7m42c'], true],
        ];
    }

    /**
     * @dataProvider provMatchElement
     * @param bool $expected
     * @param string $element
     * @param string $pattern
     * @param string[] $subPatterns
     * @param string[] $queryResult
     * @return void
     */
    public function testMatchElement($expected, $element, $pattern, $subPatterns, $queryResult) {
        $matches = [];
        $r = $this->urlFormatter->matchElement($element, $pattern, $subPatterns, $matches);
        $this->assertEquals($expected, $r);
        $this->assertEquals($queryResult, $matches);
    }
    function provMatchElement(): array {
        return [ // $expected, $element, $pattern, $subPatterns, $queryResult
            [true, 'page', 'page', [], []],
            [false, 'page', 'page1', [], []],
            [true, 'main', '{$page}', ['page'=>'\w+'], ['page'=>'main']],
            [true, 'mainPage', '{$page}Page', ['page'=>'\w+'], ['page'=>'main']],
        ];
    }

    /**
     * @dataProvider provParsePath
     * @param array $expected
     * @param string $url
     * @return void
     * @throws Exception
     */
    public function testParsePath(array $expected, string $url)
    {
        $this->assertEquals($expected, $this->urlFormatter->parseUrl($url));
    }
    function provParsePath(): array {
        return [
            [[], '/'],
            [['page'=>'page', 'act'=>'act', 'p2'=>45, 'id'=>'23'], 'page/act/23?p2=45'], // Matching '{$page}/{$act}'
            [['page'=>'page', 'act'=>'act', 'id'=>23, 'p2'=>47], 'page/act/23?p2=47'],
            [['module'=>'uhi67/uxapp-debug', 'page'=>'trace', 'session'=>'15i'], 'module/uhi67/uxapp-debug/trace?session=15i'],
        ];
    }
    /**
     * @dataProvider provParseUrl
     * @param array $expected
     * @param string $url
     * @return void
     * @throws Exception
     */
    public function testParseUrl(array $expected, string $url)
    {
        $this->tester->assertArrayHasEqualElementsByKeys($expected, $this->urlFormatter->parseUrl($url));
    }
    function provParseUrl(): array {
        return [
            [[], '/'],
            [['page'=>'page', 'act'=>'act', 'id'=>23, 'p2'=>45], 'page/act/23?p2=45'],
            [['page'=>'page', 'id'=>23, 'p2'=>46, 'act'=>'default'], 'page/23?p2=46'],
            [['page'=>'page', 'id'=>23, 'p2'=>47, 'path'=>'upd'], 'page/23/upd?p2=47'],
            [['module'=>'uhi67/uxapp-debug', 'page'=>'trace', 'session'=>'15i'], 'module/uhi67/uxapp-debug/trace?session=15i'],
        ];
    }

}
