<?php /** @noinspection PhpUnhandledExceptionInspection */

/**
 * Tk alkalmazás globális konfigurációs mintája codecept testhez
 * Másold le testconfig.php néven, és töltsd ki a környezetre jellemző adatokkal!
 *
 * Application's 'global configuration for codeception: copy to testconfig.php and fill the real data
 *
 * @filesource	testconfig-template.php
 * @package TK
 * @author uhi
 * @copyright 2021
 */

use uhi67\uxapp\BaseUser;
use uhi67\uxapp\DBXPG;
use uhi67\uxapp\FileCache;
use uhi67\uxapp\FileLogger;
use uhi67\uxapp\L10nBase;
use uhi67\uxapp\Session;
use uhi67\uxapp\SimpleUrlFormatter;
use uhi67\uxapp\testhelper\TestDebug;
use uhi67\uxapp\UXApp;

$rootpath = dirname(__DIR__);
$datapath = $rootpath.'/_output';
$defpath = dirname(__DIR__, 2) .'/def';

$env = [
	'SIMPLESAMLPHP_CONFIG_DIR' => dirname(__DIR__, 2) .'/config/simplesamlphp/config',
];
foreach($env as $var=>$value) {
	putenv("$var=$value");
	$_ENV[$var] = $value;
}

/**
 * Application's 'global configuration array for codecept testing
 */
$config = [
	'class' => UXApp::class,

	/** Application basic setup parameters */
    'rootPath' => $rootpath,
	'timeZone' => 'Europe/Budapest',
	'render' => 'server',			// default render mode: server (default), client
	'design' => 'tkdesign',		// Module name contains design theme (may be in application or in UXApp library)
	'locale' => 'hu',

	'environment' => 'development',

	/** Configurations of Application-specific modules */
	'components' => [
		/** Debugger */
		'debug' => [
			'class' => TestDebug::class,
			'enabled' => true,
			'dataPath' => $datapath.'/debug',		// writeable path to store trace files
		],

		'urlFormatter' => [
			'class' => SimpleUrlFormatter::class,
			'script' => '/index.php',
			'compress' => true,
		],

		/** Logger */
		'logger' => [
			'class' => FileLogger::class,
			'logFile' => dirname(__DIR__).'/_output/testlog.log',
			'levels' => '!debug',
		],

		/** Cache configuration for speeding up directory queries */
		'cache' => [
			'class' => FileCache::class,
			'path' => $datapath . '/cache',		// Writeable path to store cache files
			'enabled' => 1,						// Cache is enabled
			'ttl' => 1800						// Default time-to-live in seconds
		],

		/** Default database connection configuration */
		'database' => [
			'class' => DBXPG::class,
			'host' => '',
			'name' => 'tk-test',
			'port' => 5432,
			'user' => 'tk-test',
			'password' => 'tk-test-123',
			'encoding' => 'UTF-8',
		],

		// Localization
		'l10n' => [
			'class' => L10nBase::class,	// Localization classname. Must provide getText(category, text, params, language) and formatDate(DateTime, datetype, timetype, language) for UXApp::la/UXApp:fd
			'source' => 'en',							// Default source language, default is 'en' (not used in L10nBase)
		],

		'session' => [
			'class' => Session::class,
			'name' => 'sess_tk',							// Compatible with legacy (see setup.inc.php)
			'save_path' => $datapath . '/client',
			'lifetime' => 1800,
			'cookie_path' => '',
			'cookie_domain' => true, // to use current domain
			'logfile' => $datapath.'/session.log',
		],
		'user' => [
			'class' => BaseUser::class,
		],
	],
];

return $config;
