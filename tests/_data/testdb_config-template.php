<?php
/**
 * Database config
 *
 * Returns database config for testing
 *
 * First time create test database manually using tests/_data/create_tst_db.sql
 * (Use db_update_... files if exist)
 */

use uhi67\uxapp\DBXMY;
use uhi67\uxapp\DBXPG;

$dbType = 'pg'; // my

echo "DB type is $dbType.\n";

/** @noinspection PhpConditionAlreadyCheckedInspection */
return $dbType=='pg' ? [
    'class' => DBXPG::class,
    'name' => 'uxapp_test',
    'port' => 5432,
    'user' => 'uxapp_test',
    'password' => 'kalap123',
    'host' => '',
    'encoding' => 'UTF-8',
] : [
    'class' => DBXMY::class,
    'name' => 'uxapp_test',
    'user' => 'uxapp_test',
    'password' => 'kalap123',
    'host' => '',
    'encoding' => 'UTF-8',
];
