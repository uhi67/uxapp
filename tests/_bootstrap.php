<?php /** @noinspection PhpUnhandledExceptionInspection */

/** @noinspection RedundantSuppression */

use uhi67\uxapp\Component;
use uhi67\uxapp\DBXPG;
use uhi67\uxapp\testhelper\TestDebug;
use uhi67\uxapp\UXApp;

echo "Global Bootstrap\n";

date_default_timezone_set('Europe/Budapest');
$vendorDir = getenv('COMPOSER_VENDOR_DIR') ?: dirname(__DIR__) . '/vendor'; // Internal vendor for tests

if(file_exists(dirname(dirname(dirname(__DIR__))) . '/autoload.php')) {
	echo "Testing in application environment\n";
	// Testing in application environment
	$root = dirname(dirname(dirname(dirname(__DIR__))));
	/** @noinspection PhpIncludeInspection */
	require_once dirname(dirname(dirname(__DIR__))) . '/autoload.php';
	if(file_exists($root.'/config/testdb_config.php')) {
		/** @noinspection PhpIncludeInspection */
		$dbConfig = require $root.'/config/testdb_config.php';
	}
	else {
		$dbConfig = require __DIR__.'/_data/testdb_config.php';
	}
}
else {
	echo "Testing in standalone environment\n";
	/** @noinspection PhpIncludeInspection */
	require_once $vendorDir . '/autoload.php';
	$dbConfig = include __DIR__.'/_data/testdb_config.php';
}

require_once dirname(__DIR__).'/lib/testhelper/TestDebug.php';
$_SESSION = [];

$config = [
	'class' => UXApp::class,
    'rootPath' => __DIR__,
	'components' => [
		'debug' => [
			'class' => TestDebug::class,
		],
		'db' => $dbConfig,
		'l10n' => [
			'class' => \uhi67\uxapp\L10nFile::class,
			'source' => 'en',							// Default source language, default is 'en'
			'locale' => 'hu',
			'dir' => __DIR__.'/_data/def/translations',
		],
		'urlFormatter' => [
			'class' => \uhi67\uxapp\SimpleUrlFormatter::class,
			'script' => '/index.php',
			'compress' => true,
		],
	]
];
#$config = require __DIR__.'/_data/testconfig.php';

// Reset test database
echo "Resetting database using migration...\n";
$_SERVER['argv'] = ['uxapp', 'migrate', 'reset', 'confirm=yes', 'verbose=3'];
$_SERVER['argc'] = count($_SERVER['argv']);
/** @var UXApp $app */
$app = Component::create(UXApp::class, $config);
$app->runCli();

// Load SQL test data
echo "Recreating database using initialization script...\n";
$dbType = $dbConfig['class'] == DBXPG::class ? 'pg' : 'my';
$filename = __DIR__."/_data/create_test_$dbType.sql";
$sql = file_get_contents($filename);

$rs = $app->db->execute($sql);
if($rs) echo "SQL test data is loaded ($dbType)\n";

echo "Global Bootstrap OK.\n";
