/*
    uapp.js
*/
console.log('uxapp.js');
/*
    Ensures there will be no 'console is undefined' errors
*/
window.console = window.console || (function() {
    const c = {};
    c.log = c.warn = c.debug = c.info = c.error = c.time = c.dir = c.profile = c.clear = c.exception = c.trace = c.assert = function(){};
    return c;
})();

if(!String.prototype.substringBefore)
    /**
     * Returns a new substring from beginning of the string to the beginning of the given pattern.
     * If not found, returns empty (whole=false) or the whole string (whole=true, default)
     *
     * @param {string} pattern
     * @param {boolean} whole
     * @return {String|string}
     */
    String.prototype.substringBefore = function(pattern, whole=true) {
        const pos = this.indexOf(pattern);
        if(pos === -1) return whole ? this : '';
        return this.substring(0,pos);
    };

if(!String.prototype.substringAfter)
    /**
     * Returns a new substring from the end of the given pattern to the end.
     * If not found, returns empty (whole=false) or the whole string (whole=true, default)
     *
     * @param {string} pattern
     * @param {boolean} whole
     * @return {String|string}
     */
    String.prototype.substringAfter = function(pattern, whole=true) {
        const pos = this.indexOf(pattern);
        if(pos === -1) return whole ? this : '';
        return this.substring(pos+pattern.length);
    };

/*
    String.substitute(arguments)
    Returns a new string $var's substituted with values of arguments object
    Unmatched variable patterns remain in original string
*/
if(!String.prototype.substitute)
    String.prototype.substitute = function(a) {
        let r = this;
        for(const v in a)	{
            if(a.hasOwnProperty(v)) {
                r = r.replace(new RegExp('\\$' + v), a[v]);
            }
        }
        return r;
    };

/*
    Returns new string urlencoded
*/
if(!String.prototype.urlencode)
    String.prototype.urlencode = function() {
        return encodeURIComponent(this).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
            replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
    };


Object.values = function(obj) {
    let a = [];
    for(const i in obj) if(obj.hasOwnProperty(i)) a.push(obj[i]);
    return a;
};

Object.concat = function() {
    const obj = {};
    for(let i=0; i<arguments.length; i++) {
        let b;
        for (let p in b = arguments[i]) {
            if (b.hasOwnProperty(p)) obj[p] = b[p];
        }
    }
    return obj;
};

$.unserialize = function(str){
    str = decodeURI(str);
    const obj = {};
    str.replace(
        new RegExp("([^?=&]+)(=([^&]*))?", "g"),
        function($0, $1, $2, $3) { obj[$1] = $3; }
    );
    return obj;
};

jQuery.fn.showx = function() {
    this.removeClass('hidden');
}
jQuery.fn.hidex = function() {
    this.addClass('hidden');
}

/**
 * UXApp namespace (as module pattern)
 * @type {{getSourceLang: (function(): string), getLang: (function(): string), la: (function(*=, *=, *=): *), UConfirm: (function(*=, *, *, *=, *=, *=): boolean), setTranslations: Uxapp.setTranslations, setLang: Uxapp.setLang}}
 */
const Uxapp = (function() {
    // Declare here private variables and functions
    let lang = 'en';
    let sourceLang = 'en';
    let translations = {}; // Translation
    // Public interface (methods and properties)
    // noinspection JSUnusedGlobalSymbols
    return {
        /**
         * Sets translations of the specified language pair and tag
         * @param tag
         * @param source
         * @param target
         * @param {Array[]} def
         */
        setTranslations: function(tag, source, target, def) {
            if(translations[tag] === undefined) translations[tag] = {};
            if(translations[tag][source] === undefined) translations[tag][source] = {};
            translations[tag][source][target] = def;
        },

        /**
         * Sets current user language and optionally the source language
         * @param {string} userLang
         * @param {string=} sourceLang1
         */
        setLang: function(userLang, sourceLang1) {
            lang = userLang;
            if(sourceLang1) sourceLang = sourceLang1;
        },
        getLang: () => lang,
        getSourceLang: () => sourceLang,

        la: function (message, tag, la) {
            if(!la) la = lang;
            if(!tag) tag = 'app';
            let params = [];
            let msg = message;
            if(typeof message === 'object') {
                msg = message.shift();
                params = message;
            }
            if(la !== sourceLang && translations[tag] && translations[tag][sourceLang] && translations[tag][sourceLang][la]) {
                let tr = translations[tag][sourceLang][la].find(e => e[0] === msg);
                if(tr) msg = tr[1];
            }
            // Replaces $1, $2... parameters from params array
            if(params) msg = msg.replace(/$(\d+)/g, (m,p1) => params[p1+1]);
            //console.log(message, tag, la, msg, sourceLang);
            return msg;
        },

        /**
         * Usage: UConfirm('Please confirm', 'Are you sure?', okAction, cancelAction);
         * - okAction and CancelAction may be function or url-s
         * @returns {boolean}
         */
        UConfirm: function (title, message, okaction, cancelaction, yesLabel, noLabel) {
            if (!yesLabel) yesLabel = this.la('Yes', 'uxapp');
            if (!noLabel) noLabel = this.la('No', 'uxapp');
            $('<div></div>')
                .appendTo('body')
                .html('<div><h6>' + message + '</h6></div>')
                .dialog({
                    modal: true, title: title, zIndex: 10000, autoOpen: true,
                    width: 'auto', resizable: false,
                    buttons: [
                        {
                            text: yesLabel,
                            icon: 'ui-icon-check',
                            click: function () {
                                if(typeof okaction === 'function') okaction();
                                else document.location = okaction;
                                $(this).dialog("close");
                            },
                        },
                        {
                            text: noLabel,
                            icon: 'ui-icon-close',
                            click: function () {
                                if (cancelaction) {
                                    if (typeof cancelaction === 'function') cancelaction();
                                    else document.location = cancelaction;
                                }
                                $(this).dialog("close");
                            }
                        },
                    ],
                    close: function () {
                        $(this).remove();
                    }
                });
            return false;
        }
    };
})();

$(function () {
    const lang = $('html').attr('lang');
    if(lang) Uxapp.setLang(lang);

    $('a.confirm').on('click', function(event, confirmed) {
        let message = $(this).data('confirm');
        let title = $(this).data('title');
        if(!title) title = $(this).attr('title');

        if(!confirmed && message) {
            Uxapp.UConfirm(title, message, function() {
                $(this).trigger('', [true]);
            }, null);
            return false;
        }
        return true;
    });
});
