<?php
namespace uhi67\uxapp;

/**
 * # File cache
 *
 * ### config: cache
 * 		path	-- absolute path to save files. Deafult is datapath/cache
 * 		enabled -- 1=caching is enabled
 * 		ttl		-- default ttl (def 900 = 15 min)
 *
 * Data will be saved to the file with give name,
 * the ttl value into file `_ttl_name` which content is "created,expires" in unix timestamp format.
 *
 * ### Usage
 * - set(name, value, [ttl])
 * - get(name, [ttl])
 * - purge(pattern)
 *
 * @package UXApp
 * @author Peter Uherkovich
 * @copyright 2020
 */
class FileCache extends BaseCache implements CacheInterface {
	public $path;
	public $enabled;
	public $ttl;

	/**
	 * @throws UXAppException
	 */
	public function prepare() {
		if(!$this->path) $this->path = UXApp::$app->dataPath.'/cache';
		if($this->enabled) {
			if(!file_exists($this->path)) if(!mkdir($this->path)) throw new UXAppException("Cannot create cache directory ".$this->path);
			if(!$this->ttl) $this->ttl = 900; // 15 mins
		}
	}

	public function finish() {

	}

	/**
	 * Returns data from cache or null if not found or expired.
	 *
	 * @param string $name
	 * @param null   $default
	 * @param null   $ttl -- if given, overrides expiration (only for this query)
	 *
	 * @return mixed
	 */
	public function get($name, $default=null, $ttl=null) {
		if(!$this->enabled) return $default;
		$name = Util::toNameID($name);
		$filename = $this->path.'/'.$name;
		if(!file_exists($filename)) return $default;
		$ttlname = $this->path.'/_ttl_'.$name;
		if(!file_exists($ttlname)) { unlink($filename); return $default; }
		list($created, $expires) = explode(',', file_get_contents($ttlname));
		if($ttl) $expires = $created+$ttl;
		if(time()>$expires) {
 			unlink($filename);
			unlink($ttlname);
			return $default;
		}
		return unserialize(file_get_contents($filename));
	}

	public function has($name) {
		if(!$this->enabled) return false;
		$name = Util::toNameID($name);
		$filename = $this->path.'/'.$name;
		if(!file_exists($filename)) return false;
		$ttlname = $this->path.'/_ttl_'.$name;
		if(!file_exists($ttlname)) { unlink($filename); return false; }
		$item = explode(',', file_get_contents($ttlname));
		$expires = $item[1];
		if(time()>$expires) {
			unlink($filename);
			unlink($ttlname);
			return false;
		}
		return true;
	}

	/**
	 * Removes given items from the cache by name pattern
	 *
	 * @param string|array $key -- key or keys
	 *
	 * @return int -- number of deleted items, false on error
	 */
	public function delete($key) {
		if(!$this->enabled) return 0;

		// Prepare keys
		if(is_array($key)) {
			foreach($key as &$name) $name = Util::toNameID($name);
		}
		else $key = [Util::toNameID($key)];

		$c = 0;
		$dh = opendir($this->path);
		while (($file = readdir($dh)) !== false) {
			// Skip non-files and  _ttl_ files now
			if(filetype($this->path . '/' . $file)!= 'file') continue;
			if(substr($file, 0,5)=='_ttl_') continue;

			// Key filter
			if(in_array($file, $key)) {
				$filename = $this->path.'/'.$file;
				$ttlname = $this->path.'/_ttl_'.$file;
				if(file_exists($ttlname)) unlink($ttlname);
				unlink($filename);
				$c++;
			}
		}
		closedir($dh);
		return $c;
	}

	/**
	 * Saves data into cache
	 *
	 * @param string $name
	 * @param mixed $value -- null to remove the item
	 * @param int|null $ttl -- time to live in secs, default is given at cache config
	 * @return mixed -- the value itself
	 */
	public function set($name, $value, $ttl=null) {
		if(!$this->enabled) return $value;

		$filename = $this->fileName($name);
		$ttlname = $this->ttlName($name);
		if(!file_exists(dirname($filename))) mkdir(dirname($filename), 0774);

		if(file_exists($filename)) unlink($filename);
		if(file_exists($ttlname)) unlink($ttlname);
		if($value===null) return null;

		if(!$ttl) $ttl = $this->ttl;
		file_put_contents($ttlname, time().','.(time()+$ttl));
		return file_put_contents($filename, serialize($value));
	}

	protected function fileName($name) {
		$hash = crc32($name);
		$name = substr($hash,2).'_'.Util::toNameID($name);
		$dir = substr($hash,0,2);
		return $this->path.'/'.$dir.'/'.$name;
	}

	protected function ttlName($name) {
		$hash = crc32($name);
		$name = substr($hash,2).'_'.Util::toNameID($name);
		$dir = substr($hash,0,2);
		return $this->path.'/'.$dir.'/_ttl_'.$name;
	}

	/**
	 * Removes unnecessary files from the cache
	 *
	 * @param string $pattern -- RegEx pattern for filenames (not keys!!!)
	 *
	 * @return int -- number of deleted files, false on error
	 */
	public function purge($pattern) {
		if(!$this->enabled) return 0;
		return static::deleteFiles($this->path, $pattern);
	}

	/**
	 * Removes unnecessary files from the cache
	 *
	 * @param $dir
	 * @param string|null $pattern -- RegEx pattern for filenames (not keys!!!)
	 * @param bool $ttl -- handle _ttl_ files together with parent files
	 * @param bool $recurse -- recurse subdirectories
	 *
	 * @return int -- number of deleted files, false on error
	 */
	public static function deleteFiles($dir, $pattern, $ttl=true, $recurse=false) {
		UXApp::trace("Deleting $pattern files from $dir");
		$c = 0;
		$dh = opendir($dir);
		while (($file = readdir($dh)) !== false) {
			// Skip non-files and  _ttl_ files now
			if($file=='.' || $file=='..') continue;
			$ft = filetype($dir . '/' . $file);
			if($recurse && $ft == 'dir') self::deleteFiles($dir.'/'.$file, $pattern, $ttl, $recurse);
			if($ft != 'file') continue;
			if($ttl && substr($file, 0,5)=='_ttl_') continue;

			// Pattern filter
			if(!$pattern || preg_match($pattern, $file)) {
				UXApp::trace("Deleting $file from $dir");
				$filename = $dir.'/'.$file;
				if($ttl && file_exists($ttlname = $dir.'/_ttl_'.$file)) unlink($ttlname);
				unlink($filename);
				$c++;
			}
		}
		closedir($dh);
		return $c;
	}

	/**
	 * Clears all expired items from the cache
	 * (default ttl may be overridden, only older items will be deleted, no other items affected)
	 *
	 * @param int|null $ttl
	 *
	 * @return int -- number of items deleted
	 */
	public function cleanup($ttl=null) {
		if(!$this->enabled) return 0;
		$c = 0;
		$dh = opendir($this->path);
		while (($file = readdir($dh)) !== false) {
			// Skip non-files and  _ttl_ files now
			if(filetype($this->path . '/' . $file)!= 'file') continue;
			if(substr($file, 0,5)=='_ttl_') continue;

			$filename = $this->path.'/'.$file;
			$ttlname = $this->path.'/_ttl_'.$file;
			if(!file_exists($ttlname)) { unlink($filename); $c++; continue; }

			list($created, $expires) = explode(',', file_get_contents($ttlname));
			if($ttl) $expires = $created+$ttl;
			if(time()>$expires) {
				unlink($filename);
				unlink($ttlname);
				$c++;
			}
		}
		closedir($dh);
		return $c;
	}

	/**
	 * deletes all data from the cache
	 * @return int
	 */
	public function clear() {
		if(!$this->enabled) return 0;
		$c = 0;
		$dh = opendir($this->path);
		while (($file = readdir($dh)) !== false) {
			// Skip non-files and  _ttl_ files now
			if(filetype($this->path . '/' . $file)!= 'file') continue;
			$filename = $this->path.'/'.$file;
			unlink($filename);
			if(substr($file, 0,5)!='_ttl_') $c++;
		}
		closedir($dh);
		return $c;
	}
}
