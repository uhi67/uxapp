<?php
namespace uhi67\uxapp;

use uhi67\uxapp\ArrayUtils;
use uhi67\uxapp\SimpleUrlFormatter;
use uhi67\uxapp\Util;
use uhi67\uxapp\UXAppException;

/**
 * Class Url
 *
 * Creates url from abstract url data and parses url to abstract structure.
 * Uses configured urlFormat rules. Default is basic compression rules from BaseUrl
 *
 * @package app\vendor\uhi67\uxapp\lib
 */
class UrlFormatter extends SimpleUrlFormatter {
    /** @var array $urlFormat -- [[pattern, result], ...]*/
    public $urlFormat;
    /** @var string $baseUrl -- absolute base url, without dispatcher script, example: 'http://app.test' */
    public $baseUrl;

    public function prepare() {
        parent::prepare();
        if($this->urlFormat && $this->compress===null) $this->compress= true;
    }

    /**
     * Returns a formatted url using configured urlFormat rules.
     * Applies first matching rule. If none is matching, the default (BaseUrl) rule is applied.
     *
     * ### Constructing urlFormat array (example)
     *
     *      Example: ['project' 'act'=>'edit'] ==> project.php/edit/23
     *        `[['page'=>'project', 'act'=>'\w+', 'id'=>'\d+'], '{$page}.php/{$act}/{$id}',`
     *      Example: ['topic', 'id'=>1] ==> topic.php?id=1 -- default behavior
     *        `[['page'=>'topic'], '{$page}.php{$params}'],`
     *
     *      Example ruleset (SimpleUrlFormatter-compatible)
     *
     *			[['page'=>'[\w-]+', 'act'=>'default', 'id'=>'\d+'], '{$page}/{$id}'],
     *      	[['page'=>'[\w-]+', 'act'=>'default'], '{$page}'],
     *			[['page'=>'[\w-]+', 'act'=>'[\w-]+', 'id'=>'\d+'], '{$page}/{$act}/{$id}'],
     *      	[['page'=>'[\w-]+', 'act'=>'[\w-]+'], '{$page}/{$act}'],
     *			[['module'=>'[\w-]+\/[\w-]+', 'page'=>'[\w-]+'], 'module/{$module}/{$page}']
     *
     * @param array $to -- Abstract url format (see {@see BaseUrlFormatter::createUrl()})
     * @param bool $absolute -- return absolute url
     *
     * @return string
     * @throws UXAppException
     */
    public function createUrl($to, bool $absolute=false): string {
        if(is_array($to)) $to = $this->canonize($to);
        if($absolute) return $this->absoluteUrl($to);
        if(!$this->urlFormat || !$this->compress) return parent::createUrl($to);
        $to = $this->canonize($to);
        $page = $to['page'] ?? array_shift($to);
        $urla = $to;
        if(!isset($urla['path'])) $urla['path'] = null;
        if(is_array($urla['path'])) $path = $urla['path'];
        else $path = (isset($urla['path']) && $urla['path'] != '' && $urla['path'] != '/') ?
            array_filter(explode('/', $urla['path']), function($a) {
                return $a != '';
            }) :
            [];
        $query = [];
        if(isset($urla['query'])) parse_str($urla['query'], $query);
        $pathIsAbsolute = is_string($urla['path']) && substr($urla['path'], 0, 1) == '/';

        // Fragment is always fragment
        if(isset($to['#'])) {
            $fragment = $to['#'];
            unset($to['#']);
        } else $fragment = '';

        $f = false;
        if($rules = $this->urlFormat) {
            foreach($rules as $rule) {
                if(is_array($pattern = $rule[0])) {
                    if(ArrayUtils::preg_match($pattern, $to, $matches)) {
                        // Szabály alkalmazása.
                        $urlb = parse_url(Util::substitute($rule[1], $to));
                        $path = array_merge($path, explode('/', $urlb['path']));
                        $query = array_diff_key($to, $pattern);
                        $f = true;
                        break;
                    }
                }
            }
        }
        if(!$f) {
            if($page) $to['function'] = $page;
            $path[] = '/';
            $query = $to;
        }

        return
            (isset($urla['scheme']) ? ($urla['scheme'] . '://') : '') .
            ($urla['host'] ?? '') .
            (isset($urla['port']) ? (':' . $urla['port']) : '') .

            ((isset($urla['host']) && $urla['host'] && count($path) || $pathIsAbsolute) ? '/' : '') .

            (count($path) ? (implode('/', $path)) : '') .
            (count($query)>0 ? ('?' . http_build_query($query)) : '') .
            ($fragment ? ('#' . $fragment) : '');
    }

    /**
     * Converts a relative url to absolute url
     *
     * @param $url
     * @return string
     * @throws UXAppException
     */
    public function absoluteUrl($url) : string {
        return SimpleUrlFormatter::absoluteUrl($url);
//		if(is_array($url)) $url = $this->createUrl($url);
//		$path = parse_url($url, PHP_URL_PATH);
//		$data = parse_url($url, PHP_URL_QUERY);
//		$fragment = parse_url($url, PHP_URL_FRAGMENT);
//		if($path && substr($path,0,1)!=='/') $path = '/'.$path;
//		return $this->baseUrl.$path.($data ? '?'.$data : '') . ($fragment ? '#'.$fragment : '');
    }

    /**
     * Returns a canonical abstract URL from the real URL path elements,
     * using the first match from the definition array.
     *
     * Example:
     *
     * [['page'=>'[\w-]+', 'act'=>'[\w-]+', 'id'=>'\d+'], '{$page}/{$act}/{$id}'],
     *
     * ['start','doc',23] ==> ['page'='start', 'act'=>'doc', 'id'=>23]
     * ['page'='start', 'act'=>'doc', 23] ==> ['page'='start', 'act'=>'doc', 'id'=>23]
     *
     * @param array $path -- a real URL path (arrayized)
     * @param array $query
     * @return array -- canonical abstract URL
     * @throws \uhi67\uxapp\UXAppException
     */
    public function parsePath2($path, array $query=[]) {
        if(!$this->urlFormat) return parent::parsePath($path, $query);
        foreach($this->urlFormat as [$pattern, $format]) {
            $format = explode('/', $format);
            foreach($format as $f) {

                // TODO ...
            }
        }
        return parent::parsePath($path, $query);
    }

    /**
     * Decompress url element based on urlFormat rules. The result is a canonical query containing all relevant keys.
     *
     * Example:
     *  - path: "module/uhi67/uxapp-debug/control"
     *  - matching rule: `[['module'=>'[\w-]+\/[\w-]+', 'page'=>''], 'module/{$module}/{$page}']`
     *  - result: `['module'=>'uhi67/uxapp-debug', 'page'=>'control', 'session'=>'j7s2r0sqcerlr5cdnovtv03jda', 'debug-url'=>'/namespace', 'function'=>'control']`
     *
     * @param string|array $path
     * @param array $query
     * @return array
     */
    public function parsePath($path, array $query=[]): array {
        if(is_array($path)) $path = implode('/', $path);
        if(!$this->urlFormat) return parent::parsePath($path, $query);
        $rules = $this->urlFormat;
        $m = false;
        foreach($rules as $rule) {
            $subPatterns = $rule[0];
            $pattern = $rule[1];
            $m = $this->matchPath($path, $pattern, $subPatterns);
            if($m!==false) {
                break;
            }
        }
        if($m===false) {
            return parent::parsePath($path, $query);
        }
        return array_merge($m, $query);
    }

    /**
     * @param string $path -- the original raw path
     * @param string $pattern -- 'module/{$module}/{$page}'
     * @param string[] $subPatterns -- ['module'=>'[\w-]+\/[\w-]+', 'page'=>'']
     * @return string[]|false -- canonical query based on match or false if no match
     */
    public function matchPath(string $path, string $pattern, array $subPatterns) {
        $matches = $subPatterns;

        /** The keys used in pattern, in order */
        $keys = [];
        /** Build the Regex pattern */
        $regexPattern = preg_replace_callback('~{\$([\w-]+)}~', function($mm) use($subPatterns, &$keys) {
            // The keys used are collected in order
            $keys[] = $mm[1]??'';
            // The corresponding regex pattern is substituted in a ()
            return '('.($subPatterns[$mm[1]]??'').')';
        }, $pattern);
        if(preg_match('~^'.$regexPattern.'$~', $path, $mm)) {
            foreach($keys as $i=>$key) {
                $matches[$key] = $mm[$i+1];
            }
            return $matches;
        }
        return false;
    }


    /**
     * @param string $element -- 'pageName'
     * @param string $p -- 'page{$page}'
     * @param string[] $subPatterns ['page'=>'\w+']
     * @param string[] $matches
     * @return bool
     */
    public function matchElement(string $element, string $p, array $subPatterns, array &$matches) {
        /** The keys used in pattern, in order */
        $keys = [];
        /** Build the Regex pattern */
        $pr = preg_replace_callback('~{\$([\w-]+)}~', function($mm) use($subPatterns, &$keys) {
            // The keys used are collected in order
            $keys[] = $mm[1]??'';
            // The corresponding regex pattern is substituted in a ()
            return '('.($subPatterns[$mm[1]]??'').')';
        }, $p);
        if(preg_match('~^'.$pr.'$~', $element, $mm)) {
            foreach($keys as $i=>$key) {
                $matches[$key] = $mm[$i+1];
            }
            return true;
        }
        return false;
    }
}
