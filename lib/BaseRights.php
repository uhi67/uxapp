<?php
namespace uhi67\uxapp;
use uhi67\uxml\UXMLElement;

/**
 * class Rights
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2020
 * @license MIT
*/

/**
 * A right source.
 *
 * Implements session caching computed effective rights.
 * Use only one instance.
 */
abstract class BaseRights extends Module {
	private $cache;
	public $rightbits; // array(bit=>array(value, title, descr), ...)

	public function prepare() {
		/*
			Override:
				define self::$rightbits table
		*/
		$this->initCache();
	}

	public function initCache() {
		if(!is_array($this->cache))
			$this->cache = $this->parent->session->get('rightcache', []);
		return $this->cache;
	}

	public function save() {
		#Debug::tracex('cache', $this->cache);
		$_SESSION['rightcache'] = $this->cache;
	}

	/**
	 * Must create rightbit nodes for all right types (usually under node_control)
	 *
	 * @param UXMLElement $node_parent -- ez alá rightbits node(ok), alá bit
	 *
	 * @return void
	 * @throws
	 */
	abstract function createNodes($node_parent);

	/**
	 * Empty right cache
	 *
	 * @return void
	 */
	public function reload() {
		$_SESSION['rightcache'] = $this->cache = [];
	}

	/**
	 * 	Returns an effective permission bit's value of this user
	 *
	 * @param int $type - righttype, as defined in custom descendant of Rights
	 * @param int $user - user id
	 * @param int $obj  - reference to an object of given type or global if empty
	 * @param int $bit	- bitmask of an elementary permission (2^n), as defined in custom descendant of Rights
	 *
	 * @return int - 0 on no right, bit's value on
	 * @throws UXAppException
	 */
	public function right($type, $user, $obj, $bit) {
		$rv = $this->rightValue($type, $user, $obj);
		return $rv & $bit;
	}

	/**
	 * Gets a bitmapped rightvalue of right of type for an object. Uses local session cache
	 *
	 * @param int $type - righttype, as defined in custom descendant of Rights
	 * @param int $user - user id
	 * @param int $obj  - reference to an object of given type or global if empty
	 *
	 * @return int - the value of rights
	 * @throws UXAppException
	 */
	public function rightValue($type, $user, $obj=null) {
		if($obj!==null) Assertions::assertInt($obj);
		if(!is_callable([$class = get_class($this), 'getRightValue'])) throw new UXAppException("class '$class' has no method 'getRightValue'");
		if(gettype($type)=='object' || gettype($user)=='object') throw new UXAppException('Invalid argument type. Number excepted.');
		if(isset($this->cache[$key = $type.'_'.$user.'_'.$obj])) {
			#Debug::tracex('Right from cache', "$type.'_'.$user.'_'.$obj = ".$this->cache[$key]);
			return $this->cache[$key];
		}
		return $this->cache[$key] = $this->getRightValue($type, $user, $obj);
	}

	/**
	 * Stores a value into cache for the session-time.
	 *
	 * @param int $type
	 * @param int $user
	 * @param int $obj
	 * @param int $value
	 * @return int -- the value
	 */
	public function cacheValue($type, $user, $obj, $value) {
		$key = $type.'_'.$user.'_'.$obj;
		return $this->cache[$key] = $value;
	}

	/**
	 * Must retrieve a rightvalue from database. Use rightValue instead.
	 *
	 * @param int $type
	 *   righttype, one of RT_XXX constants
	 * @param int $user
	 *   user id
	 * @param int $obj
	 *   reference to an object of given type
	 * @return int
	 *   the value of rights
	 */
	abstract protected function getRightValue($type, $user, $obj);

	/**
	 * Uses rightcache to store a user-calculated right-like value
	 *
	 * @param string $name -- reference base name beginning with letters
	 * @param array $params -- params for function and cache name
	 * @param callable $fn -- inline function to calculate value if it does not exist in cache
	 *
	 * @return mixed -- the cached value (as it returned by anonymous function))
	 */
	public function useCache($name, $params, $fn) {
		Assertions::assertArray($params);
		$key = $name.'_'.implode('_', $params);
		if(!$key) throw new UXAppException('Invalid cache parameters');
		$this->initCache();
		$cf = array_key_exists($key, $this->cache);
		#Debug::tracex('cache', "$key, $cf");
		return $cf ? $this->cache[$key] : ($this->cache[$key] = $fn($params));
	}
}
