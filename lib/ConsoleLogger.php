<?php /** @noinspection PhpIllegalPsrClassPathInspection */

/**
 * Created by PhpStorm.
 * User: uhi
 * Date: 2019. 03. 20.
 * Time: 22:06
 */

namespace uhi67\uxapp;

use DateTime;
use Exception;

/**
 * Class ConsoleLogger
 *
 * Echoes log messages to the standard output
 *
 * Configuration options:
 *
 * - levels
 * 		- Empty string or *: logs all levels
 * 		- '!name...': all, except specified levels (string only!)
 * 		- '>name': all levels greater than specified (emergency, alert, critical, fatal, error, warning, notice, info, debug); also: >=, <, <=
 * - tags
 * 		- Empty: logs anyway (including no tags)
 * 		- *: matches any tag: (logs only if at least one tag is specified)
 * 		- !*: excludes any tag: (logs only if no tag is specified)
 * 		- 'name': included tags: logs, if any of tags matches this (first match terminates the evaluation)
 * 		- '!name: excluded tags (does not log if any of tags matches this
 * 		- '~pattern': tags matching pattern (RegEx)
 * 		- '!~pattern': excluded patterns
 *
 * After initialization, levels will be translated to array of level names to log.
 * Tags array holds patterns in runtime.
 *
 * @package uhi67\uxapp
 */
class ConsoleLogger extends UXAppLogger implements LoggerInterface {
    public $logFile;
    /**
     * @var string|array $levels -- logged levels: space-separated list or array of level names.
     * - Empty string or *: logs all levels
     * - '!name...': all, except specified levels (string only!)
     * - '>name': all levels greater than specified (emergency, alert, critical, fatal, error, warning, notice, info, debug); also: >=, <, <=
     * After initialization, levels will be translated to array of level names to log
     */
    public $levels = '';
    /**
     * @var array|string $tags -- logged tags: space-separated list or array of tag names or tag patterns.
     * - Empty: logs anyway (including no tags)
     * - *: matches any tag: (logs only if at least one tag is specified)
     * - !*: excludes any tag: (logs only if no tag is specified)
     * - 'name': included tags: logs, if any of tags matches this (first match terminates the evaluation)
     * - '!name: excluded tags (does not log if any of tags matches this
     * - '~pattern': tags matching pattern (RegEx)
     * - '!~pattern': excluded patterns
     */
    public $tags;

    public function prepare() {
        parent::prepare();

        // Prepares levels
        if(is_string($this->levels)) {
            $l1 = substr($this->levels, 0, 1);
            if($this->levels == '' or $this->levels == '*') {
                $this->levels = $this->severities;
            } else if(preg_match('/^(<|>|<=|>=)(\w+)$/', $this->levels, $mm)) {
                $index = array_search($mm[2], $this->severities);
                $rel = $mm[1];
                $this->levels = array_filter($this->severities, function($i) use ($index, $rel) {
                    switch($rel) {
                        case '<':
                            return $i < $index;
                        case '>':
                            return $i > $index;
                        case '<=':
                            return $i <= $index;
                        case '>=':
                            return $i >= $index;
                    }
                return true;
                }, ARRAY_FILTER_USE_KEY);
            } else {
                $not = false;
                if($l1 == '!') {
                    $not = true;
                    $this->levels = substr($this->levels, 1);
                }
                $this->levels = explode(' ', $this->levels);
                if($not) $this->levels = array_diff($this->severities, $this->levels);
            }
        }

        // prepares tags filter definition
        if(is_string($this->tags)) $this->tags = explode(' ', $this->tags);
    }

    /**
     * Logs into file
     *
     * ----
     * {@inheritDoc}
     *
     * @param string $level -- emergency, alert, critical, error, warning, notice, info, debug
     * @param string $message
     * @param array $context -- message parameters, including any optional one
     * @throws Exception
     */
    public function log($level, $message, array $context = []) {
        $tags = ArrayUtils::getValue($context, 'tags', 'app');
        if(is_string($tags)) $tags = explode(' ', $tags);
        if(!is_array($tags)) $tags = [];

        // Filter severity
        if($this->levels && !in_array($level, $this->levels)) return;

        // filter tags
        if($this->tags && !$this->tagsMatches($tags)) return;

        $timestamp = ArrayUtils::getValue($context, 'timestamp', new DateTime());
        if(!($timestamp instanceof DateTime)) $timestamp = new DateTime($timestamp);

        // interpolate placeholders using context parameters
        $message = $this->interpolate($message, $context);

        $txt = sprintf("[%s] %s %s\n",
            $level,
            Ansi::color(Util::objtostr($message, false, true), 'red'),
            Util::objtostr(ArrayUtils::getValue($context, 'tags'), false, true)
        );
        echo $txt;
    }

    /**
     * Returns true if tags satisfies $this->tags definition
     *
     * - Empty: logs anyway (including no tags)
     * - *: matches any tag: (logs only if at least one tag is specified)
     * - !*: excludes any tag: (logs only if no tag is specified)
     * - 'name': included tags: logs, if any of tags matches this (first match terminates the evaluation)
     * - '!name: excluded tags (does not log if any of tags matches this
     * - '~pattern': tags matching pattern (RegEx)
     * - '!~pattern': excluded patterns
     *
     * If no any match, returns false
     *
     * @param array $tags
     * @return bool -- to log or not to log
     */
    private function tagsMatches($tags) {
        if($this->tags === []) return true;

        foreach($this->tags as $tagDef) {
            if(substr($tagDef,0,1)=='!') {
                if($this->tagDefMatches(substr($tagDef, 1), $tags)) return false;
            }
            else {
                if($this->tagDefMatches($tagDef, $tags)) return true;
            }
        }
        return false;
    }

    /**
     * @param string $tagDef -- single pattern with optional ~
     * @param array $tags
     * @return bool -- true if any of the tags matches the definition
     */
    private function tagDefMatches($tagDef, array $tags) {
        if($tagDef=='*') {
            return count($tags)>0;
        }
        if(substr($tagDef,0,1=='~')) {
            $pattern = $tagDef . '~';
            foreach($tags as $tag) {
                if(preg_match($pattern, $tag)) return true;
            }
            return false;
        }
        else {
            foreach($tags as $tag) {
                if($tagDef == $tag) return true;
            }
            return false;
        }
    }

    public function emergency($message, array $context = array()) {
        // TODO: Implement emergency() method.
    }

    public function alert($message, array $context = array()) {
        // TODO: Implement alert() method.
    }

    public function critical($message, array $context = array()) {
        // TODO: Implement critical() method.
    }

    public function error($message, array $context = array()) {
        // TODO: Implement error() method.
    }

    public function warning($message, array $context = array()) {
        // TODO: Implement warning() method.
    }

    public function notice($message, array $context = array()) {
        // TODO: Implement notice() method.
    }

    public function info($message, array $context = array()) {
        // TODO: Implement info() method.
    }

    public function debug($message, array $context = array()) {
        // TODO: Implement debug() method.
    }
}
