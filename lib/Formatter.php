<?php


namespace uhi67\uxapp;


use uhi67\uxml\FormatterInterface;

/**
 * # Class Formatter
 *
 * uxapp formmatter is similar to standard Uxml formatter. The differences:
 * - localizes null values ('not set')
 * - localizes boolean values
 *
 * @package uhi67\uxapp
 */
class Formatter extends \uhi67\uxml\Formatter implements FormatterInterface {
    public function toString($value, $locale = null) {
        if(is_null($value) && $locale!==false) {
            $value = 'not set';
            return $locale ? UXApp::la('uxapp', $value, null, $locale) : $value;
        }
        if(is_bool($value) && $locale!==false) {
            $value = $value ? 'true': 'false';
            return $locale ? UXApp::la('uxapp', $value, null, $locale) : $value;
        }
        return parent::toString($value, $locale);
    }
}
