<?php
namespace uhi67\uxapp;

/**
 * # Session cache
 *
 * ### config: cache
 * 		name	-- name of session variable to store in
 * 		ttl		-- default ttl (def 900 = 15 min)
 *
 * Data will be saved into the cache variable as an array item [expire, value] with given name as key,
 *
 * ### Usage
 * - set(name, value, [ttl])
 * - get(name, [ttl])
 * - purge(pattern)
 * - cleanup() -- deletes all expired items
 *
 * @package UXApp
 * @author Peter Uherkovich
 * @copyright 2020
 */
class SessionCache extends BaseCache implements CacheInterface {
	public $name;
	public $ttl;

	/** @var array $_cache -- cache loaded from session and to be written back */
	private $_cache;
	/** @var int $_changed -- number of affected items */
	private $_changed;

	/**
	 * @throws UXAppException
	 */
	public function prepare() {
		if(php_sapi_name() == "cli") return;
		if(!$this->name) $this->name = 'cache';
		if(!$this->ttl) $this->ttl = 900; // 15 mins
		$this->_cache = UXApp::$app->session->get($this->name, []);
		$this->_changed = 0;
		$this->cleanup();
	}

	public function finish() {
		if($this->_changed) UXApp::$app->session->set($this->name, []);
	}

	/**
	 * Returns data from cache or default if not found or expired.
	 *
	 * @param string $name
	 * @param mixed  $default
	 * @param int|bool|null   $ttl -- if given, overrides expiration (only for this query) and restarts on hit.
	 *
	 * @return mixed
	 */
	public function get($name, $default=null, $ttl=null) {
		$c = 0; // Number of affected items (if it's needed to write back)

		if(!isset($this->_cache[$name])) return $default;
		$diff = $ttl===null || $ttl===true ? 0 : $ttl - $this->ttl;

		$item = $this->_cache[$name];

		if($item[0]+$diff < time()) {
			// Expired item
			$c++;
			unset($this->_cache[$name]);
			$result = $default;
		} else {
			$result = $item[1];
			if($ttl) {
				$this->_cache[$name][0] = $this->ttl + $diff;
				$c++;
			}
		}
		$this->_changed += $c;
		return $result;
	}

	/**
	 * @param string $name
	 *
	 * @return bool
	 */
	public function has($name) {
		if(!isset($this->_cache[$name])) return false;
		if($this->_cache[$name][0] < time()) {
			unset($this->_cache[$name]);
			$this->_changed++;
			return false;
		}
		return true;
	}

	/**
	 * Removes given items from the cache by name pattern
	 *
	 * @param string|array $key -- key or keys
	 *
	 * @return int -- number of deleted items, false on error
	 */
	public function delete($key) {
		$c = 0; // Number of affected items (if it's needed to write back)

		if(!is_array($key)) $key = [$key];
		foreach($key as $name) {
			if(!isset($this->_cache[$name])) continue;
			unset($this->_cache[$name]);
			$c++;
		}
		$this->_changed += $c;
		return $c;
	}

	/**
	 * Saves data into cache
	 *
	 * @param string   $name
	 * @param mixed    $value -- null to remove the item
	 * @param int|null $ttl -- time to live in secs, default is given at cache config
	 *
	 * @return mixed -- the value itself
	 */
	public function set($name, $value, $ttl=null) {
		if(!$ttl) $ttl = $this->ttl;
		$this->_cache[$name] = [$ttl, $value];
		$this->_changed++;
		return $value;
	}

	/**
	 * Removes unnecessary files from the cache
	 *
	 * @param string $pattern -- RegEx pattern
	 *
	 * @return int -- number of deleted files, false on error
	 */
	public function purge($pattern) {
		$c = 0;
		foreach($this->_cache as $key => $item) {
			if(preg_match($pattern, $key)) {
				unset($this->_cache[$key]);
				$c++;
			}
		}
		$this->_changed += $c;
		return $c;
	}

	/**
	 * Must clean up the expired items
	 *
	 * @param int|null $ttl
	 *
	 * @return int -- number of items deleted
	 */
	public function cleanup($ttl=null) {
		if(php_sapi_name() == "cli") return null;
		$diff = $ttl===null ? 0 : $ttl - $this->ttl;
		$c = 0;
		foreach($this->_cache as $key => $item) {
			if($item[0]+$diff < time()) {
				unset($this->_cache[$key]);
				$c++;
			}
		}
		$this->_changed += $c;
		return $c;
	}

	/**
	 * deletes all data from the cache
	 * @return int
	 */
	public function clear() {
		$c = count($this->_cache);
		$this->_cache = [];
		$this->_changed += $c;
		return $c;
	}
}
