<?php
/** @noinspection PhpIllegalPsrClassPathInspection */
/** @noinspection PhpUnused */

namespace uhi67\uxapp;

use Exception;
use ReflectionClass;
use ReflectionMethod;
use ReflectionException;
use Throwable;
use uhi67\uxapp\exception\ForbiddenException;
use uhi67\uxapp\exception\HandledException;
use uhi67\uxapp\exception\NotFoundException;
use uhi67\uxapp\exception\UserException;
use uhi67\uxml\UXMLDoc;

/**
 * # UXAppPage
 *
 * Abstract page controller. You have to extend it in your application.
 * All page and command of an application has to be a descendant of this.
 *
 * ## Callbacks during lifetime of a Page controller:
 *
 * ### 1. Preparation part (called by the constructor)
 *
 * - __prepare()__ -- must call parent.
 *
 * ### 2. Execution part (called by go())
 *
 * - __init()__  -- must call parent
 * - __actDefault()__, __actAction()__, etc
 * - __afterRender()__  -- (must call parent) called after XSL render is finished, and result has been sent to the output.
 *
 * ### 3. Terminating
 *
 * - __finish()__ -- must call parent at the end
 *
 * ## Request parameters preprocessed by UXAppPage
 *
 * - __showxml__ (via {@see UXPage})
 * - __render__ (via {@see UXPage})
 * - __msg ({@see UXAppPage::prepare()})
 * - __id__ ({@see UXAppPage::prepare()})
 * - __act__ ({@see UXAppPage::prepare()})
 * - __resetxsl__ ({@see UXAppPage::prepare()})
 * - __module__ ({@see actModule()})
 *
 * See also {@see Controller}
 *
 * @author uhi
 * @copyright 2014-2019
 * @package uxapp
 *
 * @property string $layout -- name of layout directory or file (module: or vendor/module:layout or layout)
 * @property-read BaseUser $user -- user component, if exists or null
 * @property-read BaseUrlFormatter $urlFormatter
 * @property-read RequestInterface $request
 * @property-read BaseSession $session
 * @property-read string $actName -- act if specified or "default" if not.
 * @property-read string $currentUrl --  including query paramaters
 */
abstract class UXAppPage extends Controller {
    /** @var UXApp $app - the host application instance */
    public $app;
    /** @var string|null $appname -- The name of the application from the config */
    public $appname = null;
    /** @var DBX $db -- default database connection of this page */
    public $db;
    /** @var string name -- unqualified NameID of the page for logging, etc. Default for view. */
    public $name;
    /** @var bool $resetxsl -- XSL regenerate is needed */
    public $resetxsl = false;
    /** @var UXPage $view - the renderer class */
    public $view;
    /** @var string $viewName - name of the view vithout extension. xsl-relative path or vendor/module:path */
    public $viewName;
    /** @var bool $noFlash -- if true, prevents retrieving flash messages (e.g. module page) */
    public $noFlash;
	/** @var int $jsonFlags -- flags for json response format {@see json_encode()} */
	public $jsonFlags = 0;

    /** @var string $title */
    protected $title;
    protected $h1;
    protected $h1link;
    protected $h2;
    protected $ico;
    protected $h2link;
    /** @var string $act -- action word from path or act parameter in get or post */
    public $act='';
    /** @var bool $defaultmenu -- if set, addDefaultmenu() will be invoced in the page */
    protected $defaultmenu=true;
    /** @var integer|null id -- the default id of the user object from the path if contains integer or value of id request parameter */
    public $id = null;
    /** @var string $error_url - redirect on error. Default is the base url of the function */
    protected $error_url;
    /** @var string $back_url - redirect on success. Default is the base url of the function */
    protected $back_url;
    /** @var string $viewfile -- setView() sets it, render() uses */
    protected $viewfile;
    /** @var string $originalView -- requested view if view does not exist */
    protected $originalView;
    /** @var string $pageview -- combination of pagename and viewname, e.g. dns_redirect. createXsl() sets it, available in the XML doc at /data/control/pageview */
    protected $pageview;
    /** @var string $designmodule -- name of module (namespace qualified class name) contains the design. Can be set in config/design or with {@see setDesign()}. Default is empty (application) */
    protected $designmodule;
    /** @var bool $nolog -- disables logging of current action. Use $this->nolog=true; in the action method. */
    protected $nolog;
    /** @var int $right -- current user's right on current object */
    protected $right;
    /** @var string $layout - name of the design layout. Can be set by {@see setDesign()}('module/layout'). Default is from config, it's default is 'basic'. Name of directory under xsl of design module */
    private $layout;
    /** @var array $logged -- names of already logged actions */
    private $logged;

    public $path;

    /**
     * Prepare page during construction (phase I)
     *
     * @throws HandledException
     */
    public function prepare() {
        parent::prepare();

        if(!$this->designmodule) $this->setLayout($this->app->design.':basic');

        $r = new ReflectionClass($this);
        $pageName = Util::uncamelize(preg_replace('/(\w+)Page/', '\1', $r->getShortName()));
        if(!$this->viewName) $this->viewName = $pageName;
        if(!$this->name) $this->name = $pageName;
        if(!$this->db) $this->db = $this->app->db;

        try {
            $this->logged = [];

            if(!$this->layout) $this->setLayout($this->app->design);
            UXApp::trace(['designmodule' => $this->designmodule, 'layout' => "$this->layout"]);

            $locale = $this->app->hasComponent('l10n') ? $this->app->l10n->locale : null;
            $resetxsl = $this->request->getInt('resetxsl', 0);
            if($resetxsl) {
                // Törli a generált XSL-eket
                $genxslpath = $this->app->runtimeXslPath; // Ahová a generált fájlok kerülnek
                FileCache::deleteFiles($genxslpath, '~\.xsl~', false);
                // Törli az asset cache-t
                FileCache::deleteFiles($this->app->assetManager->cacheDir, null, false, true);
            }

            $this->id = $this->request->getInt('id', $this->id);
            $this->act = $this->request->req('act', $this->act);
            $this->view = new UXPage([
                'controller' => $this,
                'locale' => $locale,
                'formatter' => $this->app->formatter ?: new Formatter(['locale' => $locale]),
            ]);

            $this->registerAssets();
            $this->view->addDefaultAssets($this->viewName);

            $this->error_url = $this->back_url = $this->createUrl($this->act);

            if(($msg = $this->request->req('msg'))) $this->view->addHead('message', $this->app->la('app', $msg));

            if(($this->appname = $this->app->moduleName)) $this->view->addHead('appname', $this->appname);
            $this->view->addHead('appversion', $this->app->ver);
            $this->view->addHead('environment', $this->app->environment);
            $this->view->addHead('lang', $this->app->lang);
            $this->view->addHead('locale', $this->app->locale);

            if($this->originalView) $this->view->addControlVar('original-view', $this->originalView);

            /** TODO: implement request filter for enabled ip, sorry message, etc. */

        }
        catch(Throwable $e) {
            UXApp::showException($e);
            if(ENV_DEV && class_exists('\Codeception\Util\Debug')) {
                $ee = $e;
                while($ee) {
                    \Codeception\Util\Debug::debug(sprintf('%s in file %s at line %s', $ee->getMessage(), $ee->getFile(), $ee->getLine()));
                    if($ee instanceof UXAppException) \Codeception\Util\Debug::debug(json_encode($ee->getExtra()));
                    \Codeception\Util\Debug::debug($ee->getTraceAsString());
                    $ee = $ee->getPrevious();
                }
            }
            throw new HandledException('Internal exception '.$e->getMessage(), 0, $e);
        }

        try {
            if($this->defaultmenu) $this->addDefaultMenu();
            if($this->act) {
                $this->view->addControlVar('act', $this->act);
                $this->view->node_content->setAttribute('act', $this->act); // For easy xsl filtering
            }
            if($this->id) $this->view->addControlVar('id', $this->id);
        }
        catch(Throwable $e) {
            $title = ($e instanceof ForbiddenException) ? 'Jogosultsági hiba' : 'Hiba történt.';
            $this->showException($e, $title, true);
            throw new HandledException('', 0, $e);
        }

        if(!$this->noFlash) $this->getMessage(); // Saved message
        $this->view->addControlVar('render', $this->view->render);
    }

    /**
     * Prepare page during execution (phase II)
     *
     * @return void
     */
    public function init() {
        foreach($this->modules as $module) {
            $module->init();
        }
    }

    /**
     * Place to register assets for the page.
     * Derived implementation must call parent::registerAssets();
     *
     * @throws UXAppException
     * @throws Exception
     */
    function registerAssets() {
        $jQueryAsset = $this->app->assetManager->register([
            'dir' => 'components/jquery',
            'patterns' => ['jquery.js'],
        ]);
        $this->view->addJs([$jQueryAsset, 'jquery.min.js']);

        $jQueryUiAsset = $this->app->assetManager->register([
            'dir' => 'components/jqueryui',
            'patterns' => ['jquery-ui.js', 'themes/ui-lightness/.../*'],
        ]);
        $jQueryUiAsset->register($this->view, ['themes/ui-lightness/jquery-ui.min.css']);
        $this->app->assetManager->registerFile($this->view, ['uhi67/uxapp', 'js/jquery-ui.js']); // Original has a bug displaying exception in XSLT-based pages

        $uXAppAsset = $this->app->assetManager->register([
            'dir' => 'uhi67/uxapp',
            'patterns' => [
                'xsl/main.xsl',
                'www/js/*',
                'www/css/main.css'
            ],
        ]);
        $uXAppAsset->register($this->view, [
            'xsl/main.xsl',
            'www/css/main.css',
            'www/js/uxapp.js',
            'www/js/translations.js'
        ]);

        $this->setFavicon();
    }

    /**
     * Shows exceptions raised during displaying a page (__construct and go)
     *
     * @param Exception|UXAppException $e
     * @param string $h -- cím
     * @param bool $d -- show details
     *
     * @return void
     * @throws
     */
    function showException($e, string $h='Internal error', bool $d=true) {
        try {
            $this->setH2($h);
            $msg = $e->getMessage();
            $showtrace = (UXApp::$app->environment != 'production') || UXApp::debugIsOn() || $d;
            $goto = ($e instanceof UXAppException) ? $e->getGoto() : $this->error_url;

            $extra = ($e instanceof UXAppException) ? $e->getExtra() : 'no details';
            if(!$this->view) {
                UXApp::showException($e, $showtrace);
                return;
            }
            $this->view->addContentNode('message', ['class'=>'error alert uxapp-exception'], [
                ['title', $msg],
                (UXApp::debugIsOn() || $d) ? ['content',
                    ['div', 'in file `'.$e->getFile().'` at line '.$e->getLine()],
                    ['div', $extra],
                    ['pre', $e->getTraceAsString()],
                    ]: ''
            ]);

            if ($showtrace) {
                $content = [];
                if (($e instanceof UXAppException) && $e->extra) {
                    if(is_array($e->extra)) foreach ($e->extra as $v) $content[] = ['div', Util::objtostr($v)];
                    else $content[] = ['div', Util::objtostr($e->extra)];
                }
                $error = error_get_last();
                UXApp::trace(['errortype' => isset($error['type']) ? $error['type'] . '&' . E_ERROR : null]);
                if(is_array($error) && ($error['type'] & E_ERROR)) {
                    $content[] = array_merge(['div', 'class'=>"trace", '['.Util::friendlyErrorType($error['type']).']'],
                        $this->colorTraceNode($error['file']),
                        [htmlspecialchars_decode('('.$error['line'].'): '.$error['message'])]
                    );
                }

                $ee = $e;
                while($ee) {
                    $info = $ee->getFile() . '(' . $ee->getLine() . '): ' . htmlspecialchars($ee->getMessage());
                    if(UXApp::debugIsOn() || $d) {
                        $content[] = ['div', 'class' => 'debug-session', 'debug-session: ' . UXApp::$app->debug->id];
                    }
                    $content[] = ['h4', $ee===$e ? 'Backtrace' : 'Previous exception'];
                    $content[] = array_merge(['div', 'class'=>"trace title"], $this->colorTraceNode($info));
                    $this->log([$info], 'error');
                    $taa = explode("\n", htmlspecialchars($ee->getTraceAsString()));
                    foreach ($taa as $trl) {
                        $content[] = array_merge(['div', 'class'=>"trace"], $this->colorTraceNode($trl));
                    }

                    $ee = $ee->getPrevious();
                    if($ee) $content[] = ['h4', 'Previous exception'];
                }
                $this->view->addContentNode('message', ['class'=>'alert alert-info error error-info uxapp-details'], [
                    'title' => ['Részletek'],
                    'content' => [$content],
                ]);
            }

            $defaultGoto = $e instanceof ForbiddenException ? $this->app->baseurl : $this->error_url;
            $goto = $goto ?: $defaultGoto;
            $goto = is_array($goto) ? $this->createUrl($goto) : $goto;
            $this->view->addControlVar('url', $goto);
            $this->view->addControlVar('original-view', $this->viewName);
            $this->setName('uxapp');
            $this->viewName = 'uhi67/uxapp:message';
            header("HTTP/1.1 500 Server Error");
            $this->render();
        }
        catch(Throwable $e1) {
            echo "Another error occured during handling an Exception\n";
            UXApp::showException($e1);
            UXApp::showException($e);
        }
    }

    /**
     * Appends color classes to parts of stack trace lines depending on position of the source file.
     *
     * @param string $trl
     * @return string
     */
    function colorTrace(string $trl) {
        $trl = preg_replace('#(vendor[\\\\/]uhi67[\\\\/]uxapp[\\\\/])([a-zA-Z0-9_-]+[\\\\/])+([a-zA-Z0-9_-]+\.php)#i', '$1$2<span class="trace-uxapp">$3</span>', $trl);
        $trl = preg_replace('#(vendor[\\\\/]uhi67[\\\\/])([a-z0-9_-]+[\\\\/])([a-zA-Z0-9_/\\\\-]+\.php)#i', '$1<span class="trace-vendor">$2</span><span class="trace-uxapp-mod">$3</span>', $trl);
        $trl = preg_replace('#(vendor[\\\\/])([a-z0-9_-]+[\\\\/][a-z0-9_-]+[\\\\/])([a-zA-Z0-9_-]+\.php)#i', '$1<span class="trace-vendor">$2</span><span class="trace-vendor-mod">$3</span>', $trl);
        $trl = preg_replace('#(\\\\(pages|www)\\\\)([a-zA-Z0-9_-]+\.php)#i', '$1<span class="trace-app">$3</span>', $trl);
        $trl = preg_replace('#(\\\\(inc|models)\\\\)([a-zA-Z0-9_-]+\.php)#i', '$1<span class="trace-inc">$3</span>', $trl);
        /** @noinspection PhpUnnecessaryLocalVariableInspection */
        $trl = preg_replace('#([a-z0-9_-]+[\\\\/])([a-zA-Z0-9_-]+\.php)\(#i', '$1<span class="trace-source">$2</span>(', $trl);
        return $trl;
    }

    /**
     * Appends color classes to parts of stack trace lines depending on position of the source file.
     *
     * @param string $trl
     * @return array -- strings and node definitions
     */
    function colorTraceNode(string $trl) {
        $patterns = [
            // A [0] minta találatait díszíti sorban az utána felsorolt classokkal.
            [/** @lang RegExp */'#^(.*vendor[\\\\/]uhi67[\\\\/]uxapp[\\\\/](?:[a-zA-Z0-9_-]+[\\\\/])+)([a-zA-Z0-9_-]+\.php)(.*)$#i', null, 'trace-uxapp', null],
            [/** @lang RegExp */'#^(.*vendor[\\\\/]uhi67[\\\\/])([a-z0-9_-]+[\\\\/])([a-zA-Z0-9_/\\\\-]+\.php)(.*)$#i', null, 'trace-vendor', 'trace-uxapp-mod', null],
            [/** @lang RegExp */'#^(.*vendor[\\\\/])([a-z0-9_-]+[\\\\/][a-z0-9_-]+[\\\\/])([a-zA-Z0-9_-]+\.php)(.*)$#i', null, 'trace-vendor', 'trace-vendor-mod', null],
            [/** @lang RegExp */'#^(.*\\\\)(pages|www)(\\\\[a-zA-Z0-9_-]+\.php)(.*)$#i', null, null, 'trace-app', null],
            [/** @lang RegExp */'#^(.*\\\\)(inc|models)(\\\\[a-zA-Z0-9_-]+\.php)(.*)$#i', null, null, 'trace-inc', null],
            [/** @lang RegExp */'#^(.*[a-z0-9_-]+[\\\\/])([a-zA-Z0-9_-]+\.php)(.*)$#i', null, 'trace-source', null],
        ];
        foreach($patterns as $pattern) {
            if(preg_match($pattern[0], $trl, $mm)) {
                $result = [];
                for($i=1; $i<count($pattern); $i++) {
                    $result[] = $pattern[$i] ? ['span', 'class' => $pattern[$i], htmlspecialchars_decode($mm[$i])] : htmlspecialchars_decode($mm[$i]);
                }
                return $result;
            }
        }
        return [htmlspecialchars_decode($trl)];
    }

    /**
     * ## UAppPage end actions (instead of destruct)
     * - Removes user
     * - Closes session
     * - Deletes stored post if this page is the target
     * - Destructs UXPage
     */
    function finish() {
        try {
            if($this->user) $this->user->finish();
            session_write_close();
            if(isset($_SESSION['_post'])) {
                $_target = $_SESSION['_post']['_target'];
                if($this->name == $_target) {
                    unset($_SESSION['_post']);
                }
            }
            parent::finish();
        }
        catch(Throwable $e) {
            // TODO: Itt már nem lehet kiírni, úgysem jelenik meg, legfeljebb fájlba logolni.
            exit;
        }
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Execute the actions of the page, and render the result after.
     * Usually called by UXApp::run() just after creating page instance.
     *
     * Serves both WEB and CLI contexts
     *
     * @param bool $returnException -- do not handle Exceptions
     * @return int|string -- integer 0 = success, or exit status code; string to display
     * @throws NotFoundException
     * @throws UXAppException
     * @throws UserException
     */
    function go($returnException=false) {
        /**
         * User action must return:
         * - false if output is already emitted, no further render necessary
         * - true if normal render required
         * - array or object if json data is returned
         * - string to display output
         * - integer 0 = success, other = status code
         */
        try {
            $className = static::class;
            $this->init();
            $result = $this->processActions($this->act);
            if (!$this->nolog) $this->log();
            if ($result === false) return UXApp::EXIT_STATUS_OK;
        }
        catch(UserException $e) {
            if($returnException) throw $e;
            $this->showException($e, $e->getTitle(), false);
            return (int)$e->getCode() ?: UXApp::EXIT_STATUS_ERROR;
        }
        catch(Exception $e) {
            if($returnException) throw $e;
            $title = 'Unexpected internal error / Váratlan hiba a művelet feldolgozása során.';
            $this->showException($e, $title);
            return (int)$e->getCode() ?: UXApp::EXIT_STATUS_EXCEPTION;
        }
        if($result===true && !$this->app->isCLI) {
            try {
                $this->render();
                $this->afterRender();
            }
            catch(Throwable $e) {
                throw new UXAppException("Error rendering action $className/$this->act ($this->designmodule:$this->layout)", null, $e);
            }
        }
        else if($result===null) throw new Exception("Action '$this->act' of '$className' returned null (should return integer, boolean, string or array)");
        else if(is_array($result) || is_object($result)) return $this->json_response($result);
        else if(is_int($result)) return $result;
        else echo $result;
        return UXApp::EXIT_STATUS_OK;
    }

    /**
     * Returns
     * - false if output is already emitted, no further render necessary
     * - true/null if normal render required
     * - object/array if json data is returned
     * - other string to output
     *
     * @param $act
     *
     * @return bool|integer|array|object|string -- null is invalid
     * @throws
     */
    function processActions($act) {
        if(!$this->view) throw new UXAppException('View is not initialized in '.$this->shortName. ' (parent::prepare() may be missing)');
        if($act=='' && is_callable([$this, $func = 'actDefault'])) {
            return call_user_func_array([$this, $func], $this->getArgs($func, $this->request));
        }
        else if($act) {
            $func = 'act'.Util::camelize($act, true);
            if(!is_callable([$this, $func])) throw new NotFoundException(UXApp::la('uxapp', 'Unknown action `{$class}/{$act}`', ['class'=>$this->shortName, 'act'=>$func]));
            return call_user_func_array([$this, $func], $this->getArgs($func, $this->request));
        }
        return true;
    }

    /**
     * Returns an array of actual arguments of the method
     *
     * @param string $methodName -- action method name with optional parameters
     * @param array $query -- the request query parameters
     * @return array -- the actual arguments to be passed
     * @throws Exception
     */
    public function getArgs($methodName, $request) {
        $args = [];
        $ref = new ReflectionMethod($this, $methodName);
        foreach($ref->getParameters() as $param) {
            $urlizedParameterName = Util::uncamelize(Util::camelize($param->name));
            $defaultValue = $param->isOptional()? $param->getDefaultValue() : null;
            $actualValue = $request->get($urlizedParameterName);
            $args[$param->name] = $actualValue ?? $defaultValue;
            if(!$param->isOptional() && !isset($actualValue) && $defaultValue===null) {
                throw new Exception("The action `$this->act` requires parameter `$urlizedParameterName`", Http::HTTP_BAD_REQUEST);
            }
        }
        return $args;
    }

    /**
     * Sets the name of the page
     * Used at logging, default view, etc.
     *
     * @param string $name
     * @return string
     */
    protected function setName(string $name) {
        return $this->name = $name;
    }

    /**
     * Sets the name of design (module: or vendor/module:layout or layout)
     *
     * Layout may be a directory in the module or a simple xsl file.
     *
     * @param string $name -- module:, module:layout or layout
     * @return string -- layout name (without extension)
     */
    public function setLayout(string $name) {
        $lp = strrpos($name, ':');
        if($lp==strlen($name)) {
            // Set module name only
            $this->designmodule = substr($name, 0, $lp);
        }
        else if($lp===false) {
            // Sets layout name only
            $this->layout = $name;
        }
        else {
            // Set both
            $this->designmodule = substr($name, 0, $lp);
            $this->layout = substr($name, $lp+1);
        }
        return $name;
    }

    /** @noinspection PhpUnused */

    public function getLayout() {
        return $this->layout;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setH1($h1, $h1link=null) {
        $this->h1 = $h1;
        $this->h1link = $h1link;
    }

    function setH2($h2, $h2link=null) {
        $this->h2 = $h2;
        $this->h2link = $h2link;
    }

    /**
     * @throws UXAppException
     */
    function setFavicon($url=null) {
        if(!$url) {
            if(file_exists($this->app->basePath.'/favicon.ico')) $url = 'favicon.ico';
            elseif(file_exists($this->app->basePath.'/favicon.png')) $url = 'favicon.png';
            else $url = $this->app->assetManager->single(['uhi67/uxapp', 'img/uxapp-logo.png']);
        }
        $this->ico = $url;
    }

    /**
     * Creates an instance of a module and registers on the page
     *
     * @param string|array $options Module classname or configuration array
     *
     * @return Module
     * @throws ReflectionException
     * @throws UXAppException
     */
    public function registerModule($options) {
        if(!is_array($options)) {
            $options = ['class' => $options];
        }
        $options['page'] = $this;
        /** @var Module $module */
        $module = Component::create($options);
        return $module;
    }

    /**
     * Registers a Module on page with it's js, css and xsl
     * (modules with or without constructor)
     *
     * Module's method init(page, dir) function may register additional assets
     *
     * With current implementation, registering modules with xsl includes must not be conditional on a page.
     *
     * @param string $module -- class name of the module (use the full-qualified namespace name)
     * @param bool $create -- create module object, else call init if exists
     *   classname of Module
     *
     * @return bool
     * @throws Exception
     * @throws UXAppException
     *
     * @deprecated -- use Module::register(page)
     */
    function register(string $module, bool $create=false) {
        $className = $module;
        if(!class_exists($className)) {
            $className = '\uhi67\\'.strtolower($module);
            if(!class_exists($className)) {
                $className = '\uhi67\uxapp-' . strtolower($module);
                if(!class_exists($className)) {
                    throw new UXAppException("Module class '$module' does not exist");
                }
            }
        }
        if(array_key_exists($className, $this->modules)) return false;

        $r = new ReflectionClass($className);
        $dirname = dirname($c = $r->getFileName());
        if($dirname) {
            $instance = null;
            if($create) {
                $instance = new $className($this);
            }
            if(file_exists($dirname.$c.'.php')) {
                if(is_callable([$c, 'init'])) {
                    $result = call_user_func([$c, 'init'], $this, $dirname);
                    if(!$instance) $instance = $result;
                }
            }
            $this->modules[$className] = $instance;

            // Registering assets
            if(!($dir = dir($dirname))) {
                UXApp::trace(['Dir error. Path' => $module], ['tags'=>'uxapp']);
                return false;
            }
            while (false !== ($entry = $dir->read())) {
                $eee = strrpos($entry, '.');
                $ext = $eee===false ? '' : substr($entry, $eee+1);
                if($ext=='js') $this->view->addJs($c.'/'.$entry);
                if($ext=='css') $this->view->addCss($c.'/'.$entry);
                if($ext=='xsl') {
                    $this->view->addXsl($module.':'.$entry);
                }
            }
        }
        else {
            throw new Exception("Registration error: Module '$c' not found");
        }
        return true;
    }

    /**
     * Builds the actual base XSL for the pageview to be render with
     *
     * The name of pageview set is will be available in DOM at control/pageview.
     *
     * The built `pageview.xsl` files will contain proper references for all of xsl files of the page (view and modules) in two version:
     *
     * 1. for server render `_:__pageview.xsl` contains local paths relative to runtime/xsl
     * 2. for client render `_:_pageview.xsl` contains remote paths relative to www
     * 3. for syntax validation, `_:___pageview.xsl` contains local paths relative to runtime/xsl
     *
     * The xsl will be rebuilt only if not exists or the php file of the page is newer, or the file is older than 2 hours.
     *
     * inputs: $this->name, $this->viewName, $this->design, $this->designmodule
     * outputs: ($this->name), $this->pageview, $this->xsl; _pageview.xsl, __pageview.xsl files;
     *
     * User resetxsl=1 in request to force generating XSL's
     *
     * TODO: check path in generated files
     *
     * @return void
     * @throws Exception
     * @throws UXAppException
     */
    function createXSL() {
        $genxslpath = $this->app->runtimeXslPath; // Ahová a generált fájlok kerülnek

        // Parse view name
        $viewName = $this->viewName;
        $lp = strrpos($viewName, ':');
        if($lp!==false) {
            // 'vendor/module:viewName' format
            $moduleName = substr($viewName, 0, $lp);
            $viewName = substr($viewName, $lp+1);
            /** string $xslpath -- relative path to xsl-gen dir */
            $xslpath = Util::getRelativePath($genxslpath, $path = $this->getModulePath($moduleName).'/xsl/');
        } else {
            $moduleName = null;
            $xslpath = Util::getRelativePath($genxslpath, $path = $this->app->xslPath.'/');
        }
        if(!$this->name) $this->name = $viewName;
        if(is_dir($path.$viewName)) {
            $actView = $viewName.'/'.$this->actName;
            if(!file_exists($path . $actView . '.xsl') && file_exists($path . $viewName.'/default.xsl')) {
                $viewName = $viewName.'/default';
            } else {
                $viewName = $actView;
            }
        }
        $this->viewfile = $path . $viewName . '.xsl';

        if(!file_exists($this->viewfile)) {
            $this->view->addHead('original-view', $moduleName.':'.$viewName, ['file'=>$this->viewfile]);
            $moduleName = 'uhi67/uxapp';
            $xslpath = Util::getRelativePath($genxslpath, $this->getModulePath($moduleName).'/xsl/');
            $viewName = 'uhi67/uxapp:viewnotfound';
        }

        $this->view->addHead('name', $this->name);
        $this->view->addHead('view', $this->app->assetManager->single($this->viewfile), [
            'path' => $xslpath.$viewName.'.xsl',
            'module' => $moduleName,
            'name' => $viewName,
            'order' => 200,
        ]);

        // determine design module and layout, apply (add all assets) (order attribute is important)
        if($this->designmodule) {
            if(class_exists($this->designmodule)) {
                /** @var DesignModuleInterface $design */
                $design = new $this->designmodule();
                Assertions::assertClass($design, 'DesignModuleInterface', false);
                $design->addAssets($this->layout);
            }
            elseif(is_dir($dirName = $this->app->rootPath.'/modules/'.$this->designmodule.'/xsl/'.$this->layout)) {
                $this->view->addDirAssets($dirName, $genxslpath, $this->designmodule, 100);
            }
            elseif(is_dir($dirName = $this->app->rootPath.'/modules/'.$this->designmodule.'/'.$this->layout)) {
                $this->view->addDirAssets($dirName, $genxslpath, $this->designmodule, 100);
            }
            elseif(file_exists($this->app->rootPath.'/modules/'.$this->designmodule.'/xsl/'.$this->layout.'.xsl')) {
                $this->view->addXsl([$this->designmodule, $this->layout], 1);
            }
            elseif(file_exists($this->app->rootPath.'/modules/'.$this->designmodule.'/'.$this->layout.'.xsl')) {
                $this->view->addXsl([$this->designmodule, $this->layout], 1);
            }
            elseif(is_dir($dirName = UXApp::$vendorDir.'/'.$this->designmodule.'/xsl/'.$this->layout)) {
                // Van ilyen modul dir, xsl-lel
                $this->view->addDirAssets($dirName, $genxslpath, $this->designmodule, 100);
            }
            elseif(file_exists($layoutFile = UXApp::$vendorDir.'/'.$this->designmodule.'/xsl/'.$this->layout.'.xsl')) {
                // Van ilyen modul és xsl file
                $url = $this->app->assetManager->single($layoutFile);
                // TODO: This path may be wrong with relocated $vendorDir
                $this->view->addXslFile($url, ['path'=>'../../vendor/'.$this->designmodule.'/xsl/'.$this->layout.'.xsl', 'order'=>1]);
            }
            elseif(file_exists($this->app->xslpath.'/'.$this->designmodule.'/'.$this->layout.'.xsl')) {
                // Van ilyen /xsl/design/layout.xsl file
                $this->view->addXsl($this->designmodule.'/'.$this->layout.'.xsl');
//                $url = $this->app->assetManager->single($layoutFile);
//                $this->view->addXslFile($url, ['path' => '../../xsl/'.$this->designmodule.'/'.$this->layout.'.xsl', 'order'=>1]);
            }
            else throw new UXAppException("Design module and layout `$dirName` is missing ($this->designmodule)");
        }
        elseif($this->layout) {
            // Otherwise find layout directory/file/class in the application
            if(is_dir($dirName = $this->app->xslPath.'/'.$this->layout)) {
                $this->view->addDirAssets($dirName, $genxslpath, null, 100);
            }
            elseif(file_exists($this->app->xslPath.'/'.$this->layout.'.xsl')) {
                $this->view->addXsl($this->layout.'.xsl', 1);
            }
            elseif(class_exists($className = 'app\lib\\'.$this->layout)) {
                $design = new $className;
                Assertions::assertClass($design, 'DesignModuleInterface', false);
                /** @var DesignModuleInterface $design */
                $design->addAssets($this->layout);
            }
            else throw new UXAppException("Design layout `$dirName` is missing");
        }

        // LATER: itt jönne az, hogy függőség szerint rendezzük a kellékeket, ha a függőség implementálva lenne

        $this->pageview = $this->name . '_' . str_replace(['/', '\\'], '_', $viewName);

        // Builds the base xsl files of the pageview for both render modes
        // These file contain proper references for all other xsl files of the page view name and used modules

        /** @noinspection PhpRedundantOptionalArgumentInspection */
        $this->genXSL($genxslpath.'/_'.$this->pageview.'.xsl', 'server');
        $this->genXSL($genxslpath.'/__'.$this->pageview.'.xsl', 'client');
        $this->genXSL($genxslpath.'/___'.$this->pageview.'.xsl', 'validate');

        // Determine the current name of the base xsl file of the pageview depending on render mode
        $cxslt = class_exists('\XSLTProcessor');
        if(!$cxslt) $this->view->render = 'client';
        #$runtime = $this->app->dataPath;
        if($this->view->render == 'client') {
            $this->view->xsl = '_:__'.$this->pageview;
        }
        if($this->view->render == 'server' || !$this->view->xsl) {
            $this->view->xsl = '_:_'.$this->pageview;
        }
        $this->view->addControlVar('pageview', $this->pageview, ['view'=>$this->viewName]);
        $this->view->addControlVar('render', $this->view->render);
    }

    /**
     * Refreshes an XSL file based on template name using $mode
     * Creates new file only if the php file of this class is newer then the existing result file
     * The template is an XSL transformation applied to xml of this page
     *
     * @param string $xslfile -- name of result xsl file
     * @param string $mode -- template selector
     *
     * @return void
     * @throws Exception -- on configuration
     */
    function genXSL(string $xslfile, string $mode='server') {
        if(!file_exists(dirname($xslfile))) {
            throw new UXAppException("Invalid genXSL runtime path", $xslfile);
        }
        $rc = new ReflectionClass($this);
        $phpfile = $rc->getFileName();
        $mtime = filemtime($phpfile);
        if(!file_exists($xslfile) || filemtime($xslfile)<$mtime || $this->resetxsl || UXApp::debugIsOn()) {
            UXApp::trace(['xslfile' => $xslfile], ['tags'=>'uxapp']);
            // _pagename.xsl generálása _page_template.xsl alapján, head/xsl elemekből
            $genpage = dirname(__DIR__)."/xsl/gen_page_$mode.xsl";
            /** @var UXMLDoc $xsl */
            $xsl = $this->view->xmldoc->process($genpage);
            $createdCommentNode = $xsl->createComment('Created at '.date('Y-m-d H:i:s'));
            $xsl->documentElement->appendChild($createdCommentNode);
            $xsl->save($xslfile);
            @chmod($xslfile, 0764);
        }
    }

    /**
     * ## UAppPage::render()
     *
     * Generates base XSL,
     * completes the head data with server and title data,
     * then calls legacy render with the base xsl.
     *
     * @return bool -- success
     * @throws Exception
     */
    function render() {
        if($this->viewName == 'raw') return true;

        try {
            $this->createXSL();
            $this->view->addHead('title', $this->title);

            if(($status_message = $this->app->getParam('status_message'))) $this->view->addHead('status', $status_message);
            if($this->h1) $this->view->addHead('h1', $this->h1, ['url'=>$this->app->createUrl($this->h1link)]);
            if($this->h2) $this->view->addHead('h2', $this->h2, ['url'=>$this->app->createUrl($this->h2link)]);
            if($this->ico) $this->view->addHead('ico', $this->ico);
            if(!$this->view->xsl) {
                if($this->viewName) $this->view->xsl = '_' . $this->viewName;
                else if($this->name) $this->view->xsl = $this->name;
            }
            UXApp::trace(['xsl' => $this->view->xsl, 'viewName' => $this->viewName, 'name' => $this->name], ['tags' => 'uxapp']);
            return $this->view->render();
        }
        catch(Throwable $e) {
            UXApp::showException($e);
            return false;
        }
    }

    /**
     * Checks if the user is logged in.
     * If not, and redirect is enabled, calls login action (false may be returned)
     * If redirect is not enabled, a ForbiddenException occurs.
     *
     * @param bool $enablelogin -- enables redirect to login
     * @param string|null $goto -- return link for exception message
     *
     * @return boolean -- is logged in
     * @throws ForbiddenException -- if not logged in and no login enabled
     * @throws Exception
     */
    function mustLogin(bool $enablelogin=true, string $goto=null) {
        if(!$this->user) throw new Exception('No user object. (check parent::prepare in caller page)');
        if($this->user->isEnabled()) return true;
        $url = $_SERVER['REQUEST_URI'];
        if(strpos($url, 'login.php')) $url='';
        $_SESSION['loginurl']=$url;
        if($this->user && $this->user->loggedIn() || !$enablelogin) throw new ForbiddenException('A rendszer használatához nincs joga', null, $goto);
        $this->redirect(['start', 'login'=>1, 'url'=>$url, 'msg'=>2]);
        return false;
    }

    /**
     * Adds one or more msg node to control.
     * msg node represents a closeable system message with a title, an optional content.
     * Rendered in Main/main.xsl or application's design
     *
     * @param mixed $msg -- message or message array
     * @param array|null $attr -- message attributes pl. array('class'=>'error')
     * @param string|array $contents -- contents of a content block attached to a single (or last) message (paragraph of multiple paragraphs)
     *
     * @return UXAppPage
     * @throws
     */
    function addMessages($msg, array $attr=null, $contents=null) {
        $node_msg = null;
        if(is_array($msg)) {
            foreach($msg as $m) {
                $node_msg = $this->view->addControlNode('msg', $attr, [['title', $m]]);
            }
        }
        else {
            if(!$this->view) throw new Exception("No view");
            $node_msg = $this->view->addControlNode('msg', $attr, [['title', $msg]]);
        }
        if(is_string($contents) && $contents!=='') $contents = [$contents];
        if(is_array($contents) && $node_msg) {
            foreach($contents as $content) {
                $node_msg->addHypertextNode('content', $content);
            }
        }
        return $this;
    }

    /**
     * Override in AppPage if you want to add default menu contents in your app.
     */
    protected function addDefaultMenu() {
    }

    /**
     * Transaction log
     *
     * If the given action is already logged, logging will be repeated only with parameters given
     * (context action overrides action parameter)
     *
     * @param string|array $params -- the message
     * @param string|null $action -- Action to be logged, default is $this->act
     * @param array $context -- more data, like [level, user, page, action, target, tags, timestamp, ip] (all has default value or rule)
     *
     * @return void
     * @throws UXAppException
     */
    function log($params='', string $action=null, array $context=[]) {
        $page = $this->name ?: get_class($this);
        $action = $action ?: ($this->act ?: '');
        Assertions::assertString($action, true);
        if(isset($context['level'])) $level = $context['level'];
        else {
            $level = 'info';
            if($action && isset($this->logged[$action]) && !$params) $level = 'trace'; // repeated action, trace only
        }
        $paramx = is_array($params) ? Util::objtostr($params, false, true, 3) : $params;
        $this->logged[$action] = true;
        $this->app->log($level, $paramx, array_merge(['tags'=>'app trans', 'page'=>$page, 'action'=>$action], $context));
    }

    /**
     * ## Redirect to the given url.
     * In debug mode, redirect is paused with a link page.
     *
     * **Always returns** after redirection written to the output.
     *
     * - If an array location is used, page-relative url will be calculated.
     * - If no location is given, $this->error_url will be applied.
     *
     * @see createUrl()
     *
     * @param string|array $location or location components
     *
     * @return bool
     * @throws
     */
    function redirect($location=null) {
        if(is_array($location)) $location = $this->urlFormatter->createUrl($location);
        if($location===null) $location = $this->error_url;
        if(!UXApp::debugIsOn()) {
            header ("Location: $location");
            $this->setView('uhi67/uxapp:redirect');
            $this->view->addHead('redirect', $location);
        }
        else {
            $this->setView('uhi67/uxapp:redirect');
            $node_redirect = $this->view->addHead('redirect', $location);
            $node_redirect->setAttribute('pause', 1);

            $msgs = $this->app->session->get('sendmessage');
            if(is_array($msgs)) foreach($msgs as $msga) {
                $this->view->addControlVar('sendmsg', $msga[0], ['class'=>$msga[1]]);
            }
        }
        return true;
    }

    /**
     * Displays a message page with one or more lines of message
     * The render is completed on this way.
     *
     * Usage in action: return showMessage(...); // returns false to avoid double rendering
     *
     * @param string $title -- Title of the window
     * @param string $h1 -- Main title
     * @param string $h2 -- Subtitle
     * @param mixed $msg -- message or array of messages or array of UCD structures.
     * @param string $url -- url for the [Continue] link
     * @param array $attr -- attributes for the messages, e.g. class
     *
     * @return void
     * @throws Exception
     */
    function showMessage($title, $h1, $h2, $msg, $url=null, $attr=null) {
        UXApp::trace(['title' => $title], ['tags'=>'uxapp']);

        if(!is_array($msg)) $msg = [$msg];
        foreach($msg as $m) {
            $this->view->addContentNode('message', $attr, [
                ['content', 'div' => [$m]],
            ]);
        }

        $this->setView('uhi67/uxapp:message');
        $this->setTitle($title);
        if($h1!=null) $this->setH1($h1);
        if($h2!=null) $this->setH2($h2);
        if($url!=null) $this->view->addControlVar('url', $url);
        $this->render();
    }

    /**
     * Stores a "flash" message to display on the next page
     * Can store more messages one after another
     *
     * Markdown syntax is accepted if msg starts with backtick, e.g.
     *
     * ``[caption](url)`
     *
     * @param string|array $msg -- Message text or array($msg, array($params)), with %s style params
     * @param string $class -- message class (error/ok)
     * @param bool $unique -- display same message is only once in one transaction (uses base message without params)
     * @param string|array $content -- optional extended content for message.
     *
     * @return void
     * @throws UXAppException
     * @throws Exception
     */
    function sendMessage($msg, string $class='', bool $unique=false, $content=null) {
        // legacy class conversion
        $classNames = [
            'error' => 'alert-danger',
            'warning' => 'alert-warning',
            'ok' => 'alert-success',
            'info' => 'alert-info',
        ];
        if(array_key_exists($class, $classNames)) $class .= ' '.$classNames[$class];
        $msg1 = is_array($msg) ? $msg[0] : $msg; // Unique message base
        Assertions::assertString($msg1);
        if(is_array($msg)) {
            $msg = vsprintf($msg1, $msg[1]);
        }
        $msgs = $this->app->session->get('sendmessage', []);
        if($unique) {
            $idx = in_array([$msg1, $class], $msgs);
            if($idx===false) $msgs[] = [$msg, $class, $content];
        }
        else $msgs[] = [$msg, $class, $content];
        $_SESSION['sendmessage'] = $msgs;
    }

    /**
     * Retrieves all flash messages from session to the DOM
     *
     * @return string -- the last flash message or '' if none.
     * @throws
     */
    function getMessage() {
        $msg = '';
        $msgs = $this->app->session->get('sendmessage');
        while(is_array($msgs) && count($msgs)>0) {
            $msga = array_shift($msgs);
            $msg = $msga[0];
            $class = $msga[1];
            $clx = ['class'=>"flash $class"];
            $this->addMessages(html_entity_decode($msg), $clx, ArrayUtils::getValue($msga, 2));
        }
        $_SESSION['sendmessage'] = $msgs;
        return $msg;
    }

    /**
     * Returns the indexed one from the path words separated by / in the url of the page. [0] is the function name
     *
     * @param integer $i
     * @return string
     */
    public function getPath(int $i) {
        return $this->path[$i] ?? null;
    }

    /**
     * ## Creates a new url based on the url of current page
     * Returns current url on empty input
     *
     * Creates another action of the same controller.
     *
     * - numeric indexed element will be the new act or page/act
     * - associative elements will be new/overridden parameters
     * - `#`-index will be the fragment
     *
     * @param string|array $params -- url tags and parameters (or a single action word)
     * @param bool $keepQuery -- keep current request's query parameters
     *
     * @return string -- the new url
     * @throws UXAppException
     * @see UXAppPage::createUrl()
     *
     */
    public function createUrl($params= [], bool $keepQuery=false) {
        if(!$params) return $keepQuery ? $this->currentUrl : parse_url($this->currentUrl, PHP_URL_PATH);
        if(is_string($params)) $params = [$params];
        if(isset($params[0])) {
            $act = array_shift($params);
            $page = null;
            if(strpos($act, '/')) [$page, $act] = explode('/', $act);
            $pa = [];
            $pa['page'] = $page ?: $this->name;
            $pa['act'] = $act;
            if($keepQuery) $params = array_merge($this->request->query, $params);
            $params = array_merge($params, $pa);
        }
        return $this->app->urlFormatter->createUrl($params);
    }

    /**
     * @throws UXAppException
     */
    public function url() {
        $params = $this->request->get();
        $params['act'] = $this->act;
        return $this->createUrl($params);
    }

    /**
     * ## Saves post data to the session and binds to this page by name
     * @return bool
     */
    function savePost() {
        $post = [];
        foreach($_POST as $i => $value) $post[$i] = $value;
        $post['_target'] = $this->name;
        $_SESSION['_post'] = $post;
        return true;
    }

    /** @noinspection PhpUnused */

    /**
     * @throws NotFoundException|UXAppException
     */
    function actModule() {
        $modulename = $this->request->req('module');
        /** @var Module $module */
        $module = ArrayUtils::getValue($this->modules, $modulename);
        if($module) $module->action();
        else throw new NotFoundException("Invalid module action: `$modulename`", $this->url());
    }

    /**
     * Creates and outputs a JSON response from given data
     *
     * @param mixed $data
     * @return string
     *
     * @noinspection PhpMethodNamingConventionInspection
     * @noinspection PhpUnused
     * @throws Exception
     */
    function json_response($data) {
        $data = json_encode($data, $this->jsonFlags);
        if(headers_sent($file, $line)) throw new Exception("Headers sent in $file at line $line");
//		header('X-JSON: '.$data);
        header('Content-Type: application/json');
        print $data;
        $this->viewName = 'raw';
        return $data;
    }

    /**
     * Sets the view name for the page.
     *
     * The view is the name of the simple XSL file without extension.
     *
     * The xsl file must not contain includes. Includes will be generated before rendering.
     *
     * Sets also the name of page, if not set.
     *
     * View file may be in application's xsl directory or in Uxml's xsl directory. First searched in application's directory. Specify 'vendor/module:viewname' to use xsl from a module explicitly.
     * If not found, Uxml's error view will be used.
     *
     * @param string $viewName (relative to /xsl or module's xsl directory; without extension)
     *
     * @return string -- the view name (can be different from input) without extension
     * @throws Exception
     */
    public function setView(string $viewName) {
        $xslpath = UXApp::$app->xslpath.'/';
        $uappxslpath = UXApp::$app->rootPath.'/xsl/';

        // "vendor/library:view" alakú
        if(preg_match('~^([\w_-]+/[\w_-]+):([\w_.-]+)$~', $viewName, $mm)) {
            $vendorPath = UXApp::$vendorDir;
            $moduleName = $mm[1];
            $moduleDir = $vendorPath.'/'.$moduleName;
            $baseName = $moduleDir.'/xsl/'.$mm[2];
            $this->viewfile = $baseName.'.xsl';
        }
        else {
            // alkalmazásbeli vagy uxapp
            $baseName = $xslpath . $viewName;
            $this->viewfile = $baseName . '.xsl';
            if(!file_exists($this->viewfile)) {
                $this->app->trace(['viewfile' => $this->viewfile], ['color' => '#C88']);
                $baseName = $uappxslpath . $viewName;
                $this->viewfile = $baseName.'.xsl';
            }
        }

        if(is_dir($baseName)) {
            $this->viewfile = $baseName.'/'.$this->actName.'.xsl';
            $viewName .= '/'.$this->actName;
        }

        if(!$this->name) $this->name = $viewName;
        if(!file_exists($this->viewfile)) {
            $this->originalView = $viewName;
            $this->view->addHead('original-view', $this->originalView, ['file'=>$this->viewfile]);
            $viewName = 'uhi67/uxapp:viewnotfound';
            $this->viewfile = $uappxslpath.'viewnotfound.xsl';
        }
        return $this->viewName = $viewName;
    }

    public function getCurrentUrl(): ?string {
        return $this->app->request->url;
    }

    public function getUser() {
        return UXApp::$app->user;
    }

    public function getUrlFormatter() {
        return $this->app->urlFormatter;
    }

    public function getRequest() {
        return $this->app->request;
    }

    public function getSession() {
        return $this->app->session;
    }

    public function getNode() {
        return $this->view->node;
    }

    public function getActName() {
        return $this->act ?: 'default';
    }
}
