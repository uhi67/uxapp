<?php
/**
 * Created by PhpStorm.
 * User: uhi
 * Date: 2019. 03. 20.
 * Time: 22:07
 */

namespace uhi67\uxapp;

interface LoggerInterface extends \Psr\Log\LoggerInterface {
	const
		LEVEL_EMERGENCY = 'emergency',
		LEVEL_ALERT = 'alert',
		LEVEL_CRITICAL = 'critical',
		LEVEL_FATAL = 'fatal',
		LEVEL_ERROR = 'error',
		LEVEL_WARNING = 'warning',
		LEVEL_NOTICE = 'notice',
		LEVEL_INFO = 'info',
		LEVEL_DEBUG = 'debug';
}
