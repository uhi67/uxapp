<?php /** @noinspection PhpIssetCanBeReplacedWithCoalesceInspection */

/** @noinspection PhpIllegalPsrClassPathInspection */

namespace uhi67\uxapp;

use Closure;
use Exception;
use ReflectionException;
use Throwable;
use uhi67\uxml\UXMLElement;

/**
 * SQL Query builder
 *
 * Usage:
 *
 * - insert (update, delete)
 *
 * ```php
 *  $query = Query::createInsert($table, $fields, $values, $connection);
 *  $result = $query->execute(); // returns number of inserted rows
 * ```
 *
 * - Non-select other query
 *
 * ```php
 *   $query = new Query(array('type'=>'sql', 'sql'=>$sql, 'params'=>$params, 'connection'=>$connection);
 *   $result = $query->execute();
 * ```
 *
 * - Select
 *
 * ```php
 *   $query = new Query(array('type'=>'select', 'from'=>$from, 'fields'=>$fields, 'where'=>$where, 'connection'=>$db, 'params'=>$params);
 *   //or
 *   $query = new Query(array('sql'=>$sql, 'connection'=>$db);
 *   $query->bind($params);
 *   //or
 *   $query = Query::createSelect($fields, $table, $joins, $conditions, $orders, $limit, $offset, $connection);
 *   //or
 *   $query = Query::createSelect($options); // including params
 *   $result = $query->all(); // first,
 * ```
 *
 * ### Properties to be set on creation
 *
 * - **connection** (DBX) -- database connection
 * - **type** (string) -- 'insert', 'select', 'update', 'delete', 'sql'
 * - **modelname** (string) -name of primary model (first element of from-list in select)
 * - **fields** (array) -select part of SELECT, or field list of INSERT
 * - array $from  - model list (optionally indexed with aliases) of FROM part of SELECT or UPDATE or a single model name
 * - **joins** (array) -model list of joined table by JOIN clause in SELECT (foreign keys) OR alias=>fk OR alias=>array(model, [jointype], conditions) element
 * - **condition** (array) -expression list of WHERE conditions in SELECT, DELETE or UPDATE
 * - **where** (array) -alias of condition
 * - **groupby** (array) -GROUP BY part of select
 * - **orders** (array) -expression list of ORDERS clause in SELECT. Does not use automatic alias of main table!
 * - **limit** (int) -LIMIT part of SELECT
 * - **offset** (int) -OFFSET part of SELECT
 * - **values** (array) -value list of VALUES part of insert or fieldname=>value pairs of UPDATE
 * - **params** (array) -the actual binded parameters
 * - **sql** (string) -the last generated raw sql before binding parameters	{@see Query::getSql()}
 *
 * Note: last property is removed because was wrong and messed up the transaction.
 *
 * @property string $modelname - name of primary model (first element of from-list in select)
 * @property array $fields - select part of SELECT, or field list of INSERT
 * @property array $from  - model list (optionally indexed with aliases) of FROM part of SELECT or UPDATE or a single model name
 * @property array $joins - model list of joined table by JOIN clause in SELECT
 * @property array $condition - expression list of WHERE conditions in SELECT, DELETE or UPDATE
 * @property array $where - alias of condition
 * @property array $groupby - GROUP BY part of select
 * @property array $orders - expression list of ORDERS clause in SELECT. Does not use automatic alias of main table!
 * @property int $limit - LIMIT part of SELECT
 * @property int $offset - OFFSET part of SELECT
 * @property array $values - value list of VALUES part of insert or fieldname=>value pairs of UPDATE
 * @property array $params - the actual binded parameters
 * @property string $sql - the last generated raw sql before binding parameters	or user specified if type='sql' {@see Query::getSql()}
 * @property array $combination - union (or intersect or except) combination data
 * @property-read resource $rs - result set of the query - set by execute functions
 * @property-read string $finalSQL - the generated SQL with parameter values
 *
 * @package UXApp
 * @author Peter Uherkovich
 * @copyright 2018
 */
class Query extends Component {
	/** @var DBX $connection - the actual connection of the Query */
	public $connection;

	/** @var string $type - type of query assigned by createXXX methods, eg. 'insert', 'select', 'update', 'delete', 'sql' */
	public $type;

	/** @var string $_modelname - name of primary model (first element of from-list in select) */
	private $_modelname;
	/** @var array $_fields - select part of SELECT, or field list of INSERT */
	private $_fields;
	/** @var string|array|null $_from  - model list (optionally indexed with aliases) of FROM part of SELECT or UPDATE or a single model name*/
	private $_from;
	/** @var array $_joins - model list of joined table by JOIN clause in SELECT */
	private $_joins;
	/** @var array $_condition - expression list of WHERE conditions in SELECT, DELETE or UPDATE */
	private $_condition;
	/** @var array $_groupby - expression list of ORDERS clause in SELECT. Does not use automatic alias of main table! */
	private $_groupby;
	/** @var array $_orders - expression list of ORDERS clause in SELECT. Does not use automatic alias of main table! */
	private $_orders;
	/** @var int $_limit - LIMIT part of SELECT */
	private $_limit;
	/** @var int $_offset - OFFSET part of SELECT */
	private $_offset;
	/** @var array $_values - value list of VALUES part of insert or fieldname=>value pairs of UPDATE */
	private $_values;
	/** @var array $_combination - union (or intersect or except) with the specified select type Query(ies) with similar fields. [Query, type, all] */
	private $_combination = [null, 'UNION', false];

	/** @var array $_params - the actual binded parameters */
	private $_params;
	/** @var string $_sql - the last generated raw sql before binding parameters */
	private $_sql;
	/** @var string $_finalSql - the last generated sql after binding parameters */
	private $_finalSql;
	/** @var string $_last - Last inserted id after executing insert query */
	private $_last;
	/** @var resource $_rs - result set of query */
	private $_rs = null;

	public $distinct;

	public function prepare() {
		if(is_string($this->_from))
			$this->_from = array($this->_from);

		if(!$this->_modelname && $this->_from) {
			if(!is_array($this->_from)) $this->_from = array($this->_from);
			$this->_modelname = $this->_from[ArrayUtils::getValue(array_keys($this->_from), 0, 0)];
		}
	}

	/**
	 * @return resource
	 */
	public function getRs() { return $this->_rs; }

	/**
	 * Returns calculated SQL without binded parameters
	 *
	 * @return string
	 * @throws
	 */
	public function getSql() {
		if($this->type!='sql' && !$this->_sql) {
			if(!$this->createConnection()) throw new UXAppException('No database connection specified');
			$this->_sql = $this->connection->buildSQL($this);
		}
		return $this->_sql;
	}

	/**
	 * @param string $sql
	 *
	 * @return Query
	 * @throws UXAppException
	 */
	public function setSql($sql) {
		if(!$this->type) $this->type = 'sql';
		if($this->type != 'sql') throw new UXAppException('Sql property can be set directly only if type is `sql`');
		$this->_sql = $sql;
		$this->_finalSql = '';
		return $this;
	}

	/**
	 * @return string
	 */
	public function getModelname() {
		return $this->_modelname;
	}

	/**
	 * @param $modelname
	 */
	public function setModelname($modelname) {
		$this->_modelname = $modelname;
		$this->_sql = '';
		$this->_finalSql = '';
	}

	/**
	 * @return array
	 */
	public function getFields() { return $this->_fields; }

	/**
	 * @param array $fields
	 * @return Query
	 * @throws UXAppException -- if Query is sql-type
	 */
	public function setFields($fields) {
		if($this->type=='sql') throw new UXAppException('Fields cannot be set on SQL type Query', $this->_sql);
		$this->_fields = $fields;
		$this->_sql = '';
		$this->_finalSql = '';
		return $this;
	}

	/**
	 * @return string|array
	 */
	public function getFrom() { return $this->_from; }

	/**
	 * @param string|array $from
	 * @return Query
	 * @throws UXAppException -- if Query is sql-type
	 */
	public function setFrom($from) {
		if($this->type=='sql') throw new UXAppException('From cannot be set on SQL type Query', $this->_sql);
		$this->_from = $from;
		if(!is_null($this->_from) && !is_array($this->_from)) $this->_from = [$this->_from];
		$this->_sql = '';
		$this->_finalSql = '';
		if(!$this->modelname && $this->_from) {
			$this->_modelname = $this->_from[ArrayUtils::getValue(array_keys($this->_from), 0, 0)];
		}
		return $this;
	}

	/**
	 * @return array
	 */
	public function getJoins() { return $this->_joins; }

	/**
	 * @param $joins
	 * @return Query
	 */
	public function setJoins($joins) {
		$this->_joins = $joins;
		$this->_sql = '';
		$this->_finalSql = '';
		return $this;
	}

	/**
	 * @return array
	 */
	public function getCondition() { return $this->_condition; }

	/**
	 * @param $condition
	 * @return Query
	 * @throws UXAppException -- if Query is sql-type
	 */
	public function setCondition($condition) {
		if($this->type=='sql') throw new UXAppException('Condition cannot be set on SQL type Query', $this->_sql);
		$this->_condition = $condition;
		$this->_sql = '';
		$this->_finalSql = '';
		return $this;
	}

	/**
	 * @return array
	 */
	public function getWhere() { return $this->_condition; }

	/**
	 * @param $condition
	 * @return Query
	 * @throws UXAppException -- if Query is sql-type
	 */
	public function setWhere($condition) {
		if($this->type=='sql') throw new UXAppException('Where cannot be set on SQL type Query', $this->_sql);
		$this->_condition = $condition;
		$this->_sql = '';
		$this->_finalSql = '';
		return $this;
	}

	/**
	 * @return array
	 */
	public function getGroupby() { return $this->_groupby; }

	/**
	 * @param $groupby
	 * @return Query
	 * @throws UXAppException -- if Query is sql-type
	 */
	public function setGroupby($groupby) {
		if($this->type=='sql') throw new UXAppException('GroupBy cannot be set on SQL type Query', $this->_sql);
		$this->_groupby = $groupby;
		$this->_sql = '';
		$this->_finalSql = '';
		return $this;
	}

	/**
	 * @return array
	 */
	public function getOrders() { return $this->_orders; }

	/**
	 * @param $orders
	 * @return Query
	 * @throws UXAppException -- if Query is sql-type
	 */
	public function setOrders($orders) {
		if($this->type=='sql') throw new UXAppException('Orders cannot be set on SQL type Query', $this->_sql);
		$this->_orders = $orders;
		$this->_sql = '';
		$this->_finalSql = '';
		return $this;
	}

	/**
	 * @return int
	 */
	public function getLimit() { return $this->_limit; }

	/**
	 * @param $limit
	 * @return Query
	 * @throws UXAppException -- if Query is sql-type
	 */
	public function setLimit($limit) {
		if($this->type=='sql') throw new UXAppException('Limit cannot be set on SQL type Query', $this->_sql);
		$this->_limit = $limit;
		$this->_sql = '';
		$this->_finalSql = '';
		return $this;
	}

	/**
	 * @return int
	 */
	public function getOffset() { return $this->_offset; }

	/**
	 * @param $offset
	 * @return Query
	 * @throws UXAppException -- if Query is sql-type
	 */
	public function setOffset($offset) {
		if($this->type=='sql') throw new UXAppException('Offset cannot be set on SQL type Query', $this->_sql);
		$this->_offset = $offset;
		$this->_sql = '';
		$this->_finalSql = '';
		return $this;
	}

	/**
	 * @return array
	 */
	public function getValues() { return $this->_values; }

	/**
	 * @param $values
	 * @return Query
	 * @throws UXAppException -- if Query is sql-type
	 */
	public function setValues($values) {
		if($this->type=='sql') throw new UXAppException('Values cannot be set on SQL type Query', $this->_sql);
		$this->_values = $values;
		$this->_sql = '';
		$this->_finalSql = '';
		return $this;
	}

	/**
	 * @param array $combine -- [Query, string, bool]
	 * @return Query -- itself
	 * @throws UXAppException -- if Query is sql-type
	 */
	public function setCombination($combine) {
		if($this->type=='sql') throw new UXAppException('Combination cannot be set on SQL type Query', $this->_sql);
		$this->_combination = $combine;
		$this->_sql = '';
		$this->_finalSql = '';
		return $this;
	}

	/**
	 * @return array
	 */
	public function getCombination() { return $this->_combination; }

	/**
	 * @return array
	 */
	public function getParams() {
		return $this->_params;
	}

	/**
	 * @param $params
	 * @return Query
	 */
	public function setParams($params) {
		$this->_params = $params;
		$this->_finalSql = '';
		return $this;
	}

	// Query builder steps

	/**
	 * @param array $fields
	 *
	 * @return $this
	 */
	public function select($fields) {
		$this->fields = $fields;
		return $this;
	}

	/**
	 * @param array $fields
	 *
	 * @return $this
	 */
	public function andSelect($fields) {
		$this->fields = array_merge($this->fields, $fields);
		return $this;
	}

	/**
	 * @param $from
	 *
	 * @return $this
	 */
	public function from($from) {
		$this->from = $from;
		return $this;
	}

	/**
	 * Replaces joins with given list
	 * @param $joins
	 *
	 * @return $this
	 */
	public function joins($joins) {
		$this->joins = $joins;
		return $this;
	}

	/**
	 * Adds or replaces a join into the joins list
	 * @param string|array $join -- foreign key name or [model, jointype, on] with alias
	 * @param string $alias -- optional alias name. Overwrites former join with same alias name.
	 *
	 * @return $this
	 */
	public function join($join, $alias=null) {
		$joins = $this->joins;
		if($alias) $joins[$alias] = $join;
		else $joins[] = $join;
		$this->joins = $joins;
		return $this;
	}

	/**
	 * @param $condition
	 *
	 * @return $this
	 */
	public function where($condition) {
		$this->condition = $condition;
		return $this;
	}

	/**
	 * @param $condition
	 *
	 * @return $this
	 */
	public function andWhere($condition) {
		$conditions = $this->condition;
        $conditions = $conditions ? [$conditions, $condition] : $condition;
		$this->condition = $conditions;
		return $this;
	}

	/**
	 * Sets the limit part of the query.
	 * Returns itself for chaining
	 *
	 * @param int $limit
	 * @return $this
	 */
	public function limit($limit) {
		$this->limit = $limit;
		return $this;
	}

	/**
	 * Sets the offset part of the query.
	 * Returns itself for chaining
	 *
	 * @param int $offset
	 *
	 * @return $this
	 */
	public function offset($offset) {
		$this->offset = $offset;
		return $this;
	}

	/**
	 * Sets the order part of the query.
	 * Returns itself for chaining.
	 * null value deletes existing orders.
	 *
	 * @param array|string|null $orders -- array of order strings or order definition arrays.
	 *
	 * @return $this
	 */
	public function orderBy($orders=null) {
		$this->orders = $orders;
		return $this;
	}

	/**
	 * Sets the GROUP BY part of the query.
	 * Returns itself for chaining
	 *
	 * @param array $groupby -- array of order strings or order definition arrays.
	 *
	 * @return $this
	 */
	public function groupBy($groupby) {
		$this->groupby = $groupby;
		return $this;
	}

	/**
	 * Sets given connection or default
	 *
	 * Returns current connection.
	 *
	 * @param DBX|array|string $connection
	 *
	 * @return DBX
	 * @throws UXAppException|ReflectionException
	 */
	public function createConnection($connection=null) {
		if($connection instanceof DBX) return $this->connection = $connection;
		if($connection) return $this->connection = DBX::createConnection($connection);
		if(!($this->connection instanceof DBX)) $this->connection = DBX::createConnection($this->connection);
		return $this->connection;
	}

	/**
	 * Executes a non-select query
	 * Returns number of affected rows or false on failure.
	 *
	 * @param DBX $connection -- database connection. Default is defined for query or default of application
	 *
	 * @return integer|false
	 * @throws UXAppException|ReflectionException
	 */
	public function execute($connection=null) {
		$this->createConnection($connection);
		$this->_last = null;
		$this->_rs = $this->connection->query($this->sql, $this->params);
		return $this->rs ? $this->connection->affected_rows($this->rs) : false;
	}

	/** @noinspection PhpMethodNamingConventionInspection */

	/**
	 * Executes a select query.
	 * Returns all rows or false on failure.
	 * The result is an array of values arrays only, empty array if no rows.
	 * The difference between all() and selectAll() is that all returns associative rows while selectAll returns only values.
	 *
	 * @param DBX $connection -- database connection. Default is defined for query or default of application
	 *
	 * @return array
	 * @throws UXAppException|ReflectionException
	 */
	public function selectAll($connection=null) {
		$this->createConnection($connection);
		if(!($this->type=='select' || !$this->type)) throw new UXAppException('`select` can be performed on select queries only.');
		return $this->connection->select_rows($this->sql, $this->params);
	}

	/** @noinspection PhpMethodNamingConventionInspection */

	/**
	 * Executes a select query
	 * Returns all rows.
	 * Throws exception on SQL error.
	 * The result is an array of Models if modelclass is specified, associative arrays of field=>value pairs otherwise.
	 *
	 *
	 * @param string|bool $modelclass -- optional modelclass to convert result into. If true is specified, the modelname of the Query will be used.
	 * @param DBX $connection -- database connection. Default is defined for query or default of application
	 *
	 * @return array|Model[]
	 * @throws UXAppException|ReflectionException
	 */
	public function all($modelclass=false, $connection=null) {
		if(!($this->type=='select' || $this->type=='sql' || !$this->type)) throw new UXAppException('`all` can be performed on `select` or `sql` queries only.');
		if($modelclass===true && $this->modelname) $modelclass = $this->modelname;

		$db = $this->createConnection($connection);

		$result = array(); // select_all will return results here

		$n = $this->connection->select_all($result, $this->sql, $this->params);
		if($n===false) {
			throw new UXAppException('SQL hiba ', $this->connection->error());
		}

    	//if($result===false) return false;
    	if($modelclass) {
			if(!class_exists($modelclass)) throw new UXAppException("Not existing model '$modelclass'");
			if(!is_callable(array($modelclass, 'attributes'))) throw new UXAppException("Incompatible model class '$modelclass'");
		    $attributes = call_user_func(array($modelclass, 'attributes'), $this->connection);
    		try {
		        $result = @array_map(function($row) use($modelclass, $attributes, $db) {
		        	//Restrict row to attributes of the model
					$row1 = array();
					foreach($attributes as $a) {
						$row1[$a] = ArrayUtils::getValue($row, $a);
					}
					/* php v5.6 only version:
			        	$row = array_filter($row, function(
							$v, $k) use($attributes) { return in_array($k, $aa); }, ARRAY_FILTER_USE_BOTH);
					*/

		            /** @var Model $model */
		            $model = new $modelclass($row1, $db);
			        $model->setOldAttributes($row1);
		            return $model;
				}, $result);
			}
			catch(Throwable $e) {
				throw new UXAppException("Query is incompatible with model `$modelclass`: ".$e->getMessage(), null, $e);
			}
		}

		return $result;
	}

	/**
	 * Executes a select query
	 * Returns first rows or false on empty set or error.
	 * The result is an associative array in field=>value form or a Model specified by modelclass
	 *
	 * ModelClass may be any class having setOldAttributes() method and supports
	 *
	 * @param string|bool $modelclass -- optional modelclass to convert result into. If true is specified, the modelname of the Query will be used.
	 * @param DBX|array|string $connection -- database connection. Default is defined for query or default of application. May also be a config array or connection name.
	 *
	 * @return array|Model|false|null -- false (array mode) or null (model mode) if no result
	 * @throws UXAppException
	 * @throws ReflectionException
	 */
	public function first($modelclass=false, $connection=null) {
		if($modelclass===true && $this->modelname) $modelclass = $this->modelname;

		$db = $this->createConnection($connection);
		if(!($this->type=='select' || !$this->type)) throw new UXAppException('`first` can be performed on select queries only.');
		$this->limit(1);
		$data =  $this->connection->select_assoc($this->sql, $this->params);
		if($modelclass) {
			if(!class_exists($modelclass)) throw new UXAppException("Not existing model '$modelclass'");
			if(!is_callable(array($modelclass, 'setOldAttributes'))) throw new UXAppException("Incompatible model class '$modelclass'");
			if($data===false) return null;
			$attributes = call_user_func(array($modelclass, 'attributes'), $this->connection); // OK

			// Restrict row to attributes of the model
			$row1 = array();
			foreach($attributes as $a) {
				$row1[$a] = ArrayUtils::getValue($data, $a);
			}

			/** @var Model $model */
			$model = new $modelclass($row1, $db);
			$model->setOldAttributes($row1);
			return $model;
		}
		else {
			return $data;
		}
	}

	/**
	 * Returns one scalar result
	 * If number of result records is 0, the default value will be returned.
	 *
	 * @param mixed $default
	 * @param DBX $connection -- database connection. Default is defined for query or default of application
	 *
	 * @return mixed
	 * @throws UXAppException|ReflectionException
	 */
	public function scalar($default=null, $connection=null) {
		$this->createConnection($connection);
		if(!($this->type=='select' || !$this->type || $this->type=='sql')) throw new UXAppException('`scalar` can be performed on select queries only.');
		return $this->connection->selectvalue($this->sql, $this->params, $default);
	}

	/**
	 * Executes the select query, and returns the data in the first column in an array
	 *
	 * @param integer $c -- the number of the column, default 0
	 * @param DBX $connection -- database connection. Default is defined for query or default of application
	 *
	 * @return array
	 * @throws UXAppException|ReflectionException
	 */
	public function column($c=0, $connection=null) {
		$this->createConnection($connection);
		if(!($this->type=='select' || !$this->type)) throw new UXAppException('`column` can be performed on select queries only.');
		return $this->connection->select_column($this->sql, $this->params, $c);
	}

	/**
	 * Creates nodes from full select
	 * Result field values will be attributes, '_content' will be node text content.
	 *
	 * see also {@see Query::buildNodes()}
	 *
	 * @param UXMLElement|Module|UXAppPage $node_parent -- parent of new nodes
	 * @param string $nodename -- name of new nodes, default is tablename
	 * @param null $connection
	 *
	 * @return UXMLElement -- the first node created or null if none
	 * @throws UXAppException
	 * @throws Exception
	 */
	public function nodes($node_parent, $nodename=null, $connection=null) {
		if($node_parent instanceof Module) $node_parent = $node_parent->node;
		if($node_parent instanceof UXAppPage) $node_parent = $node_parent->view->node_content;
		$this->createConnection($connection);
		$node_first = null;
		if(!$nodename) $nodename = Util::uncamelize(Util::shortName($this->modelname));
		$result = $this->all();
		if(!$result) return null;
		foreach($result as $row) {
			$content = isset($row['_content']) ? $row['_content'] : null;
			unset($row['_content']);
			$node = $node_parent->addNode($nodename, $row, $content);
			if(!$node_first) $node_first = $node;
		}
		return $node_first;
	}

	/**
	 * Returns the error message associated to the last execution
	 *
	 * @return string
	 */
	public function error() {
		return $this->connection->error($this->rs);
	}

	/**
	 * Associates parameters to the Query
	 * (The SQL of the query is not modified, substitution occures only at execution)
	 * The substituted SQL may be extracted by finalSQL
	 *
	 * - array: parameters are added to existing ones (including ones with numeric indices)
	 * - single scalar value: added to the end of the list of numeric parameters
	 *
	 * @param array|mixed $values -- the parameter array, or associative array or may be single literal except of null
	 * @return Query -- the query itself
	 * @throws UXAppException
	 */
	public function bind($values) {
		$this->_finalSql = '';
		if(empty($values)) return $this;
		if(is_scalar($values)) {
			$this->_params[] = $values;
		}
		else if(is_array($values)) {
			foreach ($values as $key => $value) {
				$this->_params[$key] = $value;
			}
		}
		else throw new UXAppException('Invalid parameter value type '.gettype($values));
		return $this;
	}

	/**
	 * Creates final SQL from parametrized SQL and parameter array.
	 * Substitutes both $1 and $name style parameters
	 *
	 * @return string
	 * @throws UXAppException|ReflectionException
	 */
	public function getFinalSQL() {
		if(!$this->_finalSql) {
			$this->createConnection();
			$this->_finalSql = $this->connection->bindParams($this->sql, $this->params);
		}
		return $this->_finalSql;
	}

	/**
	 * Creates a Query object and builds a SELECT sql with single main table
	 *
	 * @param string|array|null $from -- main table model name or options array (Model name is classname, usually begins with capital)
	 * @param array $fields -- field names to select, or alias => fieldname
	 *        Default is all fields of the main table.
	 *        Dotted notation is accepted for joined relations
	 *        Undotted fields will use first model's alias if given
	 *        Fieldname may in alias => (expression) or alias => array(expression) form.
	 * @param array $joins -- list of joined models (foreign keys) OR alias=>fk OR alias=>array(model, [jointype], conditions) element
	 *        Join ON conditions may be
	 *            - an associative array with mainfield=>foreignfield form, or
	 *        - any other (non-associative) expression, use array($expression)
	 * @param array $condition -- Condition expression. Use literal values as numbered ($1) or named ($name) parameters.
	 *        For and-ed list of conditions use `array('AND', expression, expression)` or `array(fieldname=>expression,...)` form
	 *        For literal condition expression use array('(conditon)'); (see also {@see DBX::buildExpression()})
	 *        You may use a single scalar value for matching to single primary key.
	 * @param array $orders -- numeric indexed array of fields or [field, direction] arrays (priority order is important); order may be expression in second form
	 * @param integer $limit
	 * @param integer $offset
	 * @param array $params -- number-referenced values for $1 sql parameters or name=>value pairs for $name parameters (params may be single literal, except of null)
	 * @param DBX $connection
	 *
	 * Alternative call method: first parameter is an associative options array with the above options
	 *
	 * @return Query
	 * @throws UXAppException
	 * @noinspection PhpTooManyParametersInspection
	 */
	public static function createSelect($from=null, $fields=null, $joins=null, $condition=null, $orders=null, $limit=null, $offset=null, $params=null, $connection=null) {
		if(is_array($from) && func_num_args()==1) {
			$options = $from;
			$modelname = ArrayUtils::getValue($options, 'modelname');
			$from = ArrayUtils::getValue($options, 'from');
			if(is_array($from)) {
				$options['from'] = $from;
				$options['modelname'] = $from[ArrayUtils::getValue(array_keys($from), 0, 0)];
			}
			else if(is_string($from)) {
				$options['from'] = [$from];
				$options['modelname'] = $from;
			}
			else if(is_array($modelname)) {
				$options['from'] = $modelname;
				$options['modelname'] = $modelname[ArrayUtils::getValue(array_keys($modelname), 0, 0)];
			}
			else if(is_string($modelname)) {
				$options['from'] = [$modelname];
				$options['modelname'] = $modelname;
			}
			$query = new Query(array_merge(array('type'=>'select'), $options));
		}
		else {
			if($connection && ! $connection instanceof DBX) throw new UXAppException('Invalid connection parameter', gettype($connection));
			$query = new Query(array('type'=>'select',
				'modelname' => is_array($from) ? $from[ArrayUtils::getValue(array_keys($from), 0, 0)]??null : $from,
				'from' => (is_null($from)||is_array($from)) ? $from : array($from),
				'joins' => $joins,
				'fields' => $fields,
				'condition' => $condition,
				'orders' => $orders,
				'limit' => $limit,
				'offset' => $offset,
				'params' => $params,
				'db' => $connection,
			));
		}
		return $query;
	}

	/**
	 * Creates an 'sql' type query, based on a parametrized sql string.
	 *
	 * @param $sql
	 * @param array|null $params
	 * @param DBX|null $connection
	 *
	 * @return static
	 */
	public static function createSql($sql, $params=null, $connection=null) {
		return new static(['sql'=>$sql, 'params'=>$params, 'connection'=>$connection]);
	}

	/**
	 * Creates a Query object and builds an INSERT sql
	 *
	 * @param $modelname -- model name or options array for all other parameters
	 * @param array $fields --
	 *   - if null, model attributes are used (or values' keys if associative)
	 *   - if $values is null, $field must contain fieldname=>expression pairs. For literal values use associative $values
	 * @param array $values -- array of row value arrays
	 *   - if $fields includes values, must be NULL, otherwise NULL is not allowed.
	 *   - if $values is a string, will be used literally
	 *   - if values is an array, the literal values will be inserted.
	 *     - if values is an associtive array, it considered as fieldname=>literalvalues. $fields must be null. Use for single row only.
	 *   - if values is a Query, a subquery will be used.
	 *   expressions not allowed in $values.
	 * @param DBX $connection -- default is app's connection
	 *
	 * Alternative call method: first parameter is an associative options array with the above options
	 * Insert does not support parameters.
	 *
	 * Alternative build:
	 *
	 * createInsert(modelname)->into(fieldnames)->values(values-or-query)
	 *
	 * @return Query -- the query object
	 * @throws UXAppException
	 */
	public static function createInsert($modelname, $fields=null, $values=null, $connection=null) {
		if(is_array($modelname) &&  func_num_args()==1) {
			$options = $modelname;
			$query = new Query(array_merge(array('type'=>'insert'), $options));
		}
		else {
			if($connection && ! $connection instanceof DBX) throw new UXAppException('Invalid connection parameter', gettype($connection));
			$query = new Query(array('type'=>'insert',
				'modelname' => $modelname,
				'fields' => $fields,
				'values' => $values,
				'db' => $connection,
			));
		}
		return $query;
	}

	/**
	 * Creates a Query object and builds an UPDATE sql
	 *
	 * @param string|array $modelname -- modelname of the main table or options array
	 * @param array $values -- fieldname=>expression pairs (for explicit literal values, use $db->literal() on values)
	 * @param array $condition
	 * @param array $params
	 * @param DBX $connection
	 *
	 * Alternative call method: first parameter is an associative options array with the above options
	 *
	 * @return Query -- the query object
	 * @throws UXAppException
	 */
	public static function createUpdate($modelname, $values=null, $condition=null, $params=null, $connection=null) {
		if(is_array($modelname) &&  func_num_args()==1) {
			$options = $modelname;
			$query = new Query(array_merge(array('type'=>'update'), $options));
		}
		else {
			if($connection && ! $connection instanceof DBX) throw new UXAppException('Invalid connection parameter', gettype($connection));
			$query = new Query(array('type'=>'update',
				'modelname' => $modelname,
				'values' => $values,
				'condition' => $condition,
				'params' => $params,
				'db' => $connection,
			));
		}
		return $query;
	}

	/**
	 * Creates a Query object and builds a DELETE  sql
	 *
	 * @param string|array $modelname -- main table modelname or options array
	 * @param array $condition -- expression
	 * @param array $params
	 * @param DBX $db
	 *
	 * Alternative call method: first parameter is an associative options array with the above options
	 *
	 * @return Query -- the query object
	 * @throws UXAppException
	 */
	public static function createDelete($modelname, $condition=null, $params=null, $db=null) {
		if(is_array($modelname) &&  func_num_args()==1) {
			$options = $modelname;
		}
		else {
			$options = array(
				'modelname' => $modelname,
				'condition' => $condition,
				'params' => $params,
				'db' => $db,
			);
		}
		if(isset($options['db']) && ! $options['db'] instanceof DBX) throw new UXAppException('Invalid connection parameter', $options['db']);
		return new Query(array_merge(
			array('type'=>'delete'),
			$options)
		);
	}

	/**
	 * @return DBX
	 * @deprecated use connection field
	 */
	public function getDb() { return $this->connection; }
	public function setDb($connection) { $this->connection = $connection; return $this; }

	/**
	 * Returns Query data as array
	 *
	 * @return array
	 */
	public function toArray() {
		$fields = array(
			'sql' => array('sql'),
			'select' => array('fields', 'from', 'joins', 'condition', 'orders', 'limit', 'offset'),
			'insert' => array('modelname', 'fields', 'values'),
			'update' => array('modelname', 'values', 'condition'),
			'delete' => array('modelname', 'condition'),
		);
		$result = array(
			'type' => $this->type,
		);
		$fields = ArrayUtils::getValue($fields, $this->type, array(
			'modelname', 'fields', 'from', 'joins', 'condition', 'orders', 'limit', 'offset', 'values'
		));
		foreach ($fields as $fieldName) $result[$fieldName] = $this->$fieldName;
		return $result;
	}

	/**
	 * Returns Query data for debugging
	 *
	 * @return string
	 */
	public function toString() {
		return Util::objtostr($this->toArray());
	}

	/**
	 * Generates subnodes under parent node using the result of the select query
	 *
	 * replaces former UXMLElement::query()
	 *
	 * see also {@see Query::nodes()}
	 *
	 * @param UXMLElement|Module|UXAppPage $node_parent
	 * @param string $name -- name of new nodes
	 * @param bool $valuenodes -- generate subnodes for all field values
	 *
	 * @return UXMLElement|null
	 * @throws UXAppException
	 * @throws Exception
	 */
	public function buildNodes($node_parent, $name, $valuenodes=false) {
		if($node_parent instanceof Module) $node_parent = $node_parent->node;
		if($node_parent instanceof UXAppPage) $node_parent = $node_parent->view->node_content;
		if(!$this->connection) throw new UXAppException('Database not defined.');

		if(!$this->type) $this->type = $this->from ? 'select' : 'sql';
		$result = $this->all();
		$firstchild = null;
		foreach($result as $row) {
			/** @var UXMLElement $child */
			$child = $node_parent->ownerDocument->createElement($name);
			if(!$firstchild) $firstchild = $child;
			if($valuenodes) {
				foreach($row as $column=>$value) {
					$node_value = $child->addHypertextNode('value', $value);
					$node_value->setAttribute('name', $column);
				}
			}
			else $child->addAttributes($row);
			$node_parent->appendChild($child);
		}
		return $firstchild;
	}

	/**
	 * Iterates through query result
	 *
	 * Similar to getting all data with all() and iterate them, but without getting all data at once
	 *
	 * @param callable $action -- void function(Model|array $data, int $index)
	 * @param string|bool $modelclass -- use/override (specified) modelclass or not. true=use Query's class if exists
	 *
	 * @return int -- number of records processed
	 *
	 * @throws UXAppException
	 */
	public function each($action, $modelclass=true) {
		if(!$this->connection) throw new UXAppException('Database not defined.');
		$attributes = [];
		if($modelclass===true && $this->modelname) $modelclass = $this->modelname;
		if($modelclass) {
			if(!class_exists($modelclass)) throw new UXAppException("Not existing model '$modelclass'");
			if(!is_callable(array($modelclass, 'attributes'))) throw new UXAppException("Incompatible model class '$modelclass'");
			$attributes = call_user_func(array($modelclass, 'attributes'), $this->connection);
		}
		$db = $this->connection;
		@$rs = $db->query($this->sql, $this->params);
		if($rs===false) {
			throw new UXAppException('SQL hiba ', $db->error());
		}
		$n = $db->num_rows($rs);
		for($i=0; $i<$n; $i++) {
			$row = $db->fetch_assoc_type($rs);
			if($modelclass) {
				//Restrict row to attributes of the model
				$row1 = array();
				foreach($attributes as $a) {
					$row1[$a] = ArrayUtils::getValue($row, $a);
				}
				/** @var Model $model */
				$model = new $modelclass($row1, $db);
				$model->setOldAttributes($row1);
				$row = $model;
			}
			call_user_func($action, $row, $i);
		}
		return $n;
	}

    /**
     * Iterates through query result and returns an array map of records.
     * Similar to {@see ArrayUtils::map()} function.
     *
     * @param string|Closure|null $indexProperty -- index property or function(Model|array $data, int $index): scalar
     * @param string|Closure|null $valueProperty -- value property or function(Model|array $data, int $index): mixed
     * @param Model|string|bool $modelclass -- use/override (specified) modelclass or not. true=use Query's class if exists
     * @param bool $skipNull -- exclude null values from result
     * @return array
     * @throws UXAppException
     */
    public function map($indexProperty, $valueProperty, $modelclass=true, $skipNull=false) {
        $result = [];
        $this->each(function($model) use(&$result, $indexProperty, $valueProperty, $skipNull) {
            /** @var Model|array $model */
            $value = ArrayUtils::getValue($model, $valueProperty);
            $index = ArrayUtils::getValue($model, $indexProperty);
            if((!$skipNull || $value!==null) && $index!==null) $result[$index] = $value;
        }, $modelclass);
        return $result;
    }

	/**
	 * Returns number of records in the query
	 * (Replaces fields to count(fields) and returns {@see scalar()} result)
	 *
	 * @param array|string $fields
	 *
	 * @return int|null
	 * @throws ReflectionException
	 * @throws UXAppException
	 */
	public function count($fields='*') {
		$query1 = clone $this;
		if(!is_array($fields)) $fields = [$fields];
		return $query1->select([array_merge(['count()'], $fields)])->orderBy()->scalar();
	}

	/**
	 * Returns actual names of SQL fields (based on selecting the first record)
	 * @throws ReflectionException
	 * @throws UXAppException
	 */
	public function fields($connection=null) {
		if($this->type=='sql') {
			$db = $this->createConnection($connection);
			$r = $db->query('select * from('.$this->finalSQL.') fields_subquery limit 1');
			return $db->fieldnames($r);
		}
		else {
			$query1 = clone $this;
			$row = $query1->select(['*'])->orderBy()->first();
			return array_keys($row);
		}
	}

	public function distinct($value=true) {
		$this->distinct = $value;
		return $this;
	}
	/**
	 * Defines an UNION select
	 * Parameters of combined query will merge into main parameters.
	 *
	 * @param Query $query
	 * @param bool $all
	 * @return $this
	 * @throws UXAppException
	 */
	public function union($query, $all=false) {
		if($this->type !== 'select') throw new UXAppException('Base query must be a SELECT.');
		if($query->type !== 'select') throw new UXAppException('Union query must be a SELECT.');
		$this->combination = [$query, 'UNION', $all];
		return $this;
	}
}
