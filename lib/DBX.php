<?php /** @noinspection PhpUnused */

namespace uhi67\uxapp;

/** @noinspection PhpClassNamingConventionInspection */

use DateTime;
use Exception;
use Throwable;
use ReflectionException;

/**
 * DBX - abstract database interface
 *
 * Extend with vendor-specific database drivers.
 * Implement abstract classes.
 *
 * @author uhi
 * @copyright uhi 2012-2018
 */
abstract class DBX extends Component {
    const ORDER_ASC = 0;
    const ORDER_DESC = 1;
    const NULLS_FIRST = 0;
    const NULLS_LAST = 1;

    public $type;
    public $host;
    public $name;
    public $user;
    public $password;
    public $port;
    /** @var resource|mysqli $connection -- a native vendor-specific connection resource or object */
    public $connection;
    /** @var string $encoding -- the actual encoding of remote database (will be converted to UTF-8) */
    public $encoding = 'UTF-8';
    /** @var string|false $connerr -- the last connection error or false if no error */
    public $connerr = false;
    public $connectNow = false;
    /** @var array $params -- User-defined parameters */
    public $params = [];
    public $migrationPath;

    /** @var array $_metadata -- cached table-metadata indexed by tablename */
    protected static $_metadata = array();
    /**
     * @var array $_operators
     * Definitions of operators, in precedence order
     * array(operator name => operands,...)
     * Special values:
     *  - 3: $1 op $2 AND $3
     *  - 4 means no limit, implode with pattern
     *  - 5: operator is after single operand
     *  - 6: 2 operands, second is an expression-list
     *  - 7: separate builder (may be vendor-specific)
     */
    protected static $_operators = array(
        'null'	=>	0,
        'true'	=>	0,
        'false'	=>	0,
        '::'    =>  2,  // cast. TODO: vendor-specific
        '~'		=>	1,	// bitwise not
        '^'		=>	2,
        '*'		=>	4,
        '/'		=>	2,
        '%'		=>	2,
        '+'		=>	4,
        '-'		=>	2,
        '<<'	=>	2,
        '>>'	=>	2,
        '&'		=>	4,
        '|'		=>	4,
        '='		=>	2,
        '!='	=>	2,
        '<'		=>	2,
        '>'		=>	2,
        '>='	=>	2,
        '<='	=>	2,
        'is null'=>	5,
        'is not null'=>5,
        'is true'=>	5,
        'like'	=>	2,
        'ilike'	=>	2,
        'rlike'	=>	2, // ~* in pg
        'regex'	=>	2, // ~ in pg
        'not rlike'	=>	2, // !~* in pg
        'not regex'	=>	2, // !~ in pg
        'in'	=>	6,
        'not in'=>	6,
        'exists'=>  1,
        'between'=>	3,
        'not'	=>	1,
        'and'	=>	4,
        'or'	=>	4,
        '||'	=>	4,      // TODO: vendor- and version dependent. May need to be converted to concat()
        'interval' => 1,
        'current_timestamp' => 0,
        'case' => 7,
    );

    public function prepare() {
        if(!$this->type) $this->type = get_class($this);
    }

    /**
     * Returns orders expresion array with inverted directions
     *
     * * Orders expression is array(order,...), where order is
     * - fieldname or
     * - array(fieldname, direction [, nulls])
     *
     * Use DBX::ORDER_ASC, DBX::ORDER_DESC constants for directions,
     * DBX::NULLS_FIRST, DBX::NULLS_LAST constants for null orders.
     *
     * Direction and NULL order will be inverted.
     *
     * @param array $ord
     * @return array - the inverted orders expression list
     */
    public static function desc($ord) {
        foreach ($ord as &$o) {
            if(!is_array($o)) $o = array($o, DBX::ORDER_DESC, DBX::NULLS_LAST);
            else {
                $d = ArrayUtils::getValue($o, 1, DBX::ORDER_ASC);
                $n = ArrayUtils::getValue($o, 2, DBX::NULLS_FIRST);
                $o = array($o[0], 1 - $d, 1 - $n);
            }
        }
        return $ord;
    }

    /**
     * Terminates this database connection
     */
    abstract function close();

    /**
     * Must create a connection to the database server
     *
     * @return null|resource
     * @throws
     */
    abstract protected function connect();

    /**
     * Returns number of fields in the resultset
     *
     * @param resource $resultset
     * @return integer
     */
    abstract function fields($resultset);

    /**
     * Returns a fieldname in the resultset
     *
     * @param resource $resultset
     * @param integer $i
     * @return string
     */
    abstract function fieldname($resultset, $i);

    /**
     * Must return the common type of a field in the resultset
     *
     * @param resource $resultset
     * @param integer $i
     * @return string
     */
    abstract function fieldtype($resultset, $i);

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Must return table metadata as in {@see DBX::getMetaData()}
     * (raw, uncached)
     * For cached version use {@see getMetaData()}
     *
     * @param string $table
     * @return array
     * @see DBX::getMetaData()
     */
    abstract public function table_metadata($table);

    /**
     * Must return php type for a connection-specific SQL type
     *
     * @param string $typename -- SQL type
     * @return string - php type or classname
     */
    abstract function mapType($typename);

    /**
     * @param string $value -- value returned from SQL query
     * @param string $type -- SQL typename
     *
     * @return mixed -- data in mapped php type
     */
    abstract function mapData($value, $type);

    /**
     * SQL query futtatása $1, $2... paraméterek behelyettesítésével
     *
     * @param string $sql
     * @param array $params -- 0-ás indexen az $1-es paraméter
     *
     * @return resource|false -- A query result resource on success or false on failure
     */
    abstract function query($sql, $params=null);

    /**
     * Frees the result set returned by query.
     * (Mandatory for MySQL)
     *
     * @param $resultSet
     *
     * @return mixed
     */
    abstract function free($resultSet);

    /**
     * SQL parancsok sorának futtatása $1, $2... paraméterek behelyettesítésével
     *
     * @param string $sql -- SQL parancsok ;-vel elválasztva
     * @param array $params -- 0-ás indexen az $1-es paraméter
     *
     * @return bool -- success of the first statement
     */
    abstract function execute($sql, $params=null);

    /**
     * @param resource $resultset
     * @param int $row
     * @param int $field
     *
     * @return array|bool|string
     */
    abstract function result($resultset, $row, $field=0);

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns last inserted autoincrement id
     * Only single id is supported
     *
     * @param string $table -- not used
     * @param string $fieldname -- not used
     * @return integer|null -- null if not defined yet
     */
    abstract function last_id($table='', $fieldname='');

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @param resource $resultset
     * @return int
     * @throws UXAppException
     */
    abstract function num_rows($resultset);

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns a row from result set
     *
     * @param resource $resultset
     * @param int $row -- 0-based index of row or next row if omitted or null
     * @return array|false -- numeric indexed string values or false on error
     *
     * An array, indexed from 0 upwards, with each value represented as a string. Database NULL values are returned as NULL.
     * FALSE is returned if row exceeds the number of rows in the set, there are no more rows, or on any other error.
     */
    abstract function fetch_row($resultset, $row=null);

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns a record to associative array
     * All field values will be strings
     *
     * @param resource $resultset
     * @param int $row
     * @return array fieldname=>stringvalue
     */
    abstract function fetch_assoc($resultset, $row=null);

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns a record to associative array
     * Fields will be mapped types
     *
     * @param resource|mysqli_result $resultset
     * @param int|null $row
     *
     * @return array
     */
    abstract function fetch_assoc_type($resultset, $row=null);

    /**
     * Must return the identifier with vendor-specific quoting
     * E.g. 'where' => '`where`'
     * This is for a single identifier only. For quoting a composite nam, like 'c.name', use quoteName
     * (If name is already quoted, it will quote again...)
     *
     * @param string $name
     * @return string
     * @see quoteName

     */
    abstract public function quoteIdentifier($name);

    /**
     * Must determine if SQL server supports NULLS LAST option in ORDER BY clause
     * @return boolean
     */
    abstract public function supportsOrderNullsLast();

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Must provide vendor specific escaping of a string literal.
     * Returns string in single quotes.
     *
     * @param string $literal
     * @return string
     */
    abstract public function escape_literal($literal);

    /**
     * Returns local name of a 'standard' operator
     *
     * @param string $op
     * @return string
     */
    abstract public function operatorName($op);

    /**
     * Returns true is table exists in database
     *
     * @param string $name
     * @return bool
     */
    abstract public function tableExists($name);

    /**
     * Returns error message associated to a resultset, or the last error
     *
     * @param resource|mysqli_result $rs
     *
     * @return string
     */
    abstract function error($rs=null);

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns number of affected rows in the last executed resultset
     *
     * @param resource|mysqli_result $rs
     *
     * @return int
     * @throws UXAppException
     */
    abstract function affected_rows($rs);

    /**
     * Begins a transaction which can be rolled back by {@see rollBack()} or commited by {@see commit()}.
     * @return void
     */
    abstract function beginTransaction();

    /**
     * Commits a transaction which beginned by {@see beginTransaction()}.
     * @return void
     */
    abstract function commit();

    /**
     * Rolls back a transaction which beginned by {@see beginTransaction()}.
     * @return void
     */
    abstract function rollBack();

    /**
     * @return resource|false
     * @throws UXAppException on failure
     */
    protected function connectNow() {
        try {
            $conn = $this->connect();
        }
        catch(Throwable $e) {
            if(UXApp::$app->hasComponent('debug')) UXApp::$app->debug->trace('Database connect failed! '.$e->getMessage());
            throw new UXAppException(UXApp::la('uxapp', 'Database connect failed'), null, $e);
        }
        if(!isset($conn) || !$conn) throw new UXAppException(UXApp::la('uxapp', 'Database connect failed'));
        return $conn;
    }

    /**
     * @return resource|false
     * @throws UXAppException on failure
     */
    function autoconnect() {
        if(!$this->connection) $this->connection = $this->connectNow();
        return $this->connection;
    }

    /**
     * @param $resultset
     * @return bool
     * @throws UXAppException
     */
    function resultisempty($resultset) {
        return $this->num_rows($resultset) == 0;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * sets unset or empty values to default (null)
     *
     * @param mixed $x
     * @param mixed $default
     * @return mixed
     */
    public function nvl($x, $default=null) {
        return (!isset($x) || ($x=='') || is_null($x)) ? $default : $x;
    }

    /**
     * Returns a single scalar form the database by the SQL query
     *
     * Ignores extra columns and rows.
     *
     * @param string $sql -- select
     * @param array $params -- $1, $2 style parameters of SQL
     * @param bool $default -- default value if no record found
     *
     * @return array|bool|string|null
     * @throws
     */
    public function selectvalue($sql, $params=array(), $default=null) {
        $sql = $this->bindParams($sql, $params);
        try {
            $r = $this->query($sql);
            if($this->resultisempty($r)) return $default;
        }
        catch(Throwable $e) {
            UXApp::trace(['SQL'=>$sql, 'Exception'=>$e->getMessage()]);
            throw new UXAppException($e->getMessage(), $sql, $e);
        }
        $result = $this->result($r, 0);
        $this->free($r);
        return $result;
    }

    /**
     * Returns a row of data based on SQL query.
     * Numeric indexed values
     * @param string $sql
     * @param array $params
     * @return array|null -- null on empty
     * @throws
     */
    public function selectvalues($sql, $params=array()) {
        $sql = $this->bindParams($sql, $params);
        $r = $this->query($sql);
        if($this->resultisempty($r)) return null;
        $row = $this->fetch_row($r, 0);
        $this->free($r);
        return $row;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns all data in two-dim numeric indexed array
     *
     * @param string $sql
     * @param array $params
     * @return array|false
     * @throws UXAppException
     */
    function select_rows($sql, $params=array()) {
        $r = $this->query($sql, $params);
        if(!$r) return false;
        $n = $this->num_rows($r);
        $aa = array();
        for($i=0;$i<$n;$i++) $aa[] = $this->fetch_row($r, $i);
        $this->free($r);
        return $aa;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns result of the first record in associative array.
     * Values are converted to corresponting php types
     *
     * @param string $sql
     * @param array $params
     * @return array|false -- false if no record found
     * @throws UXAppException
     */
    function select_assoc($sql, $params=NULL) {
        $r = $this->query($sql, $params);
        if($this->resultisempty($r)) return FALSE;
        $result = $this->fetch_assoc_type($r, 0);
        $this->free($r);
        return $result;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns all values of all rows in associative arrays
     * Values are converted to corresponting php types
     *
     * @param array $aa - the result array
     * @param string $sql
     * @param array $params
     * @return integer|false - number of rows, or false on error
     * @throws
     */
    function select_all(&$aa, $sql, $params=NULL) {
        Assertions::assertString($sql);
        $r = $this->query($sql, $params);
        if(!$r) return false;
        $n = $this->num_rows($r);
        $aa = array();
        for($i=0;$i<$n;$i++) $aa[] = $this->fetch_assoc_type($r, $i);
        $this->free($r);
        return $n;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns an array with values for first column of all rows
     *
     * @param string $sql
     * @param array $params
     * @param int $c
     * @return array -- the result
     * @throws UXAppException
     */
    function select_column($sql, $params=NULL, $c=0) {
        $r = $this->query($sql, $params);
        $n = $this->num_rows($r);
        $aa = array();
        for($i=0;$i<$n;$i++) {
            $aa[] = $this->result($r, $i, $c);
        }
        $this->free($r);
        return $aa;
    }

    /**
     * @param $sql
     * @return string
     */
    function stripcomments($sql) {
        $sql = preg_replace("/--.*$/", '', $sql);
        return preg_replace("/\*[^*]*\*/", '', $sql);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @param $sql
     * @return mixed
     * @throws
     */
    function num_rows_sql($sql) {
        $r = $this->query($sql);
        $count = $this->num_rows($r);
        $this->free($r);
        return $count;
    }

    /**
     * @param $sql
     * @param null $params
     * @throws
     */
    function errtrace($sql, $params=null) {
        if($sql && $params) $sql = $this->bindParams($sql, $params);
        $info = $sql;
        if($sql=='') $info = 'SQL string is empty.';
        //if(!UXApp::debugIsOn()) $info=false;
        throw new UXAppException($this->error(), $info);
    }

    /**
     * Substitutes $1, $2,... and $var params from $params array
     *
     * @param string $sql
     * @param array $params
     *
     * @return string the substituted sql statement
     * @throws
     */
    function bindParams($sql, $params) {
        if(empty($params)) return $sql;
        $params = Util::forceArray($params);
        foreach($params as $i=>$value) {
            $vx = $this->literal($value, true);
            $var = is_int($i) ? ($i+1) : $i;
            $re = '~\$' . $var . '(?!\d)~';
            $sql = preg_replace($re, $vx, $sql);
        }
        return $sql;
    }

    /**
     * Substitutes $1, $2,... params from $params array
     * Ignores not numeric indices
     *
     * @see bindParams()
     *
     * @param string $sql
     * @param array $params (or a single scalar)
     * @return string the substituted sql statement
     * @throws UXAppException
     *
     * @deprecated use bindParams()
     */
    static function params($sql, $params) {
        $params = Util::forceArray($params);
        foreach($params as $i=>$value) {
            if(is_int($i)) {
                /** @noinspection PhpDeprecationInspection */
                $vx = self::paramValue($value);
                $re = /** @lang text */'~\$'.($i+1).'(?!\d)~';
                $sql = preg_replace($re, $vx, $sql);
            }
        }
        return $sql;
    }

    /**
     * Substitutes $var1, $var2,... params from $params assoc array
     * Watch out for variables with similar beginning: $v / $var
     * Use {$v} formula for clarity
     * Unknown variable references will remain untouched
     * Numeric value may contain 'e' only if contains '.' also
     * Ignores numeric indices.
     * @see bindParams()
     *
     * @param string $sql
     * @param array $params
     * @return string the substituted sql statement
     * @throws UXAppException
     *
     * @deprecated use bindParams()
     */
    static function paramsx($sql, $params) {
        foreach($params as $k=>$value) {
            if(is_int($k)) continue;
            if(!isset($value) or is_null($value)) $vx = 'NULL';
            elseif ($value===false) $vx='false';
            elseif ($value===true) $vx='true';
            elseif((is_integer($value) || is_float($value)) && !(strpos($value, 'e') && !strpos($value, '.'))) $vx = $value;
            else {
                /** @noinspection PhpDeprecationInspection */
                $vx = self::quote($value);
            }
            $sql = str_replace('{$'.($k).'}', $vx, $sql);
            $sql = preg_replace(/** @lang text */'/\\$'.$k.'\b/', $vx, $sql);
        }
        return $sql;
    }

    /**
     * Converts array value into PSQL array form "ARRAY[...]"
     *
     * @param array $value
     * @return string
     * @throws UXAppException
     *
     * @deprecated use literal()
     */
    static function arrayValue($value) {
        if(count($value)==0) return '{}';
        $r = 'ARRAY[';
        $f = false;
        foreach($value as $v) {
            if($f) $r .= ','; $f = true;
            /** @noinspection PhpDeprecationInspection */
            $r .= self::paramValue($v);
        }
        $r .= ']';
        return $r;
    }

    /**
     * Converts PSQ array string {} into array
     *
     * @copyright http://stackoverflow.com/users/2828391/dmikam
     * @param string $s
     * @param integer $start -- index of first character to parse
     * @param integer $end -- index of next character following of array to parse
     * @return array
     */
    public static function parseArray($s,$start=0,&$end=NULL){
        if (empty($s) || $s[0]!='{') return NULL;
        $return = array();
        $string = false;
        $quote='';
        $len = strlen($s);
        $v = '';
        for($i=$start+1; $i<$len;$i++){
            $ch = $s[$i];

            if (!$string && $ch=='}'){
                if ($v!=='' || !empty($return)){
                    $return[] = $v;
                }
                $end = $i;
                break;
            }else
            if (!$string && $ch=='{'){
                $v = self::parseArray($s,$i,$i);
            }else
            if (!$string && $ch==','){
                $return[] = $v;
                $v = '';
            }else
            if (!$string && ($ch=='"' || $ch=="'")){
                $string = TRUE;
                $quote = $ch;
            }else
            if ($string && $ch==$quote && $s[$i-1]=="\\"){
                $v = substr($v,0,-1).$ch;
            }else
            if ($string && $ch==$quote && $s[$i-1]!="\\"){
                $string = FALSE;
            }else{
                $v .= $ch;
            }
        }
        return $return;
    }

    /**
     * Converts PSQL string result to boolean
     * null remains null,
     * t or true or integer!=0 will be true, all other false
     *
     * @param string $s
     * @return boolean
     */
    public static function parseBoolean($s) {
        if(is_null($s)) return null;
        return in_array(strtolower($s), array('t', 'true')) || (int)$s!=0;
    }

    /**
     * Converts value to literal form for SQL string.
     * Recognizes the following data types:
     * - null
     * - boolean
     * - integer, float
     * - string
     * - array
     * - DateTime
     *
     * Warning: numbers passed in strings considered to be strings.
     *
     * The default type is string.
     *
     * @param mixed $value
     * @return string
     * @throws UXAppException
     *
     * @deprecated use {@see literal()}
     */
    static function paramValue($value) {
        if(!isset($value) or is_null($value)) $vx = 'NULL';
        elseif ($value===false) $vx='false';
        elseif ($value===true) $vx='true';
        elseif(is_array($value)) {
            /** @noinspection PhpDeprecationInspection */
            $vx = self::arrayValue($value);
        }
        elseif($value instanceof DateTime) {
            /** @noinspection PhpDeprecationInspection */
            $vx = self::quote($value->format(DateTime::ATOM));
        }
        elseif($value instanceof Macaddr) {
            /** @noinspection PhpDeprecationInspection */
            $vx = self::quote($value->toString());
        }
        elseif($value instanceof Inet) {
            /** @noinspection PhpDeprecationInspection */
            $vx = self::quote($value->toString());
        }
        elseif(is_object($value)) throw new UXAppException('Invalid object value parameter '.get_class($value));
        elseif(substr($value, 0, 1)=='+') {
            /** @noinspection PhpDeprecationInspection */
            $vx = self::quote($value);
        }
        elseif(is_int($value) || is_float($value)) $vx = $value;
        else {
            /** @noinspection PhpDeprecationInspection */
            $vx = self::quote($value);
        }
        return $vx;
    }

    /**
     * decodes database encoding to utf-8
     * decodes database encoding to utf-8
     *
     * works also on flat arrays of strings
     * @param string|array $s the database-encoded string or array
     * @return string|array the utf-8 string or array
     */
    function decode($s) {
        if($this->encoding == 'UTF-8') return $s;
        if(!is_string($s)) return $s;
        if(is_array($s)) {
            foreach($s as $k=>$v) $s[$k] = iconv($this->encoding, 'UTF-8', $v);
            return $s;
        }
        else return iconv($this->encoding, 'UTF-8', $s);
    }

    /**
     * encodes utf-8 string to database encoding
     *
     * works also on flat arrays of strings
     * @param string|array $s the utf-8 string or array
     * @return string|array the database-encoded string or array
     */
    function encode($s) {
        if($this->encoding == 'UTF-8') return $s;
        if(is_array($s)) {
            foreach($s as $k=>$v) $s[$k] = iconv('UTF-8', $this->encoding, $v);
            return $s;
        }
        else return iconv('UTF-8', $this->encoding, $s);
    }

    /**
     * Executes multiple queries with same param list
     *
     * @param array $sqls
     * @param array $params
     * @return bool -- success
     */
    function queries($sqls, $params) {
        $rs = null;
        foreach($sqls as $sql) {
            if(!($rs = $this->query($sql, $params))) return false;
            $this->free($rs);
        }
        return true;
    }


    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * converts 0 to null
     *
     * @param mixed $value
     * @return mixed|null null if value was 0, the value otherwise
     */
    static function nx($value) {
        if($value==0) return null;
        return $value;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * converts empty string to null
     *
     * @param string $value
     * @return string|null null if value was empty string, the value otherwise
     */
    static function ns($value) {
        if($value=='') return null;
        return $value;
    }

    /**
     * ## Quotes a string or DateTime for sql command
     *
     * Appends single quotes around the string, and replaces inner single quotes to ''
     * Returns 'NULL' on NULL input.
     *
     * @param string $value
     * @param boolean $nulltostring -- returns NULL as string if true, null otherwise
     *
     * @return string the value in '-s or 'NULL'
     * @throws UXAppException
     * @deprecated -- use literal()
     */
    public static function quote($value, $nulltostring=false) {
        if(is_null($value) && !$nulltostring) return null;
        if(is_array($value)) throw new UXAppException('array is illegal type here');
        if($value instanceof DateTime) $value = $value->format(DateTime::ATOM);
        if($value===NULL) return 'NULL';
        return "'".str_replace("'", "''", str_replace("\\", "\\\\", str_replace("''", "'", $value)))."'";
    }

    /**
     * Returns the fieldnames in a result
     * @param resource $resultset
     * @return array
     */
    function fieldnames($resultset) {
        $fieldnames = array();
        $n = $this->fields($resultset);
        for($i=0;$i<$n;$i++) $fieldnames[$i] = $this->fieldname($resultset, $i);
        return $fieldnames;
    }


    /**
     * Returns table metadata as
     * ```php
     * array(
     *     fieldname => array(
     *        'num' => <Field number starting at 1>
     *        'type' => <data type, eg varchar, int4>
     *        'len' => <internal storage size of field. -1 or null for varying>
     *        'not null' => <boolean>
     *        'has default' => <boolean>
     *     ),
     *     ...
     * )
     * ```
     * Optionally uses a cache.
     *
     * @param string $tablename
     * @param bool $cache -- use cache (default is true)
     * @return array|null if not found
     */
    public function getMetaData($tablename, $cache=true) {
        if(!$cache) return $this->table_metadata($tablename);
        if(isset(self::$_metadata[$tablename])) return self::$_metadata[$tablename];
        $key = 'db_metadata_'.$tablename;
        $getter = function() use($tablename) { return $this->table_metadata($tablename);};
        $metadata = UXApp::cache($key, $getter);
        self::$_metadata[$tablename] = $metadata = $this->table_metadata($tablename);
        return $metadata;
    }

    /**
     * Returns foreign keys information of the table as
     *
     * 	[
     * 		'constraint_name' => [
     * 			'column_name' => 'columnName',
     * 			'foreign_schema' => 'schemaName',
     * 			'foreign_table' => 'tableName'
     * 			'foreign_column' => 'columnName'
     * 		],
     * 		...
     *  ]
     * @param string $tablename -- may contain schema prefix
     * @param string|null $schema -- optional (table prefix overrides; default is current schema)
     *
     * @return array|bool
     */
    abstract public function getForeignKeys($tablename, $schema=null);
    public function foreign_keys($tablename, $schema=null) {
        return $this->getForeignKeys($tablename, $schema);
    }

    /**
     * Returns remote foreign keys referred to this table (reverse foreign key)
     *
     * 	[
     * 		'constraint_name' => [
     * 			'remote_schema' => 'schemaName',
     * 			'remote_table' => 'tableName'
     * 			'remote_column' => 'columnName'
     * 			'table_schema' => 'columnName',
     * 			'table_name' => 'columnName',
     * 			'column_name' => 'columnName',
     * 		],
     * 		...
     *  ]
     * @param string $tablename -- may contain schema prefix
     * @param string|null $schema -- optional (table prefix overrides; default is current schema)
     *
     * @return array|bool
     */
    abstract public function getReferrerKeys($tablename, $schema=null);
    public function referrers($tablename, $schema=null) {
        return $this->getReferrerKeys($tablename, $schema);
    }

    /**
     * DEPRECATED: use Query class
     *
     * Creates where section for sql queries
     * Attributes contains variable=>value or single string clauses or array.
     * The first form means variable=value condition.
     * Clauses are and-ed.
     * Array clause contains or-ed subclauses, which may contain and-ed subclauses recursively.
     * Empty array subclauses are ignored.
     * Empty array results empty output.
     * Warning: same variable must be referenced only once in one level of attributes array.
     *
     * @param array $attributes
     * @return string -- containing 'where' clause if not empty.
     * @see createCondition
     * @throws UXAppException
     *
     * @deprecated use Query
     */
    static function createWhere($attributes) {
        if(!$attributes) return '';
        /** @noinspection PhpDeprecationInspection */
        return 'where '.self::createCondition($attributes, 'and');
    }

    /**
     * DEPRECATED: use Query class
     *
     * Constructs a condition part for where from attribute-value pairs
     * array values will be treated as subcondition with opposite and/or operator
     * non-associative strings treated as literals
     *
     * Example
     *
     * array('id'=>1, 'not deleted') results "id=1 and (not deleted)"
     * array(array('id is null', array('parent is not null', 'y>3')))) results "where (id is null or (parent is not null and y>3))"
     *
     * @param array|string $attributes
     * @param string $op
     * @return string
     * @throws UXAppException
     *
     * @deprecated use Query class
     */
    static function createCondition($attributes, $op) {
        if(!$attributes) return '';
        if(!is_array($attributes)) return $attributes;
        $result = ''; $params=array();
        $i=1;
        foreach($attributes as $var=>$value) {
            if(is_array($value)) {
                /** @noinspection PhpDeprecationInspection */
                $r = self::createCondition($value, $op=='or' ? 'and' : 'or');
                if($r) $r = '('.$r.')';
            }
            else if(is_numeric($var)) $r = $value;
            else { $r = $var . "=$$i"; $params[] = $value; $i++; }
            if($r!='') {
                if($result!='') $result .= ' '.$op.' ';
                $result .= $r;
            }
        }
        /** @noinspection PhpDeprecationInspection */
        $result = self::params($result, $params);
        return $result;
    }

    /**
     * DEPRECATED: use Query class
     *
     * Creates an update query from associative array
     * Each attribute results an field=value in set section
     * Base contains %s for set section, and optionally another for where section
     *
     * @param string $base
     * @param array $params -- substitutes $1,$2... in $base
     * @param array $attributes -- for set clauses
     * @param array $where -- associative array for anded clauses
     * @return string -- null on empty attributes
     * @throws UXAppException
     *
     * @deprecated use Query class
     */
    static function createSqlUpdate($base, $params, $attributes, $where=null) {
        if(!$attributes) return null;
        $i=count($params)+1;
        $sql = 'set ';
        $comma = '';
        foreach($attributes as $key=>$value) {
            $sql .= $comma.$key.'=$'.$i++;
            $comma = ', ';
            $params[] = $value;
        }
        /** @noinspection PhpDeprecationInspection */
        $wherex = DBX::createWhere($where);
        /** @noinspection PhpDeprecationInspection */
        $sql = DBX::params(sprintf($base, $sql, $wherex), $params);
        return $sql;
    }

    /**
     * Creates a quoted literal for sql command
     *
     * Appends single quotes around the string, and replaces inner single quotes to ''
     * Returns 'NULL' on NULL input if tostring is true.
     *
     * If tostring is false and value is an array, converts array elements to literal individually, recursively.
     * Ignores, but preserves array keys.
     *
     * If tostring is true, array will be converted to srtring
     *
     * You may override it with vendor-specific version for handling special formats.
     *
     * @param string|array $value
     * @param bool $tostring -- convert null, integer and boolean to string
     * @return string|array the value in '-s or 'NULL'
     * @throws
     */
    public function literal($value, $tostring=false) {
        if(is_null($value)) return $tostring ? 'NULL' : null;
        if(is_bool($value)) return $tostring ? ($value ? 'true' : 'false') : $value;
        if(is_integer($value) || is_float($value)) return $tostring ? (string)$value : $value;

        $db = $this;
        if(is_array($value) && !$tostring) return array_map(function($a) use($db) { return $db->literal($a); }, $value);
        if(is_array($value)) return $this->literal(Util::objtostr($value));

        // Object types always to quoted string
        else if($value instanceof DateTime) $value = $value->format(DateTime::ATOM);
        else if($value instanceof Inet) $value = $value->toString();
        else if($value instanceof Macaddr) $value = $value->toString();
        else if(is_object($value)) throw new UXAppException('Illegal object for literal ('.get_class($value).')');

        return $this->escape_literal($value);
    }

    /**
     * Returns the table or fieldname with vendor-specific quoting
     * E.g. 'where' => '`where`'
     * Maintains tablespaces and aliases:
     * E.g. 'public.where' => '`public`.`where`'
     * Does not quote identifiers already quoted.
     * Throws error if identifier contains quote character inside.
     *
     * Names must not contain '.'
     *
     * @param string $name
     *
     * @return string
     * @throws UXAppException
     */
    public function quoteName($name) {
        if(strpos($name, '.')!==false) {
            $db = $this;
            return implode('.', array_map(function($n) use($db) {return $db->quoteSingleName($n);}, explode('.', $name)));
        }
        return $this->quoteSingleName($name);
    }

    /**
     * Quotes table- or fieldname (vendor-specific)
     * If name is already quoted, returns the name.
     *
     * @param string $name
     *
     * @return string the quoted name
     * @throws UXAppException
     */
    public function quoteSingleName($name) {
        $name = trim($name);
        if($name=='*') return $name;
        $delimiter = $this->quoteIdentifier('_'); // delimiter[0] and delimiter[2] is used.
        $delimited = strlen($name)>2 && substr($name, 0, 1)==$delimiter[0] && substr($name, -1)==$delimiter[2];
        $barename = $delimited ? substr($name, 1, -1) : $name;
        if(strpos($barename, $delimiter[0]) !== false || strpos($barename, $delimiter[2]) !== false) {
            throw new UXAppException("Invalid fieldname ($name)");
        }
        if($delimited) return $name;
        return $this->quoteIdentifier($name);
    }

    /**
     * Returns a safe field name by rules of the connection database with optional alias
     * Processes .-notations
     * Does not check the model
     *
     * @param string|array $name -- field name or a parameter in form '$1' or '$name'; or expression array
     * @param string $prefix -- optional table alias connected with '.'. If fieldname already has a prefix, alias will be ignored.
     * @param string $output -- optional output alias connected with 'AS'
     *
     * @return string
     * @throws UXAppException
     */
    public function buildFieldName($name, $prefix=null, $output=null) {
        return $this->buildFieldNameValue($name, $prefix) . ($output ? ' AS '.$this->quoteSingleName($output) : '');
    }

    /**
     * Returns a safe field name by rules of the connection database without alias
     *
     * @param string|array $name
     * @param string $prefix -- optional table alias connected with '.'. If fieldname already has a prefix, alias will be ignored.
     *
     * @return string
     * @throws UXAppException
     */
    public function buildFieldNameValue($name, $prefix=null) {
        if(is_array($name)) {
            return $this->buildExpression($name, $prefix);
        }
        if(preg_match('/^($\d+|\$\w+)$/', $name)) return $name;
        if(substr($name,0,1)=="'" || substr($name,-1)=="'") return $name;
        if(substr($name,0,1)=='(' || substr($name,-1)==')') return $name;
        if(strpos($name, '.')===false && $prefix) $name = $prefix.'.'.$name;
        return $this->quoteName($name);
    }

    /**
     * Builds an SQL fragment used as expression for example in WHERE part or ON part in a JOIN.
     * The expression must not be empty.
     * Expression may be:
     *
     *  - string beginning with '(' will be returned literally. No identifier quoting or model check will be applied.
     *  - string beginning with single quote or E' will be treated as string literal. Closing quote will be omitted and internal double single quotes are allowed
     *  - string beginning with a number: a numeric literal
     *  - string beginning with a $ and a number: numeric indexed parameter
     *  - string beginning with a $: string-indexed parameter
     *  - other string: a field name. Unqualified field name will be qualified with alias if alias is given.
     *  - array (OP, ...): operator with operands (expressions), may contain fieldname=>expression elements {@see $_operators}
     *  - array (FN(), ...): function call (function existence not checked) (arguments as expressions)
     *  - array (fieldname=>expression, ...) ==> array('AND', array('=', fieldname, expression), ...) may be mixed with other array expressions
     *  - array ('(condition)') -- see string '(condition)';
     *  - empty array will be TRUE (0 elements AND)
     *
     * All subexpressions will be evaluated recursive.
     * if there are numeric and string indices mixed in an operand list, numeric indices will appear first.
     *
     * @param mixed $expression
     * @param string $alias -- optional alias for model
     * @param integer $precedence -- whether it's necessary to use paranthesis with operator below this level.: 0=always, null=never
     *
     * @return string
     * @throws UXAppException
     * @throws UXAppException on invalid expression, unknown operator, invalid number of operands, extra whitespaces in field names
     */
    public function buildExpression($expression, $alias=null, $precedence=null) {
        if(is_null($expression)) return 'NULL';
        if(is_null($precedence)) $precedence=999;
        if(is_bool($expression)) {
            return $this->literal($expression, true);
        }
        if(is_integer($expression) || is_float($expression)) {
            return $expression;
        }
        if(is_string($expression)) {
            if($expression=='') return "''";
            $expression = trim($expression);
            $c = substr($expression,0,1);
            if($c=='(') {
                if(substr($expression,-1)==')') return $expression;
                throw new UXAppException('Invalid expression: missing `)`');
            }
            if($c=="'" || $c=="E" && substr($expression,1,1)=="'") {
                if(substr($expression,-1)=="'") return $expression;
                throw new UXAppException('Invalid expression: missing `\'`');
            }
            if($c=="$") return $expression;
            $output = null;
            if(strpos($expression, ' ')) {
                $ee = preg_split('/\s+/', $expression);
                if(strtoupper($ee[1])=='AS' && count($ee)==3)  { $output = $ee[2]; $expression=$ee[0]; }
                else if(count($ee)==2)  { $output = $ee[1]; $expression=$ee[0]; }
                else throw new UXAppException(sprintf('Invalid expression: too many whitespaces in field name `%s`', $expression));
            }
            return $this->buildFieldName($expression, $alias, $output);
        }
        if(is_array($expression)) {
            if(isset($expression[0])) {
                if(is_array($expression[0])) $op = 'AND';
                else $op = array_shift($expression);
                if(substr($op,0,1)=='[' || substr($op,0,1)=='\'') throw new UXAppException('Invalid operator: '.$op);
                if(substr($op,0,1)=='(') {
                    if(substr($op,-1)==')') return $op;
                    throw new UXAppException('Invalid expression: missing `)`');
                }
                $expression = $this->preprocessAssociative($expression);
                if(($ops = static::isOperator($op))!==false) {
                    $pr = static::operatorPrecedence($op);
                    $opr = $this->operatorName(strtoupper($op));
                    if(!$opr) throw new UXAppException("Operator `$op` is not supported");

                    if($ops<4 && count($expression)!=$ops) throw new UXAppException("Operator $opr requires $ops operand.", $expression);
                    switch($ops) {
                        case 0:
                            return $opr;
                        case 1:
                            return $opr . ' '. $this->buildExpression($expression[0], $alias, $pr);
                        case 2:
                        case 4:
                            // now $expression is an operand list.
                            if($opr=='AND' && empty($expression)) return 'TRUE';
                            if($opr=='OR' && empty($expression)) return 'FALSE';
                            if($opr=='||' && empty($expression)) return "''";
                            if($opr=='*' && empty($expression)) return 1;
                            if($opr=='+' && empty($expression)) return 0;
                            if($opr=='&' && empty($expression)) return -1;
                            if($opr=='|' && empty($expression)) return 0;
                            try {
                                $db = $this;
                                $r = implode(' '.$opr.' ', @array_map(function($x) use($alias, $pr, $db) {
                                    return $db->buildExpression($x, $alias, $pr);
                                }, $expression));
                                break;
                            }
                            catch(Throwable $e) {
                                UXApp::trace(['Expression' => $expression]);
                                throw new UXAppException("Invalid internal expression at operator $opr", $expression, $e);
                            }
                        case 3:
                            $r = $this->buildExpression($expression[0], $alias, $pr) .
                                ' '. $opr . ' '. $this->buildExpression($expression[1], $alias, $pr) .
                                ' AND ' . $this->buildExpression($expression[2], $alias, $pr);
                            break;
                        case 5:
                            if(count($expression)!=1) throw new UXAppException("Operator $opr requires exactly one operand", $expression);
                            return $this->buildExpression($expression[0], $alias, $pr).' '.$opr;
                        case 6:
                            if(count($expression)!=2) throw new UXAppException("Operator $opr requires exactly two operands.");
                            $r = $this->buildExpression($expression[0], $alias, $pr) .
                                ' '. $opr . ' ('. $this->buildExpressionList($expression[1], $alias) .
                                ')';
                            break;
                        case 7:
                            return $this->buildOperator($opr, $expression, $alias);
                        default:
                            throw new UXAppException("Invalid operator definition", "$opr=$ops");
                    }

                    // Determine () on precedence order
                    if(count($expression)>1 && $pr>$precedence || $precedence==34 && is_array($expression[0])) $r='('.$r.')';
                    return $r;
                }
                // Function call
                if(substr($op, -2)=='()') {
                    $db = $this;
                    $r = substr($op,0,-1).implode(', ', array_map(function($x) use($alias, $db) {
                        return $db->buildExpression($x, $alias);
                    }, $expression)).')';
                    return $r;
                }
                else {
                    throw new UXAppException("Unknown operator `$op`", $expression);
                }
            }
            if(empty($expression)) return 'TRUE';
            // fieldname=>expression condition list
            if(ArrayUtils::isAssociative($expression)) {
                $op = 'AND';
                $pr = static::operatorPrecedence($op);
                $ex = array_merge([$op], array_map(function($value, $index){
                    if($value===null || $value===[]) return ['is null', $index];
                    if(is_array($value)) return ['in', $index, $value];
                    return ['=', $index, $value];
                }, array_values($expression), array_keys($expression)));
                $ex = $this->buildExpression($ex, $alias, $pr);
                if($pr>$precedence) $ex='('.$ex.')';
                return $ex;
            }
        }
        if($expression instanceof Query) {
            $expression->db = $this;
            return '('.$expression->sql.')';
        }
        if(is_object($expression)) return $this->literal($expression);
        throw new UXAppException('Invalid expression `'.Util::objtostr($expression).'`', $expression);
    }

    /**
     * Returns the number of operands of the operator if $op is an operator, or false if not.
     *
     * @param $op
     *
     * @return integer|false
     */
    public static function isOperator($op) {
        if(!is_string($op)) return false;
        return ArrayUtils::getValue(static::$_operators, strtolower($op), false);
    }

    /**
     * Returns precedence of the operator if $op is an operator, or false if not.
     *
     * @param $op
     *
     * @return integer|false
     */
    public static function operatorPrecedence($op) {
        if(!is_string($op)) return false;
        return array_search(strtolower($op), array_keys(static::$_operators));
    }

    /**
     * Replaces all associative entries to expression format in an expression list
     * (Converts fieldlist to expressionlist, field values treated as expressions)
     * E.g. array('apple', 'p'=>'peach') --> array('apple', array('=', 'p', 'peach'))
     *
     * @param array $list
     *
     * @return array
     */
    public function preprocessAssociative($list) {
        foreach($list as $index=>$value) {
            if(!is_integer($index)) {
                unset($list[$index]);
                $list[] = array('=', $index, $value);
            }
        }
        return $list;
    }

    /**
     * Builds a ,-separated expression list (without surrounding paranthesis)
     * or a subquery
     *
     * @param array|Query $list -- array of expressions or a subquery
     * @param integer $alias
     *
     * @return string
     * @throws UXAppException
     */
    public function buildExpressionList($list, $alias=null) {
        if($list instanceof Query) {
            $list->connection = $this;
            return $list->sql;
        }
        if(!is_array($list)) {
            UXApp::trace(['list' => $list]);
            throw new UXAppException('Invalid expression list', $list);
        }
        $db = $this;
        return implode(', ', array_map(function($expression) use($alias, $db) {
            return $db->buildExpression($expression, $alias);
        }, $list));
    }

    /**
     * Returns vendor-dependent expression part
     *
     * Define or override opBuild() methods in vendor drivers for each op.
     *
     * @param string $op -- the operator
     * @param array $expression -- list of operands
     * @param null $alias
     *
     * @return string -- the expression built
     * @throws UXAppException
     */
    public function buildOperator($op, $expression, $alias=null) {
        $functionname = strtolower($op).'Build';
        if(!is_callable(array($this, $functionname))) throw new UXAppException("Expression build function `$functionname` is missing");
        return call_user_func(array($this, $functionname), $expression, $alias);
    }

    /**
     * CASE builder (ANSI standard)
     *
     * 1. Two-argument version:
     * Needs a condition list and a value list.
     * Normally, for each condition there must be a value.
     * - If number of values are less than conditions, null values will be used.
     * - If number of values are higher then conditions, and there is no null condition, the next value will be used as value of ELSE branch
     * - All further values will be ignored.
     * - First zero condition will be used as ELSE branch. All other zero conditions will be ignored with corresponding values.
     * 2. Three-argument version:
     * Needs an expression, a compare-list and a value list.
     *
     * @param array $arguments -- list of operands
     * @param string $alias -- optional alias for main model (unqualified fieldnames)
     *
     * @return string -- the expression built
     * @throws UXAppException
     * @throws UXAppException
     */
    public function caseBuild($arguments, $alias=null) {
        if(count($arguments) < 2 || count($arguments) > 3) throw new UXAppException('Invalid arguments. CASE needs an optional expression, a condition list and a value list.');
        $expression = count($arguments)==3 ? array_shift($arguments) : null;
        $conditions = $arguments[0];
        $values = $arguments[1];
        if(!is_array($conditions) || !is_array($values)) throw new UXAppException('Invalid arguments. CASE needs a condition list and a value list.');
        $r = 'CASE';
        if($expression) $r .= ' '.$this->buildExpression($expression, $alias);
        $elsevalue = null;
        foreach($conditions as $i=>$condition) {
            if($condition===null) {
                if(!$elsevalue) $elsevalue = $values[$i];
                else continue;
            }
            if(array_key_exists($i, $values)) {
                $value = $values[$i];
                $r .= ' WHEN ' . $this->buildExpression($condition, $alias) . ' THEN ' . $this->buildExpression($value, $alias);
            }
            else break;
        }
        if(!$elsevalue && isset($values[count($conditions)])) $elsevalue = $values[count($conditions)];
        if($elsevalue) $r .= ' ELSE ' . $this->buildExpression($elsevalue, $alias);
        return $r . ' END';
    }

    /**
     * Builds an SQL command according to Query type.
     * The SQL string built is stored in $query->sql
     *
     * @param Query $query
     *
     * @return string -- the SQL built (contains unresolved parameters)
     * @throws UXAppException
     */
    public function buildSQL($query) {
        if(!$query->type) throw new UXAppException('Query type is not specified');
        if($query->type=='sql') return $query->getSql();
        $functionname = 'build'.Util::camelize(strtolower($query->type), true);
        if(!is_callable(array($this, $functionname))) throw new UXAppException("SQL build function `$functionname` is missing from class ".get_class($this));
        return call_user_func(array($this, $functionname), $query);
    }

    /**
     * builds a SELECT type SQL
     *
     * @param Query $query
     *
     * @return string
     * @throws Exception
     * @throws UXAppException
     */
    public function buildSelect($query) {
        $alias = $this->normalizeAlias($query);

        /** @noinspection PhpCastIsUnnecessaryInspection */
        return 'SELECT '.
            ($query->distinct ? 'DISTINCT ' : '') .
            $this->buildFieldNames($query->from, $query->fields, $number) .
            ($query->from ? (' FROM '. $this->buildTableNames($query->from)) : '') .
            $this->buildJoins($query->modelname, $alias, $query->joins) .
            $this->buildWhere($query->modelname, $query->condition, $alias) .
            $this->buildGroupby($query->groupby, $alias) .
            // TODO: HAVING
            // TODO: UNION, INTERSECT, EXCEPT
            $this->buildCombination($query->combination) .
            $this->buildOrders($query->orders) . // Auto alias not applied in order to using alias fields.
            ($query->limit ? ' LIMIT '.(int)$query->limit : '') .
            ($query->offset ? ' OFFSET '.(int)$query->offset : '');
    }

    /**
     * Builds an INSERT query based on query
     *
     * INSERT INTO _modelname (_fields) _values
     *
     * @param Query $query
     *
     * @return string
     * @throws Exception
     * @throws UXAppException
     */
    public function buildInsert($query) {
        $fields = $query->fields;
        $values = $query->values;

        if(!$fields && is_array($values) && ArrayUtils::isAssociative($values)) {
            $fields = array_keys($values);
            $values = array(array_values($values));
        }

        if(!$values) {
            if(!$fields) throw new UXAppException('Must specify fields or values for insert');
            if(!is_array($fields)) throw new UXAppException('invalid fields parameter');
            $fieldsPart = $this->buildFieldNames($query->modelname, array_keys($fields), $number);
            $valuesPart = $this->buildExpressionList(array_values($fields));
        }
        else {
            $fieldsPart = $this->buildFieldNames($query->modelname, $fields, $number);
            $valuesPart = $this->buildValues($values, $number);
        }

        $tablePart = $this->buildTableName($query->modelname);

        return 'INSERT '.'INTO ' . $tablePart . ' (' . $fieldsPart. ') '. $valuesPart;
    }

    /**
     * Builds an UPDATE query based on query data
     *
     * UPDATE modelname SET fields/values WHERE condition
     * Currently does not support `UPDATE ... FROM` construction.
     *
     * @param Query $query
     *
     * @return string
     * @throws Exception
     * @throws UXAppException
     */
    public function buildUpdate($query) {
        $tablePart = $this->buildTableName($query->modelname);
        $updatePart = $this->buildUpdateSet($query->values);
        $wherePart = $this->buildWhere($query->modelname, $query->condition);
        return 'UPDATE ' . $tablePart . ' SET ' . $updatePart . $wherePart;
    }

    /**
     * Builds a DELETE query based on query data
     *
     * DELETE FROM modelname WHERE condition
     *
     * @param Query $query
     *
     * @return string
     * @throws Exception
     * @throws UXAppException
     */
    public function buildDelete($query) {
        return /** @lang */'DELETE FROM ' .
            $this->buildTableName($query->modelname) .
            $this->buildWhere($query->modelname, $query->condition);
    }

    /**
     * If query has joins or more than one from table, adds alias keys to all from table to force using alias
     * in subsequent fieldname generation.
     *
     * Also returns alias name of main model table if exists, or null.
     *
     * @param Query $query -- query the from array of which will be altered
     *
     * @return string -- alias name of main model table or null
     * @throws UXAppException
     */
    public function normalizeAlias($query) {
        if(!empty($query->joins)) {
            if(is_string($query->from)) {
                $alias = $this->getTableName($query->from);
                $query->from = array($alias => $query->from);
            }
            else if(is_array($query->from)) {
                foreach($query->from as $i=>$model) {
                    if(is_int($i)) {
                        $from = $query->from; // virtual property must be cached to update
                        $alias = $this->getTableName($model);
                        $from[$alias] = $model;
                        unset($from[$i]);
                        $query->from = $from;
                    }
                }
            }
        }
        $alias = is_array($query->from) ? array_search($query->modelname, $query->from) : null;
        if($alias===false || is_int($alias)) $alias = null;
        #$alias = empty($joins) ? null : strtolower($modelname);
        return $alias;
    }

    /**
     * Retrieves field names from the named model without alias
     *
     * @param $modelname
     *
     * @return array (numeric indexed)
     * @throws UXAppException
     */
    public function getModelFields($modelname) {
        if(!is_callable(array($modelname, 'databaseAttributes'))) throw new UXAppException("Invalid model classname '$modelname'");
        return call_user_func(array($modelname, 'databaseAttributes'), $this);
    }

    /**
     * Retrieves foreign key data from the model of a named table
     * Uses default connection
     * Returns the foreign key definition item
     *
     * @param string $model -- model classname
     * @param $fkname
     *
     * @return array (remote_model, remote_id=>local_id, ...)
     * @see Model::foreignKeys
     * @throws UXAppException
     */
    public static function getModelForeignKey($model, $fkname) {
        if(!is_callable($model.'::foreignKey')) throw new UXAppException("Invalid modelname: $model, $fkname");
        return call_user_func($model.'::foreignKey', $fkname);
    }

    /**
     * Wraps the alias into double quote
     *
     * Replaces any existing double quote to single.
     * Vendor-independent. Use only for aliases
     *
     * @param string $alias
     * @return string
     */
    public static function doubleQuote($alias)  {
        return '"'.str_replace('"', '\'', $alias).'"';
    }

    /**
     * Builds an SQL fragment to field names used in select or insert or group by.
     * If fields is null, will use all fields of all tables with proper aliases
     * If you specify the field names explicitly, you must supply the proper aliases.
     * Unprefixed field names will use first model's alias if exists
     * In this case the models and aliases will not be checked.
     *
     * @param string|array $models -- main table name or array(alias=>tablename)
     *        in case of given aliases, they will be applied to fields' prefix
     *        if no alias, single table's fields will not use prefix, otherwise tablenames will be used as prefix
     * @param array $fields -- array of field names (or single field name as string)
     *        with or without table prefixes,
     *        or alias => fieldname
     *        or alias => (expression)
     * @param $number -- returns the number of fields to be inserted
     *
     * @return string
     * @throws Exception
     * @throws UXAppException
     */
    public function buildFieldNames($models, $fields, &$number) {
        $forcealias = false;
        if($models===null) $models=[]; // throw new UXAppException('No model or tablename given for select');
        if(!is_array($models)) $models = array($models);
        //if(count($models)<1) throw new UXAppException('Must specify at least one model');
        if(count($models)<1) $forcealias = false;
        if(count($models)>1) $forcealias = true;
        if(ArrayUtils::isAssociative($models, false)) $forcealias = true;

        $mk = array_keys($models);
        /** @var string $alias -- alias of first (main) table, will be used as prefix for all unqualified field when more than one table is present. */
        $alias = $mk ? $mk[0] : null;
        if(is_integer($alias)) {
            $alias = $this->getTableName($models[$alias]);
        }

        if(is_null($fields)) {
            $fields = [];
            foreach($models as $key=>$modelname) {
                if($modelname instanceof Query) {
                    // all fields from subquery
                    $fields = array_merge($fields, [$key.'.*']);
                } else {
                    if(is_integer($key) && $forcealias) $a1 = $modelname::tableName();
                    else {
                        $a1 = $key;
                    }
                    $fields1 = $this->getModelFields($modelname);
                    if($a1) $fields1 = array_map(function($f) use ($a1) {
                        return $a1 . '.' . $f;
                    }, $fields1);
                    $fields = array_merge($fields, $fields1);
                }
            }
        } else {
            if(!is_array($fields)) $fields = [$fields];
            if($forcealias) {
                $fields = array_map(function($f) use($alias) {
                    if(is_array($f)) return $f;
                    if(substr($f,0,1)=="'") return $f;
                    if(substr($f,0,1)=='(') return $f;
                    if(strpos($f, '.')) return $f;
                    return $alias.'.'.$f;
                }, $fields);
            }
        }

        if(!is_array($fields)) {
            $fields = [$fields];
        }
        $number = count($fields);
        $prefix = $forcealias ? $alias : null;
        $db = $this;
        $aliases = array_keys($fields);
        return implode(', ', array_map(function($f, $a, $i) use($prefix, $fields, $aliases, $db) {
            $output = is_integer($a) ? '' : $a;
            // Auto output alias-prefixed fieldname if similar fieldname exist before it's index
            if(!$output && is_scalar($f) && strpos($f, '.')) {
                $field_name = Util::substring_after($f, '.', true);
                // Megkeressük a field_name egyéb előfordulásának numerikus pozícióját, ahol nincs alias
                $pos = ArrayUtils::array_find_key($fields, function($item, $alias) use($f, $field_name) {
                    if(!is_numeric($alias)) return false;
                    return $item!==$f && $field_name == Util::substring_after($item, '.', true);
                });
                if(is_numeric($pos) && $pos < $i) $output =  str_replace('.', '_', $f);
            }
            try {
                return $db->buildFieldName($f, $prefix, $output);
            }
            catch(Throwable $e) {
                $f = Util::objtostr($f);
                $fields = Util::objtostr($fields);
                UXApp::trace(['e' => $e->getMessage(), 'f'=>$f, 'prefix'=>$prefix, 'output'=>$output]);
                throw new Exception("Invalid fieldname definition '$f' in $fields", 0, $e);
            }
        }, $fields, $aliases, array_keys($aliases)));
    }

    /**
     * Returns a safe single tablename by rules of the connection database
     * Processes .-notations
     * Does not check the model
     *
     * @param string|Query $model -- main table model name
     * @param string $alias -- optional alias for tablename
     *
     * @return string
     * @throws
     */
    public function buildTableName($model, $alias='') {
        if($model instanceof Query) {
            /** @var Query $model */
            $model->connection = $this;
            return '(' . $model->sql . ')'. ($alias ? (' '.$this->quoteName($alias)) : '');
        }
        /** @var string $model */
        Assertions::assertString($model);

        $name = $this->getTableName($model);
        return $this->quoteName($name). (($alias && ($alias!=$name)) ? (' '.$this->quoteName($alias)) : '');
    }

    /**
     * Returns a comma-separated aliased list of safe tablenames by rules of the connection database
     * Processes .-notations
     * Does not check the model
     *
     * @param array|string $models -- array of model names or alias=>model elements or single modelname
     *
     * @return string
     * @throws Exception
     */
    public function buildTableNames($models) {
        if(!is_array($models)) return $this->buildTableName($models);
        $db = $this;
        return implode(', ', array_map(function($model, $alias) use($db) {
            if(is_integer($alias)) $alias='';
            return $db->buildTableName($model, $alias);
        }, $models, array_keys($models)));
    }

    /**
     * Builds an SQL fragment used in SELECT as JOIN part.
     * Scalar string will returned literally, without any identifier quoting.
     * If foreign key identifiers listed, identifiers will be aliases, tablenames are based upon foreignKey definition
     * Foreign keys will generate LEFT joins, other joins are default FULL.
     *
     * @param string|array $model -- main table's modelname (single table only)
     * @param string $mainAlias -- main table's alias
     * @param array $joins -- list of joined models (foreign keys) OR alias=>fk OR alias=>array(model, [jointype], conditions) element
     *
     * @return string
     * @throws
     * @see Self::buildJoin() for join item details
     */
    public function buildJoins($model, $mainAlias, $joins) {
        if(empty($joins)) return '';
        if(is_string($joins)) return $joins;
        $result = '';

        foreach($joins as $key=>$join) {
            if(is_string($join)) {
                if(!is_integer($key)) $alias = $key;
                else $alias = $join;
                // Reference to a predefined foreign key
                $type = 'LEFT';
                if(strpos($join, '.')) {
                    // indirect reference
                    $joina = explode('.', $join);
                    $model1 = $model;
                    $prefix = '';
                    $alias1 = '';
                    $fk = null;
                    foreach($joina as $join1) {
                        $fk = static::getModelForeignKey($model1, $join1); // array(modelname, remote=>local)
                        if($fk) {
                            $model1 = $fk[0];
                            if($alias1) $prefix = $alias1 . '.'; // Only the previous alias
                            // Search for alias of $join1 in $joins. Have to add if not present
                            $alias1 = array_search($join1, $joins);
                            if($alias1===false) {
                                $alias1 = $joins[$join1] = $join1;
                            }
                        }
                    }
                    $fk1 = $fk;
                    foreach($fk1 as $k=>$v) if(!is_int($k)) {
                        unset($fk[$k]);
                        // Replace existing alias to indirect prefix
                        $k = $prefix . Util::substring_after($k, '.', true);
                        $fk[$k] = $v;
                    }
                }
                else {
                    $fk = static::getModelForeignKey($model, $join);
                }
                if(!$fk) throw new UXAppException("Foreign key $join does not exists in model $model");
                $foreignmodel = array_shift($fk);
                $conditions = $fk;
            } else if(is_array($join)) {
                if(is_integer($key)) throw new UXAppException('Explicit alias must be specifed for other joined tables.');
                $alias = $key;
                $type = '';
                $foreignmodel = array_shift($join);
                if(isset($join[0]) && is_string($join[0]) && in_array(
                    $jointype = strtoupper($join[0]),
                    array('LEFT', 'RIGHT', 'INNER', 'LEFT OUTER', 'RIGHT OUTER', 'FULL', 'FULL OUTER'))
                ) {
                    $type = $jointype;
                    array_shift($join);
                }
                $conditions = $join;
            } else throw new UXAppException('Invalid join data');
            $result .= $this->buildJoin($model, $mainAlias, $foreignmodel, $alias, $type, $conditions);
        }
        return $result;
    }

    /**
     * Builds a single JOIN part as SQL fragment.
     * If join is a foreign key identifier, alias will be the identifier, tablename is based upon foreignKey definition
     * Foreign keys will generate left join, other join is full.
     * Checks JOIN type and necessarity of conditions.
     *
     * @param string|Model $model -- main model to join to (has no alias)
     * @param string $mainAlias -- optional alias for main model
     * @param array|Model $foreignmodel -- name of foreign model
     * @param string $alias -- alias of joined table - mandatory
     * @param string $type -- 'left' or 'inner' or empty, etc.
     * @param array $conditions -- 'ON' conditions array(mainfield=>foreignfield) (mandatory for some JOIN types, must be empty for others.)
     * you may use any other expression using array($expression)
     *
     * @return string
     * @throws
     * @see Self::buildJoins()
     * @throws Exception
     */
    public function buildJoin($model, $mainAlias, $foreignmodel, $alias, $type, $conditions) {
        if(!is_a($foreignmodel, Model::class, true)) throw new Exception("Model class `$foreignmodel` is missing");
        $foreigntablename = $foreignmodel::tableName();

        // type: CROSS | [NATURAL] { [INNER] | { LEFT | RIGHT | FULL } [OUTER] }
        $type = strtoupper(trim($type));
        $types = explode(' ', $type);
        $hason = count($types)==0 || ($types[0]!='CROSS' && $types[0]!='NATURAL');
        $valid = preg_match('/^(CROSS|((NATURAL)?\s?(INNER|((LEFT|RIGHT|FULL)( OUTER)?))?))$/', $type);
        if(!$valid) throw new UXAppException("Invalid join type $type");

        if($hason) {
            $foreignAlias = $alias ? $alias : $foreigntablename;
            if(empty($conditions) || !is_array($conditions)) throw new UXAppException("Missing condition for $type JOIN");
            // Processing conditions: array('creator'=>'id'), --> array('AND', 'creator'=>'alias.id')  --> 'model.creator = alias.id'

            if(ArrayUtils::isAssociative($conditions)) {
                $expression = array('AND');
                foreach($conditions as $mainfield=>$foreignfield) {
                    Assertions::assertString($foreignfield);
                    $field = strpos($foreignfield, '.')===false ? $foreignAlias.'.'.$foreignfield : $foreignfield;
                    $expression[] = array('=', $mainfield, $field);
                }
            }
            else {
                $expression = $conditions;
            }

            if(!$mainAlias) $mainAlias = $model::tableName();
            return sprintf(' %s JOIN %s ON %s',
                $type,
                $this->quoteSingleName($foreigntablename) . ($alias ? ' '.$this->quoteSingleName($alias) : ''),
                $this->buildExpression($expression, $mainAlias)
            );
        }
        else {
            if(!empty($conditions)) throw new UXAppException("Needless condition for $type JOIN");
            return sprintf(' %s JOIN %s',
                $type,
                $this->quoteSingleName($foreigntablename) . ($alias ? ' '.$this->quoteSingleName($alias) : '')
            );
        }
    }

    /**
     * Builds an SQL fragment used in SELECT, DELETE and UPDATE as WHERE part.
     * If condition expression is empty, the WHERE clause will not present.
     * Expression may be:
     *
     *  - array of (field=>expression)
     *  - expression
     *  - a single scalar literal for single pk
     *
     * @param string $modelname
     * @param mixed $condition
     * @param null $alias
     *
     * @return string
     * @throws UXAppException
     * @see buildExpression
     *
     */
    public function buildWhere($modelname, $condition, $alias=null) {
        if(is_null($condition) || is_array($condition) && count($condition)==0) return '';
        if($condition===true) return '';
        if($condition===false) return ' WHERE false';
        if(!is_array($condition)) {
            $pk = call_user_func(array($modelname, 'primaryKey')); // returns array of field names
            if(count($pk)>1) throw new UXAppException('Invalid scalar condition for a composite primary key', Util::objtostr($condition));
            if(is_array($pk)) $pk = $pk[0];
            $condition = array($pk=>$this->literal($condition, false));
        }
        return ' WHERE '.$this->buildExpression($condition, $alias);
    }

    /**
     * @param array $groupby
     * @param string $alias
     *
     * @return string
     * @throws
     */
    public function buildGroupby($groupby, $alias=null) {
        if(is_null($groupby) || $groupby==='' || $groupby===array()) return '';
        if(is_string($groupby)) $groupby = array($groupby);
        if(!is_array($groupby)) throw new UXAppException('GROUP BY mut be an array', Util::objtostr($groupby));
        $db = $this;
        return ' GROUP BY ' . implode(', ', array_map(function($f) use($alias, $db) {
            return $db->buildExpression($f, $alias);
        }, $groupby));
    }

    /**
     * @param array $combination
     * @return string
     * @throws UXAppException
     */
    public function buildCombination($combination) {
        if(!$combination) return '';
        if(!is_array($combination)) throw new UXAppException('Combination descriptor must be an array.');
        if(!isset($combination[0])) return '';
        $query = $combination[0]; if(!$query instanceof Query) throw new UXAppException('query must be a Query.');
        $sql = $query->sql;
        $type = $combination[1] ?? 'UNION';
        if(!in_array($type, ['UNION', 'INTERSECT', 'EXCEPT'])) throw new UXAppException('Érvénytelen Query kombináció: ',$type);
        $all = isset($combination[2]) && $combination[2] ? 'ALL ' : '';
        return ' '. $type . ' ' . $all . $sql;
    }

    /**
     * Builds an SQL fragment used as ORDER BY part
     * Returns empty string on empty orders.
     *
     * @param array $orders  -- array of order definitions as in {@see buildOrderList()}
     * @param string $alias -- optional alias for model, used for fieldnames in {@see buildFieldNameValue()}
     *
     * @return string -- an 'ORDER BY ...' clause or empty string
     * @throws
     */
    public function buildOrders($orders, $alias=null) {
        if(is_null($orders)) return '';
        if(is_string($orders)) $orders = array($orders);
        if(!is_array($orders)) throw new UXAppException('Orders mut be an array', Util::objtostr($orders));
        if(!count($orders)) return '';
        return ' ORDER BY '.$this->buildOrderList($orders, $alias);
    }

    /**
     * Builds an order list without 'ORDER BY' keyword.
     *
     * @param array $orders -- numeric indexed array of strings or array(fieldname, order direction, nulls)
     *  - simple string beginning with '(' will be used literally,
     *  - all other simple string is a fieldname
     *  - order direction: ORDER_ASC (default), ORDER_DESC
     *  - nulls: NULLS_FIRST (default), NULLS_LAST
     *  - array expressions in second form are evaluated
     * @param string $alias -- optional alias for model, used for fieldnames in {@see buildFieldNameValue()}
     *
     * @return string -- the SQL fragment to be placed after the 'ORDER BY' or empty if no elements in orders.
     * @throws
     */
    public function buildOrderList($orders, $alias=null) {
        if(is_null($orders)) return '';
        if(is_string($orders)) $orders = array($orders);
        if(!is_array($orders)) throw new UXAppException('Orders mut be an array', Util::objtostr($orders));
        if(!count($orders)) return '';
        // nulls works directly on postgresql only, mysql must generate slave item ISNULL() if LAST is requested
        $db = $this;
        try {
            return implode(', ', array_map(function($o) use ($alias, $db) {
                return $db->buildOrder($o, $alias);
            }, $orders));
        }
        catch(Throwable $e) {
            throw new UXAppException('Invalid order list: ' . Util::objtostr($orders).' ('.$e->getMessage().')', $orders, $e);
        }
    }

    /**
     * Builds an order item for ORDER BY part
     *
     * @param string|array $o -- (literal) or fieldname or array(fieldname, order direction, nulls)
     *  - simple string beginning with '(' will be used literally,
     *  - all other simple string is a fieldname
     *  - order direction: ORDER_ASC (default), ORDER_DESC
     *  - nulls: NULLS_FIRST (default), NULLS_LAST
     * @param string $alias -- optional alias for model, used for fieldnames
     *
     * @return string -- an item for ORDER BY, without separator comma
     * @throws
     */
    public function buildOrder($o, $alias=null) {
        if(empty($o)) throw new UXAppException('Empty order item.');
        if(is_string($o)) {
            if(substr($o, 0, 1)=='(') {
                if(substr($o, -1)!=')') throw new UXAppException("Missing closing paranthesis in order item '$o'");
                return substr($o,1,-1);
            }
            if(strpos($o, ' ')!==false) {
                $words = explode(' ', trim($o));
                $words[0] = $this->buildFieldName($words[0], $alias, null);
                return implode(' ', $words);
            }
            return $this->buildFieldName($o, $alias, null);
        }
        if(is_array($o)) {
            if(!isset($o[0])) throw new UXAppException('Invalid order definition: '. Util::objtostr($o));
            $fieldname = $o[0];
            $order = ArrayUtils::getValue($o, 1, self::ORDER_ASC);
            $nulls = ArrayUtils::getValue($o, 2, self::NULLS_FIRST);
            $preorder = $nulls==self::NULLS_LAST && !$this->supportsOrderNullsLast() ?
                ('ISNULL('.$this->buildFieldName($fieldname, $alias).'), ') :
                '';
            try {
                return $preorder . $this->buildFieldName($fieldname, $alias) .
                    ($order == self::ORDER_DESC || strtoupper($order) == 'DESC' ? ' DESC' : '') .
                    (($this->supportsOrderNullsLast() && ($nulls == self::NULLS_LAST || strtoupper($order) == 'NULLS LAST')) ? ' NULLS LAST' : '');
            }
            catch(UXAppException $e) {
                throw new UXAppException('Invalid order expression', $fieldname, $e);
            }
        }
        throw new UXAppException('Invalid order item.');
    }

    /**
     * Builds the VALUES part of an INSERT statement. No expressions are allowed.
     *
     * If too many values are supplied: ignores the extra values.
     * If insufficient values are supplied: NULL values are inserted
     *
     * @param array $values -- array of row value arrays
     *   if values is an array, the literal values of multiple rows will be inserted.
     *   if values is a Query, a subquery will be used.
     * @param integer $number -- the number of fields to be inserted, default unspecified.
     * If given, insufficient number of values will be filled out with NULLS.
     *
     * @return string -- the values part (including VALUES keyword if necessary)
     * @throws
     */
    public function buildValues($values, $number=null) {
        if(is_string($values)) return $values;
        if(is_array($values)) {
            $result = 'VALUES ';
            foreach($values as $index=>$valuelist) {
                $result .= '(';
                if($valuelist===null) throw new UXAppException("#$index: Null values row.");
                if(!is_array($valuelist)) throw new UXAppException("#$index: Invalid values row. Values must contain arrays.", $valuelist);
                $n0 = count($valuelist);
                $n = $number ? $number : $n0;
                if($n0 > $n) throw new UXAppException("Too many values in the list #$index. $n0>$n", array($number, $valuelist));
                for($i=0;$i<$number;$i++) {
                    $result .= ($i>=$n0) ? 'NULL' : $this->literal($valuelist[$i], true);
                    if($i<$n-1) $result .= ', ';
                }
                $result .= ')';
                if($index<count($values)-1) $result .= ",\n";
            }
            return $result;
        }
        if($values instanceof Query) {
            /** @var Query $values */
            $values->connection = $this;
            return $values->sql;
        }
        $type = gettype($values);
        throw new UXAppException("Invalid datatype ($type) for values part.", $values);
    }
    /**
     * Builds an SQL fragment for 'set' part of an update
     *
     * @param array $values -- fieldname=>expression pairs
     * @return string -- without 'SET'
     */
    public function buildUpdateSet($values) {
        $db = $this;
        return implode(', ', array_map(function($fieldname, $exp) use($db) {
            return
                $db->buildFieldName($fieldname) .
                '=' .
                $db->buildExpression($exp);
        }, array_keys($values), array_values($values)));
    }

    /**
     * Converts fieldname=>value pairs to fieldname=>expression
     * In Models, array(fieldname=>value, ...) is the condition format, but in the Query builder, the array(fieldname=>expression, ...)
     * So value-based conditions must be converted before calling Query methods.
     * Other condition formats not affected.
     * - fieldname => value will converted to fieldname => 'value'
     * - fieldname => null will converted to array('null', fieldname)
     *
     * @param array $condition
     *
     * @return array
     * @throws
     */
    public function asExpression($condition) {
        if(is_array($condition) && ArrayUtils::isAssociative($condition, false)) {
            $result = array();
            foreach($condition as $field=>$value) {
                if(is_integer($field) || is_float($field)) $result[] = $value;
                else if(is_null($value)) $result[] = array('is null', $field);
                else $result[$field] = $this->literal($value);
            }
            $condition = $result;
        }
        return $condition;
    }

    /**
     * Returns tablename for a model name.
     * Returns itself if model name is not a Model classname, assuming literal tablename
     *
     * @param $modelName
     * @return mixed
     * @throws UXAppException
     */
    public function getTableName($modelName) {
        if(class_exists($modelName) && is_a($modelName, Model::class, true)) {
            if (!is_callable($callable = array($modelName, 'tableName'))) {
                throw new UXAppException("Invalid model class: $modelName");
            }
            return call_user_func($callable);
        }
        return $modelName;
    }

    /**
     * Returns metadata of all tables indexed by table name
     */
    public function getSchemaMetadata() {
        $tables = $this->getTables();
        return array_combine($tables, array_map(function($tableName) {
            return $this->getMetaData($tableName);
        }, $tables));
    }

    /**
     * Returns a valid conenction object defined by $connection parameter as:
     * - null:
     * - DBX object: returns itself
     * - string: returns named UXApp component if is a DBX
     * - array: creates a DBX component if is a DBX config
     *
     * @param DBX|string|array|null $connection
     *
     * @return DBX
     * @throws UXAppException -- if $connection parameter does not determine a valid connection*@throws \ReflectionException
     * @throws ReflectionException
     */
    public static function createConnection($connection=null) {
        if($connection === null)  $connection = UXApp::$app->db;
        if(is_string($connection) && UXApp::$app->hasComponent($connection) && UXApp::$app->$connection instanceof DBX) $connection = UXApp::$app->$connection;
        if(is_array($connection)) $connection = Component::create(DBX::class, $connection);
        if($connection instanceof DBX) return $connection;
        throw new UXAppException('Invalid connection', $connection);
    }

    /**
     * Must return array of table names (without schema prefix)
     *
     * @param string|null $schema
     *
     * @return string[]
     */
    abstract public function getTables($schema=null);
    public function database_tables($schema=null) {
        return $this->getTables($schema);
    }

    /**
     * Drops the named constraint from the table
     *
     * @param string $constraintName
     * @param string $tableName
     * @param string|null $schema
     *
     * @return false|resource
     */
    abstract public function dropForeignKey($constraintName, $tableName, $schema=null);

    /**
     * @param string $tableName
     * @param string|null $schema
     *
     * @return false|resource
     */
    abstract public function dropTable($tableName, $schema=null);

    /**
     * @param string $tableName
     * @param string|null $schema
     *
     * @return false|resource
     */
    abstract public function dropView($tableName, $schema=null);

    /**
     * Returns sequence names
     *
     * @param string|null $schema
     *
     * @return false|resource
     * @throws UXAppException
     */
    abstract public function getSequences($schema=null);

    /**
     * @param string $sequenceName
     * @param string|null $schema
     *
     * @return false|resource
     */
    abstract public function dropSequence($sequenceName, $schema=null);

    /**
     * Retuns array with function names followed by parameter list and preceded with routine type from the give schema.
     *
     * @param string $schema -- default is "public"
     * @return string[]
     * @throws UXAppException
     */
    abstract public function getRoutines($schema=null);

    /**
     * @param string $routineName
     * @param string $routineType
     * @param string|null $schema
     *
     * @return resource|false -- success
     */
    abstract public function dropRoutine($routineName, $routineType='FUNCTION', $schema=null);

    /**
     * Checks if column exists in the table
     *
     * @param string $table
     * @param string $column
     * @return bool
     *
     * @throws Exception
     */
    public function columnExists($table, $column) {
        $metaData = static::getMetaData($table);
        return array_key_exists($column, $metaData);
    }
}
