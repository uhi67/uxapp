<?php
namespace uhi67\uxapp;

/** @noinspection PhpUnused */

/**
 * Class CliDebug
 *
 * @package uhi67\uxapp
 */
class CliDebug extends BaseDebugger implements DebugInterface {
	public $on;

	/**
	 * @throws \ReflectionException
	 */
	public function prepare() {
		parent::prepare();
		$tracex = ArrayUtils::getValue(UXApp::$app->args, 'trace', '');
		if($tracex=='on') $this->switchOn();
		if($tracex=='off') $this->switchOff();
	}

	/**
	 * Options:
	 *
	 * - params ($vars to substitute into msg)
	 * - depth (effective backtrace depth from trace call)
	 * - tags (space spearated words, e.g. 'app', 'UXApp', 'sql')
	 * - mt (microtime to display)
	 * - color (message color)
	 *
	 * @param mixed $msg
	 * @param array $context
	 */
	public function trace($msg = '*', $context = []) {
		echo $msg, "\n";
	}
}
