<?php
namespace uhi67\uxapp;

use Codeception\Util\Debug;
use Exception;
use ReflectionException;

/**
 * # Class ArrayDataSource
 *
 * A data source based on a predefined data array
 *
 * ### Using
 *
 * ```
 * $ds = new ArrayDataSource([
 * 		'data' => [
 *          ['id'=>1, 'name'=>'sample1'],
 *          ['id'=>2, 'name'=>'sample2'],
 *      ],
 * ]);
 * $list = new UList(['dataSource'=>$ds]);
 * $list->createNode($this);
 * ```
 *
 * ### All parameters
 * - **data** (Query) -- the data array to provide. Must contain array or BaseModel elements
 * - **patternSetter** (callable) -- function($dataSource, $pattern, $params) -- called when search pattern is set. Default has no effect
 *
 * @property-read $count
 * @property string $modelClass -- className to return as elements
 * @property-write string $pattern
 */
class ArrayDataSource extends Component implements DataSourceInterface {
    /** @var array $data -- the array with the data to provide */
    public $data = [];
    /** @var array|null $filteredRows -- the row-indices of the pattern-filtered data array */
    protected $filteredRows = null;
    /** @var string|BaseModel $modelClass -- name of model class, read only, if data array contains BaseModel elements */
    private $modelClass = null;
    /** @var callable $setPattern -- function($dataSource, $pattern, $params) -- called when search pattern is set */
    private $patternSetter = null;

    /**
     * @inheritdoc
     * @throws Exception
     */
    public function prepare() {
        if($this->modelClass===null && isset($this->data[0]) && $this->data[0] instanceof BaseModel) $this->modelClass = get_class($this->data[0]);
        if($this->patternSetter===null) $this->patternSetter = [$this, 'basePatternSetter'];
    }

    /**
     * Total number of items in the set
     *
     * @return int the number of items in the set
     * @throws UXAppException
     */
    public function getCount() {
        return count($this->data);
    }

    /**
     * Returns the specified section of items. Returns all items if all parameters are null.
     * Returns array of Models if modelClass is specified, array of arrays otherwise.
     *
     *
     * @param int|null $start -- 0-based offset
     * @param int|null $count -- number of items
     * @param array|null $orders -- order array for Query
     *
     * @return array -- the list of items
     * @throws UXAppException
     * @throws ReflectionException
     */
    public function fetch($start=null, $count=null, $orders=null) {
        // TODO: Apply pattern search
        if($this->filteredRows !== null) {
            if($start===null && $count===null && $orders===null) return array_map(function($index) {
                return $this->data[$index];
            }, $this->filteredRows);
            return array_map(function($index) {
                return $this->data[$index];
            }, array_slice($this->filteredRows, $start, $count));
        }
        if($start===null && $count===null && $orders===null) return $this->data;
        return array_slice($this->data, $start, $count);
    }

    /**
     * @param string $pattern
     * @param array $params
     * @return ArrayDataSource
     */
    public function setPattern($pattern, $params=null) {
        if(is_callable($this->patternSetter)) {
            call_user_func($this->patternSetter, $this, $pattern, $params);
        }
        return $this;
    }

    public function setModelClass($modelClass) {
        throw new Exception('modelClass cannot be set for ArrayDataSource');
    }

    public function getModelClass() {
        return $this->modelClass;
    }

    /**
     * @param ArrayDataSource $dataSource
     * @param string $pattern -- RegEx
     * @param array $params -- ['fieldName'=>, ...]
     * @return void
     */
    public static function basePatternSetter($dataSource, $pattern, $params=[]) {
        $fieldName = $params['fieldName']??null;
        Debug::debug($fieldName);

        // Filtering original data array. Creates a matching index array
        $dataSource->filteredRows = [];
        $validRegEx = !(@preg_match($pattern, null)===false);
        Debug::debug($validRegEx);
        foreach($dataSource->data as $index => $row) {
            $match = false;
            if(is_array($row)) {
                if($fieldName) {
                    Debug::debug($fieldName);
                    Debug::debug($row[$fieldName]);
                    $match = $validRegEx ? preg_match($pattern, $row[$fieldName]??'') : strpos($row[$fieldName]??'', $pattern)!==false;
                }
                else foreach($row as $field => $value) {
                    $match = $validRegEx ? preg_match($pattern, $value) : strpos($value, $pattern)!==false;
                    if($match) break;
                }
            }
            if($row instanceof BaseModel) {
                if($fieldName) {
                    $match = $validRegEx ? preg_match($pattern, $row->$fieldName??'') : strpos($row->$fieldName??'', $pattern)!==false;
                }
                else foreach($row->attributes as $field => $value) {
                    $match = $validRegEx ? preg_match($pattern, $value) : strpos($value, $pattern)!==false;
                    if($match) break;
                }
            }
            if($match) $dataSource->filteredRows[] = $index;
        }
    }
}
