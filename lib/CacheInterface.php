<?php
namespace uhi67\uxapp;

/**
 * # Cache interface
 *
 * ### Usage
 * - set(key, value, [ttl]) -- sets value and ttl in the cache for the key
 * - get(key, [default, [ttl]]) -- returns the value if key exists (and not expired) in the cache, or default (null) if not.
 * - has(key)		-- returns true if key exists and not expired in the cache
 * - delete(key)	-- deletes single or multiple items from the cache
 * - purge(pattern)	-- deletes items matching the RegEx pattern
 * - clear()		-- deletes all data from the cache
 * - cleanUp()		-- deletes all expired data from the cache
 * - cache($key, callback, ttl) -- gets value if exists, or computes and sets otherwise. Restarts ttl.
 * - finish() 		-- called before destructor (e.g. to save data to physical store if needed)
 *
 * @package UXApp
 * @author Peter Uherkovich
 * @copyright 2020
 */
interface CacheInterface {
	/**
	 * Returns data from cache or null if not found or expired.
	 *
	 * @param string $name
	 * @param mixed  $default
	 * @param int|bool|null $ttl -- if given, overrides and restarts expiration (only for this query, and if the element is not purged yet). true=restart default ttl
	 *
	 * @return mixed
	 */
	public function get($name, $default=null, $ttl=null);

	/**
	 * Returns data from cache or null if not found or expired.
	 * (Side effect: deletes expired data)
	 *
	 * @param string $name
	 *
	 * @return bool
	 */
	public function has($name);
	/**
	 * Saves data into cache
	 *
	 * @param string $name
	 * @param mixed $value -- Specify null to remove item from the cache
	 * @param int $ttl -- time to live in secs, default is given at cache config, false to no expiration
	 * @return int -- number of bytes written or false on failure
	 */
	public function set($name, $value, $ttl=null);

	/**
	 * Removes given items from the cache by name pattern
	 *
	 * @param string|array $key -- key or keys
	 *
	 * @return int -- number of deleted items, false on error
	 */
	public function delete($key);

	/**
	 * Removes unnecessary items from the cache by key name pattern
	 *
	 * @param string $pattern -- RegEx pattern
	 *
	 * @return int -- number of deleted items, false on error
	 */
	public function purge($pattern);

	/**
	 * deletes all data from the cache
	 * @return int
	 */
	public function clear();

	/**
	 * called before destructor (e.g. to save data to physical store if needed)
	 * @return void
	 */
	public function finish();

	/**
	 * Returns a cached value or computes it if not exists
	 *
	 * @param string $key -- the name of the cached value
	 * @param callable $compute -- the function retrieves the original value
	 * @param int $ttl -- time to live in seconds (used in set only)
	 *
	 * @return mixed -- the cached value
	 */
	public function cache($key, Callable $compute, $ttl=null);

	/**
	 * Must clean up the expired items
	 * (default ttl may be overridden, only older items will be deleted, no other items affected)
	 *
	 * @param int|null $ttl
	 *
	 * @return int -- number of items deleted
	 */
	public function cleanup($ttl=null);
}
