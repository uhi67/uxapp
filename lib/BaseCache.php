<?php


namespace uhi67\uxapp;

abstract class BaseCache extends Component implements CacheInterface {
	/** @var int $ttl -- time to live from creation in seconds. default is 0 means no expiration */
	public $ttl = 0;

	/**
	 * @param string $key -- the name of the cached value
	 * @param callable $callable -- the function retrieves the original value
	 * @param int $ttl -- time to live in seconds (used in set only)
	 *
	 * @return bool|int
	 */
	public function cache($key, Callable $callable, $ttl = null) {
		if(($value = $this->get($key)) !== null) return $value;
		return $this->set($key, $value = $value = $callable(), $ttl);
	}
}
