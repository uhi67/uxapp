<?php
/**
 * Created by PhpStorm.
 * User: uhi
 * Date: 2019. 03. 20.
 * Time: 22:06
 */

namespace uhi67\uxapp;


use DateTime;
use Exception;
use Throwable;

/**
 * Class DbLogger
 *
 * Logging into database
 *
 * ### configuration options:
 *  - tags -- array,
 * 		- Empty: logs anyway (including no tags)
 * 		- *: matches any tag: (logs only if at least one tag is specified)
 * 		- !*: excludes any tag: (logs only if no tag is specified)
 * 		- 'name': included tags: logs, if any of tags matches this (first match terminates the evaluation)
 * 		- '!name: excluded tags (does not log if any of tags matches this
 * 		- '~pattern': tags matching pattern (RegEx)
 * 		- '!~pattern': excluded patterns
 *  - levels -- array, some of [emergency, alert, critical, error, warning, notice, info, debug] to log
 * 		- Empty string or *: logs all levels
 * 		- '!name...': all, except specified levels
 * 		- '>name': all levels greater than specified (emergency, alert, critical, error, warning, notice, info, debug); also: >=, <, <=
 *  - connection  -- connection component name or DBX object
 *  - table -- name of lagging table
 *  - fieldMap -- array, maps common fields into => database fields
 * 		[ts, owner, ip, message, page, action, target, severity, tags]
 * 		- if field value is true, valu will be added to message
 * 		- if field value is false or null, the value will not be used.
 *
 * @package uhi67\uxapp
 */
class DbLogger extends UXAppLogger implements LoggerInterface {
    /** @var DBX $connection -- the database connection */
    public $connection;
    /** @var string $table -- the database table to log into */
    public $table;
    /** @var string[] $fieldMap -- the database fields to log into */
    public $fieldMap = [
        'ts' => 'ts',
        'owner' => 'owner', // id of acting user
        'target' => 'target',	// id of target object (may be null if not to be logged)
        'action' => 'action',
        'page' => 'page',
        'severity' => null,
        'tags' => null,
        'message' => 'param',
        'ip' => null,
    ];

    /**
     * @var string|array $levels -- logged levels: space-separated list or array of level names.
     * - Empty string or *: logs all levels
     * - '!name...': all, except specified levels
     * - '>name': all levels greater than specified (emergency, alert, critical, error, warning, notice, info, debug); also: >=, <, <=
     * After initialization, levels will be translated to array of level names to log
     */
    public $levels;

    /**
     * @var array|string $tags -- logged tags: space-separated list or array of tag names or tag patterns.
     * - Empty: logs anyway (including no tags)
     * - *: matches any tag: (logs only if at least one tag is specified)
     * - !*: excludes any tag: (logs only if no tag is specified)
     * - 'name': included tags: logs, if any of tags matches this (first match terminates the evaluation)
     * - '!name: excluded tags (does not log if any of tags matches this
     * - '~pattern': tags matching pattern (RegEx)
     * - '!~pattern': excluded patterns
     */
    public $tags;

    /**
     * @throws Exception
     */
    public function prepare() {
        parent::prepare();

        // Prepares levels
        if(is_string($this->levels)) {
            $l1 = substr($this->levels, 0, 1);
            if($this->levels == '' or $this->levels == '*') {
                $this->levels = $this->severities;
            } else if(preg_match('/^(<|>|<=|>=)(\w+)$/', $this->levels, $mm)) {
                $index = array_search($mm[2], $this->severities);
                $rel = $mm[1];
                $this->levels = array_filter($this->severities, function($i) use ($index, $rel) {
                    switch($rel) {
                        case '<':
                            return $i < $index;
                        case '>':
                            return $i > $index;
                        case '<=':
                            return $i <= $index;
                        case '>=':
                            return $i >= $index;
                    }
                    return false;
                }, ARRAY_FILTER_USE_KEY);
            } else {
                $not = false;
                if($l1 == '!') {
                    $not = true;
                    $this->levels = substr($this->levels, 1);
                }
                $this->levels = explode(' ', $this->levels);
                if($not) $this->levels = array_diff($this->severities, $this->levels);
            }
        }

        // prepares tags filter definition
        if(is_string($this->tags)) $this->tags = explode(' ', $this->tags);
    }

    /**
     * @throws UXAppException
     * @throws \ReflectionException
     * @throws Exception
     */
    public function init() {
        parent::init();

        $this->connection = UXApp::$app->getComponent($this->connection);
        if(!$this->connection) throw new Exception('No database connection for logger.');
        if(!$this->table) throw new Exception('No table specified for logger.');
    }

    /**
     * Logs into database
     *
     * ----
     * {@inheritDoc}
     *
     * @param string $level -- a standard level name (fatal, error, warning, security, action, info, debug)
     * @param string|array $message
     * @param array $context (user, page, action, target, tags, timestamp, ip)
     *
     * @throws Exception
     */
    public function log($level, $message, array $context = []) {
        // Late initialize
        if(is_string($this->connection)) $this->connection = UXApp::$app->getComponent($this->connection);

        $tags = ArrayUtils::getValue($context, 'tags', 'app');
        if(is_string($tags)) $tags = explode(' ', $tags);
        if(!is_array($tags)) $tags = [];
        if(in_array('sql', $tags)) return; //throw new Exception('Never log sql to database');

        // Filter severity (list of allowed levels is already prepared)
        if($this->levels && !in_array($level, $this->levels)) return;

        // filter tags
        if($this->tags && !$this->tagsMatches($tags)) return;

        // Connection is not yet initialized or Database is not yet connected
        if(!$this->connection instanceof DBX || !$this->connection->connection) {
            if(in_array($level, ['debug', 'info', 'notice'])) return;
            $logger2 = new FileLogger(['logFile'=>UXApp::$app->dataPath.'/error.log']);
            $logger2->log('fatal', 'Logger connection is not yet initialized. Backtrace and the original message follows.');
            $logger2->log('debug', (new Exception())->getTraceAsString());
            $logger2->log($level, $message, $context);
            throw new UXAppException('Logger connection is not yet initialized.', [$level, $message, $context]);
        }

        $timestamp = ArrayUtils::getValue($context, 'timestamp', new DateTime());
        if(! $timestamp instanceof DateTime) $timestamp = new DateTime($timestamp);

        // interpolate placeholders using context parameters
        $message = $this->interpolate($message, $context);
        if(is_array($message)) $message = implode("\t", $message);

        /** @var string[] $values -- values to log */
        $values = [
            'ts' => $timestamp->format('Y-m-d H:i:s'),
            'owner' => ArrayUtils::getValue($context, 'user'),
            'target' => ArrayUtils::getValue($context, 'target'),
            'action' => ArrayUtils::getValue($context, 'action'),
            'page' => ArrayUtils::getValue($context, 'page'),
            'severity' => $level,
            'tags' => $tags,
            'message' => $message,
            'ip' => ArrayUtils::getValue($context, 'ip'),
        ];
        $this->save($this->map($values));
    }

    /**
     * Maps values to log using fieldMap
     *
     * @param array $values
     *
     * @return array
     */
    public function map($values) {
        $fields = [];

        // true értékű mezők adatait a 'message' értékéhez hozzáfűzi.
        foreach($this->fieldMap as $from => $to) {
            if($to===true && !is_int($from)) {
                $v = $values[$from];
                if(is_array($v)) $v = Util::objtostr($v);
                if(!is_string($values['message'])) var_dump($values['message']);
                $values['message'] .= "\t".$v;
            }
        }

        foreach($this->fieldMap as $from => $to) {
            if($to && $to!==true) {
                if(is_int($from)) $from = $to;
                $v = $values[$from];
                if(is_array($v)) $v = Util::objtostr($v);
                $fields[$to] = $v;
            }
        }
        return $fields;
    }

    /**
     * Stores the mapped value array into the database
     *
     * @param $values
     */
    public function save($values) {
        try {
            $db = $this->connection;
            $fieldNames = implode(', ', array_map(function($item) use($db) {
                return $db->quoteName($item);
            }, array_keys($values)));

            $fieldValues = implode(', ', array_map(function($v) {
                return $this->connection->literal(is_scalar($v) || $v===null ? $v : Util::objtostr($v), true);
            }, array_values($values)));

            $tablename = $this->connection->quoteName($this->table);
            $this->connection->query("insert into $tablename ($fieldNames) values ($fieldValues)");
        }
        catch(Throwable $e) {
            try {
                UXApp::$app->error('Error logging into database. ' . $e->getMessage() . '; ' . \json_encode($values));
            }
            catch(Throwable $e) {
                echo "Unable to log error into database\n";
                echo $e->getMessage() . '; ' . \json_encode($values)."\n";
                exit;
            }
        }
    }

    /**
     * Returns true if tags satisfies $this->tags definition
     * First permissive or prohibitive pattern match terminates the evaluation.
     * Returns true if no rules defined.
     * Returns false if no matching definition (you may append a '*' last).
     *
     * - Empty: logs anyway (including no tags)
     * - *: matches any tag: (logs only if at least one tag is specified)
     * - !*: excludes any tag: (logs only if no tag is specified)
     * - 'name': included tags: logs, if any of tags matches this (first match terminates the evaluation)
     * - '!name: excluded tags (does not log if any of tags matches this)
     * - '~pattern': tags matching pattern (RegEx)
     * - '!~pattern': excluded patterns
     *
     * If no any match, returns false
     *
     * @param array $tags
     * @return bool -- to log or not to log
     */
    private function tagsMatches($tags) {
        if($this->tags === []) return true;

        foreach($this->tags as $tagDef) {
            if(substr($tagDef,0,1)=='!') {
                if($this->tagDefMatches(substr($tagDef, 1), $tags)) return false;
            }
            else {
                if($this->tagDefMatches($tagDef, $tags)) return true;
            }
        }
        return false;
    }

    /**
     * @param string $tagDef -- single pattern with optional ~
     * @param array $tags -- user specified tags
     * @return bool -- true if any of the tags matches the definition
     */
    private function tagDefMatches($tagDef, array $tags) {
        if($tagDef=='*') {
            return count($tags)>0;
        }
        if(substr($tagDef,0,1=='~')) {
            $pattern = $tagDef . '~';
            foreach($tags as $tag) {
                if(preg_match($pattern, $tag)) return true;
            }
            return false;
        }
        else {
            foreach($tags as $tag) {
                if($tagDef == $tag) return true;
            }
            return false;
        }
    }
}
