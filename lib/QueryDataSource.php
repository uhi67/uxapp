<?php
namespace uhi67\uxapp;

use Exception;
use ReflectionException;

/**
 * # Class QueryDataSource
 *
 * A data source based on a Query
 *
 * ### Using
 *
 * ```
 * $ds = new QueryDataSource([
 * 		'query' => new Query()
 * ]);
 * $list = new UList(['dataSource'=>$ds]);
 * $list->createNode($this);
 * ```
 *
 * ### All parameters
 * - **query** (Query) -- the query the datasource is based on. Must be of type 'select' or 'sql'
 * - **modelClass** (string) -- name of model class implemented by the Query (if any). Default is the Query's modelname. false to disable.
 * - **countQuery** (Query) -- the query which computes total number of items from database. Optional, default computed from $query
 * - **patternSetter** (callable) -- function($dataSource, $pattern, $params) -- called when search pattern is set. Default no effect
 */
class QueryDataSource extends Component implements DataSourceInterface {
    /** @var Query $query -- the query which fetches data from database */
    public $query;
    /** @var string $modelClass -- name of model class implemented by the Query (if any). Specify false to disable auto-using Query's one */
    public $modelClass;
    /** @var Query $countQuery -- the query which computes total number of items from database. Optional, default computed from $query */
    public $countQuery;
    /** @var callable $setPattern -- function($dataSource, $pattern, $params) -- called when search pattern is set */
    public $patternSetter;

    /**
     * @inheritdoc
     * @throws Exception
     */
    public function prepare() {
        if($this->query && !$this->countQuery) {
            $this->resetCountQuery();
        }
        if($this->modelClass === null && $this->query->modelname) $this->modelClass = $this->query->modelname;
    }

    /**
     * @throws UXAppException
     */
    public function resetCountQuery() {
        $type = $this->query->type;
        if($type == 'sql') {
            // SQL based
            $this->countQuery = new Query([
                'type' => 'sql',
                'sql' => 'select count(*) from (' . $this->query->sql . ') query',
                'params' => $this->query->params,
                'connection' => $this->query->connection,
            ]);
        } elseif($type == 'select') {
            $this->countQuery = new Query([
                'params' => $this->query->params,
                'connection' => $this->query->connection,
                'type' => 'select',
                'fields' => ['(count(*))'],
                'from' => $this->query->from,
                'joins' => $this->query->joins,
                'where' => $this->query->condition,
            ]);
        } else throw new UXAppException('Invalid query type for data source', $type);
    }
    /**
     * @param $params
     *
     * @return $this
     * @throws UXAppException
     */
    public function bind($params) {
        $this->query->bind($params);
        $this->countQuery->bind($params);
        return $this;
    }

    /**
     * Total number of items in the set
     *
     * @return int the number of items in the set
     * @throws UXAppException
     * @throws ReflectionException
     */
	public function getCount() {
		try {
			return $this->countQuery->scalar();
		}
		catch (UXAppException $e) {
			throw new UXAppException('Invalid count query (see previous exception)', $this->countQuery->toString(), $e);
		}
	}

    /**
     * Returns the specified section of items. Returns all items if all parameters are null.
     * Returns array of Models if modelClass is specified, array of arrays otherwise.
     *
     *
     * @param int|null $start -- 0-based offset
     * @param int|null $count -- number of items
     * @param array|null $orders -- order array for Query
     *
     * @return array -- the list of items
     * @throws UXAppException
     * @throws ReflectionException
     */
    public function fetch($start=null, $count=null, $orders=null) {
        if($start===null && $count===null && $orders===null) return $this->query->all($this->modelClass);
        return $this->query->offset($start)->limit($count)->orderBy($orders)->all($this->modelClass);
    }

    /**
     * @param string $pattern
     * @param array $params
     * @return QueryDataSource
     * @throws UXAppException
     */
	public function setPattern($pattern, $params=null) {
		if(is_callable($this->patternSetter)) {
			call_user_func($this->patternSetter, $this, $pattern, $params);
		}
        $this->resetCountQuery();
        return $this;
    }

    public function setModelClass($modelClass) {
        $this->modelClass = $modelClass;
    }

    public function getModelClass() {
        return $this->modelClass;
    }
}
