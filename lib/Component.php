<?php
/** @noinspection PhpIllegalPsrClassPathInspection */
/** @noinspection PhpUnused */

namespace uhi67\uxapp;

use ReflectionClass;
use ReflectionException;

/**
 * # Component
 *
 * A base class for most of the others.
 * - Implements *property* features: magic getter and setter uses getProperty and setProperty methods
 * - Configurable: constructor accepts a configuration array containing values for public properties
 *
 * @package UXApp
 * @author Peter Uherkovich
 * @copyright 2017
 * @access public
 *
 * @property-read string $shortName -- unqualified class name
 */
abstract class Component implements ComponentInterface {
    /**
     * # Component constructor
     * The default implementation does two things:
     *
     * - Initializes the object with the given configuration `$config`.
     * - Calls prepare().
     *
     * If this method is overridden in a child class, it is recommended that
     *
     * - the last parameter of the constructor is a configuration array, like `$config` here.
     * - call the parent implementation in the constructor.
     *
     * @param array|mixed $config name-value pairs that will be used to initialize the object properties
     *
     * @throws
     */
    public function __construct($config = []) {
        if (!empty($config)) {
            static::configure($this, $config);
        }
        $this->prepare();
    }

    /**
     * Initializes the object.
     * This method is invoked at the end of the constructor after the object is initialized with the
     * given configuration. Default does nothing, override it if you want to use.
     * @return void
     */
    public function prepare() {
        // This function is intentionally empty. Descendants need not call it.
    }

    /**
     * Second stage initialization in UXApp after all components are created
     */
    public function init() {
        // This function is intentionally empty. Descendants need not call it.
    }

    /**
     * Returns the value of a component property.
     *
     * @param string $name the property name
     *
     * @return mixed the property value
     * @throws UXAppException
     * @see __set()
     */
    public function __get($name) {
        if(strpos($name, '-')!==false) $name = Util::camelize($name);

        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        }

        if (method_exists($this, 'set' . $name)) {
            throw new UXAppException('Getting write-only property: ' . get_class($this) . '::' . $name);
        }

        throw new UXAppException('Getting unknown property: ' . get_class($this) . '::' . $name);
    }

    /**
     * Sets the value of a component property.
     *
     * @param string $name the property name
     * @param mixed $value the property value
     *
     * @throws UXAppException if the property is not defined or the property is read-only.
     * @see __get()
     */
    public function __set($name, $value) {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            $this->$setter($value);
            return;
        }
        if (method_exists($this, 'get' . $name)) {
            throw new UXAppException('Setting read-only property: ' . get_class($this) . '::' . $name);
        }
        throw new UXAppException('Setting unknown property: ' . get_class($this) . '::' . $name);
    }

    /**
     * Checks if a property is set: defined and not null.
     * @param string $name the property name
     * @return bool whether the named property is set
     * @see http://php.net/manual/en/function.isset.php
     */
    public function __isset($name) {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter() !== null;
        }
        return false;
    }

    /**
     * Sets a component property to be null.
     *
     * @param string $name the property name
     *
     * @throws UXAppException if the property is read only.
     * @see http://php.net/manual/en/function.unset.php
     */
    public function __unset($name) {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            $this->$setter(null);
            return;
        }
        throw new UXAppException('Unsetting an unknown or read-only property: ' . get_class($this) . '::' . $name);
    }

    /**
     * Returns a value indicating whether a property is defined for this component.
     * @param string $name the property name
     * @param bool $checkVars whether to treat member variables as properties
     * @return bool whether the property is defined
     * @see canGetProperty()
     * @see canSetProperty()
     */
    public function hasProperty($name, $checkVars = true) {
        return $this->canGetProperty($name, $checkVars) || $this->canSetProperty($name, false);
    }

    /**
     * Returns a value indicating whether a property can be read.
     * @param string $name the property name
     * @param bool $checkVars whether to treat member variables as properties
     * @return bool whether the property can be read
     * @see canSetProperty()
     */
    public function canGetProperty($name, $checkVars = true) {
        if (method_exists($this, 'get' . $name) || $checkVars && property_exists($this, $name)) {
            return true;
        }
        return false;
    }

    /**
     * Returns a value indicating whether a property can be set.
     * @param string $name the property name
     * @param bool $checkVars whether to treat member variables as properties
     * @return bool whether the property can be written
     * @see canGetProperty()
     */
    public function canSetProperty($name, $checkVars = true) {
        if (method_exists($this, 'set' . $name) || $checkVars && property_exists($this, $name)) {
            return true;
        }
        return false;
    }

    /**
     * Configures an object with the initial property values.
     *
     * @param Component $object the object to be configured
     * @param array $properties the property initial values given in terms of name-value pairs.
     *
     * @return object the object itself
     * @throws
     */
    public static function configure($object, $properties=null) {
        if($properties===null) return $object;
        Assertions::assertArray($properties);
        foreach ($properties as $name => $value) {
            if($name == 'class' && !$object->hasProperty('class')) continue;
            // Set only public properties (others will raise an exception)
            Util::setter($object, $name, $value);
        }
        return $object;
    }

    /**
     * Creates a new Component using the given configuration.
     *
     * You may view this method as an enhanced version of the `new` operator.
     * The method supports creating an object based on a class name, a configuration array or
     * an anonymous function.
     *
     * Below are some usage examples:
     *
     * ```php
     * // create an object using a class name
     * $object = Component::create('Customer');
     *
     * // create an object using a configuration array
     * $object = Component::create([
     *     'class' => Customer::class,
     *     'name' => 'Foo Baar',
     * ]);
     *
     * // create an object with two constructor parameters
     * $object = Component::create('Customer', [$param1, $param2]);
     * ```
     *
     * @param string|array|callable $type the object type. This can be specified in one of the following forms:
     *
     * - a string: representing the class name of the object to be created
     * - a configuration array: the array must contain a `class` element which is treated as the object class,
     *   and the rest of the name-value pairs will be used to initialize the corresponding object properties
     * - a configuration array: the array must contain a `0` element which is treated as the object class,
     *   and the rest of the name-value pairs will be used to initialize the corresponding object properties
     * - a PHP callable: either an anonymous function or an array representing a class method (`[$class or $object, $method]`).
     *   The callable should return a new instance of the object being created.
     *
     * @param array $params the constructor parameters
     *
     * @return object the created object
     * @throws UXAppException if the configuration is invalid.
     * @throws ReflectionException
     */
    public static function create($type, array $params = array()) {
        if (is_string($type)) {
            return static::createClass($type, $params);
        } elseif (is_array($type) && isset($type['class'])) {
            $class = $type['class'];
            unset($type['class']);
            return static::createClass($class, $type);
        } elseif (is_array($type) && isset($type[0])) {
            $class = $type[0];
            unset($type[0]);
            return static::createClass($class, $type);
        } elseif (is_array($type) && is_a(get_called_class(), Component::class, true) && get_called_class() !== Component::class) {
            $class = get_called_class();
            return static::createClass($class, $type);
        } elseif (is_callable($type, true)) {
            return call_user_func_array($type, $params);
        } elseif (is_array($type)) {
            throw new UXAppException('Object configuration must be an array containing a "class" element.');
        }
        throw new UXAppException('Unsupported configuration type: ' . gettype($type));
    }

    /**
     * @param $class
     * @param $config
     *
     * @return object
     * @throws ReflectionException
     */
    private static function createClass($class, $config) {
        $reflection = new ReflectionClass($class);
        return $reflection->newInstanceArgs(array($config));
    }

    /**
     * @inheritDoc
     */
    public function getNode() {
        return null;
    }

    /**
     * Returns class name without namespace of the caller class.
     * Callable dynamically and statically as well.
     *
     * @return string
     */
    public function getShortName() {
        if(!isset($this)) return Util::shortName(get_class());
        $reflect = new ReflectionClass($this);
        return $reflect->getShortName();
    }

    /**
     * @param array $data -- name->value pairs to set
     * @param array|null $fields -- if given, a list of properies to set (filter/map: [field, field=>mapped, ...])
     */
    public function populate($data, $fields=null) {
        if($fields) {
            foreach($fields as $field=>$remote) {
                if(is_numeric($field)) $field = $remote;
                if($this->canSetProperty($field)) $this->$field = ArrayUtils::getValue($data, $remote);
            }
        } else {
            foreach($data as $name => $value) {
                if($this->canSetProperty($name)) $this->$name = $value;
            }
        }
    }
}
