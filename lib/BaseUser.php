<?php
/** @noinspection PhpIllegalPsrClassPathInspection */
/** @noinspection PhpUnused */

namespace uhi67\uxapp;

use Exception;
use uhi67\uxml\UXMLElement;

/**
 * # BaseUser
 *
 * A logged in user
 *
 * ### Public properties
 *
 * - id
 * - name
 * - email
 * - enabled
 *
 * @package UXApp
 * @author Peter Uherkovich
 * @copyright 2014-2021
 * @property-read bool $isAuth -- true if user is authenticated
 */
class BaseUser extends Module {
    /** @var mixed $id -- Application level (internal) user id, usually integer */
    public $id;
    /** @var string $name -- User display name, from displayname or other attributes */
    public $name;
    /** @var string $email */
    public $email;
    /** @var boolean $enabled -- User login is really enabled in local database */
    public $enabled;

    public function finish() {
        $this->id = null;
    }

    /**
     * Loads identity data of current user from the session
     *
     */
    protected function loadSession() {
        $this->id = $this->parent->session->getInt('user_id');
        $this->name = $this->parent->session->get('user_name');
        $this->email = $this->parent->session->get('user_email');
        $this->enabled = $this->parent->session->getInt('user_enabled') != 0;
    }

    /**
     * Saves identity data of current user to the session
     */
    public function saveSession() {
        $_SESSION['user_id'] = $this->id;
        $_SESSION['user_name'] = $this->name;
        $_SESSION['user_email'] = $this->email;
        $_SESSION['user_enabled'] = (int)$this->enabled;
    }

    /**
     * Returns logged in user's id or null if not logged in.
     *
     * @return mixed
     */
    public function loggedIn() {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isEnabled() {
        return $this->id && $this->enabled;
    }

    /**
     * Creates the user node in the DOM
     *
     * @param UXMLElement|Component $node_parent
     *
     * @return UXMLElement
     * @throws Exception
     */
    public function createNode($node_parent) {
        if($node_parent instanceof Component) $node_parent = $node_parent->node;
        $attr = [];
        if($this->name) $attr['name'] = $this->name;
        if($this->id) $attr['id'] = $this->id;
        if($this->enabled) $attr['enabled'] = $this->enabled;
        UXApp::trace(['id' => $this->id, 'name' => $this->name], ['tags'=>'uxapp']);
        $this->node = $node_parent->addNode('user', $attr);
        return $this->node;
    }

    /**
     * Registers $id as logged in user
     *
     * @param mixed $id
     * @return bool -- success
     */
    public function login($id) {
        $this->emptyuser();
        if(is_scalar($id)) $this->id = $id;
        else foreach($id as $key=>$value) {
            $this->$key = $value;
        }
        $this->saveSession();
        return true;
    }

    /**
     * Logs out the user. Empties session data.
     */
    public function logout() {
        $this->emptyuser();
        $this->saveSession();
    }

    protected function emptyuser() {
        $this->id = null;
        $this->name = null;
        $this->email = null;
        $this->enabled = false;
    }

    /**
     * User::loadUser()
     * Loads user data into session after login
     *
     * @return void
     */
    protected function loadUser() {
        $this->loadSession();
    }

    /**
     * Checks if the user is logged in.
     *
     * @return bool
     */
    public function getIsAuth() {
        return !!$this->id;
    }
}
