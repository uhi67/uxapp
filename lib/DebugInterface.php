<?php /** @noinspection PhpUnused */

/**
 * Interface for debugger classes
 *
 */

namespace uhi67\uxapp;

/**
 * @property-read int $id -- DebugInterface must be also a Component
 */
Interface DebugInterface extends LoggerInterface {
    /**
     * @return bool -- true if trace is on for the session
     */
    public function isEnabled();
    /**
     * @return bool -- true if trace is on for the request
     */
    public function isOn();

    /**
     * Returns current trace id
     * @return string
     */
    public function getId();

    /**
     * Terminates tracing and closes tracefile
     *
     * @return void
     */
    public function finish();

    /**
     * Call this to disble trace for the current session
     */
    public function disable();
    /**
     * Call this to enable trace for the current session
     */
    public function enable();

    /**
     * Call this to disble trace for the current request
     */
    public function off();
    /**
     * Call this to enable trace for the current request
     */
    public function on();

    /**
     * Logs the backtrace into tracefile
     *
     * @param int $depth
     * @param int $limit
     */
    public function backtrace($depth=0, $limit=0);
}
