<?php /** @noinspection PhpIllegalPsrClassPathInspection */

/** @noinspection PhpUnused */

namespace uhi67\uxapp;

use DOMDocument;
use Exception;
use ReflectionClass;
use ReflectionException;
use Throwable;
use uhi67\uxml\UXMLElement;

/**
 * # Model
 * Simple relational data model with validation
 *
 * ## Using
 *
 * Derive your model classes from this class.
 * Define properties of your model overriding the following methods:
 *
 * - string {@see tableName()} -- defines table name if differs from class name. Default is class name.
 * - array {@see foreignKeys()} -- defines foreign keys, default not exists.
 * - array {@see primaryKey()} -- defines primary key(s), default id 'id'
 * - array {@see autoIncrement()} -- defines autoincrement fields (default is first primary key)
 * - array {@see rules()} -- defines validation rules. You may create custom validators where you may alter data values as well. (see also {@see BaseModel::rules()})
 * - array {@see refNames()} -- defines the visible name field of the referenced tables. Associated to foreign key names.
 * - array {@see attributeLabels()} -- defines displayable labels associated to attributes
 * - array {@see attributeHints()} -- defines displayable help texts (string or array of texts for different places) associated to attributes
 *
 * You may construct new instances in the following ways:
 *
 * - `$model = new Modelname(attributes, connection)` -- using fieldname=>value pairs
 * - `$model = Modelname::first(condition)`  -- using fieldname=>value pairs or other expressions
 * - `$models = Modelname::all(condition)` -- to create array of instances
 *
 * You may batch-operate on database using your Model class:
 *
 * - `Modelname::deleteAll(mixed $condition)`
 * - `Modelname::updateAll(array $attributes, mixed $condition)`
 *
 * where condition may be:
 *
 * - **scalar**: value of primary key
 * - **array of scalars**: values of primary key
 * - **associative with column names**: equals to values of given columns
 * - **sql options**: select, where, order, limit, offset, group will be used;
 * - **model options**: readonly, include
 *
 * You can operate on your model instances:
 *
 * - `$model->attributename` -- to get or set attribute values
 * - `$model->save()` -- saves yout model into database. New model will be inserted, old updated
 * - `$model->delete()` -- deletes the model instance from database.
 *
 * @package UXApp
 * @author Peter Uherkovich
 * @copyright 2017
 *
 * @property boolean $isNew -- true if the record is new and not saved yet.
 * @property DBX $db -- model's connection
 * @property DBX $defaultDb -- application's default connection
 * @property array $oldAttributes -- the original values of the record from the database
 */
class Model extends BaseModel {
	/** @var DBX|null database connection or null if using application's default  */
	public $_db = null;

	/** @var array $_fields -- local cache for model fieldnames indexed by model classname */
    private static $_fields = [];

	/** @var array $_fields -- local cache for table fieldnames indexed by tablename */
	private static $_databasefields = [];

    /** @var array $_types -- local cache for model field types */
    private static $_types = [];

    /** @var DBX|null $defaultConnection -- default connection for static */
    public static $defaultConnection = null;

	/** @var array $attribute values indexed by attribute names */
	protected $_attributes = [];

	/**
     * @var array|null old attribute values indexed by attribute names.
     * This is `null` if the record is new.
     */
    private $_oldAttributes = null;

    /** @var array related models indexed by the foreign key names */
    private $_related = [];

    /** @var array $_cache -- local cache for 'first' queries */
	private static $_cache = [];

	/**
	 * Returns the database connection
	 *
	 * @return DBX
	 * @throws UXAppException
	 */
    public function getDb() {
		return static::defaultConnection($this->_db);
    }

	/**
	 * @param DBX|null $connection
	 *
	 * @return DBX|null
	 * @throws UXAppException
	 */
    public function setDb(?DBX $connection) {
    	if($connection) $this->_db = $connection;
		return static::defaultConnection($this->_db);
	}

	/**
	 * ## Model constructor.
	 *
	 * @param array|null $config
	 * @param DBX|null $connection
	 *
	 * @throws UXAppException
	 */
	public function __construct($config=null, ?DBX $connection=null) {
    	if($connection) {
    		Assertions::assertClass($connection, DBX::class);
    		$this->_db = $connection;
   		}
    	parent::__construct($config);
    }

	/**
	 * Returns database tablename of the model.
	 *
	 * The default implementation uses lowercase classname for tablename
	 *
	 * @return string
	 */
	public static function tableName() {
		$reflect = new ReflectionClass(get_called_class());
		return strtolower($reflect->getShortName());
	}

	/**
	 * Must specify the autoincrement fields.
	 *
	 * Default is first of primaryKey(). You must override it with an empty array if no autoincrement field in your model.
	 *
	 * Currently only one fieldname is supported.
	 *
	 * @return array
	 */
	public static function autoIncrement() {
		$pk = static::primaryKey();
		if(count($pk)>1) return [];
		return array_slice($pk, 0, 1);
	}

	/**
	 * Must return an array of primary key fields
	 *
	 * Default is array('id')
	 *
	 * @return array of fieldnames
	 */
	public static function primaryKey() {
		return ['id'];
	}

	/**
	 * Must return an associative array with foreign key symbolic names mapped to modelnames and field mappings.
	 *
	 * Returns array of keyname => array(modelname, reference_field=>foreign_field)
	 *
	 * Default is empty array.
	 *
	 * @return array -- array of keyname => array(modelname, reference_field=>foreign_field)
	 */
	public static function foreignKeys() {
		return [];
	}

	/**
	 * Must return fieldnames of default name of referenced models
	 *
	 * @return array -- array of foreign-key-name => fieldname pairs for all foreign keys.
	 */
	public static function refNames() {
		return [];
	}

    /**
     * Returns the attribute labels.
	 *
     * Attribute labels are mainly used for display purpose.
     *
     * @return array of attribute-name => label-string
     */
    public static function attributeLabels() {
        return [];
    }

   /**
     * Returns the attribute hints associated to all or some attributes
	 *
     * The associated hint may be a string or an associative array containing keys:
	 *   - `hint`: for mouse-over action
	 *   - `comment`: short, displayed after the input field
	 *   - `help`: long, displayed in overlay on pressing [?] button, may be an array of paragraphs.
	 *   - `descr`: one row long, displayed in a separate row under the input field.
	 *   - `tooltip`: tutorial, automatically pops up if tutorial is on.
     * @return array of attribute-name => hint-string|help-array
     */
    public static function attributeHints() {
        return array();
    }

	/**
	 * Returns the list of all attribute names of the model.
	 *
	 * The default implementation will return all column names of the table associated with this Model class
	 * and the custom attributes.
	 *
	 * @param DBX|null $connection -- database connection. Default is from App.
	 *
	 * @return array list of attribute names.
	 * @throws UXAppException
	 */
    public static function attributes(DBX $connection = null) {
    	$class = static::class;
		if(isset(self::$_fields[$class])) return self::$_fields[$class];

    	$attributes = static::databaseAttributes($connection);
    	$customAttributes = static::customAttributes();
    	if($customAttributes) $attributes = array_merge($attributes, $customAttributes);

		return self::$_fields[$class] = $attributes;
    }

	/**
	 * Returns the list of database-related attribute names of the model
	 *
	 * The default implementation will return all column names of the table associated with this Model class.
	 * Skips field names beginning with underscore (_).
	 *
	 * @param DBX|null $connection -- database connection. Default is from App.
	 *
	 * @return array list of attribute names.
	 * @throws UXAppException
	 */
	public static function databaseAttributes(DBX $connection = null) {
		$connection = static::defaultConnection($connection);

		$tableName = static::tableName();
		if(isset(self::$_databasefields[$tableName])) return self::$_databasefields[$tableName];

		if($tableName=='model') throw new UXAppException('Use custom models instead of Model', $tableName);
		$metadata = $connection->getMetaData($tableName);
		if(!$metadata) throw new UXAppException("Metadata of table $tableName not found");
		$fields = array_filter(array_keys($metadata), function($name) { return substr($name,0,1) !='_'; });

		sort($fields);
		return self::$_databasefields[$tableName] = $fields;
	}

    /**
	 * Returns an array of names of non-database attributes
	 * Default is keys of customTypes
	 *
	 * @return array
	 */
	public static function customAttributes() {
		if(!($types = static::customTypes())) return [];
    	return array_keys($types);
	}

	/**
	 * ## Model::rules()
	 *
	 * Must return validation rules for fields
	 * Models are validated against rules before saving to database (save, insert, update)
	 *
	 * ### Predefined rules of Model:
	 *
	 * - all rules of {@see BaseModel::rules()} (see below)
	 * - 'unique' -- field or fieldnames have to be unique in the table
	 * - ['uniqueifnull', fieldname2] -- field has to be unique, if fieldname2 is null.
	 * - ['autoincrementValidate', fieldnames] -- replaces null to unique value
	 *
	 * {@inheritDoc}
	 *
	 * @return array
	 * @see validate()
	 */
	public static function rules() {
		return [];
	}

	/**
	 * Returns a value indicating whether the model has an attribute with the specified name.
	 *
	 * @param string $name the name of the attribute
	 * @param null $connection
	 *
	 * @return bool whether the model has an attribute with the specified name.

	 * @throws UXAppException
	 */
    public static function hasAttribute($name, $connection=null) {
    	Assertions::assertString($name);
    	if(($ca = static::customAttributes()) && in_array($name, $ca)) return true;
        return in_array($name, static::attributes($connection), true);
    }

	/**
	 * Sets given connection or gets default
	 *
	 * Returns current connection.
	 *
	 * @param DBX|null $connection
	 *
	 * @return DBX
	 * @throws UXAppException
	 */
	public static function defaultConnection(DBX $connection=null) {
		if($connection && ! $connection instanceof DBX) throw new UXAppException('Invalid connection parameter', gettype($connection));
		if(!$connection) {
			if(static::$defaultConnection) return static::$defaultConnection;
			try {
				$connection = UXApp::$app ? UXApp::$app->db : null;
			}
			catch(Throwable $e) {
				throw new UXAppException('Invalid connection', null, $e);
			}
		}
		if(!$connection) throw new UXAppException('No connection defined');
		return $connection;
	}

	/** @noinspection PhpMethodNamingConventionInspection */

	/**
	 * Returns all records as an array of Models
	 *
	 * @param array|null $condition -- fieldname=>value pairs or other expression, see Query::buildWhere
	 * @param array|string|null $orders -- optional order clause data (field, ... or [field, order],...)
	 * @param array|null $params -- optional parameters if $condition contains $1, $var parameters
	 * @param DBX|null $connection -- database connection. Default is from App.
	 * @param bool $associative -- if true, result rows will be associated for primary keys (works only for single PK)
	 * @param int|null $limit -- maximum number of results if given
	 * @param int|null $offset -- skip first n results if given
	 *
	 * @return array|null -- array of Model instances or null on failure
	 * @throws UXAppException|ReflectionException
	 */
    public static function all($condition=null, $orders=null, $params=null, $connection=null, $associative=false, $limit=null, $offset=null) {
		$connection = static::defaultConnection($connection);
    	/** @var Model $classname */
    	$classname = get_called_class();

		$joins = array();

		$joins = static::addConditionForeignkeys($joins, $connection->asExpression($condition), $connection); // add fk-s from conditions
		$joins = static::addOrdersForeignkeys($joins, $orders, $connection); // add fk-s from orders

    	$query = Query::createSelect(array(
    		'from' => $classname,
			'joins' => $joins,
			'where' => $connection->asExpression($condition),
			'orders' => $orders,
			'limit' => $limit,
			'offset' => $offset,
			'params' => $params,
			'db' => $connection
		));

	    /** @var array $values the array of rows as Models */
	    $values = $query->all(true);

		if($associative) {
			$keys = $classname::primaryKey();
			if(count($keys)!=1) throw new UXAppException('Associative result is available only if there is exactly one primary key.');
			$pk = $keys[0];
			$values = array_combine(array_map(function($row) use($pk) {
				return ArrayUtils::getValue($row, $pk);
			}, $values), $values);
		}
		return $values;
    }

	/**
	 * Returns a Query as a select records from the Model
	 *
	 * @param array|null $fields -- the fields to select, default all fields of the model (Dotted notation for joined relations, alias => fieldname, alias=>(expression))
	 * @param array|null $joins -- list of joined models (foreign keys) OR alias=>fk OR alias=>array(model, [jointype], conditions) element
	 * @param array|null $condition -- fieldname=>value pairs or other expression, see {@see DBX::buildWhere()}
	 * @param array|null $params -- optional parameters if $condition contains $1, $var parameters
	 * @param DBX|null $connection -- database connection. Default is from App. (Not used and evaluated until condition is supplied)
	 *
	 * @return Query -- select Query without order and limit
 * @throws UXAppException
	 */
    public static function createSelect($fields=null, $joins=null, $condition=null, $params=null, $connection=null) {
    	$classname = get_called_class();
    	if($condition) {
		    $connection = static::defaultConnection($connection);
    		$condition = $connection->asExpression($condition);
	    }
		return Query::createSelect($classname, $fields, $joins, $condition, null, null, null, $params, $connection);
    }

	/**
	 * ## Returns first record as Model
     *
     * Maintains a local cahce only used in this (and firstCached) function.
     * Note: Cache is default OFF. Use {@see firstCached()} with cache always on.
     * No other Model or Query function uses any cache for data.
	 *
	 * @param array|mixed $condition --
	 *   - single scalar for single primary key. Null will result null.
	 *   - array(fieldname=>literal value, ...)
	 *     - other expression, as in {@see DBX::buildExpression()}
	 * @param array $orders -- optional order clause data (field, ... or [field, dir, nulls],...) -- field may be expression in second form
	 * @param array $params -- optional parameters if $condition contains $1, $var parameters (params may be single literal, except of null)
	 * @param DBX $connection -- database connection. Default is from App.
	 * @param bool $nocache -- do not read from the cache (but refresh it)
	 *
	 * @return static -- the model instance, or null if not found
	 * @throws UXAppException|ReflectionException
	 */
    public static function first($condition, $orders=null, $params=null, $connection=null, $nocache=true) {
    	$connection = static::defaultConnection($connection);

		if($condition===null || $condition===array()) {
			UXApp::trace(['condition' => 'empty!'], ['tags'=>'uxapp', 'color'=>'#900']);
			return null;
		}

    	$classname = get_called_class();
		$cachekey = md5(serialize($condition).';'.serialize($params).';'.serialize($orders));
		if(isset(self::$_cache[$classname][$cachekey]) && !$nocache) return self::$_cache[$classname][$cachekey];

		$conditionExpression =  $connection->asExpression($condition);

		$joins = array();
		$joins = static::addConditionForeignkeys($joins, $conditionExpression, $connection); // add fk-s from conditions
		$joins = static::addOrdersForeignkeys($joins, $orders, $connection); // add fk-s from orders

    	$query = Query::createSelect($classname, null, $joins, $conditionExpression, $orders, null, null, $params, $connection);
//    	UXApp::trace(['condition', $connection->asExpression($condition)]);
//		UXApp::trace(['query', $query->finalSQL]);
		$values = $query->first();
    	if($values===false) $model = null;
    	else {
    		/** @var Model $model */
	   	 	$model = new $classname($values, $connection);
	    	$model->setOldAttributes($values);
    	}
    	if(!isset(self::$_cache[$classname])) self::$_cache[$classname] = array();
    	return self::$_cache[$classname][$cachekey] = $model;
    }

    /**
     * @throws ReflectionException
     * @throws UXAppException
     */
    public static function firstCached($condition, $orders=null, $params=null, $connection=null) {
        return static::first($condition, $orders, $params, $connection, false);
    }

	/**
	 * Adds foreign key names to joins array occurs in conditions
	 *
	 * @param array $joins -- original list of foreign keys
	 * @param array $condition -- expression
	 * @param DBX $connection
	 *
	 * @return array -- joins completed
	 * @throws UXAppException
	 */
    static function addConditionForeignkeys($joins, $condition, $connection=null) {
		$connection = static::defaultConnection($connection);
		if(is_array($condition)) {
			$cx = $connection->buildExpression($condition);
			return static::addForeignkeys($joins, $cx);
		}
		return $joins;
    }

	/**
	 * Adds foreign key names to joins array occurs in orders
	 *
	 * @param array $joins -- original list of foreign keys
	 * @param array $orders -- array of order definitions as in first() or all()
	 * @param DBX $connection
	 *
	 * @return array -- joins completed

	 * @throws UXAppException
	 */
    public static function addOrdersForeignkeys($joins, $orders, $connection=null) {
		$connection = static::defaultConnection($connection);
		if(is_array($orders)) {
			try {
				$cx = $connection->buildOrders($orders);
			}
			catch(Throwable $e) {
				throw new UXAppException('Invalid orders definition: '. Util::objtostr($orders));
			}
			return static::addForeignkeys($joins, $cx);
		}
		return $joins;
	}

    /**
     * Adds foreign key names to joins array occurs in the string
     *
     * @param array $joins -- original list of foreign keys
     * @param string $cx -- SQL fragment with "fk1"."field" patterns
     * @return array -- joins completed
     */
	static function addForeignkeys($joins, $cx) {
		$n = preg_match_all('/"(\w+)"."[^"]+"/', $cx, $mx, PREG_SET_ORDER);
		if($n) {
			$fknames = array_keys(static::foreignKeys());
			foreach($mx as $m) {
				$fk = $m[1];
				if(in_array($fk, $fknames) && !in_array($fk, $joins)) $joins[] = $fk;
			}
		}
		return $joins;
    }

	/**
	 * Deletes rows in the table using the provided conditions.
	 *
	 * For example, to delete all customers whose status is 3:
	 *
	 * ```php
	 * Customer::deleteAll('status' => 3);
	 * ```
	 *
	 * > Warning: If you do not specify any condition, this method will not delete all rows, but throws an exception.
	 *
	 * For example an equivalent of the example above would be:
	 *
	 * ```php
	 * $models = Customer::all('(status = 3)');
	 * foreach($models as $model) {
	 *     $model->delete();
	 * }
	 * ```
	 *
	 * @param string|array $condition the conditions that will be put in the WHERE part of the DELETE SQL.
	 *  - array(fieldname=>value, ...)
	 *  - literal value of single primary key
	 *  - other expression formats
	 * @param array $params the parameter values for $1 parameters or name=>value pairs for $name parameters
	 * @param DBX $connection -- database connection. Default is from App.
	 *
	 * @return int|false the number of rows deleted, or false on failure
 	 * @throws UXAppException|ReflectionException
	 */
    public static function deleteAll($condition = null, $params = array(), $connection=null) {
		$connection = static::defaultConnection($connection);
    	$query = Query::createDelete(get_called_class(), $connection->asExpression($condition), $params, $connection);
        return $query->execute();
    }

	/**
	 * Updates all matching record in database. Literal values only.
	 * To update with expressions, use {@see updateAllE()}
	 *
	 * @param array $attributes -- associative list of fieldname=>value pairs
	 * @param array|integer $condition -- condition for where part the sql
	 *  - array(fieldname=>value, ...)
	 *  - literal value of single primary key
	 *  - other expression formats
	 * @param DBX|null $connection -- database connection. Default is from App.
	 *
	 * @return integer|false -- number of affected records or false on error
 	 * @throws UXAppException|ReflectionException
	 */
    public static function updateAll($attributes, $condition, $connection=null) {
		$connection = static::defaultConnection($connection);

		// Convert values to literal expressions
    	$values = array_map(function($v) use($connection) { return $connection->literal($v); }, $attributes);

    	$query = Query::createUpdate(get_called_class(), $values, $connection->asExpression($condition), null, $connection);

    	return $query->execute();
    }

	/**
	 * Updates all matching record in database using expressions
	 *
	 * @param array $attributes -- associative list of fieldname=>expression pairs
	 * @param array $condition -- expression (see {@see Query::buildExpression}) for where part of the sql
	 * @param array $params
	 * @param DBX $connection -- database connection. Default is from App.
	 *
	 * @return integer|false -- number of affected records or false on error
	 * @throws UXAppException
	 * @throws ReflectionException
	 * @see updateAll -- to update using literal values
	 */
    public static function updateAllE($attributes, $condition, $params=null, $connection=null) {
		$connection = static::defaultConnection($connection);
    	$query = Query::createUpdate(get_called_class(), $attributes, $condition, $params, $connection);
    	return $query->execute();
    }

	/**
	 * Returns first foreign key name belongs to specified field
	 * (Returns also complex fk-s!)
	 *
	 * @param string $fieldname
	 * @return string the name of the foreign key or false if not exists
	 */
	public static function getReference($fieldname) {
    	$fks = static::foreignKeys();
    	foreach($fks as $fkname => $fkdef) {
    		if(isset($fkdef[$fieldname])) return $fkname;
    	}
    	return false;
	}

	/**
	 * Returns all foreign key name belongs to specified field
	 * (Returns also complex fk-s!)
	 *
	 * @param string $fieldname
	 * @return array the names of the foreign keys or empty array if none exists
	 */
	public static function getReferences($fieldname) {
    	$fks = static::foreignKeys();
    	$result = array();
    	foreach($fks as $fkname => $fkdef) {
    		if(isset($fkdef[$fieldname])) $result[] = $fkname;
    	}
    	return $result;
	}

	/* --------------------------------------------------------------------------------------------------------- */
	/**
	 * ## Validates model to unicity of the given field(s)
	 *
	 * Using as global validator, fieldnames will get the array of fieldname(s),
	 *
	 * Using as field validator, fieldname gets the fieldname.
	 *
	 * Returns true if any of given fields has a value of null.
	 *
	 * ## Using as global validator
	 * ```
	 *     array('unique', array('fieldname1', 'fieldname2'))
	 * ```
	 *
	 * @param string $fieldname -- for single field (null if called as multiple)
	 * @param array $fieldnames -- for multiple fields (null if called as single)
	 *
	 * @return bool -- model is valid
	 * @throws
	 */
	public function uniqueValidate($fieldname, $fieldnames=null) {
		#App::trace(['unique', $fieldname.', '.Util::objtostr($fieldnames));
		$fieldnames = $fieldnames ?: array($fieldname);
		$values = $this->namedAttributes($fieldnames);
		foreach($values as $value) if(is_null($value)) return true;
		if($this->isNew) {
			$existingInstance = static::first($values, null, null, $this->db);
			#App::trace(['cond', $values);
		} else {
			// If already saved, itself must be ignored
			$existingInstance = static::first(array_merge(
				['and'],
				$this->db->asExpression($this->namedAttributes($fieldnames)),
				[['not', [$this->db->asExpression($this->getOldPrimaryKey(true))]]]
			), null, null, $this->db);
		}
		if($existingInstance) {
			$this->setError($fieldnames, UXApp::la('uxapp', 'must be unique'));
			return false;
		}
		return true;
	}

	/**
	 * Validates model to unicity of the given field if field2 is null
	 *
	 * Use only as a field validator.
	 * Returns true if field2 has a value of not null.
	 * Returns true if all fields have a value of not null.
	 *
	 * Not equivalent with `ifValidate($fieldname, fieldname2, null, 'mandatory')`
	 *
	 * When used as global rule, the first parameter is the null-checked field.
	 *
	 * @param string|null $fieldname -- for single field (null if called as multiple)
	 * @param string|array $fieldname2 -- null-checked field on single call, fieldnames array on multiple call
	 *
	 * @return bool -- model is valid
	 * @throws
	 */
	public function uniqueifnullValidate($fieldname, $fieldname2) {
		$fieldnames = array($fieldname);
		if($fieldname === null) {
			// Multiple call
			$fieldnames = $fieldname2;
			$fieldname2 = array_shift($fieldnames);
		}

		/** @noinspection PhpVariableVariableInspection */
		$value2 = $this->$fieldname2;
		if($value2!==null) return true;

		$values = $this->namedAttributes($fieldnames);
		if(array_reduce($values, function($carry, $item) { return $carry && $item===null; }, true)) return true;
		if($this->isNew) {
			$e = static::first(array_merge($values, array($fieldname2 => null)), null, null, $this->db);
			#App::trace(['cond', $values);
		} else {
			// If already saved, itself must be ignored
			$cond = array_merge($values, array(
				$fieldname2 => null,
				array('not', $this->getOldPrimaryKey(true))
			));
			$e = static::first($cond, null, null, $this->db);
		}
		if($e) {
			$this->setError($fieldnames, UXApp::la('uxapp', 'must be unique'));
			return false;
		}
		return true;
	}

	/**
	 * Ensures unicity of fieldname depending on with other filenames when fieldname is empty
	 *
	 * Always pass, modifies value of $fieldname.
	 *
	 * @param $fieldname
	 * @param $fieldnames
	 *
	 * @return bool
	 * @throws
	 */
	public function autoincrementValidate($fieldname, $fieldnames) {
		/** @noinspection PhpVariableVariableInspection */
		$value = $this->$fieldname;
		if($value) return true;

		#App::trace(['auto', $fieldname . ', ' . Util::objtostr($fieldnames));
		$query_max = $this->createSelect(
			array(array('max()', $fieldname)),
			null,
			$this->namedAttributes($fieldnames),
			null,
			$this->db
		);
		$max = $query_max->scalar(0);
		/** @noinspection PhpVariableVariableInspection */
		$this->$fieldname = $max+1;
		return true;
	}

	/**
	 * Field validator only
	 *
	 * Checks whether value references an existing record in the referenced table.
	 *
	 * If reference is not specified, the first matching foreignkey is used.
	 * If reference is a foreign key, the foreignkey is used if matching.
	 * If reference is an array, [Model, foreign-field] form is used.
	 *
	 * @param string $fieldname
	 * @param mixed $reference
	 *
	 * @return bool
	 * @throws UXAppException|ReflectionException
	 */
	public function referencesValidate($fieldname, $reference=null) {
		/** @noinspection PhpVariableVariableInspection */
		$value = $this->$fieldname;
		if($value===null) return true;

		//  fk example: 'block' => [Resource::class, 'block_id'=>'id'],
		UXApp::trace(['reference' => $reference], ['tags'=>'uxapp']);
		$fkval = null;
		if($reference===null) {
			$fkval = ArrayUtils::array_usearch(static::foreignKeys(), $fieldname, function($item, $value) {
				return array_key_exists($value, $item);
			}, true);
		}
		else if(is_string($reference)) $fkval = ArrayUtils::getValue(static::foreignKeys(), $reference);
		if(!is_array($reference)) {
			if(!$fkval) throw new UXAppException('Foreign key not found for field ' . $fieldname);
			$reference = array($fkval[0], $fkval[$fieldname]);
		}

		$r = Query::createSelect($reference[0], array('id'))->where(array($reference[1] => $value))->setDb($this->db)->first();
		UXApp::trace(['r' => $r], ['tags'=>'uxapp']);
		if($r===false) return $this->setError($fieldname, UXApp::la('uxapp', 'reference to a non-existing record'));
		return true;
	}

	/**
	 * Returns the value of a property
	 * Attributes and related objects can be accessed like properties.
	 *
	 * Do not call this method directly as it is a PHP magic method that
	 * will be implicitly called when executing `$value = $component->property;`.
	 *
	 * @param string $name the property name
	 *
	 * @return mixed the property value

	 * @throws UXAppException if the property is not defined
	 * @throws ReflectionException
	 */
    public function __get($name) {
        // Retrieved attribute
		if (isset($this->_attributes[$name]) || array_key_exists($name, $this->_attributes)) {
            return $this->_attributes[$name];
        }

		// Existing but not loaded attribute
		if ($this->hasAttribute($name, $this->getDb())) {
			$getter = 'get' . $name; // function names are case-insensitive in PHP!
			if (method_exists($this, $getter)) {
				return $this->$getter();
			}
			else return null;
  		}

  		// Retrieved relation
        if (isset($this->_related[$name]) || array_key_exists($name, $this->_related)) {
            return $this->_related[$name];
        }
        // Existing foreign-key
        if (($foreignKeyDefinition = static::foreignKey($name))) {
        	$classname = array_shift($foreignKeyDefinition);
        	$primaryKeyValues = array();
        	$valid = false;
        	foreach($foreignKeyDefinition as $lf=>$ff) {
        		if(($primaryKeyValues[$ff] = $this->getAttribute($lf)) !== null) $valid=true;
       		}
        	if(!$valid) return null;

			if(empty($primaryKeyValues)) throw new UXAppException('Empty condition', $primaryKeyValues);

			// UXApp::trace([$name, $primaryKeyValues]);
        	$result = call_user_func(array($classname, 'first'), $primaryKeyValues, null, null, $this->db);
        	if(is_null($result)) UXApp::trace(['relation is null'=>$name],['tags'=>'uxapp']);
            return $result ? $this->_related[$name] = $result : null;
        }

        // Reference composition (e.g. 'ref1.name')
        if(strpos($name, '.')) {
        	$names = explode('.', $name);
        	$name0 = array_shift($names);
        	if(isset($this->_related[$name0]) || array_key_exists($name0, $this->_related) || static::foreignKey($name0)) {
        		$ref = $this->__get($name0);
                /** @var Model $ref */
        		return $ref ? $ref->getAttribute(implode('.', $names)) : null;
        	}
       	}

        // Other
		return parent::__get($name);
    }

	/**
	 * Sets the value of a Model attribute via PHP setter magic method.
	 *
	 * @param string $name property name
	 * @param mixed $value property value
	 *

	 * @throws UXAppException
	 */
    public function __set($name, $value) {
		$db = $this->getDb();
        if (static::hasAttribute($name, $db)) {
        	$type = static::attributeType($name, $db);
        	if($value==='' && in_array($type, array('integer', 'date', 'time', 'DateTime', 'Macaddr', 'Inet'))) $value=null;
        	if($value==='' && in_array($type, array('string', 'xml')) && ($rules = $this->getRule($name)) && !in_array('mandatory', $rules)) $value=null;
            $this->_attributes[$name] = $value;

        } else {
			parent::__set($name, $value);
        }

		// delete cached related model on changing fk field
		$this->refreshRelated(array($name));
    }

	/**
	 * Returns the list of all attribute 'uxml' types of the model (name=>type)
	 *
	 * This default implementation will return types from database table metadata and custom types
	 *
	 * ### The 'uxml' type names:
	 * - basic php types (lowercase) Default is 'string'
	 * - 'xml' denotes xml valid string
	 * - php class names (Uppercase, eg. DateTime)
	 * - date (denotes a DateTime without time)
	 * - time (denotes a DateTime without date or integer in sec)
	 *
	 * @param DBX|null $connection
	 *
	 * @return array -- list of attribute name=>type associations

	 * @throws UXAppException
	 */
    public static function attributeTypes($connection = null) {
    	$class = static::class;
    	if(isset(self::$_types[$class])) return self::$_types[$class];

		$types = static::databaseAttributeTypes($connection);
		$customTypes = static::customTypes();
		if($customTypes) $types = array_merge($types, $customTypes);

        return self::$_types[$class] = $types;
    }

	/**
	 * If overrided, may return the 'uxml' types of non-database fields
	 *
	 * @see Model::attributeTypes()
	 * @return array|null
	 */
    public static function customTypes() {
    	return null;
	}

	/**
	 * Returns the list of all attribute types of the model (name=>type)
	 *
	 * This default implementation will return types from database table metadata and custom types
	 *
	 * ### Valid type names:
	 * - basic php types (lowercase) Default is 'string'
	 * - 'xml' denotes xml valid string
	 * - php class names (Uppercase, eg. DateTime)
	 * - date (denotes a DateTime without time)
	 * - time (denotes a DateTime without date or integer in sec)
	 *
	 * @param DBX|null $connection
	 *
	 * @return array -- list of attribute name=>type associations

	 * @throws UXAppException
	 */
	public static function databaseAttributeTypes($connection = null) {
		$connection = static::defaultConnection($connection);
		$tableName = static::tableName();

		if($tableName=='model') throw new UXAppException('Use custom models instead of Model', $tableName);
		return array_map(function($v) use($connection) { return $connection->mapType($v['type']); }, $connection->table_metadata($tableName));
	}

	/**
	 * Returns the type of the named attribute.
	 * This default Model implementation uses result of attributeTypes (which is cached based on caller classname).
	 * The difference from inherited method is using $connection.
	 *
	 * @param string $name -- the attribute name
	 * @param DBX $connection -- the database connection for retrieving types from database table
	 *
	 * @return string - the common typename of the attribute

	 * @throws UXAppException if attribute nor exists
	 * @see attributeTypes()
	 *
	 */
    public static function attributeType($name, $connection=null) {
    	if(static::hasAttribute($name, $connection)) {
    		$types = static::attributeTypes($connection);
    		return ArrayUtils::getValue($types, $name, 'string');
   		}
    	$model = static::class;
    	throw new UXAppException("$model has no attribute '$name'");
    }

	/**
	 * Returns the type of the named attribute.
	 * The live version allows instance-based inheritance (acces to database connection property)
	 *
	 * @param string $name - the attribute name
	 *
	 * @return string - the common typename of the attribute (@see self::attributeTypes())

	 * @throws UXAppException if attribute not exists
	 */
    public function attributeTypeLive($name) {
    	return static::attributeType($name, $this->db);
    }

	/**
	 * Returns the foreign key definition for a name or false if not exists.
	 *
	 * @param string $name
	 * @return array|boolean array(foreign_modelname, main_field=>foreign_field, ...)
	 */
    public static function foreignKey($name) {
    	$fks = static::foreignKeys();
    	return $fks[$name] ?? false;
    }

    /**
     * Returns the text label for the specified attribute.
     * @param string $attribute the attribute name
     * @return string the attribute label
     * @see attributeLabels()
     */
    public static function attributeLabel($attribute) {
        $labels = static::attributeLabels();
        return $labels[$attribute] ?? $attribute;
    }

	/**
	 * ## Returns attribute values.
	 *
	 * @param array $names list of attributes whose value needs to be returned.
	 * Defaults to null, meaning all attributes listed in attributes() will be returned.
	 * If it is an array, only the attributes in the array will be returned.
	 *
	 * @return array attribute values in associative array (name => value).

	 * @throws UXAppException
	 */
    public function namedAttributes($names = null) {
        $values = array();
        if ($names === null) {
            $names = $this->attributes();
        }
        Assertions::assertArray($names);
        foreach ($names as $name) {
			Assertions::assertString($name);
			/** @noinspection PhpVariableVariableInspection */
			$values[$name] = $this->$name;
        }
        return $values;
    }

	/**
	 * ## Sets the attribute values in a massive way
	 *
	 * Skips unknown attributes without error.
	 * Does not refresh preloaded referenced models.
	 *
	 * @param array $values attribute values (name => value) to be assigned to the model.
	 * @return static
	 *
	 * @throws UXAppException
	 * @see refreshRelated()
	 * @see attributes()
	 */
    public function setAttributes($values) {
        if (is_array($values)) {
            $attributes = array_flip($this->attributes());
            foreach ($values as $name => $value) {
                if (isset($attributes[$name])) {
					/** @noinspection PhpVariableVariableInspection */
					$this->$name = $value;
                }
            }
        }
        return $this;
    }

	/**
	 * Returns the list of fields that should be returned by default by toArray() when no specific fields are specified.
	 *
	 * A field is a named element in the returned array by toArray().
	 *
	 * This method should return an array of field names or field definitions.
	 * If the former, the field name will be treated as an object property name whose value will be used
	 * as the field value. If the latter, the array key should be the field name while the array value should be
	 * the corresponding field definition which can be either an object property name or a PHP callable
	 * returning the corresponding field value. The signature of the callable should be:
	 *
	 * ```php
	 * function ($model, $field) {
	 *     // return field value
	 * }
	 * ```
	 *
	 * For example, the following code declares four fields:
	 *
	 * - `email`: the field name is the same as the property name `email`;
	 * - `firstName` and `lastName`: the field names are `firstName` and `lastName`, and their
	 *   values are obtained from the `first_name` and `last_name` properties;
	 * - `fullName`: the field name is `fullName`. Its value is obtained by concatenating `first_name`
	 *   and `last_name`.
	 *
	 * ```php
	 * return [
	 *     'email',
	 *     'firstName' => 'first_name',
	 *     'lastName' => 'last_name',
	 *     'fullName' => function ($model) {
	 *         return $model->first_name . ' ' . $model->last_name;
	 *     },
	 * ];
	 * ```
	 *
	 * The default implementation of this method returns attributes() indexed by the same attribute names.
	 *
	 * @return array the list of field names or field definitions.

	 * @throws UXAppException
	 *@see toArray()
	 */
    public static function fields() {
        $fields = static::attributes();
        return array_combine($fields, $fields);
    }

	/**
	 * Converts fieldname=>value pairs to fieldname=>expression
	 * In Models, array(fieldname=>value, ...) is the condition format, but in the Query builder, the array(fieldname=>expression, ...)
	 * So value-based conditions must be converted before calling Query methods.
	 * Other condition formats not affected.
	 * - fieldname => value will converted to fieldname => 'value'
	 * - fieldname => null will converted to array('null', fieldname)
	 *
	 * @param array $condition
	 *
	 * @return array
	 */
   	public function asExpression($condition) {
   		return $this->db->asExpression($condition);
   	}

	/**
	 * Returns the named related model instance.
	 *
	 * @param string $name the foreign key name
	 * @param boolean $failsafe -- does not throw exception on invalid key, simply returns null
	 *
	 * @return Model the related model object. `null` if the relation not exists or has null value.

	 * @throws UXAppException on invalid primary key value type
	 */
    public function getRelation($name, $failsafe=false) {
  		// Retrieved relation
  			// isset() does not return TRUE for array keys that correspond to a NULL value, while array_key_exists() does.
  			// however, isset() has much better performance
        if (isset($this->_related[$name]) || array_key_exists($name, $this->_related)) {
            return $this->_related[$name];
        }
        // Existing foreign-key
        if (($foreignKeyDefinition = static::foreignKey($name))) {
        	$classname = array_shift($foreignKeyDefinition);
        	$primaryKeyValues = array();
        	/** $isnull if primary key has only null values */
        	$isnull = true;
        	foreach($foreignKeyDefinition as $lf=>$ff) {
        		$value = $this->getAttribute($lf);
        		if(!class_exists($classname)) throw new UXAppException("Class '$classname' does not exist");
        		if($value!==null && !call_user_func([$classname, 'checkFieldType'], $ff, $value)) {
        			if($failsafe) return null;
					throw new UXAppException('Invalid value for primary key', $value);
				}
        		if(($primaryKeyValues[$ff] = $value)!==null) $isnull=false;
       		}
            $result = $isnull ? null : call_user_func(array(ucfirst(strtolower($classname)), 'first'), $primaryKeyValues, null, null, $this->db);
            if($result) $this->_related[$name] = $result;
            if(!is_null($result) && !($result instanceof Model)) throw new UXAppException('Invalid class '.get_class($result));
            #App::trace(['related', $result);
            return $result;
        }
        return null;
    }

	/**
	 * Returns a value indicating whether the given record is the same as the current one.
	 * The comparison is made by comparing the table names and the primary key values of the two records.
	 * If one of the records isNew|is new they are also considered not equal.
	 *
	 * @param Model $record record to compare to
	 *
	 * @return bool whether the two records refer to the same row in the same database table.
	 */
    public function equals($record) {
        if ($this->isNew || $record->isNew) {
            return false;
        }
        return static::tableName() === $record->tableName() && $this->primaryKey() === $record->primaryKey();
    }

    /**
     * Sets the old attribute values.
     * All existing old attribute values will be discarded.
     * @param array|null $values old attribute values to be set.
     * If set to `null` this record is considered to be new.
     */
    public function setOldAttributes($values) {
        $this->_oldAttributes = $values;
    }

	/**
	 * Gets the old attribute values.
	 * The old values are retrieved from database, and not overwritten yet by actual values with an update()
	 * If the record is new, null value will be returned
	 */
	public function getOldAttributes() {
		return $this->_oldAttributes;
	}

	/**
     * Returns the old value of the named attribute.
     * If this record is the result of a query and the attribute is not loaded,
     * `null` will be returned.
     * @param string $name the attribute name
     * @return mixed the old attribute value. `null` if the attribute is not loaded before
     * or does not exist.
     * @see hasAttribute()
     */
    public function getOldAttribute($name) {
        return $this->_oldAttributes[$name] ?? null;
    }

	/**
	 * Sets the old value of the named attribute.
	 *
	 * @param string $name the attribute name
	 * @param mixed $value the old attribute value.
	 *

	 * @throws UXAppException if the named attribute does not exist.
	 * @see hasAttribute()
	 */
    public function setOldAttribute($name, $value) {
        if (isset($this->_oldAttributes[$name]) || $this->hasAttribute($name, $this->db)) {
            $this->_oldAttributes[$name] = $value;
        } else {
            throw new UXAppException('Model does not have no attribute named "' . $name . '".', get_class($this));
        }
    }

	/**
	 * Deletes a Model record without considering transaction.
	 *
	 * @return int|false the number of rows deleted, or `false` if the deletion is unsuccessful for some reason.
	 * Note that it is possible the number of rows deleted is 0, even though the deletion execution is successful.
	 * The last values of attributes will be still available after deletion.
 	 * @throws UXAppException
	 * @throws UXAppException|ReflectionException
	 */
    public function delete() {
        $condition = $this->getOldPrimaryKey(true);
        $result = static::deleteAll($condition, null, $this->db);
        $this->setOldAttributes(null);
        return $result;
    }

	/**
	 * Saves the current record with or without validation
	 *
	 * This method will call {@see insert()} when {@see isNew} is `true`, or {@see update()}
	 * when {@see isNew} is `false`.
	 *
	 * For example, to save a customer record:
	 *
	 * ```php
	 * $customer = new Customer; // or $customer = Customer::findOne($id);
	 * $customer->name = $name;
	 * $customer->email = $email;
	 * $customer->save();
	 * ```
	 *
	 * ## Validation
	 * - validate=false: no validation
	 * - validate=true: validate all fields
	 * - validate=1: validate only listed fields
	 * - validate=array(): validate specified fields
	 *
	 * @param array $attributeNames list of attribute names that need to be saved. Defaults to null,
	 * meaning all attributes that are loaded from DB will be saved.
	 * @param boolean|int $validate -- if validation should be performed, or which fields.
	 *
	 * @return bool whether the saving succeeded
	 * @throws UXAppException
	 * @throws ReflectionException
	 */
    public function save($attributeNames = null, $validate=1) {
    	if(is_array($attributeNames) && $validate===1) $validate = $attributeNames;
        if ($this->getIsNew()) {
            return $this->insert($attributeNames, $validate);
        } else {
            return $this->update($attributeNames, $validate) !== false;
        }
    }

	/**
	 * Saves the changes to this active record into the associated database table.
	 *
	 * For example, to update a customer record:
	 *
	 * ```php
	 * $customer = Customer::findOne($id);
	 * $customer->name = $name;
	 * $customer->email = $email;
	 * $customer->update();
	 * ```
	 *
	 * Note that it is possible the update does not affect any row in the table.
	 * In this case, this method will return 0. For this reason, you should use the following
	 * code to check if update() is successful or not:
	 *
	 * ```php
	 * if ($customer->update() !== false) {
	 *     // update successful
	 * } else {
	 *     // update failed
	 * }
	 * ```
	 *
	 * @param array $attributeNames list of attribute names that need to be saved. Defaults to null,
	 * meaning all attributes that are loaded from DB will be saved. If empty array, no operation will be performed, and returns 0.
	 * Only changed (dirty) attributes will be included, if there's no any, no operation will be performed, and returns 0.
	 * @param boolean|int|array $validate -- if validation should be performed. if an array specified, validates only specified fields.
	 *
	 * @return int|false the number of rows affected
	 * @throws UXAppException in case update failed.
	 * @throws ReflectionException
	 */
    public function update($attributeNames = null, $validate = true) {
        if($validate) {
        	if(!$this->validate(is_array($validate) ? $validate : null)) return false;
        }
        if($attributeNames===array()) return 0;

        $values = $this->getDirtyAttributes($attributeNames);
        if($values===array()) return 0;
        $condition = $this->getOldPrimaryKey(true);
        $rows = static::updateAll($values, $condition, $this->db);

        foreach ($values as $name => $value) {
            $this->_oldAttributes[$name] = $value;
        }
        return $rows;
    }

	/**
	 * ## Updates the specified attributes.
	 *
	 * This method is a shortcut to update() when only a small set of attributes need to be updated.
	 *
	 * You may specify the attributes to be updated as name list or name-value pairs.
	 * If the latter, the corresponding attribute values will be modified accordingly.
	 * The method will then save the specified attributes into database.
	 *
	 * @param array $attributes the attributes (names or name-value pairs) to be updated
	 *
	 * @return int the number of rows affected.
	 * @throws
	 */
    public function updateAttributes($attributes) {
        $attrs = array();
        foreach ($attributes as $name => $value) {
            if (is_int($name)) {
            	Assertions::assertString($value);
                $attrs[] = $value;
            } else {
				/** @noinspection PhpVariableVariableInspection */
				$this->$name = $value;
                $attrs[] = $name;
            }
        }

        $values = $this->getDirtyAttributes($attrs);
        if (empty($values) || $this->isNew) {
            return 0;
        }

        $rows = static::updateAll($values, $this->getOldPrimaryKey(true), $this->db);

        foreach ($values as $name => $value) {
            $this->_oldAttributes[$name] = $this->_attributes[$name];
        }

        return $rows;
    }

    /**
     * Flushes given or all cached related model.
     * If no relations are given, all related model will be flushed.
     * An array of relation names or referring field names may be given.
     * If empty array is given, no data will be flushed.
     *
     * @param array $relations -- names of relations or field names to flush
     * @return void
     */
    public function refreshRelated($relations = null) {
    	if($relations===null) {
    		$this->_related = array();
    	}
    	/** $fks array of relation-name => array(modelname, reference_field=>foreign_field) definition items */
    	$fks = $this->foreignKeys();
    	foreach($relations as $relation) {
    		if(array_key_exists($relation, $this->_related)) {
    			unset($this->_related[$relation]);
   			}
   			else {
   				foreach($fks as $fk=>$fkd) {
   					// May be more then one
   					if(array_key_exists($relation, $fkd)) unset($this->_related[$fk]);
   				}
   			}
    	}
    }

    /**
     * Returns the old primary key value(s).
     * This refers to the primary key value that is populated into the record
     * after executing a find method (e.g. first()).
     * The value remains unchanged even if the primary key attribute is manually assigned with a different value.
     *
     * @param bool $asArray whether to return the primary key value as an array. If `true`,
     * the return value will be an array with column name as key and column value as value.
     * If this is `false` (default), a scalar value will be returned for non-composite primary key.
     *
     * @return mixed the old primary key value. An array (column name => column value) is returned if the primary key
     * is composite or `$asArray` is `true`. A string is returned otherwise (null will be returned if
     * the key value is null).
     * @throws UXAppException if the model does not have a primary key
     *@property mixed The old primary key value. An array (column name => column value) is
     * returned if the primary key is composite. A string is returned otherwise (null will be
	 * returned if the key value is null).
     */
    public function getOldPrimaryKey($asArray = false) {
        $keys = $this->primaryKey();
        if (empty($keys)) {
            throw new UXAppException('Model does not have a primary key.', get_class($this));
        }
        if (!$asArray && count($keys) === 1) {
            return $this->_oldAttributes[$keys[0]] ?? null;
        } else {
            $values = array();
            foreach ($keys as $name) {
                $values[$name] = $this->_oldAttributes[$name] ?? null;
            }
            return $values;
        }
    }

	/**
	 * Returns the attribute values that have been modified since they are loaded or saved most recently.
	 * The comparison of new and old values is made for identical values using `===`.
	 * If you want all fields to be returned, use setOldAttributes(null) before.
	 *
	 * TODO: compare processed values (old is canonized, current is raw)
	 *
	 * Counts only database attributes.
	 *
	 * @param string[]|null $names the names of the attributes whose values may be returned, null = all
	 *
	 * @return array the changed name-value pairs

	 * @throws UXAppException
	 */
    public function getDirtyAttributes($names = null) {
		$databaseAttributes = static::databaseAttributes($this->db);
		Assertions::assertArray($databaseAttributes);
        if ($names === null) {
            $names = $databaseAttributes;
        } else {
			Assertions::assertArray($names);
			$names = array_intersect($names, $databaseAttributes);
		}
        $names = array_flip($names);
	    #App::trace(['names', $names);
	    #App::trace(['old', $this->_oldAttributes);
	    #App::trace(['attr', $this->_attributes);
        $attributes = array();
        if ($this->_oldAttributes === null) {
            foreach ($this->_attributes as $name => $value) {
                if (isset($names[$name])) {
                    $attributes[$name] = $value;
                }
            }
        } else {
            foreach ($this->_attributes as $name => $value) {
                if (isset($names[$name]) && (!array_key_exists($name, $this->_oldAttributes) || $value !== $this->_oldAttributes[$name])) {
                    $attributes[$name] = $value;
                }
            }
        }
        return $attributes;
    }

    /**
     * Returns a value indicating whether the current record is new.
     * @return bool whether the record is new and should be inserted when calling save().
     */
    public function getIsNew() {
        return $this->_oldAttributes === null;
    }

    /**
     * Sets the value indicating whether the record is new.
     * @param bool $value whether the record is new and should be inserted when calling save().
     * @see getIsNew()
     */
    public function setIsNew($value) {
        $this->_oldAttributes = $value ? null : $this->_attributes;
    }

	/**
	 * Inserts a row into the associated database table using the attribute values of this record.
	 *
	 * Only the changed attribute values will be inserted into database.
	 * If the table's autoincrement key is `null` during insertion, it will be populated with the actual value after insertion.
	 *
	 * For example, to insert a customer record:
	 *
	 * ```php
	 * $customer = new Customer;
	 * $customer->name = $name;
	 * $customer->email = $email;
	 * $customer->insert();
	 * ```
	 *
	 * @param array $attributes list of attributes that need to be saved. Defaults to `null` = all fields
	 * @param bool|array $validate -- true for validate all fields, array to specify fields to validate
	 *
	 * @return bool the record is inserted successfully.
	 * @throws UXAppException in case insert failed.
	 * @throws Exception
	 */
    public function insert($attributes = null, $validate = true) {
	    if($validate) {
		    if(!$this->validate(is_array($validate) ? $validate : null)) return false;
	    }
        /** $values the changed fieldname=>value pairs */
        $values = $this->getDirtyAttributes($attributes);

        /** $aa autoincrement primary key field names */
		$aa = static::autoIncrement();
        // If $attributes is null, exclude autoincrement field(s) containing null values
        if(is_null($attributes)) {
        	foreach($aa as $ai) {
        		if(array_key_exists($ai, $values) && $values[$ai]===null) {
        			unset($values[$ai]);
       			}
       		}
		}

        $query = Query::createInsert(get_class($this), array_keys($values), array(array_values($values)), $this->getDb());
        try {
	        $query->execute($this->db);
        }
        catch(Throwable $e) {
        	throw new Exception('Insert query failed: '.$query->toString(). "\n" .$e->getMessage(), 0, $e);
        }

        // Populate autoincrement fields
		if(!empty($aa)) {
            if(count($aa)==1) {
                $auto = $aa[0]; // Only one ai field is supported
                if(!array_key_exists($auto, $values)) {
                    $id = $this->db->last_id($this->tableName(), $auto);
                    $this->setAttribute($auto, $id);
                    $values[$auto] = $id;
                }
            }
		}
        $this->setOldAttributes($values);
        return true;
    }

	/**
	 * ## Model::createNode()
	 * Creates an XML node from the model under parent node
	 * ### Options:
	 * - **associations**:
	 *      - If associations is true, associated tables will be included (using foreign key index names!)
	 *      - If associations is false or empty array, no associated tables will be included.
	 *      - If associations is an array, the given associated foreign keys will be included
	 *      - If associations is an associative array, the 'key' foreign keys will be included with their 'value' fields.
	 *
	 *            In this case, 'nodeName' may specify the generated node name (default is association name)
	 *            = 'value' may be an associative array to map fields to another attribute name.
	 *            In the value array:
	 *
	 *           - 'nodeName' may define the name of subnode
	 *           - key '_nodename' defines subnode field
	 *           - key '__content' will create the content of the child node
	 *
	 * - **nodeName**: the name of created node, default is model name
	 * - **attributes**: the attributes to generate, default is null = use fields(), or associative array to map attributes (alias=>attribute)
	 * - **contentField**: name of attribute to include in node content (default none) (content field is excluded from attributes)
	 * - **contentValue**: value of content, effective only if contentField is not given or has no value.
	 * - **subnodeFields**: name of attributes to include in subnodes instead of attributes (or alias=>fieldname pairs). xml fields will include nativ xml values
	 * - **extra**: extra attributes [name, name=>value, name=>function(item,index), name=>array...] see {@see BaseModel::createNode()}
	 *        - name with numeric index: adds the name property
	 *        - key => value: adds value constant with key as attribute name
	 *        - function(index, item): callback
	 *        - array: adds (sub-)property using getvalue
	 *
	 * Explicit declared and same-name associations are recursive.
	 *
	 * @see BaseModel::createNode()    for basic options
	 *
	 * @param UXMLElement|Component $node_parent
	 * @param array $options
	 *
	 * @return UXMLElement -- the created node (UXMLElement)
	 * @throws
	 */
	public function createNode($node_parent, $options= []) {
		if($node_parent instanceof DOMDocument) $node_parent = $node_parent->documentElement;
		if($node_parent instanceof Component) $node_parent = $node_parent->node;
		$node = parent::createNode($node_parent, $options);

		$associations = ArrayUtils::getValue($options, 'associations', false);
		// using associated tables
		 if($associations) {
		 	if(!is_array($associations)) $associations = array_keys(static::foreignKeys());

	 		// Unified to fkname=>value format (value may be simply true)
			$association_keys = array_map(function($k, $v) {
	 			return is_integer($k) ? $v : $k;
	 		}, array_keys($associations), array_values($associations));
	 		$association_values = array_map(function($k, $v) {
	 			return is_integer($k) ? true : $v;
	 		}, array_keys($associations), array_values($associations));
	 		$associations1 = array_combine($association_keys, $association_values);

		 	foreach($associations1 as $key=>$value) {
	 			$association = $key;
	 			$fields = $value;
		 		$nodename = ArrayUtils::fetchValue($fields, 'nodeName', $association);
	 			$relatedModel = $this->getRelation($association, true); // related object
	 			if($relatedModel) { // Do nothing on null subelement
		 			#App::trace([$association, $relatedModel->toArray());
		 			if(is_null($fields) || $fields===true) $fields = $relatedModel->fields();
					if(!is_array($fields)) $fields = array($fields);

			 		// Explicit named associations are recursive with the same name.
			 		/*$associations_keys2 =*/ array_filter($association_keys, function($k) use($relatedModel) {
			 			return $relatedModel::foreignKey($k);
			 		});
				    #$associations2 = array_intersect_key($associations1, array_flip( $associations_keys2));
					try {
						$relatedModel->createNode($node, array(
							'nodeName' => $nodename,
							'attributes' => $fields,
							#'associations' => $associations2, // TODO: ez nem jó
						));
					}
					catch(Throwable $e) {
						Throw new UXAppException("Valami nem stimmel a $nodename mezőivel", $fields, $e);
					}
		 		}
		 	}
		 }
		return $node;
	}

	/**
	 * Resolve a reverse foreign key relation
	 * get all related records referring to this record.
	 *
	 * @param string $modelname -- Classname of referer record
	 * @param string $reference_name -- name of foreign key of the refer
	 * @param mixed $order -- optional order of result models (single string or array)
	 *
	 * @return Model[]|null -- array of Models or null on failure
	 * @throws UXAppException
	 * @throws ReflectionException
	 */
	public function getReferers(string $modelname, string $reference_name, $order=null) {
		$query = $this->getRefererQuery($modelname, $reference_name);
		if($order) $query->orderBy($order);
		return $query->all(true);
	}

	/**
	 * Resolve a reverse foreign key relation
	 * get query of related records referring to this record.
	 *
	 * @param string|Model $modelname -- Classname of referer record
	 * @param string $reference_name -- name of foreign key of the refer
	 *
	 * @return Query|null -- array of Models or null on failure
	 * @throws UXAppException
	 */
	public function getRefererQuery($modelname, string $reference_name) {
		/** @type Model $modelname */
		/** @var array|bool $fk */
		$fk = $modelname::foreignKey($reference_name);
		if($fk===false) throw new UXAppException('Invalid reference name `' . $reference_name.'`');
		$referred = array_shift($fk); // Redundant, must be same as $this model
		$targetClass = get_called_class();
		if(!(is_a($targetClass, $referred, true))) throw new UXAppException("Invalid reference ($referred) for ".$modelname.' should be '.get_called_class());
		//$condition = array_combine(array_keys($fk), array_values($this->namedAttributes(array_values($fk))));

		$condition = []; $params = [];
		foreach($fk as $k=>$v) {
			$condition[$k] = '$'.$k;
			$params[$k] = $this->$v;
		}
		return $modelname::createSelect()->where($condition)->bind($params)->setDb($this->db);
	}

	/**
	 * Returns translatable validation rule message for client validation
	 *
	 * @param string $rule_name
	 *
	 * @return string|null
	 */
	static public function ruleMessage($rule_name) {
		$rulemessages = array(
			'unique' => 'must be unique',
		);
		return $rulemessages[$rule_name] ?? parent::ruleMessage($rule_name);
	}

	/**
	 * Checks if value fits to type of fieldname
	 * No side effects, no error message
	 * Purpose is only ensure syntax of SQL select conditions
	 *
	 * @param string $fieldname
	 * @param mixed $value
	 * @param DBX|null $connection
	 *
	 * @return boolean
 * @throws UXAppException
	 */
    public static function checkFieldType(string $fieldname, $value, DBX $connection=null) {
    	$typename = static::attributeType($fieldname, $connection);
    	$r = static::checkType($typename, $value);
    	if(!$r) UXApp::trace(['type' => $typename], ['tags'=>'uxapp']);
    	return $r;
    }

    /**
     * Returns name of the human-readable name field of the referenced model.
     * The default implementation uses refNames() array.
	 * If name is not defined, the default is 'name' if defined in the referenced model, otherwise null
     *
     * @param string $ref - reference name
     * @return string|null
     */
    public static function getRefName(string $ref) {
    	$name = ArrayUtils::getValue(static::refNames(), $ref, false);
    	if($name===false) {
    		$fk = static::foreignKey($ref);
    		if(!$fk) return null;
    		if($fk[0]::hasAttribute('name')) return 'name';
    		return null;
    	}
    	return $name;
    }

	/**
	 * Returns the tablename as name for XML nodes.
	 * See {@see createNode()} method
	 *
	 * @return string
	 */
	static public function nodeName() {
		return static::tableName();
	}

	/**
	 * Creates nodes from full select
	 * Result field values will be attributes, '_content' will be node text content.
	 *
	 * Options:
	 * - string $nodename -- name of new nodes, default is tablename
	 * - DBX $connection
	 * - string namespace
	 * - array extra -- fieldname=>callable(array $item, int $index) to create extra attributes
	 *
	 * @param UXMLElement $node_parent -- parent of new nodes
	 * @param Query $query
	 * @param array $options
	 *
	 * @return UXMLElement -- the first node created or null if none
	 * @throws UXAppException
	 * @throws Exception
	 */
	public static function createNodesQuery(UXMLElement $node_parent, Query $query, array $options=[]) {
		$nodename = ArrayUtils::getValue($options, 'nodeName');
		$namespace = ArrayUtils::getValue($options, 'namespace');
		$extra = ArrayUtils::getValue($options, 'extra');

		$node_first = null;
		if(!$nodename) {
			$nodename = $query->modelname ? Util::uncamelize(Util::shortName($query->modelname)) : 'item';
		}
		$query->each(function($row, $index) use($node_parent, $nodename, $namespace, $extra, &$node_first) {
			if($row instanceof BaseModel) {
				$row = $row->attributes;
			}
			if(!is_array($row) && is_callable([$row, 'toArray'])) $row = call_user_func([$row, 'toArray']);
			if(!is_array($row)) throw new Exception('query result must be Model or array');
			$content = ArrayUtils::getValue($row, '_content');
			unset($row['_content']);
			$node = $node_parent->addNode($nodename, $row, $content, $namespace);
			if(is_array($extra)) foreach($extra as $key=>$value) {
				if(is_callable($value)) $value = call_user_func($value, $row, $index);
				$node->setAttribute($key, $value);
			}
			if(!$node_first) $node_first = $node;
		});
		return $node_first;
	}

	public static function clearCache() {
		self::$_cache = [];
	}

	/**
	 * @throws UXAppException
	 */
	public function attributesExcept($except) {
		$attributes = static::databaseAttributes($this->db);
		Assertions::assertArray($attributes);
		if(is_string($except)) $except = [$except];
		Assertions::assertArray($except);
		return array_diff($attributes, $except);
	}
}
