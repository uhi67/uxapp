<?php /** @noinspection PhpIllegalPsrClassPathInspection */


namespace uhi67\uxapp;

use Exception;

/**
 * Class Request
 *
 * Canonical url format: 'http://domain/path/index.php?page=ctrl&act=action&id=23&param=p' where
 * - 'ctrl' corresponds to controller class in '\app\pages\CtrlPage.php', may contain further path elements
 * - 'baseurl' is 'http://domain/path'
 *
 * Compressed url format is 'baseurl/ctrl/act/id?param=p'
 *
 * Other examples with compressed counterpart:
 *
 * - baseurl/index.php?module=moduleName&page=ctrl&act=action&id=23&param=p -> baseurl/module/modulename/page/act/id?param=p
 * - baseurl/index.php?asset=module/css/mod.css -> baseurl/asset/module/css/mod.css
 * - baseurl/index.php?asset=vendor/module/css/mod.css -> baseurl/asset/vendor/module/css/mod.css
 * - baseurl/index.php?asset=xsl/view.xsl -> baseurl/asset/xsl/view.xsl
 * - baseurl/index.php?asset=_/page.xsl -> baseurl/asset/_/page.xsl
 *
 * Special query variables (extracted from the compressed path)
 * - module
 * - page
 * - act
 * - id
 *
 * @package uhi67\uxapp
 */
class Request extends Component implements RequestInterface {
    /** @var string|null $request -- original full request uri including query paramaters */
    public $url = null;
    public $baseUrl = null;
    public $compress;
    /** @var array $query -- get query variables including ones extracted from the path */
    public $query;
    /** @var array $path -- original path (no further path elements are used after parsing compressed url) */
    public $path;

    /**
     * @throws Exception
     */
    public function prepare() {
        parent::prepare();

        if(!$this->url) $this->url = ArrayUtils::getValue($_SERVER, 'REQUEST_URI');

        // Determines original baseurl (canonic)
        if(!$this->baseUrl && isset($_SERVER['HTTP_HOST'])) {
            $prot = $_SERVER['REQUEST_SCHEME'] ?? 'http';
            $prot = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] ? "https" : $prot;
            $this->baseUrl = $prot . "://" . $_SERVER['HTTP_HOST'] . ArrayUtils::getValue($_SERVER, 'SCRIPT_NAME', '');
            if(substr($this->baseUrl,-10)=='/index.php') $this->baseUrl = substr($this->baseUrl, 0, -10);
        }
        $this->baseUrl = $this->baseUrl ? trim($this->baseUrl, '/\\') : null;
        if(!$this->query) $this->query = $_GET;
    }

    /**
     * @throws Exception
     */
    public function init() {
        parent::init();
        // Path elements
        $path = urldecode($this->urlPart(PHP_URL_PATH));
        $basePath = $this->urlPart(PHP_URL_PATH, $this->baseUrl);
        $relativePath = trim(Util::substring_after($path, $basePath, true), '/');
        $this->path = $path ? explode('/', $relativePath) : [];
        $this->query = UXApp::$app->urlFormatter->parsePath($relativePath, $this->query);
    }


    public function urlPart($part, $url=null) {
        if(!$url) $url = $this->url;
        return $url ? parse_url($url, $part) : null;
    }

    /**
     * Returns a value from the GET only, or all variables.
     *
     * @param string $name
     * @param mixed $default
     *
     * @return array|mixed
     */
    public function get($name=null, $default = null) {
        if($name===null) return $this->query;
        return ArrayUtils::getValue($this->query, $name, $default);
    }

    /**
     * Returns a value from the POST only, or all variables.
     *
     * @param string $name
     * @param mixed $default
     *
     * @return array|mixed
     */
    public function post($name = null, $default = null) {
        if($name===null) return $_POST;
        return ArrayUtils::getValue($_POST, $name, $default);
    }

    /**
     * Returns an uploaded file from the request
     *
     * Returns a value from previously stored query if present.
     * This value can be set by Util::setReq()
     *
     * Returns array of FileUpload if request variable is an array variable[]
     *
     * @param string $name
     *
     * @return FileUpload|array|null
     */
    public function file($name) {
        if(!isset($_FILES) || !isset($_FILES[$name])) return null;
        if(is_array($_FILES[$name])) {
            $result = [];
            if(is_array($_FILES[$name]['name'])) {
                foreach($_FILES[$name]['name'] as $fieldname => $filename) {
                    $result[$fieldname] = FileUpload::createFromField($name, $fieldname);
                }
            }
            else {
                $result = FileUpload::createFromField($name);
            }
            return $result;
        } else $fileupload = FileUpload::createFromField($name);
        return $fileupload;
    }

    /**
     * @inheritDoc
     * @param string $name
     * @return array|FileUpload|null
     * @deprecated use file()
     */
    public function getFile($name) { return $this->file($name); }

    /**
     * Gets a variable from get or post (get first)
     *
     * @inheritDoc
     */
    public function req($name=null, $default = null) {
        if($name===null) return null;
        $value = ArrayUtils::getValue($this->query, $name);
        if($value==='' || $value===null) return $this->post($name, $default);
        return $value;
    }

    /**
     * Returns an integer value from request (GET or POST):
     *
     * - integer of value if not empty
     * - null if value is empty and no default or default is null
     * - default (as integer), if value is empty
     *
     * @inheritDoc
     */
    public function reqInt($name, $default = null) {
        $value = $this->req($name, $default);
        if($value === '') $value = $default;
        if($value !== null) $value = (int)$value;
        return $value;
    }


    /**
     * @inheritDoc
     */
    public function reqArray($name, $default = []) {
        $result = $this->req($name, $default);
        if(!is_array($result)) return $default;
        return $result;
    }

    /**
     * @inheritDoc
     */
	public function reqMultiOr($name, $default = 0) {
        $a = array_values($this->getArray($name));
        if(count($a) == 0) return null;
        $r = 0;
        /** @noinspection PhpStatementHasEmptyBodyInspection */
        for($i = 0; $i < count($a); $r |= $a[$i++]) ;
        return $r;
    }

    /**
     * TODO: convert to only GET (next major version)
     *
     * Returns an integer value from request:
     *
     * - integer of value if not empty
     * - null if value is empty and no default or default is null
     * - default (as integer), if value is empty
     *
     * @inheritDoc
     */
    public function getInt($name, $default = null) {
        $value = $this->req($name, $default);
        if($value === '') $value = $default;
        if($value !== null) $value = (int)$value;
        return $value;
    }

    /**
     * TODO: convert to only GET (next major version)
     *
     * @inheritDoc
     */
    public function getArray($name, $default = []) {
        $result = $this->req($name, $default);
        if(!is_array($result)) return $default;
        return $result;
    }

    /**
     * TODO: convert to only GET (next major version)
     *
     * @inheritDoc
     */
	public function getMultiOr($name, $default = 0) {
        $a = array_values($this->getArray($name));
        if(count($a) == 0) return null;
        $r = 0;
        /** @noinspection PhpStatementHasEmptyBodyInspection */
        for($i = 0; $i < count($a); $r |= $a[$i++]) ;
        return $r;
    }

    /**
     * Returns an integer value from the POST
     *
     * - integer of value if not empty
     * - null if value is empty and no default or default is null
     * - default (as integer), if value is empty
     *
     * @inheritDoc
     */
    public function postInt($name, $default = null) {
        $value = $this->post($name, $default);
        if($value === '') $value = $default;
        if($value !== null) $value = (int)$value;
        return $value;
    }

    /**
     * @inheritDoc
     */
    public function postArray($name, $default = []) {
        $result = $this->post($name, $default);
        if(!is_array($result)) return $default;
        return $result;
    }

    /**
     * @inheritDoc
     */
	public function postMultiOr($name, $default = 0) {
        $a = array_values($this->postArray($name));
        if(count($a) == 0) return null;
        $r = 0;
        /** @noinspection PhpStatementHasEmptyBodyInspection */
        for($i = 0; $i < count($a); $r |= $a[$i++]) ;
        return $r;
    }

    /**
     * Modifies a given or current URL with new parameter values
     *
     * @param string|null $url -- url to modify
     * @param array $params -- new parameter values
     *
     * @return string
     * @deprecated use UXApp::$app->urlFormatter->modUrl()
     */
	public static function modUrl(string $url=null, array $params=[]) {
        if($url===null) $url = ArrayUtils::getValue($_SERVER, 'REQUEST_URI', '');
        $fragment = '';
        if(isset($params['#'])) {
            $fragment = $params['#'];
            unset($params['#']);
        }

        $u = parse_url($url);
        $base = (isset($u['scheme']) ? $u['scheme'].'://' : '') .
            ($u['user'] ?? '').
            (isset($u['pass']) ? ':' . $u['pass']  : '').
            ($u['host'] ?? '').
            (isset($u['port']) ? ':' . $u['port'] : '').
            ($u['path'] ?? '');
        $query = [];
        if(isset($u['query']))	parse_str($u['query'], $query);
        $params = array_filter(array_merge($query, $params), function($item) { return $item!== null; });
        return
            $base .
            (count($params) ? ('?' . http_build_query($params)) : '') .
            ($fragment ? ('#' . $fragment) : '');
    }
}
