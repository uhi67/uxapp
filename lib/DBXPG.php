<?php
/** @noinspection PhpIllegalPsrClassPathInspection */
/** @noinspection PhpUnused */

namespace uhi67\uxapp;

/**
 * DBXPG.php
 *
 * Supports Postgresql <=10
 *
 * @package UXApp
 * @author uhi
 * @copyright 2016-2021
 * @abstract database functions implemented for PostgreSQL
 */

use DateTime;
use Exception;

if(!function_exists('pg_connect')) {
    Util::fatalError(500, 'Internal Server Error / Belső szerverhiba', 'PostgreSQL is not supported / PostgreSQL támogatás hiányzik.');
}

class DBXPG extends DBX {
	public $port = 5432;

	private $db_last_error; // muszáj tárolni, mert a debug kiolvassa, akkor másnak nem marad
	/**
	 * @var array $typeNames -- sql_type => common_type mapping
	 * common type names:
	 *  - basic php types (lowercase)
	 *  - 'xml' denotes xml valid string
	 *  - php classnames (Uppercase, eg. DateTime)
	 *  - 'string' is default, not indicated
	 *  - date (DateTime without time)
	 *  - time (DateTime without date or integer in sec)
	 */
	public static $typeNames = array(
		'int4' => 'integer',
		'integer' => 'integer',
		'numeric' => 'float',
		'xml' => 'xml',
		'bool' => 'boolean',
		'boolean' => 'boolean',
		'timestamp' => 'DateTime',
		'timestamptz' => 'DateTime',
		'timestamp without time zone' => 'DateTime',
		'timestamp with time zone' => 'DateTime',
		'date' => 'date',
		'time' => 'time',
		'interval' => 'time',
		'money' => 'float',
		'bit' => 'integer',
		'serial' => 'integer',
		'inet' => 'Inet',
		'cidr' => 'Inet',
		'macaddr' => 'Macaddr',
	);

	/**
	 * @throws UXAppException
	 */
	public function prepare() {
		$this->type = 'PG';
		if($this->connectNow) $this->connection = $this->connect();
	}

	/**
	 * @return null|resource
	 * @throws UXAppException
	 */
	public function connect() {
	    $hostx = $this->host ? " host=$this->host " : '';
		$connectString = "port=$this->port user=$this->user password=$this->password dbname=$this->name $hostx";
		$this->connection = @pg_connect($connectString);
        if(!$this->connection) {
      		$this->connerr = true;
      		throw new UXAppException("Database connection error, 'port=$this->port user=$this->user dbname=$this->name $hostx'", error_get_last());
		}
		else pg_exec($this->connection, "SET client_encoding to '$this->encoding'");
		return $this->connection;
	}

	/**
	 * Terminates this database connection
	 *
	 * @throws UXAppException
	 */
	public function close() {
		if($this->connection) {
			if(($t = get_resource_type($this->connection)) != 'pgsql link') throw new UXAppException('invalid connection resource: '.$t);
			pg_close($this->connection);
		}
	}

	/**
	 * Returns error message associated to a resultset, or the last error
	 * The message is stored internally, it is readable multiple times.
	 *
	 * @param resource $rs
	 * @return string
	 */
	function error($rs=null) {
		if($rs) $e = pg_result_error($rs);
		else $e = pg_last_error($this->connection);
		if(!$e) $e = $this->db_last_error;
		else $this->db_last_error = $e;
		return $e;
	}

	/** @noinspection PhpMethodNamingConventionInspection */

	/**
	 * @param resource $rs
	 *
	 * @return int
	 * @throws UXAppException
	 */
	function affected_rows($rs) {
		if(!$rs) throw new UXAppException('Invalid result set');
		return pg_affected_rows($rs);
	}

	/**
	 * @throws UXAppException
	 */
	function beginTransaction() {
		$rs = pg_query($this->connection, "BEGIN");
		if(!$rs) throw new UXAppException('Could not start transaction');
	}

	/**
	 * @throws UXAppException
	 */
	function commit() {
		$rs = pg_query($this->connection, "COMMIT");
		if(!$rs) throw new UXAppException('Transaction commit failed');
	}

	/**
	 * @throws UXAppException
	 */
	function rollBack() {
		$rs = pg_query($this->connection, "ROLLBACK");
		if(!$rs) throw new UXAppException('Transaction rollback failed');
	}

	/**
	 * SQL query futtatása $1, $2..., $varx, ...  paraméterek behelyettesítésével
	 *
	 * @param string $sql
	 * @param array $params -- 0-ás indexen az $1-es paraméter és/vagy név=>érték párok
	 *
	 * @return resource|false -- A query result resource on success or false on failure
	 * @throws
	 */
	function query($sql, $params=NULL) {
		$this->autoconnect();
	    if($params) $sql = self::bindParams($sql, $params);
	    $st = UXApp::debugIsOn() ? microtime(true) : 0;
		if(!($r = @pg_query($this->connection, $sql))) $this->errtrace($sql, $params);
	    if(UXApp::debugIsOn()) UXApp::trace($this->bindParams($sql, $params), ['depth'=>3, 'tags'=>'sql', 'mt' => microtime(true)-$st]);
		return $r;
	}

	function execute($sql, $params=null) {
		return $this->query($sql, $params);
	}


	/**
	 * @param resource $resultset
	 * @param int $row
	 * @param int $field
	 *
	 * @return array|bool|string
	 * @throws Exception
	 */
	function result($resultset, $row, $field=0) {
		if(!$this->connection) return false;
		/** @var string|null|false $r */
		$r = @pg_fetch_result($resultset, $row, $field);
		if($r===false) throw new Exception('Result error '.$this->error($resultset));
		return $this->decode($r);
	}

	/** @noinspection PhpMethodNamingConventionInspection */

	/**
	 * @param resource $rs
	 *
	 * @return bool
	 */
	function free_result($rs) {
		return pg_free_result($rs);
	}

	/** @noinspection PhpMethodNamingConventionInspection */

	/**
	 * @param resource $resultset
	 *
	 * @return int
	 * @throws UXAppException
	 */
	function num_rows($resultset) {
	    $n = @pg_num_rows($resultset);
	    if($n===-1) throw new UXAppException($this->error());
		return $n;
	}

	/**
	 * Returns last OID
	 *
	 * @param resource $resultset
	 *
	 * @return bool|string
	 */
	function lastoid($resultset) {
		return pg_last_oid($resultset);
	}

	/** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns last inserted autoincrement id
     * Only single id is supported
     *
     * @param string $table -- not used
     * @param string $fieldname -- not used
     * @return string|null -- null if not defined yet
     * @throws UXAppException
     */
	function last_id($table='', $fieldname='') {
		$r = pg_query($this->connection, "select lastval()");
        // Note: suppressing lastval error destroys transaction
		if($r===false) throw new UXAppException("No lastval defined for $fieldname.");
		return pg_fetch_result($r, 0, 0);
	}

	/**
	 * Returns the number of fields in a result
	 * @param resource $resultset
	 * @return integer
	 */
	function fields($resultset) {
		return pg_num_fields($resultset);
	}

	/**
	 * Returns a fieldname in the resultset
	 *
	 * @param resource $resultset
	 * @param integer $i
	 * @return string
	 */
	function fieldname($resultset, $i) {
		return pg_field_name($resultset, $i);
	}

	/**
	 * Returns the common type of a field in the resultset
	 *
	 * @param resource $resultset
	 * @param integer $i
	 * @return string
	 */
	function fieldtype($resultset, $i) {
		return self::mapType(pg_field_type($resultset, $i));
	}

	/** @noinspection PhpMethodNamingConventionInspection */

	/**
	 * Returns a row from result set
	 *
	 * @param resource $resultset
	 * @param int $row -- 0-based index of row or next row if omitted or null
	 * @return array -- numeric indexed string values or false on error
	 *
	 * An array, indexed from 0 upwards, with each value represented as a string. Database NULL values are returned as NULL.
	 * FALSE is returned if row exceeds the number of rows in the set, there are no more rows, or on any other error.
	 */
	function fetch_row($resultset, $row=null) {
		return $this->decode(pg_fetch_row($resultset, $row));
	}

	/** @noinspection PhpMethodNamingConventionInspection */

	/**
	 * Returns a record to associative array
	 * All field values will be strings
	 *
	 * @param resource $resultset
	 * @param int $row
	 * @return array fieldname=>stringvalue
	 */
	function fetch_assoc($resultset, $row=null) {
		return $this->decode(pg_fetch_assoc($resultset, $row));
	}

	/** @noinspection PhpMethodNamingConventionInspection */

	/**
	 * Returns a record as an associative array.
	 * Field values will be mapped to php types.
	 *
	 * @param resource $resultset
	 * @param int|null $row
	 *
	 * @return array
	 * @throws Exception
	 */
	function fetch_assoc_type($resultset, $row=null) {
		$result = $this->decode(pg_fetch_assoc($resultset, $row));
		// Mezőtípusok
		for($i=0;$i<pg_num_fields($resultset);$i++) {
			$name = pg_field_name($resultset, $i);
			$type = pg_field_type($resultset, $i);
			$result[$name] = $this->mapData($result[$name], $type);
		}
		return $result;
	}

	/** @noinspection PhpMethodNamingConventionInspection */

	/**
	 * Returns table metadata as
	 *  array(
	 *     'field name' => array(
	 *        'num' => <Field number starting at 1>
	 *        'type' => <data type, eg varchar, int4>
	 *        'len' => <internal storage size of field. -1 for varying>
	 *        'not null' => <boolean>
	 *        'has default' => <boolean>
	 *     ),
	 *       ...
	 *  )
	 *
	 * Returns false if table not exists.
	 *
	 * @param string $table
	 * @return array|boolean
	 * @throws Exception
	 */
	public function table_metadata($table) {
	    $sql = /** @lang text */'SELECT column_name, 
			data_type as type, 
			character_maximum_length as len, 
			ordinal_position as num,
			(column_default is not null) as "has default",
			is_nullable=\'NO\' as "not null"
		FROM information_schema.columns WHERE table_name=$1 ORDER BY column_name';
	    $this->select_all($result, $sql, array($table));
	    if(empty($result)) return false;
	    return array_combine(
			array_map(function ($v) {return $v['column_name'];}, $result),
			array_map(function ($v) {unset($v['column_name']); return $v;}, $result)
		);
	}

	/**
	 * Returns array of table names in this database
	 *
	 * @param string|null $schema
	 *
	 * @return string[]
	 * @throws UXAppException
	 */
	public function getTables($schema=null) {
		if(!$schema) $schema = 'public';
		return $this->select_column(/** @lang text */"SELECT table_name 
			FROM information_schema.tables 
			WHERE table_type='BASE TABLE' 
				AND table_schema=$2
				AND table_catalog=$1
		", [$this->name, $schema]);
	}

	/**
	 * Returns foreign keys information of the table as
	 *
	 * 	[
	 * 		'constraint_name' => [
	 * 			'column_name' => 'columnName',
	 * 			'foreign_schema' => 'schemaName',
	 * 			'foreign_table' => 'tableName'
	 * 			'foreign_column' => 'columnName'
	 * 		],
	 * 		...
	 *  ]
	 * @param string $tablename -- may contain schema prefix
	 * @param string|null $schema -- optional (table prefix overrides; default is current schema)
	 *
	 * @return array
	 */
	public function getForeignKeys($tablename, $schema=null) {
		if(!$schema) $schema = 'public';
		$sql = /** @lang */"SELECT
					tc.table_schema, 
					tc.constraint_name, 
					tc.table_name, 
					kcu.column_name, 
					ccu.table_schema AS foreign_schema,
					ccu.table_name AS foreign_table,
					ccu.column_name AS foreign_column 
				FROM 
					information_schema.table_constraints AS tc 
					JOIN information_schema.key_column_usage AS kcu
					  ON tc.constraint_name = kcu.constraint_name
					  AND tc.table_schema = kcu.table_schema
					JOIN information_schema.constraint_column_usage AS ccu
					  ON ccu.constraint_name = tc.constraint_name
					  AND ccu.table_schema = tc.table_schema
				WHERE tc.constraint_type = 'FOREIGN KEY' AND tc.table_name=$1 AND tc.table_schema=$2";
		$this->select_all($result, $sql, [$tablename, $schema]);
		if(empty($result)) return [];
		$rr = [];
		foreach($result as $r) $rr[$r['constraint_name']] = $r;
		return $rr;
	}

	/**
	 * Returns remote foreign keys referred to this table (reverse foreign key)
	 *
	 * 	[
	 * 		'constraint_name' => [
	 * 			'remote_schema' => 'schemaName',
	 * 			'remote_table' => 'tableName'
	 * 			'remote_column' => 'columnName'
	 * 			'table_schema' => 'columnName',
	 * 			'table_name' => 'columnName',
	 * 			'column_name' => 'columnName',
	 * 		],
	 * 		...
	 *  ]
	 * @param string $tablename -- may contain schema prefix
	 * @param string|null $schema -- optional (table prefix overrides; default is current schema)
	 *
	 * @return array
	 */
	public function getReferrerKeys($tablename, $schema=null) {
		if(!$schema) $schema = 'public';
		$sql = /** @lang */"SELECT
					tc.table_schema AS remote_schema, 
					tc.constraint_name, 
					tc.table_name AS remote_table, 
					kcu.column_name AS remote_column, 
					ccu.table_schema,
					ccu.table_name,
					ccu.column_name 
				FROM 
					information_schema.table_constraints AS tc 
					JOIN information_schema.key_column_usage AS kcu
					  ON tc.constraint_name = kcu.constraint_name
					  AND tc.table_schema = kcu.table_schema
					JOIN information_schema.constraint_column_usage AS ccu
					  ON ccu.constraint_name = tc.constraint_name
					  AND ccu.table_schema = tc.table_schema
				WHERE tc.constraint_type = 'FOREIGN KEY' AND ccu.table_name=$1 AND ccu.table_schema=$2";
		$this->select_all($result, $sql, [$tablename, $schema]);
		if(empty($result)) return [];
		$rr = [];
		foreach($result as $r) $rr[$r['constraint_name']] = $r;
		return $rr;
	}

	/**
	 * Must return the identifier with vendor-specific quoting
	 * E.g. 'where' => '`where`'
	 *
	 * @param string $name
	 * @return string
	 * @throws
	 */
	public function quoteIdentifier($name) {
		if(function_exists('pg_escape_identifier')) {
			$this->autoconnect();
			return pg_escape_identifier ($this->connection, $name); // > php 5.4
		}
		return '"'.str_replace('"', '_', $name).'"';
	}

	/**
	 * Returns true if database driver supports `Nulls Last` option in `Order` clause
	 *
	 * @return bool
	 */
	public function supportsOrderNullsLast() {
		return true;
	}

	/** @noinspection PhpMethodNamingConventionInspection */

	/**
	 * Returns a quoted string literal value
	 *
	 * @param string $literal
	 * @return string
	 * @throws
	 */
	public function escape_literal($literal) {
		$this->autoconnect();
		if(function_exists('pg_escape_literal')) // > php 5.4.4
			return @pg_escape_literal($this->connection, $literal);
		return "'" . pg_escape_string($this->connection, $literal) . "'";
	}

	/**
	 * Creates a quoted literal for sql command
	 *
	 * Appends single quotes around the string, and replaces inner single quotes to ''
	 * Returns 'NULL' on NULL input if `tostring` is true.
	 *
	 * Converts DateTime, Macaddr and Inet into PG format
	 *
	 * @param $value
	 * @param bool $tostring -- convert null, integer and boolean to string, array to PG array format
	 *
	 * @return string the value in '-s or 'NULL'
	 * @throws UXAppException
	 */
	public function literal($value, $tostring=false) {
		if(is_array($value) && $tostring) {
			// PG array syntax is '{{1,2,3},{4,5,6},{7,8,9}}'
			// PG array constructor syntax is ARRAY[elements,...]
			return "ARRAY[".implode(',', array_map(function($item) { return $this->literal($item); }, $value)) . "]";
		}
		if(is_object($value)) {
			if($value instanceof DateTime) {
				if($value->getOffset()==0) $value = $value->format('Y-m-d H:i:s');
				else $value = $value->format('Y-m-d H:i:sP');
			} else if ($value instanceof Macaddr) {
				$value = $value->toString();
			} else if ($value instanceof Inet) {
				$value = $value->toString();
			}
		}
		return parent::literal($value, $tostring);
	}

	/**
	 * Returns local name of a 'standard' operator
	 *
	 * @param string $op
	 * @return string
	 */
	public function operatorName($op) {
		if($op=='RLIKE') return '~*';
		if($op=='NOT RLIKE') return '!~*';
		if($op=='REGEXP') return '~';
		if($op=='NOT REGEXP') return '!~';
		return $op;
	}

	/**
	 * Returns true if table exists int database
	 * @param string $name
	 * @return bool
	 * @throws Exception
	 */
	public function tableExists($name) {
		#$sql = "SELECT table_name FROM information_schema.columns WHERE table_name=$1 AND table_catalog=$2"; // Visible only if has rights
		$sql = /** @lang text */"SELECT relname FROM pg_class WHERE relname=$1"; // Visible even without rights
		$table_name = $this->selectvalue($sql, array($name));
		return $table_name==$name;
	}

	/**
	 * Returns php type for a connection-specific SQL type
	 *
	 * @param string $typename -- SQL type
	 * @return string - php type or classname
	 */
	function mapType($typename) {
		return ArrayUtils::getValue(self::$typeNames, $typename, 'string');
	}

	/**
	 * @param string $value
	 * @param string $type
	 *
	 * @return bool|DateTime|Inet|int|Macaddr|null|string
	 * @throws Exception
	 */
	public function mapData($value, $type) {
		if($value===null) return null;
		switch($type) {
			case 'int4':
			case 'int2':
				return (int)$value;
			case 'bool':
				return $value=='t';
			case 'timestamp':
			case 'timestamptz':
			case 'timestamp with time zone':
			case 'timestamp without time zone':
				return new DateTime($value); // TODO: complement, test
			case 'inet':
				return new Inet($value);
			case 'macaddr':
				return new Macaddr($value);
			default:
				return $value;
		}
	}

	/**
	 * Drops the named constraint from the table
	 *
	 * @param string $constraintName
	 * @param string $tableName
	 * @param string|null $schema
	 *
	 * @return false|resource
	 */
	public function dropForeignKey($constraintName, $tableName, $schema=null) {
		if(!$schema) $schema='public';
		if(!strpos($tableName, '.')) $tableName = $this->quoteIdentifier($schema).'.'.$this->quoteIdentifier($tableName);
		$constraintName = $this->quoteIdentifier($constraintName);
		return $this->query(/** @lang text */"ALTER TABLE $tableName DROP CONSTRAINT $constraintName");
	}

	/**
	 * @param string $tableName
	 * @param string|null $schema
	 *
	 * @return false|resource
	 */
	public function dropTable($tableName, $schema=null) {
		if(!$schema) $schema='public';
		if(!strpos($tableName, '.')) $tableName = $this->quoteIdentifier($schema).'.'.$this->quoteIdentifier($tableName);
		return $this->query(/** @lang text */"DROP TABLE $tableName");
	}

	/**
	 * @param string $tableName
	 * @param string|null $schema
	 *
	 * @return false|resource
	 */
	public function dropView($tableName, $schema=null) {
		if(!$schema) $schema='public';
		if(!strpos($tableName, '.')) $tableName = $this->quoteIdentifier($schema).'.'.$this->quoteIdentifier($tableName);
		return $this->query("DROP VIEW $tableName");
	}

	/**
	 * @param string $sequenceName
	 * @param string|null $schema
	 *
	 * @return false|resource
	 */
	public function dropSequence($sequenceName, $schema=null) {
		if(!$schema) $schema='public';
		if(!strpos($sequenceName, '.')) $sequenceName = $this->quoteIdentifier($schema).'.'.$this->quoteIdentifier($sequenceName);
		return $this->query('DROP SEQUENCE IF EXISTS '.$sequenceName);
	}

	/**
	 * Returns sequence names
	 *
	 * @param string|null $schema
	 *
	 * @return string[]
	 * @throws UXAppException
	 */
	public function getSequences($schema=null) {
		if(!$schema) $schema='public';
		$params = [$schema];
		$sql = 'SELECT sequence_name FROM information_schema.sequences WHERE sequence_schema = $1 ';
		return $this->select_column($sql, $params);
	}

	/**
	 * @param string $routineName -- must contain () with parameter types, may contain shcema attached with .
	 * @param string $routineType -- FUNCTION or AGGREGATE. if skipped, function name must have it in prefix
	 * @param string $schema -- default is "public"
	 *
	 * @return false|resource
	 * @throws UXAppException
	 */
	public function dropRoutine($routineName, $routineType='FUNCTION', $schema=null) {
		//	'DROP '||routine_type||' IF EXISTS '||routine_name(type,type...)'
		if(!$schema) $schema='public';
		if(strpos($routineName, ' ')) list($routineType, $routineName) = explode(' ', $routineName, 2);
		if(!strpos($routineName, '.')) $routineName = $this->quoteIdentifier($schema).'.'.$this->quoteRoutineDef($routineName);
		else $routineName = $this->quoteIdentifier($routineName);
		return $this->query('DROP '.$routineType.' IF EXISTS '.$routineName);
	}

	/**
	 * Quotes a routine name with an optional parameter list
	 *
	 * @param string $routineDef
	 *
	 * @throws UXAppException
	 */
	private function quoteRoutineDef($routineDef) {
		if(!($p = strpos($routineDef, '('))) return $this->quoteIdentifier($routineDef);
		if(substr($routineDef = trim($routineDef),-1)!=')') throw new UXAppException("Invalid routine name `$routineDef`");
		$name = trim(substr($routineDef, 0, $p));
		$paramlist = trim(substr($routineDef,$p+1,-1));
		if($paramlist=='') return $this->quoteIdentifier($name).'()';
		$quotedParams = implode(',', array_map(function($param) { return $this->quoteIdentifier(trim($param)); }, explode(',', $paramlist)));
		return $this->quoteIdentifier($name).'('.$quotedParams.')';
	}

	/**
	 * Retuns array with function names followed by parameter list and preceded with routine type from the give schema.
	 *
	 * @param string $schema -- default is "public"
	 * @return string[]
	 * @throws UXAppException
	 */
	public function getRoutines($schema=null) {
		// Postgresql <=10
		// SELECT proisagg, p.proname, pg_catalog.pg_get_function_identity_arguments(p.oid)
		// FROM pg_catalog.pg_proc p
		// JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace
		// WHERE n.nspname = 'public'
		if(!$schema) $schema='public';
		$params = [$schema, $this->name];
		$sql = '-- noinspection SqlResolve @ column/"proisagg"
			SELECT proisagg, p.proname, pg_catalog.pg_get_function_identity_arguments(p.oid)
			FROM pg_catalog.pg_proc p
			JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace
			WHERE n.nspname = $1';
		return array_map(function($routine) {
			$type = substr($routine[0],0,1)=='t' ? 'AGGREGATE' : 'FUNCTION';
			return $type . ' ' . $routine[1].'('.$routine[2].')';
		}, $this->select_rows($sql, $params));
	}

	function free($resultSet) {
		return pg_free_result($resultSet);
	}
}
