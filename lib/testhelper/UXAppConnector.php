<?php /** @noinspection PhpMissingParamTypeInspection */
/** @noinspection PhpMissingReturnTypeInspection */
/** @noinspection PhpMissingFieldTypeInspection */

/** @noinspection PhpIllegalPsrClassPathInspection */

namespace uhi67\uxapp\testhelper;

use app\inc\App;
use Codeception\Lib\Connector\Shared\PhpSuperGlobalsConverter;
use Codeception\Util\Debug;
use Codeception\Util\Debug as CCDebug;
use Exception;
use Symfony\Component\BrowserKit\AbstractBrowser;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\BrowserKit\Response;
use Throwable;
use uhi67\uxapp\ArrayUtils;
use uhi67\uxapp\DBX;
use uhi67\uxapp\Model;
use uhi67\uxapp\Util;
use uhi67\uxapp\UXApp;
use uhi67\uxapp\UXAppException;
use uhi67\uxml\UXMLDoc;

/**
 * Class UAppConnector
 *
 * Using: see UXApp.php
 *
 * @package Helper
 */
class UXAppConnector extends AbstractBrowser {
    use PhpSuperGlobalsConverter;

    /** @var array the config array of UXApp application */
    public $uxappConfig;

    /** @var string $sapi -- Framework module sets to 'cli' for unit suite based on yml */
    public $sapi;

    public $defaultServerVars = [];

    /** @var array */
    public $headers;
    public $statusCode;

    /** @var UXApp */
    public $app;

    /** @var DBX  */
    public static $db;

    /** @var array $session -- the content of the $_SESSION after the last request */
    public $session;

    /** @var UXMLDoc */
    public $xml;

    /**
     * @return UXApp
     * @throws Exception
     */
    public function getApplication() {
        if (!isset($this->app)) {
            $this->startApp($this->sapi);
        }
        return $this->app;
    }

    public function resetApplication() {
        $this->app->finish();
        $this->app = null;
        UXApp::$app = null;
    }

    /**
     * @param string $sapi
     *
     * @throws UXAppException
     */
    public function startApp($sapi = 'apache') {
        if(static::$db && !(is_object(static::$db) && static::$db instanceof DBX)) throw new UXAppException('invalid connection resource');
        if($this->app) {
            CCDebug::debug('# UXAppConnector Using existing App');
            $this->app->init();
            return;
        }
        CCDebug::debug('# UXAppConnector Creating new UXApp');
        $this->app = new UXApp(array_merge($this->uxappConfig, ['sapi'=>$sapi]));
    }

    public function destroyApplication() {
        if($this->app) $this->app->finish();
        $this->app = null;
        UXApp::$app = null;
    }

    public function resetPersistentVars() {
        static::$db = null;
        // TODO: reset uploaded file
    }

    /**
     *
     * @param Request $request
     *
     * @return Response
     * @throws Exception
     */
    public function doRequest($request) {
        CCDebug::debug('# UXAppConnector doRequest '.Util::objtostr($request));
        $_COOKIE = $request->getCookies();
        $_SERVER = $request->getServer();
        $this->restoreServerVars();
        $_FILES = $this->remapFiles($request->getFiles());
        $_REQUEST = $this->remapRequestParameters($request->getParameters());
        $_POST = $_GET = [];

        if (strtoupper($request->getMethod()) === 'GET') {
            $_GET = $_REQUEST;
        } else {
            $_POST = $_REQUEST;
        }

        $uri = $request->getUri();
        CCDebug::debug('#UXAppConnector uri: '.$uri);
        $pathString = parse_url($uri, PHP_URL_PATH);
        $queryString = parse_url($uri, PHP_URL_QUERY);
        $_SERVER['REQUEST_URI'] = $queryString === null ? $pathString : $pathString . '?' . $queryString;
        $_SERVER['REQUEST_METHOD'] = strtoupper($request->getMethod());
        CCDebug::debug('#UXAppConnector REQUEST_URI: '.$_SERVER['REQUEST_URI']);
        parse_str($queryString, $params);
        foreach ($params as $k => $v) {
            $_GET[$k] = $v;
        }

        $this->headers    = [];
        $this->statusCode = null;

        ob_start();
        // __
        $this->app = $this->getApplication();
        $pages = $this->app->pagesPath ?? $this->app->basePath.'/pages'; //ArrayUtils::getValue($this->uxappConfig, 'pages', $apppath.'/pages');
        $defaultPage = $this->app->defaultPage ?? 'start';

        $this->app->request->url = $_SERVER['REQUEST_URI'];	// /index.php?id=1181&page=unit vagy /page/id?...
        $this->app->request->prepare();
        $page = $this->app->request->get('page');

        if($page=='') $page=$defaultPage;
        $this->headers['X-Powered-By'] = 'UXApp';

        $pageclass = Util::camelize($page, true) . 'Page';
        $pageFile = $pages . '/' . $pageclass . '.php';
        CCDebug::debug('# UXAppConnector Pageclass: '.$pageclass. ', file: '. $pageFile);
        if(file_exists($pageFile)) {
            $this->headers["X-UXapp-Source"] = "1; $page";
//			if(!empty($_REQUEST['debug'])) {
//				return \Debug::callback();
//			}
            try {
                Model::clearCache();
                $this->app->runPage('\app\pages\\' . $pageclass);
            }
            catch(Throwable $e) {
                CCDebug::debug('# UAppConnector catched Exception: '.$e->getMessage().' in file '.$e->getFile(). ' at line '.$e->getLine());
                CCDebug::debug("# UAppConnector backtrace: \n".$e->getTraceAsString());
            }
            $this->statusCode = 200;
            $this->session = $this->app->session->get();
            $this->xml = ArrayUtils::getValue($this->app, ['page', 'view', 'xmldoc']);
        }
        else {
            $this->statusCode = 404;
            $this->xml = null;
        }
        // __
        $content = ob_get_clean();

        // TODO: kibocsátott headereket el kellene fogni
        // headers_list()

        // headers and $content mentése _output mappába
        file_put_contents($this->app->rootPath.'/tests/_output/headers_'.Util::toNameID($this->app->request->url).'.json', json_encode($this->headers));
        file_put_contents($this->app->rootPath.'/tests/_output/content_'.Util::toNameID($this->app->request->url).'.html', $content);

        // catch "location" header and display it in debug, otherwise it would be handled
        // by symfony browser-kit and not displayed.
        if (isset($this->headers['location'])) {
            Debug::debug("[Headers] " . json_encode($this->headers));
        }

        CCDebug::debug('# UXAppConnector Headers:');
        CCDebug::debug($this->headers);
        CCDebug::debug('# UXAppConnector Content:');
        CCDebug::debug($content);
        CCDebug::debug('# UXAppConnector Session:');
        CCDebug::debug($this->session);

        return new Response($content, $this->statusCode, $this->headers);
    }

    public function restoreServerVars()
    {
        $this->server = $this->defaultServerVars;
        foreach ($this->server as $key => $value) {
            $_SERVER[$key] = $value;
        }
    }

}
