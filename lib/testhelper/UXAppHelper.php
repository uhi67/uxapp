<?php /** @noinspection PhpMissingFieldTypeInspection */
/** @noinspection PhpMissingParamTypeInspection */
/** @noinspection PhpMissingReturnTypeInspection */
/** @noinspection PhpUnused */
/** @noinspection PhpIllegalPsrClassPathInspection */

namespace  uhi67\uxapp\testhelper;

use Codeception\Configuration;
use Codeception\Exception\ExternalUrlException;
use Codeception\Exception\ModuleConfigException;
use Codeception\Exception\ModuleException;
use Codeception\Lib\Framework;
use Codeception\Step;
use Codeception\TestInterface;
use Codeception\Util\Debug;
use DOMDocument;
use DOMXPath;
use Exception;
use ReflectionException;
use Symfony\Component\DomCrawler\Crawler;
use uhi67\uxapp\ArrayUtils;
use uhi67\uxapp\BaseUser;
use uhi67\uxapp\Model;
use uhi67\uxapp\UXApp;
use uhi67\uxapp\UXAppException;
use uhi67\uxml\UXMLDoc;

require_once 'UXAppConnector.php';

/**
 * # Test helper for UXApp framework
 *
 * ### Using:
 *
 * 1. Include in your `...suite.yml` as:
 *
 * ```
 * modules:
 *   enabled:
 *     - \uhi67\uxapp\testhelper\UXAppHelper:
 *           configFile: 'config/config.php'
 *           loader: 'full' # optional, default is core only
 *           sapi: 'cli' for unit, 'apache' for functional
 * ```
 *
 * 2. Alternatively, you may use a separate test config in the above yml.
 *
 * 3. Run `codecept build`
 *
 * ### Limitations
 * - `amLoggedIn()` assertion does not work yet.
 *
 * @property UXAppConnector $client
 */
class UXAppHelper extends Framework {
    protected $requiredFields = ['configFile'];
    protected $config = ['loader'=>'core', 'sapi'=>'apache']; // optional parameters and defaults of module

    /** @var UXApp $app -- the application instance started by client */
    public $app;
    /** @var string $configFile -- path of your configFile */
    protected $configFile;
    /** @var array $uxappConfig -- config of your UXApp application */
    protected $uxappConfig;

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @throws ModuleConfigException
     * @throws ModuleException
     */
    public function _initialize() {
        $this->configFile = Configuration::projectDir() . $this->config['configFile'];
        Debug::debug('configFile:'.$this->configFile);
        if (!is_file($this->configFile)) {
            throw new ModuleConfigException(
                __CLASS__,
                "The application config file does not exist: " . $this->configFile
            );
        }
        $this->uxappConfig = require($this->configFile);
        if(!is_array($this->uxappConfig)) throw new ModuleException(__CLASS__, "The UXApp test config is invalid: `$this->configFile` (may be return is missing)");
        $this->uxappConfig['params']['testsuite'] = $this->config;

        defined('ENV') || define('ENV', getenv('APPLICATION_ENV'));
        defined('ENV_DEV') || define('ENV_DEV', ENV != 'production');
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Minden teszt előtt
     *
     * @param TestInterface $test
     *
     * @throws Exception
     */
    public function _before(TestInterface $test) {
        Debug::debug('_before');
        $this->client = new UXAppConnector([]);
        $this->client->uxappConfig = $this->uxappConfig;
        $this->client->sapi = $this->config['sapi'];
        $this->app = $this->client->getApplication();
        if(!$this->app) {
            Debug::debug('No application instance created.');
        }
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @param TestInterface $test
     */
    public function _after(TestInterface $test) {
        Debug::debug('_after');
        $_SESSION = [];
        $_FILES = [];
        $_GET = [];
        $_POST = [];
        $_COOKIE = [];
        $_REQUEST = [];

        if($this->app) {
            Debug::debug('Destroying application');
            $this->client->destroyApplication();
        }
        else {
            Debug::debug('Application instance not found');
        }

        parent::_after($test);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @param array $settings
     *
     * @throws Exception
     */
    public function _beforeSuite($settings = []) {
        Debug::debug('_beforeSuite');
    }


    /** @noinspection PhpMethodNamingConventionInspection */

    public function _afterSuite() {
        Debug::debug('_afterSuite');
    }


    /** @noinspection PhpMethodNamingConventionInspection */

    public function _beforeStep(Step $step) {

    }


    /** @noinspection PhpMethodNamingConventionInspection */

    public function _afterStep(Step $step) {

    }


    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @param TestInterface $test
     * @param $fail
     *
     */
    public function _failed(TestInterface $test, $fail) {
        Debug::debug('_failed');
//		$this->logDir = Configuration::outputDir(); // prepare log dir
    }


    /** @noinspection PhpMethodNamingConventionInspection */

    public function _cleanup() {
        Debug::debug('_cleanup');
    }

    /**
     * To support to use the behavior of urlManager component
     * for the methods like this: amOnPage(), sendAjaxRequest() and etc.
     *
     * @param $method
     * @param $uri
     * @param array $parameters
     * @param array $files
     * @param array $server
     * @param null $content
     * @param bool $changeHistory
     *
     * @return Crawler
     * @throws ExternalUrlException
     * @throws Exception
     */
    protected function clientRequest($method, $uri, array $parameters = [], array $files = [], array $server = [], $content = null, $changeHistory = true)
    {
        if (is_array($uri)) {
            $uri = $this->app->createUrl($uri);
        }
        return parent::clientRequest($method, $uri, $parameters, $files, $server, $content, $changeHistory);
    }

    /**
     * @param int|array|null $userid -- null to log out
     * @throws ReflectionException
     * @throws UXAppException
     */
    public function amLoggedInAs($userid=null) {
        if(!$this->app->getComponent('user')) throw new UXAppException('No user component defined in application config.');
        if(!is_a($this->app->user, BaseUser::class)) throw new UXAppException('User component must be a BaseUser.');
        if($userid===null) $this->app->user->logout();
        else $this->app->user->login($userid);
        Debug::debug('# UXAppHelper Session data');
        Debug::debug($_SESSION);
    }

    /**
     * Inserts record into the database.
     *
     * ``` php
     * <?php
     * $user_id = $I->haveRecord('User', array('name' => 'Davert'));
     * ?>
     * ```
     *
     * @param string $model - modelname
     * @param array $attributes
     *
     * @return boolean
     * @throws Exception
     */
    public function haveRecord($model, $attributes = []) {
        if(!is_a($model, Model::class, true)) throw new Exception('Invalid modelname '.$model);
        $record = new $model($attributes);
        return $record->save();
    }

    /**
     * Checks that record exists in database.
     *
     * ``` php
     * $I->seeRecord('User', array('name' => 'davert'));
     * ```
     *
     * @param       $model
     * @param array $attributes
     *
     * @throws UXAppException
     */
    public function seeRecord($model, $attributes = []) {
        $this->assertNotNull($this->app->db);
        $record = $this->findRecord($model, $attributes);
        if (!$record) {
            $this->fail("Couldn't find $model with " . json_encode($attributes));
        }
        $this->debugSection($model, json_encode($record));
    }

    /**
     * Checks that record does not exist in database.
     *
     * ``` php
     * $I->dontSeeRecord('app\models\User', array('name' => 'davert'));
     * ```
     *
     * @param       $model
     * @param array $attributes
     *
     * @part orm
     * @throws UXAppException
     */
    public function dontSeeRecord($model, $attributes = [])
    {
        $record = $this->findRecord($model, $attributes);
        $this->debugSection($model, json_encode($record));
        if ($record) {
            $this->fail("Unexpectedly managed to find $model with " . json_encode($attributes));
        }
    }

    /**
     * Retrieves record from database
     *
     * ``` php
     * $category = $I->grabRecord('app\models\User', array('name' => 'davert'));
     * ```
     *
     * @param       $model
     * @param array $attributes
     *
     * @return mixed
     * @part orm
     * @throws UXAppException
     */
    public function grabRecord($model, $attributes = []) {
        return $this->findRecord($model, $attributes);
    }

    /**
     * @throws UXAppException
     */
    protected function findRecord($model, $attributes = [])	{
        if(!is_a($model, Model::class, true)) throw new UXAppException("Class `$model` is not a Model");
        return call_user_func([$model, 'first'], $attributes, null, null, $this->app->db);
    }

    /**
     * @param string|array $page
     */
    public function amOnPage($page) {
        parent::amOnPage($page);
    }

    public function seeInSession($key, $value=null) {
        $this->assertArrayHasKey($key, $this->client->session);
        if($value!==null) $this->assertEquals($value, $this->client->session[$key]);
    }

    /**
     * @return UXMLDoc
     */
    public function grabXml() {
        return $this->client->xml;
    }

    /**
     * @param string $query
     * @throws UXAppException
     */
    public function seeInXml($query) {
        $this->assertXmlContains($this->client->xml, $query);
    }

    /**
     * @param DOMDocument $dom
     * @param string $query
     * @throws UXAppException
     */
    public function assertXmlContains($dom, $query, $namespaces=null) {
        if(! $dom instanceof DOMDocument) throw new UXAppException("Invalid context document");
        $xpath = new DOMXPath($dom);
        if(is_array($namespaces)) foreach($namespaces as $alias=>$namespace) $xpath->registerNamespace($alias, $namespace);
        $result = $xpath->query($query, $dom->documentElement);
        if($result===false) throw new UXAppException("Invalid query expression '$query' or context node");
        $this->assertNotEquals(0, $result->length, "The XML document contains '$query'");
    }
}
