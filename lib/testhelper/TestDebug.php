<?php /** @noinspection PhpIllegalPsrClassPathInspection */

namespace uhi67\uxapp\testhelper;

use Codeception\Util\Debug;
use uhi67\uxapp\ArrayUtils;
use uhi67\uxapp\BaseDebugger;
use uhi67\uxapp\Util;

/**
 * A test-replacement for Debug module
 *
 * Using:
 * 1. include this file in `tests/unit/_bootstrap.php` in UXApp config
 * 2. enable _bootstrap in (global or suite) yml:
 * ```yml
 * bootstrap: _bootstrap.php
 * ```
 */
class TestDebug extends BaseDebugger {

    public function prepare() { $this->trace('# TestDebug: Trace start.'); }
    public function finish() { $this->trace('# TestDebug: Trace end.'); }
    public function isOn() { return true; }
    public function getId() { return 1; }

    /**
     * @param string $msg
     * @param string $params
     * @param int $depth
     * @param string $tags
     */
    public function trace($msg='*', $params='', $depth=1, $tags='app') {
        if(is_array($msg) || is_object($msg)) $msg = Util::objtostr($msg);
        $da = debug_backtrace();
        if($tags=='sql') {
            for($i=$depth;$i<count($da);$i++) {
                $dx = $da[$i];
                if(!preg_match('#uxapp[/\\\\]lib[/\\\\](DBX|Query)[a-z]*.php#i', ArrayUtils::getValue($dx, 'file', '$?'))) {
                    $depth = $i+1; break;
                }
            }
        }
        $dd = isset($da[$depth]) ? $da[$depth] : [];
        $where = (isset($dd['class']) ? $dd['class'] : '') . (isset($dd['type']) ? $dd['type'] : '') . (isset($dd['function']) ? $dd['function']: '');

        Debug::debug("\t".$where.': '. $msg);
    }
}
