<?php /** @noinspection PhpIllegalPsrClassPathInspection */

namespace uhi67\uxapp;

abstract class Migration extends Component {
	/** @var DBX $db */
	public $db;
	/** @var UXApp $app */
	public $app;

	abstract public function up();
}
