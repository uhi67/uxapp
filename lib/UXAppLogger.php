<?php

namespace uhi67\uxapp;

use Psr\Log\LoggerTrait;

/**
 * UXAppLogger implements a "dummy" logger wich does nothing, and a base class for other UXApp loggers
 */
class UXAppLogger extends Component implements LoggerInterface {
    use LoggerTrait;

    /** @var array $severities -- all severity level names in priority order */
    protected $severities = ['emergency', 'alert', 'critical', 'error', 'warning', 'notice', 'info', 'debug'];
    /** @var string $componentName -- optional name (index) for the component */
    public $componentName;

    /**
     * Logs data with context fields:
     *
     * - timestamp (optional, default is current timestamp)
     * - user (recommended)
     * - ip
     * - tags -- space separated tag names (recommended, default is 'app')
     * - page -- controller class (function) name
     * - action -- action method
     * - depth (for debuggers only: effective backtrace depth from trace call for debuggers)
     * - mt (optional, microtime)
     * - color (for debuggers only: message color)
     *
     * Levels order
     * - emergency: System is unusable
     * - alert: security violation or other urgent event which must be sent to administrator
     * - critical: exception or other event causes terminating current request
     * - error: event causes user transaction failed or not completed
     * - warning: when the desired result in not fully completed, or other unexpected minor event during completed transaction
     * - notice: user transaction
     * - info: any other information
     * - debug: debug info
     *
     * ----
     * {@inheritDoc}
     *
     * @param string $level -- [emergency, alert, critical, error, warning, notice, info, debug]
     * @param string $message
     * @param array $context
     */
    public function log($level, $message, array $context = []) {
    }

    /**
     * Interpolates context values into the message placeholders.
     */
    public static function interpolate($message, array $context = array()) {
        // build a replacement array with braces around the context keys
        $replace = array();
        foreach ($context as $key => $val) {
            // check that the value can be cast to string
            if (!is_array($val) && (!is_object($val) || method_exists($val, '__toString'))) {
                $replace['{' . $key . '}'] = $val;
            }
        }

        // interpolate replacement values into the message and return
        if(is_string($message)) $message = strtr($message, $replace);
        else if(is_array($message)) foreach($message as $i=>&$msg) {
            if(is_string($msg)) $msg = strtr($msg, $replace);
        }
        return $message;
    }
}
