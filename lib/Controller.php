<?php
namespace uhi67\uxapp;

use Exception;
use ReflectionClass;

/**
 * Class Controller
 *
 * The common ancestor of UAppPage and UAppCommand
 *
 * @package uhi67/uxapp
 */
abstract class Controller extends Component {
    /** @var UXApp  */
    public $app;
    /** @var DBX $db */
    public $db;

    /** @var Module[] $modules -- registered modules */
    public $modules = [];

    /**
     * Returns the base-path of the module class or module name
     *
     * module may be
     * - a Module object,
     * - a Module FQN classname
     * - a short modulename of an uxapp- vendor module
     * - a 'vendor/modulename' form
     * - a short modulename in app/modules directory
     *
     * @param string|Model $module -- module name. Full qualified class name, or vendor/module, or uhi67 module name
     *
     * @return string
     * @throws UXAppException
     */
    function getModulePath($module) {
        if($module instanceof Module) {
            $dir = $module->modulePath;
            return preg_replace('~[/\\\\]src[/\\\\]?$~', '', $dir);
        }
        if(class_exists($module)) return $this->getClassPath($module);
        if(class_exists($className = '\uhi67\\'.strtolower($module).'\\'.$module)) return $this->getClassPath($className);
        if(class_exists($className = '\uhi67\uxapp-' . strtolower($module).'\\'.$module)) return $this->getClassPath($className);
        if(is_dir($dirName = dirname(__DIR__, 3) .'/'.$module)) return $dirName;
        if(is_dir($dirName = dirname(__DIR__, 2) .'/'.$module)) return $dirName;
        if(is_dir($dirName = dirname(__DIR__, 2) .'/uxapp-'.$module)) return $dirName;
        if(is_dir($dirName = UXApp::$app->modulesPath . '/' . $module)) return $dirName;
        throw new UXAppException("Module class '$module' does not exist");
    }

    /**
     * Returns module name by class path
     *
     * - 'modulename' for local modules,
     * - 'vendorname/modulename' for vendor classes
     * - '' otherwise
     *
     * @param string $path -- absolute path of class file
     *
     * @return string
     */
    public static function getModuleName($path) {
        if(preg_match('~[/\\\\]vendor[/\\\\]([\w_-]+[/\\\\][\w_-]+)~', $path, $mm)) {
            return $mm[1];
        }
        if(preg_match('~[/\\\\]modules[/\\\\]([\w_-]+)~', $path, $mm)) {
            return $mm[1];
        }
        return '';
    }

    /**
     * @param $className
     *
     * @return string
     */
    function getClassPath($className) {
        $r = new ReflectionClass($className);
        $dir = dirname($r->getFileName());
        $dir = preg_replace('~[/\\\\]src[/\\\\]?$~', '', $dir);
        return preg_replace('~[/\\\\]lib[/\\\\]?$~', '', $dir);
    }

    /**
     * Displays an error message and finishes render.
     * Displays a next button with given url.
     * @deprecated use: throw an exception instead
     *
     * @param mixed $title
     * @param string $message
     * @param mixed $url
     *
     * @return void
     * @throws Exception
     */
    function errorMessage($title, $message='', $url=null) {
        UXApp::trace(['cim' => $title], ['tags'=>'uxapp']);
        $bt = debug_backtrace();
        $b = $bt[1];
        $c = $bt[0];
        $hely = $b['class'].$b['type'].$b['function'].':'.$c['file'].'#'.$c['line'];
        $e = UXApp::la('uxapp', 'Error');
        if($url===null && $this instanceof UXAppPage) $url = $this->error_url;
        $this->showMessage($e, $e, $title, [$message, $hely], $url, ['class'=>'main error']);
        exit;
    }

    function finish() {}

    /**
     * Callback runs after render.
     * The default implementation does nothing.
     *
     * @return void
     */
    protected function afterRender() {
    }


    abstract function showMessage($title, $h1, $h2, $msg, $url=null, $attr=null);
    abstract function go();
}
