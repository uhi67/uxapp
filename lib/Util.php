<?php /** @noinspection PhpIllegalPsrClassPathInspection */

/** @noinspection PhpUnused */

namespace uhi67\uxapp;
/**
 * Parts of former uxapp-util, without file, url and http related methods.
 */

use Closure;
use DateTime;
use DateTimeInterface;
use DOMDocument;
use DOMElement;
use DOMNode;
use Exception;
use IntlDateFormatter;
use ReflectionClass;
use Throwable;
use uhi67\uxml\FormatterInterface;
use uhi67\uxml\UXMLDoc;
use uhi67\uxml\UXMLElement;

class Util {

    /** @var array $dateformats: [preg, dateformat] */
    static public $dateformats = array(
        array('/^\d{4}\.\d{2}\.\d{2}$/', '!Y.m.d'),
        array('/^\d{4}\.\d{2}\.\d{2}\.$/', '!Y.m.d.'),
        array('/^\d{4}\.\d{2}\.\d{2}\s\d{1,2}:\d{1,2}$/', '!Y.m.d H:i'),
        array('/^\d{4}\.\d{2}\.\d{2}\.\s\d{1,2}:\d{1,2}$/', '!Y.m.d. H:i'),
        array('/^\d{4}\.\d{2}\.\d{2}\s+\d{1,2}:\d{1,2}:\d{1,2}$/', '!Y.m.d H:i:s'),
        array('/^\d{4}\.\d{2}\.\d{2}\.\s+\d{1,2}:\d{1,2}:\d{1,2}$/', '!Y.m.d. H:i:s'),

        array('/^\d{4}\.\s\d{2}\.\s\d{2}$/', '!Y. m. d'),
        array('/^\d{4}\.\s\d{2}\.\s\d{2}\.$/', '!Y. m. d.'),
        array('/^\d{4}\.\s\d{2}\.\s\d{2}\.\s\d{1,2}:\d{1,2}$/', '!Y. m. d. H:i'),
        array('/^\d{4}\.\s\d{2}\.\s\d{2}\.\s\d{1,2}:\d{1,2}:\d{1,2}$/', '!Y. m. d. H:i:s'),
    );

    /**
     * Creates a DateTime object from any format
     *
     * @param DateTime|string|int $dt
     *
     * @return DateTime
     * @throws UXAppException
     */
    public static function createDateTime($dt) {
        if($dt instanceof DateTime) return $dt;
        if(is_int($dt)) return DateTime::createFromFormat( 'U', $dt);
        Assertions::assertString($dt);
        foreach(self::$dateformats as $format) {
            if(preg_match($format[0], $dt)) return DateTime::createFromFormat($format[1], $dt);
        }
        try {
            return new DateTime($dt);
        }
        catch(Throwable $e) {
            throw new UXAppException("Invalid date format '$dt'", null, $e);
        }
    }

    /**
     * Returns a named global variable if present, or default
     *
     * @param string $name
     * @param mixed $default
     *
     * @return mixed
     */
    static function getGlobal($name, $default=null) {
        return $GLOBALS[$name] ?? $default;
    }

    /**
     * Creates a technical compact visible form of any objects, similar to JSON
     *
     * @param mixed $obj
     * @param bool $quotestrings
     * @param bool $short -- do not list content of objects
     * @param int $maxdepth -- maximum depth of recursion (max 100)
     *
     * @return string
     */
    static function objtostr($obj, $quotestrings=true, $short=false, $maxdepth=10) {
        if($maxdepth>100 || $maxdepth<0) $maxdepth=100;
        if(is_null($obj)) return 'null';
        if(is_bool($obj)) return $obj ? 'true': 'false';
        if(is_int($obj)) return $obj;
        if(is_float($obj)) return $obj;
        $qs = ($quotestrings ? "'" :'');
        if(is_string($obj)) return $qs.$obj.$qs;
        if(is_array($obj)) {
            if($maxdepth) {
                $result = '';
                foreach($obj as $key => $item) {
                    if($result != '') $result .= ', ';
                    $result .= $key . ':' . self::objtostr($item, $quotestrings, $short, $maxdepth - 1);
                }
                return '[' . $result . ']';
            }
            return '[Array]';
        }
        if($obj instanceof Exception) {
            $m = $obj->getMessage();
            $f = $obj->getFile();
            $l = $obj->getLine();
            return "{Exception: $m in file $f on line $l}";
        }
        if($obj instanceof Inet) {
            return '{'.$obj->toString().'}';
        }
        if($obj instanceof Macaddr) {
            return '{'.$obj->toString().'}';
        }
        if($obj instanceof DateTime) {
            return '{'.$obj->format(DateTimeInterface::ATOM).'}';
        }
        if(is_object($obj) && $short) return get_class($obj);
        if($obj instanceof DOMDocument) {
            $r = $obj->saveXML($obj->documentElement);
            if(strlen($r)>100) $r = substr($r,0,97).'...';
            return htmlspecialchars($r);
        }
        if($obj instanceof DOMElement) {
            if(!$obj->ownerDocument) {
                $doc = new UXMLDoc();
                $doc->documentElement->appendChild($doc->importNode($obj, true));
            }
            $r = $obj->ownerDocument->saveXML($obj);
            if(strlen($r)>100) $r = substr($r,0,97).'...';
            return htmlspecialchars($r);
        }
        if($obj instanceof DOMNode) {
            return get_class($obj).'{'.$obj->nodeName.'='.$obj->nodeValue.'}';
        }
        if(is_object($obj)) {
            if(is_callable(array($obj, '_toString'))) {
                try {
                    return get_class($obj) . '{' . $obj->_toString() . '}';
                }
                catch(Throwable $e) {
                    return get_class($obj) . '{! ' . $e->getMessage() . '}';
                }
            }
            if(is_callable(array($obj, 'toArray'))) return get_class($obj).self::objtostr($obj->toArray(), $quotestrings, $short, $maxdepth-1);
            $result = '';
            foreach( $obj as $key => $item ) {
                if($result!='') $result.=',';
                $result .= $maxdepth ? $key.'='.self::objtostr($item, $quotestrings, $short, $maxdepth-1) : '{Object}';
            }
            return get_class($obj).'{'.$result.'}';
        }
        return $obj;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns substring before delimiter
     *
     * @param string $s -- string
     * @param string $d -- delimiter
     * @param bool $full -- returns full string if pattern not found
     * @return string -- substring to delimiter or empty string if not found
     */
    static function substring_before($s, $d, $full=false) {
        $p = strpos($s, $d);
        if($full && $p===false) return $s;
        return substr($s, 0, $p);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns substring after delimiter.
     * If delimiter is not found or empty, the full string or empty string is returned depending of $full parameter.
     *
     * @param string $s -- string
     * @param string $d -- delimiter -- empty delimiter never found.
     * @param bool $full -- returns full string if pattern not found
     *
     * @return string -- substring to delimiter or empty string if not found
     * @throws Exception
     */
    static function substring_after($s, $d, $full=false) {
        if(empty($d) && $full) return $s;
        if(empty($d)) return '';
        if(!is_string($d)) $d = (string)$d;
        $p = strpos($s, $d);
        if($p===false) return $full ? $s : '';
        return substr($s, $p+strlen($d));
    }

    /**
     * returns binary representation of a value
     *
     * @param int $v
     * @param int $length
     *
     * @return string
     */
    static function bitstring($v, $length=16) {
        if($length==0) return '';
        return str_pad(substr(decbin($v),-$length),$length,'0',STR_PAD_LEFT);
    }

    static function friendlyErrorType($type) {
        switch($type) {
            case E_ERROR: // 1 //
                return 'E_ERROR';
            case E_WARNING: // 2 //
                return 'E_WARNING';
            case E_PARSE: // 4 //
                return 'E_PARSE';
            case E_NOTICE: // 8 //
                return 'E_NOTICE';
            case E_CORE_ERROR: // 16 //
                return 'E_CORE_ERROR';
            case E_CORE_WARNING: // 32 //
                return 'E_CORE_WARNING';
            case E_COMPILE_ERROR: // 64 //
                return 'E_COMPILE_ERROR';
            case E_COMPILE_WARNING: // 128 //
                return 'E_COMPILE_WARNING';
            case E_USER_ERROR: // 256 //
                return 'E_USER_ERROR';
            case E_USER_WARNING: // 512 //
                return 'E_USER_WARNING';
            case E_USER_NOTICE: // 1024 //
                return 'E_USER_NOTICE';
            case E_STRICT: // 2048 //
                return 'E_STRICT';
            case E_RECOVERABLE_ERROR: // 4096 //
                return 'E_RECOVERABLE_ERROR';
            case E_DEPRECATED: // 8192 //
                return 'E_DEPRECATED';
            case E_USER_DEPRECATED: // 16384 //
                return 'E_USER_DEPRECATED';
        }
        return "type_$type";
    }

    /**
     * Substitutes {$key} patterns of the text to values of associative data
     * Used primarily for native language texts, but used for SQL generation where substitution is not based on SQL data syntax.
     * If no substitution possible, the pattern remains unchanged without error
     * Special cases:
     *    - {DMY$var} - convert hungarian date to english (deprecated)
     *  - {$var/subvar} - array resolution within array values (using multiple levels possible)
     *  - Using special characters if necessary: `{{}` -> `{`, `}` -> `}`
     *    - values of DateTime will be substituted as SHORT date of the application's language.
     *
     * @param string $text
     * @param array $data
     * @param FormatterInterface $formatter
     *
     * @return string
     */
    static function substitute($text, $data, $formatter=null) {
        return preg_replace_callback('#{(DMY|MDY)?(\\$[a-zA-Z_]+[\\\\/a-zA-Z0-9_-]*)}#', function($mm) use($data, $formatter) {
            if($mm[2]=='{') return '{';
            if(substr($mm[2],0,1)=='$') {
                // a keyname
                $subvars = explode('/', substr($mm[2],1));
                $d = $data;
                foreach($subvars as $subvar) {
                    if(is_array($d) && array_key_exists($subvar, $d)) $d = $d[$subvar]===null ? '#null#' : $d[$subvar];
                    else return $mm[0];
                }
            }
            else {
                // Other expression (not implemented)
                return $mm[0];
            }
            if($d instanceof DateTime) $d = $formatter->formatDateTime($d, IntlDateFormatter::SHORT, IntlDateFormatter::NONE);
            if($mm[1]=='MDY') {
                $d = $formatter->formatDateTime($d, IntlDateFormatter::SHORT, IntlDateFormatter::NONE, 'en');
            }
            if($mm[1]=='DMY') {
                $d = $formatter->formatDateTime($d, IntlDateFormatter::SHORT, IntlDateFormatter::NONE, 'en-GB');
            }
            return $d;
        }, $text);
    }

    /**
     * @param string|null $str
     * @param mixed $default
     *
     * @return bool
     */
    static function stringToBool($str, $default) {
        if($str===null) return $default;
        $str = trim(strtolower($str));
        if($str==='') return $default;
        $s = substr($str,0,1);
        if($s=='t' || $s=='i' || $s=='y' || $str==-1 || $str > 0) return true;
        if($s=='f' || $s=='h' || $s=='n' || $str==0) return false;
        if($str>' ') return true;
        return $default;
    }

    /**
     * Converts 'apple-banane' to 'AppleBanane'
     *
     * @param string $string -- string containing '_', '-' or ' ' as word separator
     * @param bool $pascalCase -- true if first character should be uppercase
     * @return string - camelized joint word without separators
     */
    static function camelize($string, $pascalCase = false) {
       $string = str_replace(array('-', '_'), ' ', $string);
       $string = ucwords($string);
       $string = str_replace(' ', '', $string);
       if (!$pascalCase) {
         return lcfirst($string);
       }
       return $string;
    }

    /**
     * Uncamelize a string, joining the words by separator character.
     * @param string $text to uncamelize
     * @param string $separator
     * @return string Uncamelized text
     */
    static function uncamelize($text, $separator='_') {
      // Replace all capital letters by separator followed by lowercase one
      $text = preg_replace_callback('/[A-Z]/', function ($matches) use($separator) {
        return $separator . strtolower($matches[0]);
      }, $text);

      // Remove first separator (to avoid _hello_world name)
      return preg_replace("/^" . $separator . "/", '', $text);

    }

    /**
     * Returns class name without namespace
     *
     * @param string|object $class -- FQN class name or an object
     *
     * @return string
     */
    static function shortName($class) {
        if(is_object($class)) {
            $reflect = new ReflectionClass($class);
            return $reflect->getShortName();
        }
        $p = strrpos($class, '\\');
        if($p===false) return $class;
        return substr($class, $p+1);
    }

    /**
     * Returns first argument, which evaluates true
     *
     * @param mixed $arg1,...
     *
     * @return bool|mixed
     */
    static function coalesce($arg1) {
        if($arg1) return $arg1;
        for($i=1; $i<func_num_args(); $i++) {
            if(($b = func_get_arg($i))) return $b;
        }
        return false;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Concatenates array values including keys.
     *
     * Similar to implode, but alpplies an additional glue between key and value.
     * If $recurse is not false, the glue may be an array for different levels (and uses the last one for additional levels).
     * If $recurse is not false it should be an two-element array for outer boundaries of array elements.
     * It may be also an array of two-elements arrays, each one for different leveles, likewise glue array.
     *
     * @param string|array $glue1 -- delimiter(s) between values
     * @param string|array $glue2 -- delimiter(s) between key and value
     * @param mixed $array -- data to convert (may be recursive)
     * @param boolean|array $recurse -- (array of) two-element array for boundaries.
     * @return string
     */
    static function implode_with_keys($glue1, $glue2, $array, $recurse=false) {
        $output = array();
        $g1 = is_array($glue1) ? $glue1[0] : $glue1;
        $g2 = is_array($glue2) ? $glue2[0] : $glue2;
        /** @var string|array|object $item */
        foreach($array as $key => $item ) {
            if(is_array($item)) {
                if($recurse) {
                    $glue1x = $glue1; if(is_array($glue1x) && count($glue1x)>1) array_shift($glue1x);
                    $glue2x = $glue2; if(is_array($glue2x) && count($glue2x)>1) array_shift($glue2x);
                    $recursex = $recurse;
                    if(is_array($recurse)) {
                        if(is_array($recurse[0])) {
                            $r1 = $recurse[0][0]; $r2 = $recurse[0][1];
                            if(count($recursex)>1) array_shift($recursex);
                        }
                        else {
                            $r1 = $recurse[0]; $r2 = $recurse[1];
                        }
                    }
                    else {
                        $r1 = '['; $r2 = ']';
                    }
                    $item = $r1. self::implode_with_keys($glue1x, $glue2x, $item, $recursex). $r2;
                }
                else $item = 'Array';
            }
            if($item instanceof DateTime) $item = $item->format('Y-m-d H:i');
            if(is_object($item)) {
                if(is_callable(array($item, '_toString'))) $item = $item->_toString();
                else $item = '{'.get_class($item).'}';
            }
            $output[] = $key . $g2 . $item;
        }
        return implode($g1, $output);
    }

    /**
     * Generates a valid XML name-id based on given string
     *
     * Replaces invalid characters to valid ones. Replaces accented letters to ASCII letters.
     *
     * - Element names must start with a letter or underscore
     * - Element names can contain letters, digits, underscores, and the specified enabled characters
     * - Element names cannot contain spaces
     *
     * @param string $str
     * @param string $def -- replace invalid characters to, default _
     * @param string $ena -- more enabled characters, e.g. '-' (specify - last, escape ] chars.)
     * @param int $maxlen -- maximum length or 0 if no limit. Default is 64.
     *
     * @return string -- the correct output, or empty if input was empty or null
     */
    static function toNameID($str, $def='_', $ena='.-', $maxlen=64) {
        return UXMLElement::toNameID($str, $def, $ena, $maxlen);
    }

    /**
     * Returns a random string contains only [a-z0-9]
     *
     * @param integer $len
     * @return string
     * @throws Exception
     */
    static function randStr($len) {
        $b = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $first = 24; // Range of first character is 0-first
        $l = strlen($b);
        $r = $b[random_int(0,$first)];
        for($i=1;$i<$len;$i++) {
            $r .= $b[random_int(0,$l-1)];
        }
        return $r;
    }

    /**
     * Calculates relative path to $to based on $from
     *
     * If a folder name is provided as 'to', it must be ended by '/'
     *
     * @author http://stackoverflow.com/users/208809/gordon
     * @param string $from -- absolute basepath
     * @param string $to -- absolute path to where
     * @return string -- the relative to $to
     */
    static function getRelativePath($from, $to) {
        // some compatibility fixes for Windows paths
        $from = is_dir($from) ? rtrim($from, '\/') . '/' : $from;
        $to   = is_dir($to)   ? rtrim($to, '\/') . '/'   : $to;
        $from = str_replace('\\', '/', $from);
        $to   = str_replace('\\', '/', $to);

        $from     = explode('/', $from);
        $to       = explode('/', $to);
        $relPath  = $to;

        foreach($from as $depth => $dir) {
            // find first non-matching dir
            //if(!array_key_exists($depth, $to)) throw new Exception('Invalid to: '.$xto);
            if(array_key_exists($depth, $to) && $dir === $to[$depth]) {
                // ignore this directory
                array_shift($relPath);
            } else {
                // get number of remaining dirs to $from
                $remaining = count($from) - $depth;
                if($remaining > 1) {
                    // add traversals up to first matching dir
                    $padLength = (count($relPath) + $remaining - 1) * -1;
                    $relPath = array_pad($relPath, $padLength, '..');
                    break;
                } else {
                    if(array_key_exists(0, $relPath)) $relPath[0] = './' . $relPath[0];
                }
            }
        }
        return implode('/', $relPath);
    }

    /**
     * Similar to standard str_split, but operates on multibyte strings.
     * Converts the string to an array of equal length pieces.
     *
     * @param $str
     * @param int $l
     *
     * @return array|array[]|false|string[]
     */
    static function mb_str_split($str, $l = 0) {
        if ($l > 0) {
            $ret = array();
            $len = mb_strlen($str, "UTF-8");
            for ($i = 0; $i < $len; $i += $l) {
                $ret[] = mb_substr($str, $i, $l, "UTF-8");
            }
            return $ret;
        }
        return preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
    }

    /**
     * multibyte-string version of strcspn
     *
     * @param string $subject
     * @param string $mask
     * @param int $start
     * @param null $length
     *
     * @return int
     */
    static function mb_strcspn($subject, $mask, $start=0, $length=null) {
        if($start || $length) return static::mb_strcspn(mb_substr($subject, $start, $length), $mask);
        $len = mb_strlen($subject);
        for($i=0;$i<$len;$i++) {
            if(mb_strpos($mask, mb_substr($subject,$i,1))!==false) break;
        }
        return $i;
    }

    static function mb_strspn($subject, $mask, $start=0, $length=null) {
        if($start || $length) return static::mb_strspn(mb_substr($subject, $start, $length), $mask);
        $len = mb_strlen($subject);
        for($i=0;$i<$len;$i++) {
            if(mb_strpos($mask, mb_substr($subject,$i,1))===false) break;
        }
        return $i;
    }

    /**
     * Ha az argumentum tömb, azt adja vissza. Ha nem, egyelemű tömbbe teszi.
     *
     * @param mixed $data
     * @return array
     */
    public static function forceArray($data) {
        return is_array($data) ? $data : array($data);
    }

    /**
     * Returns the number of bytes in the given string.
     * This method ensures the string is treated as a byte array by using `mb_strlen()`.
     * @param string $string the string being measured for length
     * @return int the number of bytes in the given string.
     */
    public static function byteLength($string)
    {
        return mb_strlen($string, '8bit');
    }

    /**
     * Check if given string starts with specified substring.
     * Binary and multibyte safe.
     *
     * @param string $string Input string
     * @param string $with Part to search inside the $string
     * @param bool $caseSensitive Case sensitive search. Default is true. When case sensitive is enabled, $with must exactly match the starting of the string in order to get a true value.
     * @return bool Returns true if first input starts with second input, false otherwise
     */
    public static function startsWith($string, $with, $caseSensitive = true)
    {
        if (!$bytes = static::byteLength($with)) {
            return true;
        }
        if ($caseSensitive) {
            return strncmp($string, $with, $bytes) === 0;
        } else {
            return mb_strtolower(mb_substr($string, 0, $bytes, '8bit'), 'UTF-8') === mb_strtolower($with, 'UTF-8');
        }
    }

    /**
     * Check if given string ends with specified substring.
     * Binary and multibyte safe.
     *
     * @param string $string Input string to check
     * @param string $with Part to search inside of the $string.
     * @param bool $caseSensitive Case sensitive search. Default is true. When case sensitive is enabled, $with must exactly match the ending of the string in order to get a true value.
     * @return bool Returns true if first input ends with second input, false otherwise
     */
    public static function endsWith($string, $with, $caseSensitive = true)
    {
        if (!$bytes = static::byteLength($with)) {
            return true;
        }
        if ($caseSensitive) {
            // Warning check, see http://php.net/manual/en/function.substr-compare.php#refsect1-function.substr-compare-returnvalues
            if (static::byteLength($string) < $bytes) {
                return false;
            }
            return substr_compare($string, $with, -$bytes, $bytes) === 0;
        } else {
            return mb_strtolower(mb_substr($string, -$bytes, mb_strlen($string, '8bit'), '8bit'), 'UTF-8') === mb_strtolower($with, 'UTF-8');
        }
    }

    /**
     * Converts a free-spacing Regex to canonical form
     * @see https://www.regular-expressions.info/freespacing.html
     *
     * @param string $fsre -- free spacing regex
     * @return string -- canonical Regex
     */
    public static function canonRegex($fsre) {
        // eliminating white spaces and comments as(?#comment) and ...#comment\n
        // echo htmlspecialchars('/\(\?\#[^)]*\)|(?<![\\\\\?])#[^\n]*\n|(?<!\(\?|\\\\)\s/m');
        return preg_replace('/\(\?#[^)]*\)|(?<![\\\\?])#[^\n]*\n|(?<!\(\?|\\\\)\s/m', '', $fsre);
    }

    /**
     * Visszaadja egy egész érték bináris képét, a bittömb alapján
     *
     * @param int $value
     * @param array $data -- bittérképdefiníció
     *
     * @return string
     * @deprecated Use {@see bitString()}
     * @codeCoverageIgnore
     */
    public static function bitStr($value, $data) { // eg. $rightbits
        $r='';
        foreach($data as $item)
            $r .= ($value & $item[0]) ? substr($item[1], 0, 1) : '.';
        return $r;
    }

    /**
     * Creates a delimited RegEx pattern from a bare pattern.
     *
     * @param $pattern -- pattern without delimiter
     * @return string -- pattern with ~ delimiters and escaped inner ~s
     */
    public static function toRegExp($pattern) {
        return '~' . str_replace('~', '\~', $pattern) . '~';
    }

    /**
     * Shows html error message without framework and exits
     *
     * @param mixed $code
     * @param mixed $title
     * @param mixed $message
     * @return void
     * @throws UXAppException
     */
    static function fatalError($code, $title, $message) {
        $title1 = substr($title, 0, strpos($title, '/'));
        header("Content-type: text/html");
        header("Status: $code $title1");
        $mainCss = UXApp::$app->hasComponent('assetManager') ? UXApp::$app->assetManager->single(['uhi67/uxapp', 'css/main.css']) : '';
        echo <<<EOT
<html lang="en">
<head>
    <title>UXApp error</title>
    <link rel="stylesheet" type="text/css" href="$mainCss" />
</head>
<body>
    <div id="fatal">
        <h2>$code $title</h2>
        <p>$message</p>
    </div>
</body>
</html>
EOT;
        $protocol = $_SERVER['SERVER_PROTOCOL'] ?? 'HTTP/1.0';
        header($protocol . ' ' . $code . ' ' . $title);
        exit($code);
    }

    /**
     * Creates backtrace array with args converted to printable
     *
     * @param int $skip
     * @param int $limit
     * @param int $depth -- depth of args
     *
     * @return array
     */
    static function getBacktrace($skip = 0, $limit = 0, $depth = 3) {
        $result = [];
        try {
            $bt = debug_backtrace(0, $limit);
            foreach($bt as $i => $s) {
                $index = $i - $skip;
                if($index < 0) continue;
                $s['args'] = self::objtostr($s['args'], true, true, $depth);
                if(isset($s['file']) && UXApp::$app) $s['file'] = UXApp::$app->relativePath($s['file']);
                $result[] = $s;
            }
        }
        catch(Throwable $e) {
            $result[] = [
                'file' => '!',
                'line' => '!',
                'message' => 'Exception occured in backtrace: '.$e->getMessage(), ' in file '.$e->getFile(). ' at line '.$e->getLine(),
                'backtrace' => str_replace(dirname(__DIR__,4), '', $e->getTraceAsString())
            ];
        }
        return $result;
    }

    /**
     * Returns $title with upper case first letter
     * (~mb_ucfirst)
     *
     * @param string $title
     * @return string
     */
    public static function upperCaseFirst($title) {
        if(!$title) return $title;
        return mb_strtoupper(mb_substr($title,0,1)).mb_substr($title,1);
    }

    /**
     * Extends the regex-pattern with accented letters
     *
     * @param $p
     *
     * @return string|string[]
     */
    public static function extendPattern($p) {
        if(strpos($p, '[')===false) $p = str_replace(
            ['á', 'a', 'é', 'e', 'í', 'i', 'ó', 'ö', 'o', 'ú', 'ü', 'u', 'y', '[i'],
            ['[áÁ]', '[aäáÁ]','[éÉ]', '[eéÉ]', '[íÍ]', '[iíÍ]','[óÓ]', '[öÖőŐ]', '[oóöőÓÖŐ]','[úÚ]', '[üÜűŰ]', '[uúüűÚÜŰ]','[yi]', '[yi'],
            strtolower($p)
        );
        return $p;
    }

    public static function is_valid_preg_pattern($pattern) {
        $old = set_error_handler(function($code, $message) {});
        $valid = !(@preg_match($pattern, null)===false);
        set_error_handler($old);
        return $valid;
    }

    /**
     * Returns file extension without .
     * 
     * @param $fileName
     * @return string
     */
    public static function ext($fileName) {
        if($p=strpos($fileName, '?')) $fileName = substr($fileName,0,$p);
        return strtolower(substr(strrchr($fileName, "."), 1));
    }

    /**
     * @param $dir
     * @param int $depth
     *
     * @return bool
     * @throws Exception
     */
    public static function makeDir($dir, $depth = 1) {
        if(!file_exists($dir)) {
            if($depth == 0) return false;
            static::makeDir(dirname($dir), $depth - 1);
            if(@!mkdir($dir, 0774)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Create a printable backtrace of an exception including original exception and all previous one
     * @param Throwable|Exception $e
     */
    public static function backtraceException($e) {
        $msg = sprintf("%s in file %s at line %d\nBacktrace:\n", $e->getMessage(), $e->getFile(), $e->getLine());
        $msg .= $e->getTraceAsString();
        if($e->getPrevious()) {
            $msg .= "\nPrevious exception:\n" . static::backtraceException($e->getPrevious());
        }
        return $msg;
    }

    public static function colorBacktrace($traceString) {
        $pattern1 = /** @lang RegExp */"~([\\\\/]vendor[\\\\/]uhi67[\\\\/])(uxapp[\\\\/][\w.-]+([\\\\/][\w-]+)*\\.php)\((\d+)\):~";
        $pattern2 = "~([\\\\/]vendor[\\\\/]uhi67[\\\\/])(uxapp-[\w.-]+[\\\\/][\w.-]+([\\\\/][\w-]+)*\\.php)\((\d+)\):~";
        $pattern3 = "~\((\d+)\):~";
        $trace = explode("\n", $traceString);
        $html = '';
        foreach($trace as $tr) {
            $tr = preg_replace($pattern1, '$1<span class="uxapp">$2 (</span><b>$4</b>):', $tr);
            $tr = preg_replace($pattern2, '$1<span class="uxapp-lib">$2 (</span><b>$4</b>):', $tr);
            $tr = preg_replace($pattern3, ' (<b>$1</b>):', $tr);
            $tr = preg_replace('~([\w-]+)\.php~', '<b>$1</b>.php:', $tr);
            $tr = preg_replace('~([\w-]+)\(~', '<b>$1</b>(:', $tr);
            $html .= "<div class='trace'>$tr</div>\n";
        }
        return $html;
    }

    /**
     * Waits for a test to satisfy
     *
     * @param Closure $test -- test to run. Must return true on success.
     * @param int $timeout -- timeout in sec
     * @param int $interval -- test interval in sec
     * @return bool -- true if test succeeded within timeout, false otherwise
     */
    public static function waitFor($test, $timeout=60, $interval=1) {
        $startTime = time();
        do {
            $lastTry = time();
            if($test()) return true;
            while(time() < $lastTry+$interval) sleep(1);
        }
        while(time() < $startTime+$timeout);
        return false;
    }

    /**
     * Components configure public properties here to make sure that only public properties are set.
     *
     * @param $object
     * @param $property
     * @param $value
     * @return void
     */
    public static function setter($object, $property, $value) {
        $object->$property = $value;
    }

}

