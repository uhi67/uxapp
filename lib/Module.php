<?php /** @noinspection PhpIllegalPsrClassPathInspection */

namespace uhi67\uxapp;

use DOMElement;
use DOMNode;
use Exception;
use ReflectionClass;
use ReflectionException;
use uhi67\uxml\UXMLElement;

/**
 * Base class for modules
 *
 * Modules can be registered for web or cli applications. First created instance registers the module class and it's assets.
 * Modules may have their own assets in the similar directory structure like the application
 * Assets of the modules are accessible:
 * - $scripturl/module/$this->moduleId/filename
 * - $baseUrl/module/
 *
 * @package UXApp
 * @author uhi
 *
 * @property-read string $moduleId -- Module ID for module asset references
 * @property-read RequestInterface|Request $request
 * @property-read BaseUrlFormatter $urlFormatter
 * @property-read Session $session
 */
abstract class Module extends Component {
    /** @var array $params -- user parameters for the component */
    public $params;
    /** @var UXAppPage $page -- owner page (mandatory for Xlet) */
    public $page;
    /** @var UXApp $parent -- root application */
    public $parent;
    /** @var string $moduleName -- module or application name */
    public $moduleName;

    /** @var string $rootPath -- application's root path */
    public $rootPath;
    /** @var string $modulePath -- this module's root path (Don't confuse with UXApp::$modulesPath) */
    public $modulePath;
	/** @var string $dataPath -- Runtime data, logs, ignore from git */
	public $dataPath;
    /** @var string $defPath -- xml definitions, syntaxes, other data included in git */
    public $defPath;
    public $incPath;
    public $pagesPath;
    public $commandsPath;
    /** @var string $basePath -- path of www assets */
    public $basePath;
    public $xslPath;

    /** @var string $baseurl -- base url for accessing module assets or application base url */
    public $baseurl;
    /** @var UXMLElement */
    public $node;
    /** @var Component[] $components */
    public $components = [];

    /** @var RequestInterface $_request -- cached Request object */
    private $_request;
    /** @var Session $_request -- cached Request object */
    private $_session;
    /** @var BaseUrlFormatter $_urlFormatter -- cached SimpleUrlFormatter object */
    private $_urlFormatter;

    /**
     * @throws ReflectionException
     * @throws UXAppException
     */
    public function prepare() {
        parent::prepare();

        $r = new ReflectionClass($this);
        if(!$this->moduleName) $this->moduleName = $r->getShortName();
        if(!$this->rootPath) $this->rootPath = $this->rootPath();
        if(!$this->modulePath) $this->modulePath = $this->modulePath();

        // Computing default paths
        $defaults = [
            'incPath'=> 'inc',
            'pagesPath'=> 'pages',
            'basePath'=> 'www',
            'xslPath'=>	'xsl',
            'dataPath'=> 'runtime',
            'defPath' => 'def',
        ];
        foreach($defaults as $key=>$value) {
            $newValue = $this->rootPath.'/'.$value;
            if(!$this->$key) $this->$key = is_dir($newValue) ? $newValue : $this->rootPath;
        }
        if($this->page instanceof UXAppPage && !$this->parent) $this->parent = $this->page->app;
        if($this->parent && !$this->baseurl) {
            $this->baseurl = UXApp::$app->urlFormatter->createUrl(['module' => $this->moduleId]);
        }
        if($this->page instanceof UXAppPage) $this->register();

        if($this->components === null) $this->components = [];
    }


    /**
     * Registers the module on the page (only at creating first instance)
     *
     * Calls init() if method exists (may be static)
     *
     * Called by Module::prepare()
     *
     * @return bool -- false: already registered, true: successful (failed: Exception)
     * @throws ReflectionException
     * @throws Exception
     */
    public function register() {
        if(!$this->page) throw new Exception('Page is mandatory for registerable Modules');
        if(array_key_exists(static::class, $this->page->modules)) return false;
        $this->page->modules[static::class] = $this;

        $r = new ReflectionClass($this);
        $dirname = dirname($c = $r->getFileName());
        if(!$dirname) throw new Exception("Module registration error: File '$c' not found");

        //  Calls init if exists
        if(is_callable([$this, 'init'])) {
            call_user_func([$this, 'init']);
        }

        if($this->page->view) {
            $this->registerAssets();
        }
        return true;
    }

    /**
     * TODO: Ezt át kell gondolni, fog ez menni instance nélkül is. TabControl::prepare érintett a változtatásban.
     *
     * @throws UXAppException
     * @throws Exception
     */
    public static function registerAssetsStatic($page) {
        // Statikus hívás esetén $page kötelező
        if(!$page) throw new Exception('Page not specified');
        Assertions::assertClass($page, UXAppPage::class);
        new static(['page' => $page]);
        // Létrehozás egyébként rekurzívan ugyanide vezet
    }

    /**
     * Registers module's assets in the view
     *
     * The default implementation registers all js, css and xsl files found in the module
     * (Searches base-dir, xsl, www, www/js, www/css)
     *
     * Called by register()
     *
     * Static version should be called from controller init, if a module needs registration:
     * ´UList::registerAssetsStatic($this);´
     *
     * @throws Exception
     */
    public function registerAssets($page = null) {
        if(!isset($this)) {
            // Statikus hívás esetén $page kötelező
            if(!$page) throw new Exception('Page not specified');
            Assertions::assertClass($page, UXAppPage::class);
            new static(['page' => $page]);
            // Létrehozás egyébként rekurzívan ugyanide vezet
            return;
        }
        else {
            $module = $this;
        }

        $page = $module->page;
        UXApp::trace("Registering assets of $module->moduleName");

        $r = new ReflectionClass($module);
        $dirname = dirname($c = $r->getFileName());
        if(!$dirname) throw new Exception("Module registration error: File '$c' not found");

        $dirname = preg_replace('~[/\\\\]src[/\\\\]?$~', '', $dirname);

        $assetConfig = [
            'dir' => $dirname,
            'patterns' => $patterns = [
                '*.js', '*.css', '*.xsl',
                'www/*.js', 'www/*.css', 'www/*.xsl',
                'www/js/*.js',
                'www/css/.../*',
                'xsl/*.xsl',
            ],
        ];
        $moduleAsset = $module->parent->assetManager->register($assetConfig);
        $moduleAsset->register($page->view, $patterns);
    }

    /**
     * Returns assets' url of the module.
     *
     * ### Usage
     *
     * - `$this->assets()` returns the assets url of the current module
     * - `Module::assets('vendor/component')` returns the assets url of the given module
     *
     * @param string|null $moduleId
     *
     * @return string
     */
    public function assets($moduleId=null) {
        if($moduleId===null && $this->parent) {
            return $this->parent->urlBase . '/module/' . $this->moduleId;
        }
        return UXApp::$app->urlBase . '/module/' . $moduleId;
    }

    /**
     * Must return module's root path
     * Default is the parent dir of the module class file.
     *
     * - rootPath property: user defined rootPath (UXApp: application's root path)
     * - rootPath(): computed module path
     *
     * @return string
     */
    public function rootPath() {
        if($this->rootPath) return $this->rootPath;
        return $this->modulePath();
    }

    public function modulePath() {
        $r = new ReflectionClass($this);
        $dirname = dirname($r->getFileName());
        $isVendor =  preg_match('~^([\\w:\\\\/.-]+[/\\\\]vendor[/\\\\]([\\w-]+[/\\\\][\\w-]+))~', $dirname, $mm);
        return $isVendor ? str_replace('\\', '/', $mm[1]) : $dirname;
    }

    function ajaxResult($a) {
        header('Content-Type: application/json');
        echo json_encode($a);
        return true;
    }

    /**
     * Processes runtime actions when request contains act=module
     *
     * The default implementation does nothing.
     *
     * @return void
     */
    function action() {}

    /**
     * Module ID is
     * - "vendorname/modulename" if a vendor module,
     * - "modulename" if is a legacy application module,
     * - full path otherwise.
     *
     * @return bool|string -- the module id as 'xxx/yyy'
     */
    public function getModuleId() {
        $rootPath = $this->modulePath;
        if(preg_match('~[/\\\\]vendor[/\\\\]([\w-]+[/\\\\][\w-]+)~', $rootPath, $mm))
            return str_replace('\\', '/', $mm[1]);
        if(preg_match('~[/\\\\]modules[/\\\\]([\w-]+)~', $rootPath, $mm))
            return $mm[1];
        return $rootPath;
    }

    public function getUrlFormatter() {
        if(!$this->_urlFormatter) {
            if($this->parent && $this->parent->hasComponent('urlFormatter')) {
                $this->_urlFormatter = $this->parent->urlFormatter;
            }
            else {
                $this->_urlFormatter = new SimpleUrlFormatter();
            }
        }
        return $this->_urlFormatter;
    }

    public function getRequest() {
        if(!$this->_request) {
            if($this->parent && $this->parent->hasComponent('request')) {
                $this->_request = $this->parent->request;
            }
            else {
                $this->_request = new Request();
            }
        }
        return $this->_request;
    }

    public function getSession() {
        if(!$this->_session) {
            if($this->parent && $this->parent->hasComponent('session')) {
                $this->_session = $this->parent->session;
            }
            else {
                return null; // Don't store, session may be initialized later
            }
        }
        return $this->_session;
    }

    public function getNode() {
        return $this->node;
    }

    /**
     * ## Determines the real parent node based on object class.
     * ### Supports
     * - UAppPage: uses node_content
     * - Xlet: uses first generated node
     *
     * @param DomElement|Component|UXAppPage $node_parent
     *
     * @return UXMLElement|DOMNode
     * @throws UXAppException
     */
    public function parentNode($node_parent) {
        if(!$node_parent && $this->page) return $this->page->view->node_content;
        if($node_parent instanceof Component) $node_parent = $node_parent->node;
        else Assertions::assertClass($node_parent, DOMElement::class);
        return $node_parent;
    }
}
