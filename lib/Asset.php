<?php

namespace uhi67\uxapp;

/**
 * Asset kezelés (cachelés)
 * ========================
 *
 * Kihívás: A vendor könyvtárakban és a modules-ben lévő asset elemek (js, css, jpg, xsl, stb) nincsenek a www scope-ben, ezért
 * nem hivatkozhatók közvetlenül a nézet lapokról.
 *
 * - (A) Cachelt megoldás az Asset class használatával. Az Asset class az asset fájlt a /www/assets/ mappába másolja, és oda mutató linket generál.
 *        A kiszolgálás nem igényel .htaccess bejegyzést és php futást. Példa: `<script src="/assets/4a/1de7-popper.min.js">`
 * - (B) [deprecated] Cache nélküli megoldás: .htaccess az /asset/.. és /vendor/... címeket az asset.php közvetítésével szolgálja ki futásidőben
 *      Példa: `<script src="/asset/popper.js/dist/umd/popper.min.js">`
 *
 * ## Az **Asset** class használata
 *
 * Az **Asset** classt az AssetManager hozza létre.
 *
 * Konfigurációs opciók:
 *
 * - $cacheDir -- web-accessible directory to store asset files.
 * - $cacheUrl -- url-path of the asset directory. E.g '/assets' -- inherited from AssetManager
 * - $itemCache-- cache to keep names and paths of registered assets -- inherited from AssetManager
 * - $dir -- base directory of the current asset
 * - $patterns -- filenames or filename patterns to be copid in the current asset
 * - $ttl -- time to live in the www cache -- default is -1 = forever. Specify 0 to replace on every registration.
 */
class Asset extends Component {
	// Configuration options
	/** @var string $cacheDir -- web-accessible directory to store asset files. E.g '/www/assets' -- inherited from AssetManager */
	public $cacheDir;
	/** @var string $cacheUrl -- url-path of the asset directory. E.g '/assets' -- inherited from AssetManager */
	public $cacheUrl;
	/** @var CacheInterface $itemCache -- cache to keep names and paths of registered assets -- inherited from AssetManager */
	public $itemCache;
	/** @var string $dir -- base directory of the current asset */
	public $dir;
	/** @var string[] $patterns -- filenames or filename patterns to be copied in the current asset */
	public $patterns;
	/** @var int $ttl -- time to live in the www cache -- default is -1 = forever. Specify 0 to replace on every registration. */
	public $ttl = -1;

	public $extensions = [
		'css' => ['css.map', 'min.css', 'min.css.map'],
		'js' => ['js.map', 'min.js', 'min.js.map'],
	];

	/** @var string $path -- base url of this asset within the cache directory (filenames appended with '/') */
	private $path;
	/** @var string $crc -- hash of the current asset directory */
	private $crc;

	/**
	 * Asset constructor.
	 *
	 * Configuration options:
	 * - dir -- asset directory name (mandatory, absolute or relative to project root or vendor or modulesPath)
	 * - patterns -- filename patterns (mandatory) enclose in ~~ for RegEx patterns
	 *
	 * @throws UXAppException
	 */
	public function prepare() {
		if(!is_dir($this->dir)) {
			if(is_dir($dir1 = UXApp::$app->rootPath . '/' . trim($this->dir, '/'))) $this->dir = $dir1;
			else if(is_dir($dir2 = UXApp::$vendorDir . '/' . trim($this->dir, '/'))) $this->dir = $dir2;
			else if(is_dir($dir3 = UXApp::$app->modulesPath . '/' . trim($this->dir, '/'))) $this->dir = $dir3;
			else throw new UXAppException("Asset directory '$this->dir' does not exist ($dir1, $dir2)");
		}

		if(!is_array($this->patterns)) throw new UXAppException('Patterns must be an array');
		$this->crc = dechex(crc32($this->dir));
		$this->path = substr($this->crc, 0, 2) . '/' . substr($this->crc, 2);
		$this->itemCache->set('asset_' . $this->dir, '/assets/' . $this->path);
		foreach($this->patterns as $pattern) {
			$this->matchPattern($this->dir, '', $pattern, function($file) {
				$this->copyFile($file);
			});
		}
	}

	/**
	 * Iterates through files of given pattern and calls the callback function for every file.
	 *
	 * Special pattern matches:
	 *
	 *        ~pattern~ -- denotes a single level RegEx pattern (always begins with ~)
	 *        dirPattern/subPattern -- matches directories first, then recurses subpattern
	 *        *,?          -- single level directory or file pattern may contain legacy metacharacters
	 *        ...          -- multiple level directory pattern: matches any levels and names of subdirectories
	 *
	 * @param string   $baseDir -- search root (search and return filenames in relation to this; no trailing '/')
	 * @param string   $dir -- iterated subdirectory, empty or 'dir/' (must have trailing / if non-empty)
	 * @param string   $pattern -- literal filename, directory pattern or RegEx pattern or dir/pattern
	 * @param callable $callback -- function(string $file) -- operation on found file
	 * @param null     $missing -- function(string $file) -- operation on missing file or directory
	 *
	 * @throws UXAppException
	 */
	private function matchPattern($baseDir, $dir, $pattern, $callback, $missing = null) {
		$d = $baseDir . '/' . $dir;
		if(substr($pattern, 0, 1) == '~' && substr($pattern, -1) == '~') {
			// Single level RegEx pattern
			$dh = dir($d);
			while(($file = $dh->read($dh)) !== false) {
				if($file == '.' || $file == '..') continue;
				if(filetype($d . $file) != 'dir') continue;
				if(preg_match($pattern, $file)) {
					$callback($dir . $file);
				}
			}
			$dh->close();
		} else if(($p = strpos($pattern, '/')) !== false) {
			// Match $dir and $subpattern
			$dirPattern = substr($pattern, 0, $p);
			$subpattern = substr($pattern, $p + 1);
			if($dirPattern == '...') {
				// any-depth directory pattern (may contain more directory patterns, heavy recursion follows)
				$dh = dir($d);
				while(($file = $dh->read()) !== false) {
					if($file == '.' || $file == '..') continue;
					if(filetype($d . $file) != 'dir') continue;
					$this->matchPattern($baseDir, $dir . $file . '/', $subpattern, $callback);
					$this->matchPattern($baseDir, $dir . $file . '/', $pattern, $callback); // restart ... matching!
				}
				$dh->close();
			} else if(preg_match('~[*?]~', $dirPattern)) {
				// Single level subdirectory pattern
				$dh = dir($d);
				while(($file = $dh->read($dh)) !== false) {
					if($file == '.' || $file == '..') continue;
					if(filetype($d . $file) != 'dir') continue;
					if(!fnmatch($dirPattern, $file)) continue;
					$this->matchPattern($baseDir, $dir . $file . '/', $subpattern, $callback);
				}
				$dh->close();
			} else {
				// literal subdirectory
				if(is_dir($d . $dirPattern)) {
					$this->matchPattern($baseDir, $dir . $dirPattern . '/', $subpattern, $callback, $missing);
				} else {
					if($missing) $missing($d . $dirPattern);
				}
			}
		} else if(preg_match('~[*?]~', $pattern)) {
			// match directory pattern in the current directory (use fnmatch)
			if($dh = opendir($d)) {
				while(($file = readdir($dh)) !== false) {
					if(filetype($d . $file) != 'file') continue;
					if(fnmatch($pattern, $file)) {
						$callback($dir . $file);
					}
				}
				closedir($dh);
			}
		} else {
			// Single literal filename
			$fileName = $dir . $pattern;

			// Use a single file (fileName includes path relative to asset root)
			if($missing && !file_exists($this->dir . '/' . $fileName)) $missing($fileName);
			else $callback($fileName);
		}
	}

	/**
	 * Copies a single file into the asset cache if not present
	 *
	 * Auto-includes (see {@see Asset::$extensions}):
	 *        ~.css -> ~.css.map, ~.min.css, ~.min.css.map
	 *        ~.js ->  ~.js.map, ~.min.js, ~.min.js.map;
	 *
	 * @param string $fileName -- relative fileName to the asset dir
	 *
	 * @return string -- cache path (relative to cacheDir)
	 */
	private function copyFile($fileName) {
		$filePath = $this->dir . '/' . $fileName;
		$cacheFileName = $this->cacheDir . '/' . $this->path . '/' . $fileName;
		if($this->ttl === 0 || !file_exists($cacheFileName) || $this->expired($fileName)) {
			if(!file_exists(dirname($cacheFileName))) {
				if(!@mkdir(dirname($cacheFileName), 0774, true)) {
					UXApp::$app->error('Cannot create asset dir', [$fileName . '->' . $cacheFileName]);
				}
			}
			copy($filePath, $cacheFileName);

			// Auto-include related extensions (.min.map.*)
			$ext = Util::ext($fileName);
			if(array_key_exists($ext, $this->extensions)) {
				$baseName = substr($fileName, 0, strrpos($fileName, "."));
				foreach($this->extensions[$ext] as $ext2) {
					$extName = $baseName . '.' . $ext2;
					if(file_exists($this->dir . '/' . $extName)) $this->copyFile($extName);
				}
			}
		}
		return $this->path . '/' . $fileName;
	}

	/**
	 * Checks if file is expired in the www cache according to ttl.
	 * If ttl is 0 the file is always expired.
	 * If ttl is -1 the file is never expired.
	 *
	 * @param string $fileName -- relative fileName to the asset dir
	 */
	public function expired($fileName) {
		if($this->ttl == 0) return true;
		if($this->ttl == -1) return false;
		$cacheFileName = $this->cacheDir . '/' . $this->path . '/' . $fileName;
		return (filemtime($cacheFileName) + $this->ttl < time());
	}

	/**
	 * Returns an url path for a specific file from an asset, or false if not exists
	 *
	 * @param string $fileName
	 *
	 * @return string|false
	 */
	public function url($fileName) {
		$url = '/assets/' . $this->path;
		if(!$fileName) return $url;
		if(!file_exists($this->dir . '/' . $fileName)) return false;
		return $this->cacheUrl . '/' . $this->copyFile($fileName);
	}

	/**
	 * Registers resources on the view from the asset
	 *
	 * See pattern syntax at {@see matchPattern()}
	 *
	 * @param UXPage          $view
	 * @param string|string[] $resources -- filenames or filename patterns
	 *
	 * @throws
	 */
	public function register($view, $patterns) {
		if(!is_array($patterns)) $patterns = [$patterns];

		foreach($patterns as $pattern) {
			// Search in the cache
			$this->matchPattern($this->cacheDir . '/' . $this->path . '/', '', $pattern, function($resource) use ($view) {
				$type = strtolower(substr(strrchr($resource, "."), 1));
				if(!in_array($type, ['js', 'css', 'xsl'])) {
					$url = $this->url($resource);
					if(!$url) throw new UXAppException("Resource '$resource' not found in asset");
					$module = basename(dirname($this->dir)) . '-' . basename($this->dir);
					$view->addAssetFile($module, $resource, $url);
					return;
				}
				if($type == 'xsl') {
					$relativePath = Util::getRelativePath(UXApp::$app->rootPath, UXApp::$app->basePath . '/');
					$path = str_replace('//', '/', '../../' . $relativePath . $this->url($resource));

					$view->addXslFile($this->url($resource), [
						'path' => $path, // Szerver render alapja
						'module' => null,
						'order' => 0,
						'module-path' => null,
					]);
				} else {
					$url = $this->url($resource);
					if(!$url) throw new UXAppException("Resource '$resource' not found in asset");
					$view->addAsset($type, $url);
				}
			});
		}
	}
}
