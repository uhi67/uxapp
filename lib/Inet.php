<?php /** @noinspection PhpUnused */

namespace uhi67\uxapp;

/**
 * Inet -- an internet IPv4 or IPv6 address with optional netmask.
 *
 * @package UXApp
 * @author Peter Uherkovich
 * @copyright 2017
 *
 * @property $ver
 * Protocol version or 0 if no valid address is defined
 * @property $mask
 * The effective mask. 32 indicates single host in IPv4 or 128 in IPv6
 * @property $valid
 * Returns false if it is an invalid address
 * @property $end
 * Returns the last address of the range. This is the same as the $value if single host (mask is NULL/32/128)
 */
class Inet extends Component {
	#const VALID = '/^(((?=.*(::))(?!.*\3.+\3))\3?|([\dA-F]{1,4}(\3|:\b|$)|\2))(?4){5}((?4){2}|(((2[0-4]|1\d|[1-9])?\d|25[0-5])\.?\b){4})\z/i';
	const VALID_IP_NAME = '/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$|^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$|^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/';
	const VALID =         '/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$|^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/';

	/** @var string $value -- packed host address. 4 or 16 bytes long. False represents invalid value */
	public $value = null;
	/** @var integer $netmask -- number of host bits, NULL or /32 indicates single host in IPv4 or /128 in IPv6 */
	public $netmask = null;

	/**
	 * Inet constructor.
	 *
	 * @param string|array $ipaddr
	 */
	public function __construct($ipaddr=[]) {
		if(is_array($ipaddr)) parent::__construct();
		else {
			// Strip out the netmask, if there is one.
			$cx = strpos($ipaddr, '/');
			if($cx) {
				$this->netmask = (int)(substr($ipaddr, $cx + 1));
				$ipaddr = substr($ipaddr, 0, $cx);
			} else $this->netmask = null; // No netmask present

			// Convert address to packed (binary) format
			$valid = preg_match(static::VALID, $ipaddr);
			$this->value = $valid ? @inet_pton($ipaddr) : false;
		}
	}

	/**
	 * Returns protocol version (4,6)
	 * Returns 0 on invalid address
	 * @return int
	 */
	public function getVer() {
		$len = strlen($this->value);
		return $len==4 ? 4 : ($len==16 ? 6 : 0);
	}

	public function getValid() {
		return $this->value !== false;
	}

	/**
	 * Returns effective mask length. If no mask was given, returns single-host mask
	 *
	 * @return int
	 */
	public function getMask() {
		return $this->netmask ? $this->netmask : ($this->ver==6 ? 128 : ($this->ver==4 ? 32 : 0));
	}

	/**
	 * Determines if address represents a single host
	 *
	 * @return boolean
	 */
	public function isHost() {
		return $this->ver==6 ? $this->mask==128 : $this->mask==32;
	}

	/**
	 * Returns address value in hexadecimal format without mask.
	 *
	 * @return string
	 */
	public function toHex() {
		// Let's display it as hexadecimal format
		$result = '';
		foreach(str_split($this->value) as $char) $result .= str_pad(dechex(ord($char)), 2, '0', STR_PAD_LEFT);
		return $result;
	}

	/**
	 * Returns an inet data with optional netmask (PostgreSQL syntax)
	 *
	 * @return string
	 */
	public function toString() {
		return $this->valid ? inet_ntop($this->value) .($this->netmask ? ('/'.$this->netmask) : '') : '';
	}

	public function __toString() {
		return '{'.$this->toString().'}';
	}

	/**
	 * Checks $ip string if it is a valid MAC address
	 * Returns parsed string value (without separators) if valid
	 *
	 * @param string $ip -- value to check
	 * @return string|boolean -- false on invalid
	 */
	public static function isValid($ip) {
		$new = new Inet($ip);
		return $new->valid;
	}

	/**
	 * Returns packed mask value
	 *
	 * 1-value bits on network bits.
	 *
	 * @return string (4 or 16 bytes) or false on invalid
	 */
	public function maskValue() {
		if(!$this->valid) return false;
		$netmask = $this->mask; // Number of network bits
		$len = $this->ver==4 ? 32 : 128; // Total number of bits
		$r = '';
		for($i=0; $i<$len/8; $i++) {
			// i-th byte from the beginning
			if($netmask>=8) { $r .= chr(255); $netmask -= 8; }
			else if($netmask==0) { $r .= chr(0); }
			else { $r .= chr(255 - ((1<<(8-$netmask))-1)); $netmask = 0; }
			#if($netmask>0 and $netmask<8) App::trace(['x', (1<<4) . ', '. ((1<<4) - 1) . ", $netmask: " . ((1<<(8-$netmask))-1));
		}
		return $r;
	}

	/**
	 * Returns the first address of the range (the 'network' address)
	 *
	 * This is the same as the $value if single host (mask is NULL/32/128)
	 *
	 * @return string -- single-address or false on failure
	 */
	public function getFirst() {
		if(!$this->valid) return false;
		if($this->netmask === NULL || $this->ver==4 && $this->netmask==32 || $this->ver==6 && $this->netmask==128) return inet_ntop($this->value);

		$first = $this->value & $this->maskValue();
		return inet_ntop($first);
	}
	/**
	 * Returns the last address of the range (the 'broadcast' address)
	 *
	 * This is the same as the $value if single host (mask is NULL/32/128)
	 *
	 * @return string -- new single-address or false on failure
	 */
	public function getLast() {
		if(!$this->valid) return false;
		if($this->netmask === NULL || $this->ver==4 && $this->netmask==32 || $this->ver==6 && $this->netmask==128) return inet_ntop($this->value);

		$last = $this->value | ~$this->maskValue();
		return inet_ntop($last);
	}

	/**
	 * Checks if given ip/range is contained in the current range
	 *
	 * Returns false on false address
	 *
	 * @param string|Inet $ip
	 * @return boolean
	 */
	function contains($ip) {
		if(is_string($ip)) $ip = new Inet($ip);
		if(!$this->valid || !$ip->valid) return false;
		# select $ip << $this, but based on value/netmask
		if($ip->value < $this->value) return false;

		$first = $this->value & ($mv = $this->maskValue());
		$net = $ip->value & $mv;
		return $first==$net;
	}
	/**
	 * Returns a new Inet object with the next ip address.
	 * Does not check the range boundary!
	 *
	 * @throws InternalException
	 * @throws UAppException
	 */
	public function next() {
		if(!$this->valid) return null;
		return new Inet(inet_ntop(static::add($this->value, 1)), $this->netmask);
	}

	/**
	 * Returns a new Inet object with the previous ip address.
	 * Does not check the range boundary!
	 *
	 * @throws InternalException
	 * @throws UAppException
	 */
	public function prev() {
		if(!$this->valid) return null;
		return new Inet(inet_ntop(static::add($this->value, -1)), $this->netmask);
	}

	/**
	 * Adds a signed integer to a packed ip address.
	 *
	 * @param string $ip -- packed ip address (4 or 16-byte long string)
	 * @param int $num -- number to add (can be negative)
	 * @return string -- Resulted packed ip address
	 */
	public static function add($ip, $num) {
		$c = $num;
		$i = strlen($ip)-1;
		while($i>=0 && $c) {
			$n = ord($ip[$i]) + $c;
			$c = 0;
			if($n > 255) {
				$c = floor($n / 256);
				$n = $n % 256;
			}
			if($n < 0) {
				$c = 0;
				while($n < 0) {
					$n += 256;
					$c--;
				}
				$n = $n % 256;
			}

			$ip[$i--] = chr($n);
		}
		return $ip;
	}
}
