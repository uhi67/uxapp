<?php
namespace uhi67\uxapp;
use ReflectionException;

/**
 * L10n (Localization is 12 letter long)
 * Basic solution for L10n functions
 * 
 * Uses systext table for translations
 * (Apply UXApp/lib/sql/db_update_2017_1121_systext_create.sql to create table)
 * Uses default L10nBase for formatting dates.
 *
 * ### Configuration
 *
 * ```
 * 'l10n' => array(
 * 		'class' => L10nFile::class,
 * 		'textClass' => 'Systext',
 * 		'db' => 'anydb',		// connection config name or config array. Default is 'db'
 * 		'defaultLocale' => 'hu',		// Default language with optional locale, may be changed by UXApp::setLang(lang/locale)
 * 		'source' => 'hu',		// Default source language, default is 'en'
 *      'param' => 'la',                // Language swith parameter
 *      'cookieName' => 'language',     // Cookie name for selected language if cookie is enabled
 * 		'cookieParams' => [],			// Optional cookie parameters
 * ),
 * ```
 *
 * @author uhi
 * @package UXApp
 * @copyright 2017 - 2019
 */

class L10n extends L10nBase {
	public $textClass;
	public $db;

	public function prepare() {
		parent::prepare();
		if($this->db) {
			if(is_array($this->db)) $this->db = Component::create($this->db);
			else if(is_string($this->db)) {
				$this->db = UXApp::$app->database;
			}
		}
	}

	/**
	 * Translates a system text from source language to given language
	 * 'uxapp' category is translated by parent class, all others from `systext` table.
	 * If translation is not found, creates a default translation as '*source*'
	 * If source is not found, creates a default source record with default source language
	 *
	 * @param string $category -- message category, the framework itself uses 'uxapp'. Application default is 'app'
	 * @param string $source - source language text or text identifier
	 * @param array $params - replaces $var parameters
	 * @param string $lang - target language code or user default language
	 *
	 * @return string
	 * @throws UXAppException
	 * @throws ReflectionException
	 */
	public function getText($category, $source, $params=NULL, $lang=null) {
		if($lang===null) $lang = $this->lang;
		if($lang===true || $lang===null) $lang = UXApp::$app->lang;
		if($category=='uxapp') return parent::getText($category, $source, $params, $lang);
		$la = substr($lang,0,2);

		/** @var Query $query */
		$query = Query::createSelect([
			'from' => $this->textClass,
			'fields' => ['value'],
			'joins' => ['s'=>'source1'],
			'condition' => [['=', 'lang', '$2'], ['or',
				[(is_int($source) ? 's.id' : 's.value') => '$1'],
				['source'=>null, (is_int($source) ? 'id': 'value') => '$1']
			]],
			'params' => [$source, $lang],
			'connection' => $this->db,
		]);
		$text = $query->scalar();

		if($text===null) {
			/** @var string $dsl default language */
			$dsl = $this->source;
			$text = '*'.$source.'*';
			if(is_int($source)) {
				$systext_source = call_user_func([$this->textClass, 'first'], $source); // Systext::first($source);
				$text = '*'.$source.'*';
				if(!$systext_source) {
					// We have no clue what the text actually should be.
					/** @var Model $systext_source */
					$systext_source = new $this->textClass(['id'=>$source, 'lang' => $dsl, 'value' => $text]);
					if(!$systext_source->save()) {
						throw new UXAppException('Error creating system text source item', $systext_source->errorValues);
					}
				} 
				/** @var Model $systext_translation */
				$systext_translation = new $this->textClass(['lang'=>$la, 'value'=>$text, 'source'=>$systext_source->id]);
				if(!$systext_translation->save()) {
					throw new UXAppException('Error creating default translation', $systext_translation->errorValues);
				}
			}
			else {
				// Find default
				$systext_source = call_user_func([$this->textClass, 'first'], // $condition, $orders=null, $params=null, $connection=null, $nocache=false
					['value'=>$source],
					[[['=', 'lang', '$1'], 'desc']],
					[$dsl],
					$this->db
				); // null lang, default lang, others
				// Create default record template
				if(!$systext_source) {
					/** @var Model $systext_source */
					$systext_source = new $this->textClass(['lang' => $dsl, 'value' => $source], $this->db); // $config=null, $connection=null
					if(!$systext_source->save()) {
						throw new UXAppException('Error creating system text source item', $systext_source->errorValues);
					}
				}
				if($la != $dsl) {
					// Create translation record template
					/** @var Model $systext_translation */
					$systext_translation = new $this->textClass(['lang'=>$la, 'value'=>$text, 'source'=>$systext_source->id], $this->db);
					if(!$systext_translation->save()) {
						throw new UXAppException('Error creating default translation', $systext_translation->errorValues);
					}
				}
			}
		}
		
		// substitute parameters
        if($params) {
        	if(!is_array($params)) $params = [$params];
        	$text = Util::substitute($text, $params);
       	}
		return $text;
	}	
}
