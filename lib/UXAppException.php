<?php
namespace uhi67\uxapp;

use Exception;
use Throwable;

/**
 * UmxcException
 */
class UXAppException extends Exception {
    /** @var string|array $extra -- debug info */
    public $extra;
    /** @var int HTTP status code */
    public $statusCode;
    /** @var string $goto -- url for user to continue */
    public $goto;

    /**
     * This constructor most get the exception text in english
     *
     * @param string|array $message message text or [message, params, info]
     * @param string $extra
     * @param Exception $previous
     * @param int $code
     * @param null $goto
     */
    public function __construct($message, $extra=null, $previous=null, $code=0, $goto=null) {
        if(is_array($message)) $message = vsprintf($message[0], $message[1]);
        parent::__construct($message, $code, $previous);
        $this->extra = $extra;
        $this->goto = $goto;
        $this->statusCode = $code;
    }

    /**
     * Returns extra info as string
     * @return string
     */
    public function getExtra() {
        try {
            return $this->extra===null ? '' : Util::objtostr($this->extra);
        }
        catch(Throwable $e) {
            return '(* Undisplayable extra info *)';
        }
    }

    /**
     * Returns url of continue link for user, or an array containing the link caption and the url.
     *
     * @return string|array
     */
    public function getGoto() {
        return $this->goto;
    }

    public function getTitle() {
        return 'Application Error / Alkalmazáshiba';
    }

    public function getSubtitle() {
        return 'Internal Error / Belső hiba';
    }
}
