<?php
namespace uhi67\uxapp;

/**
 * Created by PhpStorm.
 * User: uhi
 * Date: 2018. 08. 29.
 * Time: 21:18
 */

/**
 * DataSourceInterface must be implemented by data sources
 *
 * Data sources provide data for UList and similar components
 *
 * @property-read $count
 * @property string $modelClass -- className to return as elements
 * @property-write string $pattern
 */
interface DataSourceInterface {
    /**
     * Total number of items in the set
     *
     * @return int the number of items in the set
     */
    public function getCount();

    /**
     * Returns the specified section of items
     *
     * If modelClass is set, model elements are returned, arrays otherwise.
     *
     * @param int $start -- 0-based offset
     * @param int $count -- number of items
     * @param array $orders -- order array (same as for Query)
     * @return Model[] -- the list of items
     */
    public function fetch($start, $count, $orders);

    /**
     * Sets the search pattern if the data source supports it.
     * The search pattern will narrow the set of items.
     * Optional params may specify the scope of the pattern.
     * For example, in case of UList, $params['fieldName'] will contain $list->fieldName()
     *
     * @param string $pattern
     * @param array $params
     * @return DataSourceInterface
     */
    public function setPattern($pattern, $params=null);

    /**
     * @param string|BaseModel $modelClass
     */
    public function setModelClass($modelClass);

    /**
     * @return string|BaseModel
     */
    public function getModelClass();
}
