<?php /** @noinspection PhpIllegalPsrClassPathInspection */

namespace uhi67\uxapp;

Interface RequestInterface {

    public function post($name=null, $default=null);
    public function get($name=null, $default=null);

    /**
     * Returns a named request variable if present or default
     * The variable may be in GET or POST.
     * Returns a value from previously stored query if present.
     * This value can be set by Request::setReq()
     *
     * @param string $name
     * @param string $default
     *
     * @return string
     */
    public function req($name=null, $default=null);

    /**
     * Returns an integer value from a named request variable if present or default
     *
     * @param $name
     * @param null $default
     *
     * @return int|mixed|null
     */
    public function reqInt($name, $default = null);

    /**
     * Returns an array value from a named request variable if present or default
     *
     * You may post array value form a form using variable name(s) containing []
     * You may post associative array as well.
     *
     * @param string $name
     * @param array $default
     *
     * @return array -- no non-array value will be accepted
     */
    public function reqArray($name, $default = []);

    /**
     * Returns a value from multiple OR-ed field (e.g checkbox set)
     *
     * @param string $name
     * @param int|mixed $default
     *
     * @return integer
     * @example
     * On the form use name 'var[]' for each field and values 1,2,4,8,...
     * The returned value will be 5 if first and third box is checked.
     * Variable indices will be ignored.
     * Returns default if variable not found.
     */
	public function reqMultiOr($name, $default=0);

    /**
     * Returns an integer value from a named request variable if present or default
     *
     * @param $name
     * @param null $default
     *
     * @return int|mixed|null
     */
    public function getInt($name, $default = null);

    /**
     * Returns an array value from a named request variable if present or default
     *
     * You may post array value form a form using variable name(s) containing []
     * You may post associative array as well.
     *
     * @param string $name
     * @param array $default
     *
     * @return array -- no non-array value will be accepted
     */
    public function getArray($name, $default = []);

    /**
     * Returns a value from multiple OR-ed field (e.g checkbox set)
     *
     * @param string $name
     * @param int|mixed $default
     *
     * @return integer
     * @example
     * On the form use name 'var[]' for each field and values 1,2,4,8,...
     * The returned value will be 5 if first and third box is checked.
     * Variable indices will be ignored.
     * Returns default if variable not found.
     */
	public function getMultiOr($name, $default=0);

    /**
     * Returns an integer value from a named POST variable if present or default
     *
     * @param $name
     * @param null $default
     *
     * @return int|mixed|null
     */
    public function postInt($name, $default = null);

    /**
     * Returns an array value from a named POST variable if present or default
     *
     * You may post array value form a form using variable name(s) containing []
     * You may post associative array as well.
     *
     * @param string $name
     * @param array $default
     *
     * @return array -- no non-array value will be accepted
     */
    public function postArray($name, $default = []);

    /**
     * Returns a value from multiple OR-ed field (e.g checkbox set) from the POST only
     *
     * @param string $name
     * @param int|mixed $default
     *
     * @return integer
     * @example
     * On the form use name 'var[]' for each field and values 1,2,4,8,...
     * The returned value will be 5 if first and third box is checked.
     * Variable indices will be ignored.
     * Returns default if variable not found.
     */
	public function postMultiOr($name, $default=0);

    /**
     * Returns an uploaded file from the request
     *
     * Returns a value from previously stored query if present.
     * This value can be set by Util::setReq()
     *
     * Returns array of FileUpload if request variable is an array variable[]
     *
     * @param string $name
     *
     * @return FileUpload|array|null
     */
    public function file($name);
    /**
     * @inheritDoc
     * @param string $name
     * @return array|FileUpload|null
     * @deprecated use file()
     */
    public function getFile($name);
}
