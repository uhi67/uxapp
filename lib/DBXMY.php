<?php /** @noinspection PhpComposerExtensionStubsInspection */
/** @noinspection PhpIllegalPsrClassPathInspection */

/** @noinspection PhpUnused */

namespace uhi67\uxapp;

use DateTime;
use Exception;
use mysqli;
use mysqli_result;
use Throwable;

/**
 * DBXMY.php
 * 
 * @package UXApp
 * @author uhi
 * @copyright 2016 
 * @abstract database functions implemented for MySQL
 */

if(!function_exists('mysqli_connect')) {
    Util::fatalError(500, 'Internal Server Error / Belső szerverhiba', 'MySQL not supported / MySQL támogatás hiányzik.');
}

class DBXMY extends DBX {
	private $db_last_error;
	
	/** 
	 * @var array $typeNames -- sql_type => common_type mapping
	 * common type names:
	 *  - basic php types (lowercase)
	 *  - 'xml' denotes xml valid string
	 *  - php classnames (Uppercase, eg. DateTime)
	 *  - 'string' is default, not indicated
	 *  - date (DateTime without time)
	 *  - time (DateTime without date or integer in sec)
	*/
	public static $typeNames = array(
		1 => 'tinyint',
		2 => 'smallint',
		3 => 'int',
		4 => 'float',
		5 =>'double',
		7 =>'timestamp',
		8 =>'bigint',
		9 =>'mediumint',
		10 => 'date',
		11 => 'time',
		12 => 'DateTime',
		13 => 'year',
		16 => 'bit',
		252 => 'string',
		//252 is currently mapped to all text and blob types (MySQL 5.0.51a)
		253 => 'string',
		254 => 'string',
		246 => 'double'
	);

	/**
	 * @throws Exception
	 */
	public function prepare() {
		$this->type = 'MY';
		if($this->connectNow) $this->connection = $this->connect();
	}

	/**
	 * @return mysqli|null|resource
	 * @throws Exception
	 */
	protected function connect() {
		$this->connection = mysqli_connect($this->host, $this->user, $this->password, $this->name, $this->port);
        if(!$this->connection) {
      		$this->connerr = true;
      		throw new Exception('Database connection error');
		}
		else mysqli_set_charset($this->connection, $this->encoding);
		return $this->connection;
	}


	function close() {
		if($this->connection) mysqli_close($this->connection);
	}
	
	function error($rs=null) {
		$e = mysqli_error($this->connection);
		if(!$e) $e = $this->db_last_error;
		else $this->db_last_error = $e;
		return $e;
	}

	/** @noinspection PhpMethodNamingConventionInspection */

	/**
	 * @param mysqli $rs
	 *
	 * @return int
	 * @throws Exception
	 */
	function affected_rows($rs) {
		if(!$rs) throw new Exception('Invalid result set');
		return mysqli_affected_rows($rs);
	}

	/**
	 * Run SQL query substituting $1, $2 style parameters.
	 * Only sinlge SQL statement is allowed. Use execute to run multiple statements.
	 *
	 * @param string $sql
	 * @param array $params -- $1 parameter has 0-index
	 *
	 * @return bool|mysqli_result
	 * @throws Exception
	 */
	function query($sql, $params=NULL) {
		$this->autoconnect();
		$sql = trim($sql, " \t\n\r\0\x0B;");
	    if($params) $sql = $this->bindParams($sql, $params);
		if(!($r = mysqli_query($this->connection, $sql))) $this->errtrace($sql);
		return $r;
	}

	function free($resultSet) {
		mysqli_free_result($resultSet);
		return true;
	}

	/**
	 * Executes a series of SQL commands
	 *
	 * @param string $sql
	 * @param null $params
	 */
	function execute($sql, $params=null) {
		$this->autoconnect();
		if($params) $sql = $this->bindParams($sql, $params);
		if(!($r = mysqli_multi_query($this->connection, $sql))) $this->errtrace($sql);
		do {
			if ($result = mysqli_store_result($this->connection)) mysqli_free_result($result);
		} while (mysqli_next_result($this->connection));
		return $r;
	}

	/**
	 * @param mysqli_result $resultset
	 * @param int $row
	 * @param int $field
	 *
	 * @return array|bool|string
	 */
	function result($resultset, $row, $field=0) {
		if(!$this->connection) return false;
		mysqli_data_seek($resultset, $row);
		#$rowid = 'my_'.spl_object_hash($resultset).'_'.$row;
		$row = mysqli_fetch_assoc($resultset);
		if(is_numeric($field)) { $row1 = array_values($row); $r = $row1[$field]; }
		else $r = $row[$field];
		return $r;
	}

	/** @noinspection PhpMethodNamingConventionInspection */
	/**
	 * @param $rs
	 */
	function free_result($rs) {
		mysqli_free_result($rs);
	}

	/**
	 * @param mysqli_result $resultset
	 *
	 * @return int
	 */
	function fields($resultset) {
		return mysqli_num_fields($resultset);
	}

	/** @noinspection PhpMethodNamingConventionInspection */

	/**
	 * @param mysqli_result $resultset
	 *
	 * @return int
	 * @throws Exception
	 */
	function num_rows($resultset) {
		if(!$resultset) throw new Exception($this->error());
		return @mysqli_num_rows($resultset);
	}
	
	function lastoid(/** @noinspection PhpUnusedParameterInspection */ $recordset) {
		return false;
	}

	/** @noinspection PhpMethodNamingConventionInspection */

	/**
	 * @param string $table
	 * @param string $fieldname
	 *
	 * @return int|mixed|null
	 * @throws UXAppException
	 */
	function last_id($table='', $fieldname='') {
		return $this->selectvalue(/** @lang SQL */"select last_insert_id()");
	}

	/**
	 * @param mysqli_result $resultset
	 * @param int $i
	 *
	 * @return string
	 */
	function fieldname($resultset, $i) {
		$fx = mysqli_fetch_field_direct($resultset, $i);
		return $fx->name;
	}

	/**
	 * Must return the common type of a field in the resultset
	 * 
	 * @param mysqli_result $resultset
	 * @param integer $i
	 * @return string
	 */
	function fieldtype($resultset, $i) {
		$fx = mysqli_fetch_field_direct($resultset, $i); 
		return self::mapType($fx->type);
	}

	/** @noinspection PhpMethodNamingConventionInspection */

	/**
	 * Elhoz egy sort indexelt tĂ¶mbbe
	 * 
	 * @param mysqli_result $resultset
	 * @param int $row -- sor indexe (0-tĂłl) -- default: kĂ¶vetkezĹ‘ sor
	 * @return array -- stringek; vagy false
	 * 
	 * An array, indexed from 0 upwards, with each value represented as a string. Database NULL values are returned as NULL.
	 * FALSE is returned if row exceeds the number of rows in the set, there are no more rows, or on any other error.
	 */
	function fetch_row($resultset, $row=null) {
		mysqli_data_seek($resultset, $row);		
		return $this->decode(mysqli_fetch_row($resultset));
	}

	/** @noinspection PhpMethodNamingConventionInspection */

	/**
	 * @param mysqli_result $resultset
	 * @param null $row
	 *
	 * @return array|string
	 */
	function fetch_assoc($resultset, $row=null) {
		mysqli_data_seek($resultset, $row);
		return $this->decode(mysqli_fetch_assoc($resultset));
	}

	/** @noinspection PhpMethodNamingConventionInspection */

	/**
	 * Returns a record to associative array
	 * Fields will be converted to mapped php types
	 *
	 * @param mysqli_result $resultset
	 * @param null $row
	 *
	 * @return array
	 * @throws Exception
	 */
	function fetch_assoc_type($resultset, $row = null) {
		if($row) mysqli_data_seek($resultset, $row);
		$data = mysqli_fetch_row($resultset);

		/** @var array $fields -- array of field definitions */
		$fields = mysqli_fetch_fields($resultset);

		$result = array();
		foreach($fields as $i=>$field) {
			$name = $field->name;
			$type = $field->type;
			$value = $data[$i];
			$result[$name] = $this->mapData($value, $type);
		}
		return $result;
	}

	/**
	 * Must return the identifier with vendor-specific quoting
	 * E.g. 'where' => '`where`' 
	 * 
	 * @param string $name
	 * @return string
	 */
	public function quoteIdentifier($name) {
		return '`'.$name.'`';
	}

	public function supportsOrderNullsLast() { return false; }

	/** @noinspection PhpMethodNamingConventionInspection */

	/**
	 * @param string $literal
	 *
	 * @return string
	 */
	public function escape_literal($literal) {
		$this->autoconnect();
		return "'".mysqli_escape_string($this->connection, $literal)."'";
	    #return "'".str_replace("'", "''", str_replace("\\", "\\\\", str_replace("\\'", "'", $v)))."'";
	} 
	/**
	 * Returns php type for a connection-specific SQL type
	 * 
	 * @param string $typename -- SQL type
	 * @return string - php type or classname
	 */
	function mapType($typename) {
		return ArrayUtils::getValue(self::$typeNames, $typename, 'string');
	}

	/*
	 * Replaces common operator names to MYSQL version
	 */
	function operatorName($op) {
		return $op;
	}

	/**
	 * @param string $name
	 *
	 * @return bool
	 */
	function tableExists($name) {
		$result = true;
		try {
			$this->table_metadata($name);
		}
		catch(Throwable $e) {
			$result = false;
		}
		return $result;
	}

	/** @noinspection PhpMethodNamingConventionInspection */

	/**
	 * Returns table metadata for function {@see DBX::getMetaData()}
	 * (Associative to field names)
	 *
	 * @param string $table
	 *
	 * @return array|boolean -- returns false if table does not exist
	 */
	function table_metadata($table) {
		$this->autoconnect();
		$rs = mysqli_query($this->connection, 'show fields from '.$table);
		if(!$rs) return false;
		$i = 1;
		$result = array();
		while ($row = $rs->fetch_assoc()) {
			$type = $row['Type'];
			$len = -1;
			if(preg_match('/(\w+)\((\d+)\)/', $type, $mm)) {
				$type = $mm[1];
				$len = (int)$mm[2];
			}
			$result[$row['Field']] = array(
				'num' => $i++,
				'type' => $type,
				'len' => $len,
				'not null' => $row['Null']=='NO',
				'has default' => $row['Default']!==null,
			);
		}
		$rs->close();
		return $result;
	}

	function beginTransaction() {
		mysqli_begin_transaction($this->connection);
	}

	function commit() {
		mysqli_commit($this->connection);
	}

	function rollBack() {
		mysqli_rollback($this->connection);
	}

	/**
	 * @param string $value
	 * @param string $type
	 *
	 * @return DateTime|float|int|string|null
	 * @throws Exception
	 */
	public function mapData($value, $type) {
		if($value===null) return null;
		switch($type) {
			case MYSQLI_TYPE_DECIMAL:
			case MYSQLI_TYPE_NEWDECIMAL:
			case MYSQLI_TYPE_FLOAT:
			case MYSQLI_TYPE_DOUBLE:
				return (double)$value;
			case MYSQLI_TYPE_BIT:
			case MYSQLI_TYPE_TINY:
			case MYSQLI_TYPE_SHORT:
			case MYSQLI_TYPE_LONG:
			case MYSQLI_TYPE_LONGLONG:
			case MYSQLI_TYPE_INT24:
			case MYSQLI_TYPE_YEAR:
			case MYSQLI_TYPE_ENUM:
				return (int)$value;

			case MYSQLI_TYPE_TIMESTAMP:
			case MYSQLI_TYPE_DATE:
			case MYSQLI_TYPE_TIME:
			case MYSQLI_TYPE_DATETIME:
			case MYSQLI_TYPE_NEWDATE:
			case MYSQLI_TYPE_INTERVAL:
				return new DateTime($value);

			case MYSQLI_TYPE_SET:
			case MYSQLI_TYPE_VAR_STRING:
			case MYSQLI_TYPE_STRING:
			case MYSQLI_TYPE_CHAR:
			case MYSQLI_TYPE_GEOMETRY:
			case MYSQLI_TYPE_TINY_BLOB:
			case MYSQLI_TYPE_MEDIUM_BLOB:
			case MYSQLI_TYPE_LONG_BLOB:
			case MYSQLI_TYPE_BLOB:
			default:
				return $value;
		}
	}

	/**
	 * @inheritDoc
	 */
	public function getForeignKeys($tablename, $schema=null) {
		if(!$schema) $schema = $this->name;
		$sql = /** @lang */"SELECT table_name, column_name, constraint_name, 
				referenced_table_name as foreign_table, 
				referenced_column_name as foreign_column 
			FROM information_schema.key_column_usage 
			WHERE
				referenced_table_name is not null and 
				table_name = $1 AND table_schema=$2";
		$this->select_all($result, $sql, [$tablename, $schema]);
		if(empty($result)) return [];
		$rr = [];
		foreach($result as $r) $rr[$r['constraint_name']] = $r;
		return $rr;
	}

	/**
	 * @inheritDoc
	 */
	public function getReferrerKeys($tablename, $schema=null) {
		if(!$schema) $schema = $this->name;
		$sql = /** @lang */"SELECT table_name, column_name, constraint_name, 
				referenced_table_name as foreign_table, 
				referenced_column_name as foreign_column 
			FROM information_schema.key_column_usage 
			WHERE referenced_table_name = $1 AND referenced_table_schema=$2";
		$this->select_all($result, $sql, [$tablename, $schema]);
		if(empty($result)) return [];
		$rr = [];
		foreach($result as $r) $rr[$r['constraint_name']] = $r;
		return $rr;
	}

	/**
	 * Must return array of table names (without schema prefix)
	 *
	 * @param string|null $schema
	 *
	 * @return string[]
	 * @throws UXAppException
	 */
	public function getTables($schema=null) {
		if(!$schema) $schema = $this->name;
		return $this->select_column(/** @lang SQL */"SELECT table_name 
			FROM information_schema.tables 
			WHERE table_schema=$1
		", [$schema]);
	}

	/**
	 * Drops the named constraint from the table
	 *
	 * @param string $constraintName
	 * @param string $tableName
	 * @param string|null $schema
	 *
	 * @return false|resource
	 * @throws Exception
	 */
	public function dropForeignKey($constraintName, $tableName, $schema = null) {
		if(!$schema) $schema = $this->name;
		// ALTER TABLE <tableName> DROP FOREIGN KEY <constraintName>
		if(!strpos($tableName, '.')) $tableName = $this->quoteIdentifier($schema).'.'.$this->quoteIdentifier($tableName);
		$constraintName = $this->quoteIdentifier($constraintName);
		return $this->execute(/** @lang text */"ALTER TABLE $tableName DROP FOREIGN KEY $constraintName");
	}

	/**
	 * @param string $tableName
	 * @param string|null $schema
	 *
	 * @return false|resource
	 * @throws Exception
	 */
	public function dropTable($tableName, $schema = null) {
		if(!$schema) $schema = $this->name;
		if(!strpos($tableName, '.')) $tableName = $this->quoteIdentifier($schema).'.'.$this->quoteIdentifier($tableName);
		return $this->execute(/** @lang text */"DROP TABLE IF EXISTS $tableName");
	}

	/**
	 * @param string $tableName
	 * @param string|null $schema
	 *
	 * @return false|resource
	 * @throws Exception
	 */
	public function dropView($tableName, $schema = null) {
		if(!$schema) $schema = $this->name;
		if(!strpos($tableName, '.')) $tableName = $this->quoteIdentifier($schema).'.'.$this->quoteIdentifier($tableName);
		return $this->execute(/** @lang text */"DROP VIEW IF EXISTS $tableName");
	}

	/**
	 * MySQL cannot drop sequences
	 */
	public function dropSequence($sequenceName, $schema = null) {
		return true;
	}

	public function getSequences($schema = null) {
		return [];
	}

	/**
	 * @param string $routineName
	 * @param string $routineType
	 * @param string|null $schema
	 *
	 * @return resource|false -- success
	 * @throws Exception
	 */
	public function dropRoutine($routineName, $routineType = 'FUNCTION', $schema = null) {
		//	'DROP '||routine_type||' IF EXISTS '||routine_name'
		echo "dropping $routineName\n";
		if(!$schema) $schema = $this->name;
		if(strpos($routineName, ' ')) [$routineType, $routineName] = explode(' ', $routineName, 2);
		if(!strpos($routineName, '.')) $routineName = $this->quoteIdentifier($schema).'.'.$this->quoteIdentifier($routineName);
		else $routineName = $this->quoteIdentifier($routineName);
		$sql = 'DROP '.$routineType.' IF EXISTS '.$routineName;
		echo $sql."\n";
		return $this->execute($sql);
	}

	/**
	 * Retuns array with function names preceded with routine type from the give schema.
	 * Function names do not contain parameter list.
	 *
	 * @param string $schema
	 *
	 * @return string[]
	 * @throws UXAppException
	 */
	public function getRoutines($schema = null) {
		// SELECT routine_name, routine_type, routine_schema FROM information_schema.routines WHERE routine_schema = 'vote';
		if(!$schema) $schema = $this->name;
		$params = [$schema];
		$sql = 'SELECT routine_name, routine_type, routine_schema FROM information_schema.routines WHERE routine_schema = $1';
		return array_map(function($routine) {
			return $routine[1] . ' ' . $routine[0];
		}, $this->select_rows($sql, $params));
	}
}
