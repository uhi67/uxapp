<?php /** @noinspection PhpClassConstantAccessedViaChildClassInspection */
/** @noinspection PhpIllegalPsrClassPathInspection */
/** @noinspection PhpUnused */

namespace uhi67\uxapp;

use DateTime;
use DateTimeZone;
use DOMDocument;
use DOMElement;
use DOMNode;
use Exception;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;
use uhi67\uxml\UXMLElement;

/**
 * # BaseModel
 * Abstract data model with validation
 *
 * ## Using
 *
 * - Derive your database model classes from {@see Model} class.
 * - Derive other model classes from here.
 * - Define properties of your model overriding the following methods:
 *
 *     - array {@see rules()} -- defines validation rules. You may create custom validators where you may alter data values as well.
 *     - array {@see attributeLabels()} -- defines displayable labels associated to attributes
 *     - array {@see attributeHints()} -- defines displayable help texts (string or array of texts for different places) associated to attributes
 *     - array {@see attributeTypes()} -- associates typenames to attributes.
 *
 * Only from direct BaseModel descendants:
 *     - array {@see attributes()} -- defines attribute names
 *     - array {@see attributeTypes()} -- defines attribute types
 *
 * You may construct new instances on the following ways:
 *
 * - `$model = new Modelname(attributes)` -- using fieldname=>value pairs
 *
 * You can handle your model instances:
 *
 * - `$model->attributename` -- to get or set attribute values
 *
 * @package UXApp
 * @author Peter Uherkovich
 * @copyright 2017
 *
 * @property  array $attributes -- all attribute values - massive assign/read
 * @property-read  array $errors -- validation error message arrays indexed by field name
 * @property-read  array $errorValues -- errors extended with values
 * @property-read  array $errorNodes -- friendly errors as node array for UXMLDoc::addContent()
 * @property-read  array $friendlyErrors -- friendly error texts for each fields (simple array of texts)
 * @property-read  UXMLElement $node -- first created node in the output DOM document
 */
abstract class BaseModel extends Component {
    const VALID_EMAIL = '/^\w+[\w\-\.]*\@\w+[\w\-\.]+$/';
    #const VALID_URL = '/^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i';
    #const VALID_URL = '/https?:\/\/[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/';
    const VALID_URL = '/^(https?|ftp):\/\/[^\s\/$.?#].[^\s]*$/iS'; // stephenhay
    #const VALID_URL = '/(?i)\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))/iS'; // gruber v2

    /**
     * @var array $_errors -- validation errors associated to field names:
     * `fieldname => array(message)`
     * Messages are sentence fragments: "Fieldname $fragment."
     * E.g.: 'is mandatory'.
     * validate() empties this at the beginning
     */
    protected $_errors = [];

    /** @var UXMLElement|null $_node -- first created node in the output DOM document */
    protected $_node = null;

    /**
     * Must return the list of all attribute names of the model.
     * Default is all public properties
     *
     * @return array list of attribute names.
     */
    public static function attributes() {
        $class = new ReflectionClass(static::class);
        $names = array();
        foreach ($class->getProperties(ReflectionProperty::IS_PUBLIC) as $property) {
            if (!$property->isStatic()) {
                $names[] = $property->getName();
            }
        }
        return $names;
    }

    /**
     * Must return the list of all attribute types of the model (name=>type)
     *
     * ### Valid type names:
     *
     *  - basic php types (lowercase) Default is 'string'
     *  - 'xml' denotes xml valid string
     *  - php class names (Uppercase, eg. DateTime)
     *  - date (denotes DateTime without time)
     *  - time (denotes DateTime without date or integer in sec)
     *  - file (filename in database, uploaded file in forms)
     *
     * @param DBX|null $connection [optional] -- only for compatibility with Model
     *
     * @return array|void -- list of attribute name=>type associations
     * @codeCoverageIgnore
     * @throws UXAppException
     */
    public static function attributeTypes(DBX $connection = null) {
        throw new UXAppException('attributeTypes() is not implemented.', get_called_class());
    }

    /**
     * ## BaseModel::rules()
     *
     * Must return validation rules for fields.
     *
     * Models may be validated with {@see validate()} method
     *
     * ### Member of rules may be:
     *
     * - rule -- numeric indexed rules are global rule, e.g. unique for multiple fields
     * - fieldname => [rule, rule...] -- fieldname indexed rules are for single field
     *
     * Where rule is:
     *
     * - rulename
     * - array(rulename, arguments, ...)
     *
     * Rulename always refers to method `rulenameValidate`.
     *
     * ### Rules in forms
     *
     * - rulename will be added to the field class as `rule-rulename`
     * - Arguments will be added to field as data-rulename-data (always an array, for all remaining arguments)
     * - Associative arguments 'key'=>value will be used in php validator method as normal argument,
     * but in forms will be added as data-rulename-key and excluded from data-rulename.
     *
     * Some validators may change values during validation. Some of them even passes always.
     *
     * ### Predefined rules:
     *
     * - 'mandatory' -- field is not null and not empty string
     * - ['default', value] -- always passes, replaces null value to default.
     * - 'defaultNow' -- for date/time fields, always passes, replaces null value to current timestamp.
     * - typenames (boolean, int, float, number, string, xml, time, date, datetime, macaddr, inet)
     * - ['length', min, max]
     * - ['pattern', pattern(s)] -- valid if at least one of RE patterns is valid (second level: all of them)
     * - ['type', typename]
     * - ['between', lower, upper] -- valid value between (inluding) limits
     * - [custom, arglist] -- any custom name refers to validateCustom($fieldname, array $arglist) method.
     * - ['if', fieldname2, refvalue, rule] -- conditional validation
     *
     * ### Signature of `rulenameValidate` functions:
     *
     * - must accept parameters (fieldname, arg1, arg2, ...) where fieldname is null on global calls.
     * - parameters in rule definition after rulename will be passed as arg1, arg2.
     * - must return boolean
     * - may set error text of the invalid field on the form by setError(fieldname, message)
     *
     * ### Global rules
     * If a rule is called as global rule, the first (fieldname) argument will be null, any other arguments
     * defined in rule after rulename will be passed from the second position.
     * In this way you may use the same validator method as gloabal as well as field validator.
     *
     * For predefided database-related rules see {@see Model::rules()}
     *
     * @return array
     * @see validate()
     * @codeCoverageIgnore
     */
    public static function rules() {
        return [];
    }

    /**
     * ## Validates the model data.
     * Invalid data returns false and errors will contain the errors in form field=>errors array.
     *
     * Default validates against user defined rules().
     * If you override it, and want to use rules as well, don't forget to call parent::validate()
     *
     * For rules of rule definitions, see {@see rules()}
     *
     * @param array|null $attributeNames -- if an array is specified, validates only given fields (ignores unknown fieldnames)
     *
     * @return boolean -- true if data is valid and may saved to the database.
     * @throws UXAppException
     */
    public function validate(?array $attributeNames=null) {
        $this->_errors = array();
        $valid = true;
        $rules = static::rules();
        foreach($rules as $field=>$def) {
            if($def===null) continue; // Overridden rule may be null
            if(is_numeric($field)) {
                if($attributeNames) continue;
                // Global rule
                #UXApp::trace(['validating global rule ', $def);
                $rulename = is_array($def) ? array_shift($def) : $def;
                Assertions::assertString($rulename);
                $args = is_array($def) ? $def : array();
                $functionname = Util::camelize($rulename) . 'Validate';
                if (!is_callable(array($this, $functionname))) throw new UXAppException("Validator function `$functionname` is missing");
                if (!call_user_func_array(array($this, $functionname), array_merge(array(null), $args))) {
                    UXApp::trace(['Global validate failed' => $functionname],['color'=> '#900', 'tags'=>'uxapp']);
                    $valid = false;
                }
            }
            else if(!$attributeNames || is_array($attributeNames) && in_array($field, $attributeNames)) {
                $valid = $this->validateRules($field, $def) && $valid; // Order is important!
            }
        }
        return $valid;
    }

    /**
     * @param string $fieldName
     * @param array $def -- array of multiple rules to validate against
     *
     * @return bool
     * @throws UXAppException
     */
    public function validateRules(string $fieldName, array $def) {
        $valid = true;
        Assertions::assertArray($def);
        foreach($def as $rule) {
            $rulename = is_array($rule) ? $rule[0] : $rule;
            try {
                $valid = ($valid1 = $this->validateRule($fieldName, $rule)) && $valid;
            }
                // @codeCoverageIgnoreStart
            catch (Exception $e) {
                throw new UXAppException('Invalid rule `'.$rulename."` on field `$fieldName`: ".$e->getMessage(), $rule, $e);
            }
            // @codeCoverageIgnoreEnd
            if(!$valid1) {
                UXApp::trace(['Validate failed' => $rule], ['tags'=>'uxapp', 'color'=>'#900']);
                #UXApp::trace(['Value', $this->$field, '#850');
            }
            $rulename = is_array($rule) ? array_shift($rule) : $rule;
            if(!$valid1 && $rulename=='mandatory') break; // If mandatory failed, no more check.
        }
        return $valid;
    }

    /**
     * Validates a field against a rule definition
     *
     * @param string $fieldname
     * @param string|array $rule -- rulename or array(rulename, params...)
     *
     * @return bool
     * @throws UXAppException
     */
    public function validateRule(string $fieldname, $rule) {
        #UXApp::trace(['validating field '.$field, $rule);
        $rulename = is_array($rule) ? array_shift($rule) : $rule;
        $functionname = Util::camelize($rulename).'Validate';
        if(!ctype_alnum($functionname)) throw new UXAppException("Invalid validator rule: `$rulename`.");
        $args = is_array($rule) ? $rule : array();
        if(!is_callable(array($this, $functionname))) throw new UXAppException("Validator function `$functionname` is missing");
        if(!call_user_func_array(array($this, $functionname), array_merge(array($fieldname), $args))) {
            UXApp::trace([$fieldname.' validate failed' => $functionname], ['tags'=>'uxapp', 'color'=>'#900']);
            return false;
        }
        return true;
    }

    /**
     * Inserts fieldname and it's error message into $errors array.
     *
     * @param string|array $fieldname -- the name of field or multiple fieldnames
     * @param string $message ($1 is a placeholder for fieldname)
     *
     * @return false -- always
     * @throws UXAppException
     */
    public function setError($fieldname, string $message) {
        if(is_array($fieldname)) {
            $field = array_shift($fieldname);
            if(count($fieldname)) {
                $db = $this;
                $fieldname = array_map(function($f) use($db) {return $db->attributeLabel($f); }, $fieldname);
                $message = UXApp::la('uxapp', 'with {$fields} {$message}', array('fields'=>implode(', ', $fieldname), 'message'=>$message));
            }
            $fieldname = $field;
        }
        $message = str_replace('$1', $fieldname, $message);
        if(!isset($this->_errors[$fieldname])) $this->_errors[$fieldname] = array();
        $this->_errors[$fieldname][] = $message;
        return false;
    }

    /**
     * Returns last error of fieldname. Null if none.
     *
     * @param string $fieldname
     * @param bool $remove
     *
     * @return string|null;
     */
    public function lastError(string $fieldname, bool $remove=false) {
        if(!isset($this->_errors[$fieldname])) return null;
        $last = count($this->_errors[$fieldname]);
        $err = $this->_errors[$fieldname][$last - 1];
        if($remove) array_pop($this->_errors[$fieldname]);
        return $err;
    }

    /**
     * Converts empty string value of the field to NULL value
     * Does NOT trim whitespaces.
     *
     * Writes back the value to the field.
     *
     * @param string $fieldname
     *
     * @return mixed -- the value
     */
    public function emptyIsNull(string $fieldname) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if($value==='')
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = ($value=null);
        return $value;
    }

    /**
     * Validates field if condition returns true, skips to true otherwise
     *
     * @param string $fieldName
     * @param bool|callable $condition -- literal or function($model)
     * @param string|array $rules -- single rule name or array of multiple rules
     *
     * @return bool
     * @throws UXAppException
     */
    public function condValidate(string $fieldName, $condition, $rules) {
        if(!is_scalar($condition)) {
            if(is_callable($condition)) $condition = $condition($this);
            else return $this->setError($fieldName, 'Invalid condition');
        }
        if(!$condition) return true;
        if(!is_array($rules)) $rules = array($rules);
        return $this->validateRules($fieldName, $rules);
    }

    /**
     * Validates field data against 'uxapp' types
     *
     * Null values always pass
     *
     * ### Supported types are:
     * - string
     * - integer
     * - double
     * - boolean
     * - array
     * - resource
     * - class names without namespace ('uhi67\uxapp' is supposed)
     * - types having BaseModel::typeValidate() method
     * TODO: static uxapp type validator outside of Model
     *
     * @param string $fieldname -- name of field
     * @param string $type -- common type (mapped from database by $conn->mapType())
     *
     * @return bool -- true is value is compatible to field type
     * @throws UXAppException
     */
    public function typeValidate(string $fieldname, string $type) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if(is_null($value)) return true;
        if(gettype($value) == $type) return true;
        if(is_object($value) && is_a($value, $type)) return true;
        if(is_object($value) && is_a($value, 'uhi67\uxapp\\'.$type)) return true;
        $functionname = Util::camelize($type).'Validate';
        if(is_callable(array($this, $functionname))) return $this->$functionname($fieldname);
        return $this->setError($fieldname, 'is not a '.$type. ' object.');
        #throw new UAppException(sprintf('Validator %s does not exist.', $functionname));
    }

    /**
     * Validates an xml text or DOMDocument or DOMNode
     *
     * @param string $fieldname
     * @param bool $toString -- Converts DOM objects to xml string
     *
     * @return bool
     * @throws UXAppException
     */
    public function xmlValidate(string $fieldname, bool $toString=true) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if($value===null) return true;
        if(gettype($value)=='string') {
            $doc = new DOMDocument();
            $r = @$doc->loadXML($value);
            if($r) return true;
            return $this->setError($fieldname, UXApp::la('uxapp', 'is invalid XML string'));
        }
        if(is_object($value)) {
            $r = false;
            if($value instanceof DOMDocument) { if($toString) $r = $value->saveXML(); }
            else if($value instanceof DOMNode) {
                if(is_null($value->ownerDocument)) throw new UXAppException($fieldname.'is invalid XML data ');
                if($toString) $r = $value->ownerDocument->saveXML($value);
            }
            else {
                throw new UXAppException($fieldname.' is invalid XML object');
            }
            if($toString && !$r) {
                return $this->setError($fieldname, UXApp::la('uxapp', 'is invalid DOM Node'));
            }
            if($toString) {
                /** @noinspection PhpVariableVariableInspection */
                $this->$fieldname = $r;
            }
            return true;
        }
        return $this->setError($fieldname, UXApp::la('uxapp', 'is not an XML'));
    }

    /**
     * Validates a string
     *
     * Fails if not scalar, otherwise converts value to string.
     *
     * @param string $fieldname
     *
     * @return bool
     * @throws UXAppException
     */
    public function stringValidate(string $fieldname) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if($value===null) return true;
        if(is_object($value) && is_callable(array($value, 'toString'))) {
            $value = $value->toString();
        }
        if(!is_scalar($value)) return $this->setError($fieldname, UXApp::la('uxapp', 'is not a string'));
        if(!is_string($value))
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = (string)$value;
        return true;
    }

    /**
     * Validates a scalar or array length
     *
     * @param string $fieldname
     * @param int|null $minlength -- 0 or null to skip check
     * @param int|null $maxlength -- 0 or null to skip check
     *
     * @return bool
     * @throws UXAppException
     */
    public function lengthValidate(string $fieldname, ?int $minlength=-1, ?int $maxlength=null) {
        if($minlength===-1) throw new UXAppException('Missing min length argument in length validator rule');
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if($value===null) return true;

        if(is_array($value)) {
            if($maxlength && count($value)>$maxlength) return $this->setError($fieldname, UXApp::la('uxapp', 'is too long'));
            if($minlength && count($value)<$minlength) return $this->setError($fieldname, UXApp::la('uxapp', 'is too short'));
            return true;
        }

        if(!is_scalar($value)) return $this->setError($fieldname, UXApp::la('uxapp', '$1 is not a scalar'));
        if($maxlength && strlen($value)>$maxlength) return $this->setError($fieldname, UXApp::la('uxapp', 'is too long'));
        if($minlength && strlen($value)<$minlength) return $this->setError($fieldname, UXApp::la('uxapp', 'is too short'));
        return true;
    }

    /**
     * @param string $fieldname
     *
     * @return bool|false
     * @throws UXAppException
     */
    public function booleanValidate(string $fieldname) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if($value===null) return true;
        if(is_bool($value)) return true;
        if(is_string($value)) $value = strtolower($value);
        if(in_array($value, array(true, 1, -1, '1', '-1', 'true', 't', 'y'), true)) {
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = true;
            return true;
        }
        if(in_array($value, array(false, 0, '0', 'false', 'f', 'n'), true)) {
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = false;
            return true;
        }
        return $this->setError($fieldname, UXApp::la('uxapp', 'is invalid boolean'));
    }

    /**
     * ## Validates an integer
     *
     * - Ignores null
     * - Accepts also boolean and converts to 0/1
     * - Accepts float if not exceeds PHP_INT_MAX and converts (truncates) to integer
     * - Converts to integer all non-integer representation
     *
     * @param string $fieldname
     *
     * @return bool
     * @throws UXAppException
     */
    public function integerValidate(string $fieldname) {
        $value = $this->emptyIsNull($fieldname);
        if($value===null) return true;
        if(!is_scalar($value)) return $this->setError($fieldname, UXApp::la('uxapp', 'is not a scalar'));
        if(is_int($value)) return true;
        if(is_numeric($value) && is_float($value + 0) && ($value + 0) > PHP_INT_MAX) return $this->setError($fieldname, UXApp::la('uxapp', 'is too big for integer'));
        if(is_numeric($value) && (is_float($value + 0) || (int)$value==$value)) {
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = (int)(float)$value;
            return true;
        }
        if(is_bool($value)) {
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = ($value ? 1: 0);
            return true;
        }
        return $this->setError($fieldname, UXApp::la('uxapp', 'is invalid integer'));
    }

    /**
     * ## Validates any integer-like string
     * - Ignores empty string and null
     * - Accepts float if not exceeds PHP_INT_MAX and converts (truncates) to string
     * - Accepts also boolean and converts to 0/1
     * - Converts to string
     *
     * @param string $fieldname
     *
     * @return bool
     * @throws UXAppException
     */
    public function intValidate(string $fieldname) {
        $value = $this->emptyIsNull($fieldname);
        if($value===null) return true;
        if(is_int($value)) {
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = (string)$value;
            return true;
        }
        if(is_numeric($value) && is_float($value + 0)) {
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = number_format(floor($value), 0, '', '');
            return true;
        }
        if(is_bool($value)) {
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = ($value ? '1': '0');
            return true;
        }
        if(!is_string($value) || !preg_match('~^((:?+|-)?[0-9]+)$~', $value)) {
            return $this->setError($fieldname, UXApp::la('uxapp', 'is invalid integer'));
        }
        return true;
    }

    /**
     * Validates any number (no conversion)
     *
     * @param string $fieldname
     *
     * @return bool
     * @throws UXAppException
     */
    public function numberValidate(string $fieldname) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if($value===null) return true;
        if(is_numeric($value)) return true;
        return $this->setError($fieldname, UXApp::la('uxapp', '$1 is invalid number'));
    }

    /**
     * Validates any number and converts to flot
     *
     * @param string $field
     *
     * @return bool
     * @throws UXAppException
     */
    public function floatValidate(string $field) {
        /** @noinspection PhpVariableVariableInspection */
        if($this->$field===null) return true;
        if($this->numberValidate($field)) {
            /** @noinspection PhpVariableVariableInspection */
            $this->$field = (float)$this->$field;
            return true;
        }
        return false;
    }

    /**
     * Converts a string to lowercase
     *
     * Always passes
     *
     * @param string $fieldname
     *
     * @return bool
     * @throws UXAppException
     */
    public function lowercaseValidate(string $fieldname) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if($value===null) return true;
        if(!$this->stringValidate($fieldname)) return false;
        /** @noinspection PhpVariableVariableInspection */
        $this->$fieldname = strtolower($value);
        return true;
    }

    /**
     * Checks and convert value to a valid Macaddr
     *
     * @param string $fieldname
     *
     * @return boolean
     * @throws UXAppException
     */
    function macaddrValidate(string $fieldname) {
        $value = $this->emptyIsNull($fieldname);
        if($value===null) return true;
        if($value instanceof Macaddr) return true;
        Assertions::assertString($value);
        $value = trim($value);
        if(!Macaddr::isValid($value)) return $this->setError($fieldname, UXApp::la('uxapp', 'is invalid MAC address'));
        $value = new Macaddr($value);
        /** @noinspection PhpVariableVariableInspection */
        $this->$fieldname = $value;
        return true;
    }

    /**
     * Trims whitespaces
     * Fails if not a string
     *
     * @param string $fieldname
     *
     * @return boolean
     * @throws UXAppException
     */
    function trimValidate(string $fieldname) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if($value===null) return true;
        if(!$this->stringValidate($fieldname)) return false;
        /** @noinspection PhpVariableVariableInspection */
        $this->$fieldname = trim($value);
        return true;
    }

    /**
     * Checks and convert value to a valid Inet
     *
     * @param string $fieldname
     *
     * @return boolean
     * @throws UXAppException
     */
    function inetValidate(string $fieldname) {
        $value = $this->emptyIsNull($fieldname);
        if($value===null) return true;
        if(is_object($value)) {
            if($value instanceof Inet && $value->valid) return true;
            return $this->setError($fieldname, UXApp::la('uxapp', 'is invalid Inet object'));
        }
        Assertions::assertString($value);
        $value = new Inet($value);
        if(!$value->valid) return $this->setError($fieldname, UXApp::la('uxapp', 'is invalid IP address'));
        /** @noinspection PhpVariableVariableInspection */
        $this->$fieldname = $value;
        return true;
    }

    /**
     * @param string $fieldname
     *
     * @return bool
     * @throws UXAppException
     */
    public function datetimeValidate(string $fieldname) {
        $value = $this->emptyIsNull($fieldname);
        if($value===null) return true;
        if($value instanceof DateTime) return true;
        Assertions::assertString($value);
        $formats = array(DateTime::ATOM, 'Y-m-d H:i:s', 'Y-m-d H:i', 'Y.m.d. H:i:s', 'd-M-y H:i:s', 'Y.m.d. H:i', 'd-M-y H:i',
            'Y.m.d.', 'd-M-y', 'Y. m. d. H:i', 'Y. m. d.');
        foreach($formats as $i=>$format) {
            $v = DateTime::createFromFormat($format, $value);
            if($v!==false) {
                if($i > 6) $v->setTime(0, 0);
                break;
            }
        }
        if($v!==false) {
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = $v;
            return true;
        }
        return $this->setError($fieldname, UXApp::la('uxapp', 'is invalid DateTime'));
    }

    /**
     * If value is empty, defaults to current timestamp.
     * Fails if not a datetime
     *
     * @param string $fieldname
     *
     * @return bool
     * @throws UXAppException
     */
    public function defaultNowValidate(string $fieldname) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if($value instanceof DateTime) return true;
        if($value===null || $value==='') {
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = new DateTime();
            return true;
        }
        return $this->datetimeValidate($fieldname);
    }

    /**
     * @param string $fieldname
     *
     * @return bool
     * @throws UXAppException
     */
    public function dateValidate(string $fieldname) {
        $value = $this->emptyIsNull($fieldname);
        if($value===null) return true;
        if($value instanceof DateTime) return true;
        $formats = array('Y.m.d.', 'Y.m.d', 'Y. m. d.', 'd-M-y', 'Y-m-d');
        foreach($formats as $format) {
            $value1 = DateTime::createFromFormat($format, $value);
            if($value1) break;
        }
        if($value1!==false) {
            $value1->setTime(0,0);
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = $value1;
            return true;
        }
        return $this->setError($fieldname, UXApp::la('uxapp', 'is invalid date').': `'.$value.'`');
    }

    /**
     * ## Validates a datetime (without date)
     *
     * - accepts positive integer as number of seconds
     * - does not removes date part
     * - converts string to DateTime using accepting only time formats
     *
     * @param string $field
     *
     * @return bool
     * @throws UXAppException
     */
    public function timeValidate(string $field) {
        $value = $this->emptyIsNull($field);
        if($value===null) return true;

        if($value instanceof DateTime) {
            return true;
        }

        if(is_int($value) && $value >= 0) {
            $new_value = new DateTime();
            /** @noinspection PhpVariableVariableInspection */
            $this->$field = $new_value->setTimestamp($value);
            return true;
        }

        if(is_string($value)) {
            $formats = array('H:i:s', 'H:i');
            foreach($formats as $format) {
                $value1 = DateTime::createFromFormat($format, $value);
                if($value1) break;
            }
            if($value1!==false) {
                /** @noinspection PhpVariableVariableInspection */
                $this->$field = $value;
                return true;
            }
        }

        return $this->setError($field, UXApp::la('uxapp', 'is invalid time'));
    }

    /**
     * ## Validates a datetime without time zone (removes time zone)
     *
     * @param string $field
     *
     * @return bool
     * @throws UXAppException
     */
    public function timestampValidate(string $field) {
        if(!$this->datetimeValidate($field)) return false;
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$field;
        if($value!==null) {
            $timezone = (UXApp::$app && UXApp::$app->timeZone) ? UXApp::$app->timeZone : 'Europe/Budapest';
            $value->setTimezone(new DateTimeZone($timezone));
        }
        return true;
    }

    /**
     * ## Validates a field using preg pattern (use // delimiters)
     *
     * Field type must be string or covertible to string
     * Null values always pass!
     * If multiple patterns given, at least one of them must match.
     * (if multiple patterns given at level 2, all of them must match)
     *
     * @param string $field
     * @param $patterns
     * @param string|null $custommessage
     *
     * @return bool
     * @throws UXAppException
     */
    public function patternValidate(string $field, $patterns, string $custommessage=null) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$field;
        if(is_null($value)) return true;
        if(!is_array($patterns)) $patterns = array($patterns);

        try {
            if(!static::checkPatterns($value, $patterns)) {
                return $this->setError($field, $custommessage ?: UXApp::la('uxapp', 'has invalid format'));
            }
        }
        catch(UXAppException $e) {
            throw new UXAppException(UXApp::la('uxapp', 'Field `{$field}`: {$message}', array('field'=>$field)), $e->getExtra(), $e);
        }
        return true;
    }

    /**
     * ## Validates multiple values using preg pattern array (use // delimiters)
     *
     * If value is an array, all elements validated consequently, and returns on first error.
     * If multiple patterns given, at least one of them must match (if no pattern at all, always fails)
     * If multiple patterns given at level 2, all of them must match (zero patterns always match)
     *
     * @param string|array $values
     * @param array $patterns
     *
     * @return bool
     * @throws UXAppException
     */
    public static function checkPatterns($values, array $patterns) {
        if(!is_array($values)) $values = array($values);
        foreach($values as $value) {
            Assertions::assertString($value);
            $f = false;
            foreach($patterns as $pattern) {
                if(!is_string($pattern) && !is_array($pattern)) throw new UXAppException(UXApp::la('uxapp', 'pattern is not a string or array'));
                if(is_string($pattern) && preg_match($pattern, $value)==1) return true;
                if(is_array($pattern)) {
                    $m = true;
                    foreach($pattern as $p) {
                        if(!is_string($p)) throw new UXAppException(UXApp::la('uxapp', 'pattern is not a string ({$p})', array('p'=>$p)), print_r($patterns, true));
                        if(!preg_match($p, $value)==1) { $m = false; break; }
                    }
                    if($m) { $f = true; break; }
                }
            }
            if(!$f) return false;
        }
        return true;
    }

    /**
     * Checks if field value is not null
     *
     * @param string|null $fieldname
     *
     * @return bool
     * @throws UXAppException
     */
    public function notNullValidate(?string $fieldname) {
        /** @noinspection PhpVariableVariableInspection */
        $valid = !is_null($this->$fieldname);
        if(!$valid) return $this->setError($fieldname, UXApp::la('uxapp', 'must not be null'));
        return true;
    }

    /**
     * Checks if field value is not empty
     *
     * @param string $fieldname
     *
     * @return boolean
     * @throws UXAppException
     */
    public function mandatoryValidate(string $fieldname) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if($value !== false && empty($value)) {
            return $this->setError($fieldname, UXApp::la('uxapp', 'is mandatory'));
        }
        return true;
    }

    /**
     * Checks if field value is empty
     *
     * @param string $fieldname
     *
     * @return boolean
     * @throws UXAppException
     */
    public function emptyValidate(string $fieldname) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if($value === false || !empty($value)) {
            return $this->setError($fieldname, UXApp::la('uxapp', 'must be empty'));
        }
        return true;
    }

    /**
     * validate a rule, if another field has a value, pass otherwise.
     *
     * @param string $fieldname
     * @param string $fieldname2
     * @param mixed $refvalue
     * @param array $rule ...
     *
     * @return boolean
     * @throws UXAppException
     */
    public function ifValidate(string $fieldname, string $fieldname2, $refvalue, array $rule) {
        /** @noinspection PhpVariableVariableInspection */
        $value2 = $this->$fieldname2;
        UXApp::trace(['If' => $value2], ['tags'=>'uxapp']);
        if(!$rule) throw new UXAppException('Invalid arguments for if rule');
        if($value2!=$refvalue) return true;
        $f = $this->validateRule($fieldname, $rule);
        if(func_num_args() > 4) {
            for($i=4;$i<func_num_args();$i++) {
                $rulex = func_get_arg($i);
                if($f) $f = $this->validateRule($fieldname, $rulex);
            }
        }
        if(!$f) {
            $type2 = $this->attributeType($fieldname2);
            UXApp::trace(['type2' => $type2], ['tags'=>'uxapp']);
            $err = $type2=='boolean' ?
                ($refvalue ? '{$err} if `{$fieldname2}`' : '{$err} if not `{$fieldname2}`') :
                ($refvalue===null ? '{$err} if `{$fieldname2}` is not specified' : '{$err} if `{$fieldname2}` is {$refvalue}');
            return $this->setError($fieldname,
                UXApp::la('uxapp', $err, array(
                    'fieldname2'=>$this->attributeLabel($fieldname2),
                    'refvalue'=>$refvalue,
                    'err'=>$this->lastError($fieldname, true)
                ))
            );
        }
        return true;
    }

    /**
     * Validate a rule, if another field has a value, pass otherwise.
     * Repeats original messages
     *
     * @param string $fieldname
     * @param string $fieldname2
     * @param mixed $refvalue
     * @param array $rule ...
     *
     * @return boolean
     * @throws UXAppException
     */
    public function if2Validate(string $fieldname, string $fieldname2, $refvalue, array $rule) {
        /** @noinspection PhpVariableVariableInspection */
        $value2 = $this->$fieldname2;
        UXApp::trace(['If' => $value2], ['tags'=>'uxapp']);
        if(!$rule) throw new UXAppException('Invalid arguments for if rule');
        if($value2!=$refvalue) return true;
        $f = $this->validateRule($fieldname, $rule);
        if(func_num_args() > 4) {
            for($i=4;$i<func_num_args();$i++) {
                $rulex = func_get_arg($i);
                if($f) $f = $this->validateRule($fieldname, $rulex);
            }
        }
        return $f;
    }

    /**
     * Validate a rule, unless another field has a value, pass otherwise.
     *
     * @param string $fieldname
     * @param string $fieldname2 -- reference filename
     * @param mixed $refvalue -- reference value - simple comparison applied
     * @param array $rule -- rule to apply if reference file has not contain the reference value
     *
     * @return bool|false
     * @throws Exception
     */
    public function ifNotValidate(string $fieldname, string $fieldname2, $refvalue, array $rule) {
        /** @noinspection PhpVariableVariableInspection */
        $value2 = $this->$fieldname2;
        UXApp::trace(['IfNot', $value2]);
        if(!$rule) throw new Exception('Invalid arguments for if rule');
        if($value2==$refvalue) return true;
        $f = $this->validateRule($fieldname, $rule);
        if(func_num_args() > 4) {
            for($i=4;$i<func_num_args();$i++) {
                $rulex = func_get_arg($i);
                if($f) $f = $this->validateRule($fieldname, $rulex);
            }
        }
        if(!$f) {
            $type2 = $this->attributeType($fieldname2);
            UXApp::trace(['type2', $type2]);
            $err = $type2=='boolean' ?
                ($refvalue ? '{$err} if `{$fieldname2}`' : '{$err} if not `{$fieldname2}`') :
                ($refvalue===null ? '{$err} if `{$fieldname2}` is not specified' : '{$err} if `{$fieldname2}` is {$refvalue}');
            return $this->setError($fieldname,
                UXApp::la('uxapp', $err, [
                    'fieldname2'=>$this->attributeLabel($fieldname2),
                    'refvalue'=>$refvalue,
                    'err'=>$this->lastError($fieldname, true)
                ])
            );
        }
        return true;
    }

    /**
     * Checks if field value is between given limits (including)
     * null limits are ignored
     *
     * @param string $fieldname
     * @param mixed $min
     * @param mixed $max
     *
     * @return boolean
     * @throws UXAppException
     */
    public function betweenValidate(string $fieldname, $min, $max=null) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if(is_null($value)) return true;
        $valid = ($min===null || $value >= $min) && ($max===null || $value <= $max);
        if(!$valid) return $this->setError($fieldname, Util::substitute(UXApp::la('uxapp', 'must be between {$min} and {$max}'), array('min'=>$min, 'max'=>$max)));
        return true;
    }

    /**
     * Special validator which always succeeds.
     * Modifies empty values to given default.
     *
     * @param string $fieldname
     * @param mixed $default -- may be a callable($model)
     *
     * @return true
     */
    public function defaultValidate(string $fieldname, $default) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if(empty($value)) {
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = is_callable($default) ? call_user_func($default, $this) : $default;
        }
        return true;
    }

    /**
     * Converts string value to array
     *
     * @param string $fieldname
     * @param string $pattern -- delimiter to explode with
     *
     * @return boolean
     * @throws UXAppException
     */
    public function arrayValidate(string $fieldname, string $pattern) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if(is_null($value)) return true;
        if(is_array($value)) return true;
        if(!is_string($value)) return $this->setError($fieldname, UXApp::la('uxapp', 'Invalid array value'));
        /** @noinspection PhpVariableVariableInspection */
        $this->$fieldname = explode($pattern, $value);
        return true;
    }

    /**
     * Validates field as e-mail address
     *
     * @param string $fieldname
     *
     * @return bool
     * @throws UXAppException
     */
    public function emailValidate(string $fieldname) {
        return $this->patternValidate($fieldname, array(self::VALID_EMAIL), UXApp::la('uxapp', 'is invalid e-mail address'));
    }

    /**
     * Validates field as url address
     *
     * @param string $fieldname
     *
     * @return bool
     * @throws UXAppException
     */
    public function urlValidate(string $fieldname) {
        return $this->patternValidate($fieldname, array(self::VALID_URL), UXApp::la('uxapp', 'is invalid url address'));
    }

    /**
     * Returns the value of a property
     * Attributes and related objects can be accessed like properties.
     *
     * Do not call this method directly as it is a PHP magic method that
     * will be implicitly called when executing `$value = $component->property;`.
     *
     * 1. if value in _attributes array is set, return that; or
     * 2. if getter function exists, use that; or
     * 3. if attribute is registered, but not loaded, return null.
     * 4. use parent's
     *
     * @param string $name the property name
     *
     * @return mixed the property value
     * @throws ReflectionException
     * @throws UXAppException
     */
    public function __get($name) {
        // Getter function (function names are case-insensitive in PHP!)
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        }

        // Existing but not loaded attribute
        if ($this->hasAttribute($name)) {
            return null;
        }
        
        // Other
        return parent::__get($name);
    }

    /**
     * Sets the value of a Model attribute via PHP setter magic method.
     *
     * 1. If attribute setter exists, use that; or
     * 2. Set value in _attrinbutes array.
     *
     * @param string $name property name
     * @param mixed $value property value
     *
     * @throws UXAppException
     */
    public function __set($name, $value) {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            $this->$setter($value);
            return;
        }
        if (method_exists($this, 'get' . $name)) {
            throw new UXAppException('Setting read-only property: ' . get_class($this) . '::' . $name);
        }
        throw new UXAppException('Setting non existing property: ' . get_class($this) . '::' . $name);
    }

    /**
     * ## Checks if a property is set, i.e. defined and not null.
     * This method will check in the following order and act accordingly:
     *
     *  - a property defined by a setter: return whether the property is set
     *  - return `false` for non existing properties
     *
     * Do not call this method directly as it is a PHP magic method that
     * will be implicitly called when executing `isset($component->property)`.
     *
     * @param string $name the property name or the event name
     * @return bool whether the named property is set
     * @see http://php.net/manual/en/function.isset.php
     */
    public function __isset($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter() !== null;
        }

        return false;
    }

    /**
     * ## Sets a component property to be null
     *
     * Do not call this method directly as it is a PHP magic method that
     * will be implicitly called when executing `unset($component->property)`.
     *
     * @param string $name the property name
     *
     * @throws UXAppException if the property is read only.
     */
    public function __unset($name) {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            $this->$setter(null);
            return;
        }
        throw new UXAppException('Unsetting an unknown or read-only property: ' . $name, get_class($this));
    }

    /**
     * Returns the type of the named attribute.
     *
     * @param string $name - the attribute name
     * @param DBX|null $connection -- only for Model compatibility
     * @return string - the common typename of the attribute (@see self::attributeTypes())
     * @throws UXAppException if attribute not exists
     */
    public static function attributeType(string $name, ?DBX $connection=null) {
        if(static::hasAttribute($name)) {
            $types = static::attributeTypes();
            return ArrayUtils::getValue($types, $name, 'string');
        }
        throw new UXAppException("Unknown attribute $name");
    }

    /**
     * Returns the type of the named attribute.
     * The live version allows instance-based inheritance (Used in Model for different database connections)
     *
     * @param string $name - the attribute name
     *
     * @return string - the common typename of the attribute (@see self::attributeTypes())
     * @throws UXAppException if attribute not exists
     */
    public function attributeTypeLive(string $name) {
        return static::attributeType($name);
    }

    /**
     * ## Must return the attribute labels.
     * Attribute labels are mainly used for display purpose
     * Order of labels is the default order of fields.
     *
     * For getting label for a specific attribute, see {@see attributeLabel()}
     *
     * @return array attribute labels (name => label)
     */
    public static function attributeLabels() { return array(); }

   /**
     * If overridden, must return the attribute hints.
     * Attribute hints are mainly used for display purpose on forms
     * It may be an array, with following keys:
     * - hint: for mouse over it
     * - comment: short, always visible after field
     * - help: long, visible under [?] button
     * - descr: in next line (may be more)
     * - tooltip: tutorial, in yellow
     * @return array attribute hints (name => hints)
     */
    public static function attributeHints()  { return array(); }

    /**
     * Returns the text label for the specified attribute, based on attributeLabels() array.
     * Default returns the fieldname itself, if label is not defined.
     *
     * @param string $attribute the attribute name
     * @return string the attribute label
     * @see attributeLabels()
     */
    public static function attributeLabel(string $attribute) {
        $labels = static::attributeLabels();
        return $labels[$attribute] ?? $attribute;
    }

    /**
     * Returns the text hint (or array of hint-like texts) for the specified attribute.
     * Keys of array may be:
     * - hint: for mouse over it
     * - comment: short, always visible after field
     * - help: long, visible under [?] button
     * - descr: in next line (may be more)
     * - tooltip: tutorial, in yellow
     *
     * @param string $attribute the attribute name
     * @param string|null $type -- null or a specific key of multiple hint-type texts
     * @return string|array the attribute hint
     * @see attributeHints()
     */
    public static function attributeHint(string $attribute, ?string $type=null) {
        $hints = static::attributeHints();
        $hint = $hints[$attribute] ?? '';
        if(is_string($type) && is_array($hint)) $hint = ArrayUtils::getValue($hint, $type, '');
        return $hint;
    }

    /**
     * Returns attribute values.
     *
     * @param array|null $names list of attributes whose value needs to be returned.
     * Defaults to null, meaning all attributes listed in attributes() will be returned.
     * If it is an array, only the attributes in the array will be returned.
     *
     * @return array attribute values (name => value).
     */
    public function namedAttributes(?array $names = null) {
        $values = array();
        if ($names === null) {
            $names = static::attributes();
        }
        foreach ($names as $name) {
            if(!is_string($name)) continue; // because subnodeFields may contain callables
            /** @noinspection PhpVariableVariableInspection */
            $values[$name] = $this->$name;
        }
        return $values;
    }

    /**
     * Returns all attribute values.
     *
     * For getting selective attributes, see {@see namedAttributes()} method.
     *
     * @return array attribute values (name => value).
     */
    public function getAttributes() {
        return $this->namedAttributes(static::attributes());
    }

    /**
     * Sets the attribute values in a massive way.
     *
     * @param array $values attribute values (name => value) to be assigned to the model.
     *
     * @see attributes()
     */
    public function setAttributes(array $values) {
        $attributes = array_flip(static::attributes());
        foreach ($values as $name => $value) {
            if (isset($attributes[$name])) {
                /** @noinspection PhpVariableVariableInspection */
                $this->$name = $value;
            }
        }
    }

    /**
     * Returns the list of fields that should be returned by default by toArray() when no specific fields are specified.
     *
     * A field is a named element in the returned array by toArray().
     *
     * This method should return an array of field names or field definitions.
     * If the former, the field name will be treated as an object property name whose value will be used
     * as the field value. If the latter, the array key should be the field name while the array value should be
     * the corresponding field definition which can be either an object property name or a PHP callable
     * returning the corresponding field value. The signature of the callable should be:
     *
     * ```php
     * function ($model, $field) {
     *     // return field value
     * }
     * ```
     *
     * For example, the following code declares four fields:
     *
     * - `email`: the field name is the same as the property name `email`;
     * - `firstName` and `lastName`: the field names are `firstName` and `lastName`, and their
     *   values are obtained from the `first_name` and `last_name` properties;
     * - `fullName`: the field name is `fullName`. Its value is obtained by concatenating `first_name`
     *   and `last_name`.
     *
     * ```php
     * return [
     *     'email',
     *     'firstName' => 'first_name',
     *     'lastName' => 'last_name',
     *     'fullName' => function ($model) {
     *         return $model->first_name . ' ' . $model->last_name;
     *     },
     * ];
     * ```
     *
     * The default implementation of this method returns attributes() indexed by the same attribute names.
     *
     * @return array the list of field names or field definitions.
     * @see toArray()
     */
    public static function fields() {
        $fields = static::attributes();
        return array_combine($fields, $fields);
    }


    /**
     * Returns a value indicating whether the model has an attribute with the specified name.
     *
     * @param string $name the name of the attribute
     * @param DBX|null $connection -- Model compatibility
     * @return bool whether the model has an attribute with the specified name.
     */
    public static function hasAttribute(string $name, ?DBX $connection = null) {
        return in_array($name, static::attributes(), true);
    }

    /**
     * Returns the named attribute value.
     *
     * @param string $name the attribute name
     *
     * @return mixed the attribute value. `null` if the attribute is not set or does not exist.
     */
    public function getAttribute(string $name) {
        /** @noinspection PhpVariableVariableInspection */
        return $this->$name;
    }

    /**
     * ## Sets the named attribute value.
     *
     * @param string $name the attribute name
     * @param mixed $value the attribute value.
     *
     * @throws UXAppException
     */
    public function setAttribute(string $name, $value) {
        Assertions::assertString($name);
        /** @noinspection PhpVariableVariableInspection */
        $this->$name = $value;
    }

    /**
     * ## Determines which fields can be returned by {@see toArray()} or {@see createNode()}
     *
     * This method will check the requested fields against those declared in {@see fields()}
     * to determine which fields can be returned.
     *
     * If a specified field is not found, the attribute with the same name will be used.
     *
     * @param array|null $fieldlist -- the fields (or attributes) being requested for exporting. Default is all of fields(). Associative array indicates name mapping.
     * @return array the list of fields to be exported. The array keys are the field names, and the array values
     * are the corresponding object property names or PHP callables returning the field values.
     */
    public function resolveFields(array $fieldlist=null) {
        $result = array();
        $fields = $this->fields();

        if(empty($fieldlist)) {
            foreach ($fields as $field => $definition) {
                if(is_int($field)) $field = $definition;
                $result[$field] = $definition;
            }
            return $result;
        }

        foreach ($fieldlist as $alias => $field) {
            if(is_int($alias)) $alias = $field;
            if(is_callable($field)) {
                $definition = $field;
            }
            elseif(array_key_exists($field, $fields)) {
                $definition = $fields[$field];
            }
            else {
                $definition = $field;
            }
            $result[$alias] = $definition;
        }
        return $result;
    }

    /**
     * ## Converts the model into an array
     *
     * This method will first identify which fields to be included in the resulting array by calling resolveFields().
     * It will then turn the model into an array with these fields. If `$recursive` is true,
     * any embedded objects will also be converted into arrays.
     *
     * @param array|null $fields the fields being requested. If empty, all fields as specified by fields() will be returned.
     * @param bool $recursive whether to recursively return array representation of embedded objects.
     *
     * @return array the associative array representation of the object
     */
    public function toArray(?array $fields = null, $recursive = false) {
        $data = array();
        foreach ($this->resolveFields($fields) as $field => $definition) {
            /** @noinspection PhpVariableVariableInspection */
            $data[$field] = is_string($definition) ? $this->$definition : call_user_func($definition, $this, $field);
        }

        return $recursive ? ArrayUtils::toArray($data) : $data;
    }

    /**
     * Must return a name for XML nodes.
     * Default implementation returns the classname in lowercase letters.
     * See {@see createNode()} method
     *
     * @return string
     */
    static public function nodeName() {
        $className = explode('\\', strtolower(get_called_class()));
        return array_pop($className);
    }


    /**
     * # BaseModel::createNode()
     *
     * Creates an XML node from the model under parent node (does not handle associations like Model::createNode does)
     *
     * ### Options:
     * - **nodeName**: the name of created node, default is model name
     * - **attributes**: the attributes to generate, default is null = use fields(), or array of field names to use.
     * - **contentField**: name of attribute to include in node content (default none) (content field is excluded from attributes)
     * - **contentValue**: value of content, effective only if contentField is not given or has no value.
     * - **subnodeFields**: name of attributes to include in subnodes instead of attributes (or alias=>fieldname pairs). xml fields will include native xml values
     *        Instead of fieldname you may specify:
     *        - _callable($model, $index)_; callable may generate scalar or array, scalar may be an xml (DOMElement)
     *        - _array_:  multiple literal node values (recursive, {@see UXMLElement::addContent()})
     * - **extra**: extra attributes [name, name=>value, name=>function(item,index), name=>array...]
     *        - _name_ with numeric index: adds the name property
     *        - _name_ => value: adds value constant with key as attribute name
     *        - _name => function(item, index)_: callback
     *        - _name => array_: adds (sub-)property using getvalue (alias)
     * - **overwrite**: true = overwrite/complement existing node (given as parent)
     * - **labels**: If set to true, generates label subnodes. Default is false. May be an associative array of custom labels.
     *
     * Explicit declared and same-name associations are recursive.
     *
     * @param UXMLElement|ComponentInterface $node_parent
     * @param array $options
     *
     * @return UXMLElement
     * @throws ReflectionException
     * @throws Exception
     * @see Model::createNode()    for model options
     */
    public function createNode($node_parent, array $options= []) {
        if($node_parent instanceof DOMDocument) $node_parent = $node_parent->documentElement;
        if($node_parent instanceof ComponentInterface) $node_parent = $node_parent->node;

        $nodename = ArrayUtils::getValue($options, 'nodeName', static::nodeName());
        $attributes = $this->resolveFields(ArrayUtils::getValue($options, 'attributes'));
        $contentField = ArrayUtils::getValue($options, 'contentField');
        $defaultContentValue = ArrayUtils::getValue($options, 'contentValue');
        $subnodeFields = ArrayUtils::getValue($options, 'subnodeFields', []);
        $extra = ArrayUtils::getValue($options, 'extra', []);
        $index = ArrayUtils::getValue($options, 'index', 0); // set by createNodes
        $overwrite = ArrayUtils::getValue($options, 'overwrite', false); // Overwrite existing node (given as parent)
        $contentValue = $defaultContentValue;
        $labels = ArrayUtils::getValue($options, 'labels', false);

        foreach($attributes as $n=>$f) {
            if(is_int($n)) { $attributes[$f] = $f; unset($attributes[$n]); }
        }

        if($contentField) {
            /** @noinspection PhpVariableVariableInspection */
            $contentValue = $this->$contentField;
            if($contentValue===null) $contentValue = $defaultContentValue;
            unset($attributes[array_search($contentField, $attributes)]);
        }

        foreach($subnodeFields as $snf) {
            unset($attributes[array_search($snf, $attributes)]);
        }

        /** associative array of attributename=>attributevalue pairs */
        $attributevalues = array();
        foreach($attributes as $key=>$value) {
            if(!is_string($value) && is_callable($value)) $attributevalues[$key] = $value($this);
            else if(is_scalar($value)) {
                if(!preg_match('/^([\w.]+)(:(\w+))?$/', $value, $mm)) throw new UXAppException("Invalid field specifier `$value` for model `$this->shortName`");
                $value = $mm[1];
                $format = $mm[3] ?? false; // Format patterns recognized depends on configured formatter
                if($format) $value = UXApp::$app->format($value, $format);
                /** @noinspection PhpVariableVariableInspection */
                $attributevalues[$key] = $this->$value;
            }
        }

        foreach($attributevalues as $name=>$value) {
            if(substr($name, 0, 1)=='_') {
                $name = substr($name,1);
                $subnodeFields[$name] = $name;
                unset($attributevalues[$name.'_']);
            }
        }

        if($overwrite) {
            $node = $node_parent;
            foreach($attributevalues as $name=>$value) $node->addAttribute($name, $value);
        }
        else {
            $node = $node_parent->addNode($nodename, $attributevalues);
        }
        if($contentValue) {
            $node->addHypertextContent($contentValue, false, true);
        }

        foreach($subnodeFields as $key=>$snf) {
            $tagname = is_int($key) ? (is_string($snf) ? $snf : 'item_'.$key) : $key;
            if(is_callable($snf)) {
                // Subnodes generated by function: `array function($index, $model)`
                $values = call_user_func($snf, $this, $index);
                if(is_array($values)) {
                    foreach($values as $value) {
                        $node->addNode($tagname, $value);
                    }
                }
                else $node->addNode($tagname, $values);
            }
            else if(is_array($snf)) {
                foreach($snf as $v) $node->addNode($tagname, $v);
            }
            else {
                $node->addNode($tagname, $this->$snf);
            }
        }

        // extra (computed) attributes
        Assertions::assertArray($extra);
        foreach($extra as $name=>$fn) {
            if(is_numeric($name) && is_string($fn)) {
                /** @noinspection PhpVariableVariableInspection */
                $node->addAttribute($fn, $this->$fn);
                #UXApp::trace([$fn, $this->$fn);
            }
            else if(is_callable($fn)) {
                $node->addAttribute($name, call_user_func($fn, $this, $index));
            }
            else if(is_array($fn)) {
                $node->addAttribute($name, ArrayUtils::getValue($this, $fn));
            }
            else if(is_scalar($fn)) {
                $node->addAttribute($name, $fn);
            }
        }

        if($labels) {
            if(!is_array($labels)) $labels = $this->attributeLabels();
            $node->addNode('label', array_map(function($name, $value) {
                return array(array('name'=>$name), $value);
            }, array_keys($labels), array_values($labels)));
        }

        if(!$this->_node) $this->_node = $node;
        return $node;
    }

    /**
     * Creates XML nodes into DOM from all model entities of the array
     *
     * @param DOMElement|Component $node_parent -- If an UAppPage is given, it's content node will be applied
     * @param BaseModel[]|Query $models -- numeric indexed
     * @param mixed $options -- {@see BaseModel::createNode()}, {@see Model::createNode()}
     *
     * @return DOMElement -- the first node inserted or null if none
     * @throws UXAppException
     * @see BaseModel::createNode()    for basic options
     * @see Model::createNode()    for model options
     * @throws ReflectionException
     */
    public static function createNodes($node_parent, $models, $options=null) {
        if($node_parent instanceof Component) $node_parent = $node_parent->node;
        if(empty($models)) return null;
        $first_node = null;
        if($models instanceof Query) return static::createNodesQuery($node_parent, $models, $options);
        foreach($models as $index=>$item) {
            $options['index'] = $index;
            Assertions::assertClass($item, BaseModel::class);
            $node = $item->createNode($node_parent, $options);
            if(!$first_node) $first_node = $node;
        }
        return $first_node;
    }

    /**
     * Returns errors extended with values.
     *
     * @return array -- fieldname indexed array of numeric indexed errors and 'value'=> value
     */
    public function getErrorValues() {
        $errors = $this->_errors;
        foreach($errors as $fieldname => $fielderrors) {
            /** @noinspection PhpVariableVariableInspection */
            $errors[$fieldname]['value'] = $this->$fieldname;
        }
        return $errors;
    }

    /**
     * Returns friendly error texts.
     * Error strings contain all errors of the referred field with field label.
     *
     * @return array of fieldname => errorstring
     * @throws UXAppException
     */
    public function getFriendlyErrors() {
        $errortexts = array();
        foreach($this->_errors as $fieldname=>$fielderrors) {
            $label = $this->attributeLabel($fieldname);
            /** @noinspection PhpVariableVariableInspection */
            $value = $this->$fieldname;
            $errortexts[$fieldname] = UXApp::la(
                'uxapp',
                'Field `{$fieldname}` {$message}',
                array('fieldname'=>$label, 'message'=>implode(', ', $fielderrors), 'value'=>$value));
        }
        return $errortexts;
    }

    /**
     * Returns friendly errors as node array.
     * Suitable for UXMLElement::addContent()
     *
     * @return array -- [['error', 'field'=>name, error], ...]
     */
    public function getErrorNodes() {
        $errors = $this->friendlyErrors;
        return array_map(function($item, $key) {
            return ['error', 'field'=>$key, $item];
        }, $errors, array_keys($errors));
    }

    /**
     * Returns the ruleset for the named field
     * Returns empty array if rules not defined for the field.
     *
     * @param string $name
     * @return array
     */
    public function getRule(string $name) {
        $rules = $this->rules();
        return ArrayUtils::getValue($rules, $name, array());
    }

    /**
     * Returns first created node if any or null
     * @see property $node
     * @return DOMElement|null
     */
    public function getNode() {
        return $this->_node;
    }

    /**
     * Returns translatable validation rule message for client validation
     *
     * @param $rule_name
     *
     * @return string|null
     */
    static public function ruleMessage($rule_name) {
        $rulemessages = array(
            'mandatory' => 'is mandatory',
            'pattern' => 'has invalid format',
            //'length' => 'is too long or too short',
            'length' => 'length must be between $1 and $2',
            'between' => 'must be between $1 and $2',
        );
        return ArrayUtils::getValue($rulemessages, $rule_name);
    }

    /**
     * Checks if value fits to common php typename
     * No side effects, no error message
     *
     * @param string $type
     * @param mixed $value
     * @return boolean
     */
    public static function checkType(string $type, $value) {
        if(is_null($value)) return true;
        if(gettype($value) == $type) return true;
        if(is_object($value) && is_a($value, $type)) return true;
        if($type=='integer') {
            if(is_int($value)) return true;
            if (is_float($value + 0) && ($value + 0) > PHP_INT_MAX) return false;
            return (string)(int)$value == $value;
        }
        if($type=='float') return is_numeric($value);
        if($type=='string') return true;
        if($type=='boolean') {
            return in_array(strtolower($value), array('true', 'false', 't', 'f', '0', '1'));
        }
        if($type=='xml') {
            if($value instanceof DOMNode) return true;
            if(is_string($value) && substr($value,0,1)=='<') return true;
            return false;
        }
        // TODO: other types
        return false;
    }

    public function getErrors() {
        return $this->_errors;
    }

    public function resetErrors() {
        $this->_errors = array();
    }

}
