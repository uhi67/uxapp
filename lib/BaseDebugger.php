<?php /** @noinspection PhpIllegalPsrClassPathInspection */

namespace uhi67\uxapp;

use DOMDocument;
use DOMElement;
use Exception;
use ReflectionException;
use Throwable;
use uhi67\uxml\UXMLDoc;

/** @noinspection PhpUnused */

/**
 * Class BaseDebugger
 *
 * Only to switch trace state on and off.
 * (Affects exception screen details)
 *
 * ### Used session variables
 * - debug-enabled: enabled for session
 *
 * @package uhi67\uxapp
 */
class BaseDebugger extends Module implements DebugInterface {
    /** @var bool $enabled Debug is enabled for the session */
    public $enabled;
    /** @var string $on Debug is enabled for the request -- current session id */
    public $on;

    /**
     * @throws ReflectionException
     */
    public function prepare() {
        parent::prepare();
        $tracex = UXApp::$app->request->get('trace');
        if($tracex=='on') $this->enable();
        if($tracex=='off') $this->disable();
    }

    public function init() {
        try {
            $this->enabled = UXApp::$app->session ? UXApp::$app->session->get('debug-enabled') : false;
        }
        catch(UXAppException $e) {
            $this->enabled = false;
        }
        $this->on = $this->enabled ? UXApp::$app->session->id : false;
        $debugSession = UXApp::$app->request->get('debug-session');
        if($debugSession) $this->on = $debugSession;
    }

    /**
     * Returns current trace id
     *
     * @return string
     */
    public function getId() {
        return $this->on;
    }

    public function enable() {
        $this->enabled = true;
        $_SESSION['debug-enabled'] = true;
    }

    public function disable() {
        $this->enabled = false;
        $_SESSION['debug-enabled'] = false;
    }

    /**
     * Options:
     *
     * - params ($vars to substitute into msg)
     * - depth (effective backtrace depth from trace call)
     * - tags (space spearated words, e.g. 'app', 'UXApp', 'sql')
     * - mt (microtime to display)
     * - color (message color)
     *
     * @param mixed $msg
     * @param array $context
     */
    public function trace($msg = '*', $context = []) {
    }

    /**
     * Writes out an XML document or string fragment to the traceFile
     *
     * @param DOMDocument|DOMElement|string $xml
     * @param array $options -- ['tags', 'depth']
     *
     * @return void
     */
    public function trace_xml($xml, $options=[]) {
        if(!$this->isEnabled()) return;
        if(is_null($xml)) {
            $this->trace(null, $options);
            return;
        }
        $p = dirname(__DIR__).'/xsl';
        if(is_string($xml)) {
            $doc = new UXMLDoc();
            if(!$doc->loadXML($xml)) {
                $this->trace('Invalid XML:'.$xml, array_merge($options, ['color' => '#E00']));
                return;
            }
            $xml = $doc;
        }
        if($xml instanceof DOMElement) {
            $doc = new DOMDocument();
            $doc->appendChild($doc->importNode($xml));
            $xml = $doc;
        }

        try {
            $result = $xml->saveXML();
        } catch(Throwable $e) {
            $result = '{Invalid xml}';
        }

        $tags = explode(' ', $t = ArrayUtils::getValue($options, 'tags', ''));
        if(in_array('output', $tags)) {
            echo '</div><div id="output"><div class="dx">'.$result.'</div></div>';
        }
        else {
            $depth = ArrayUtils::getValue($options, 'depth', 2);
            $this->debug($result, ['depth'=>$depth+1, 'tags' => $t.' xml']);
        }
    }

    /**
     * Terminates tracing and closes tracefile
     *
     * @return void
     */
    public function end() {
    }

    public function backtrace($depth=0, $limit=0) {
    }

    /**
     * Terminates tracing and closes tracefile
     */
    public function finish() {
    }

    public function emergency($message, array $context = array()) {
        $this->log(LoggerInterface::LEVEL_EMERGENCY, $message, $context);
    }

    public function alert($message, array $context = array()) {
        $this->log(LoggerInterface::LEVEL_ALERT, $message, $context);
    }

    public function critical($message, array $context = array()) {
        $this->log(LoggerInterface::LEVEL_CRITICAL, $message, $context);
    }

    public function error($message, array $context = array()) {
        $this->log(LoggerInterface::LEVEL_ERROR, $message, $context);
    }

    public function warning($message, array $context = array()) {
        $this->log(LoggerInterface::LEVEL_WARNING, $message, $context);
    }

    public function notice($message, array $context = array()) {
        $this->log(LoggerInterface::LEVEL_NOTICE, $message, $context);
    }

    public function info($message, array $context = array()) {
        $this->log(LoggerInterface::LEVEL_INFO, $message, $context);
    }

    public function debug($message, array $context = array()) {
        $this->log(LoggerInterface::LEVEL_DEBUG, $message, $context);
    }

    public function log($level, $message, array $context = array()) {
    }

    public function off() {
        $this->on = false;
    }

    public function on() {
        if(!$this->on) $this->on = $this->session->id;
    }

    public function isOn() {
        return $this->on;
    }
    /**
     * @return bool -- true if trace is on
     */
    public function isEnabled() {
        return $this->enabled;
    }
}
