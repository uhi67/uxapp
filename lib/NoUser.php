<?php /** @noinspection PhpUnused */

namespace uhi67\uxapp;

use Exception;
use uhi67\uxml\UXMLElement;

/**
 * # BaseUser
 *
 * A logged in user
 *
 * ### Public properties
 *
 * - id
 * - name
 * - email
 * - enabled
 *
 * @package UXApp
 * @author Peter Uherkovich
 * @copyright 2014-2018
 */
class NoUser extends Module {
}
