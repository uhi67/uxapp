<?php
namespace uhi67\uxapp;
use Exception;

/**
 * L10n (Localization is 12 letter long)
 * File-based solution for L10n functions
 *
 * Uses php files in translation directory for translations
 * Uses default L10nBase for formatting dates.
 *
 * ### Configuration
 *
 * ```
 * 'l10n' => array(
 * 		'class' => L10nFile::class,
 * 		'uappDir' => dirname(__DIR__).'/def/translations', // Place of UXApp translation files. This is the default
 * 		'dir' => $defpath.'/translations', // Place of app translation files. This is the default
 * 		'defaultLocale' => 'hu',		// Default language with optional locale, may be changed by UXApp::setLang(lang/locale)
 *      'supportedLocales' => ['hu'=>'Magyar', 'en'=>'English', 'en-US'], // Supported locales with optional name
 * 		'source' => 'hu',		// Default source language, default is 'en'
 *      'param' => 'la',                // Language swith parameter
 *      'cookieName' => 'language',     // Cookie name for selected language if cookie is enabled
 * 		'cookieParams' => [],			// Optional cookie parameters
 * ),
 * ```
 *
 * ### Translation file format
 *
 * *File name is 'category/target_lang.php'*
 *
 * ```
 * return array(
 * 		'original text' => 'eredeti szöveg',
 * 		...
 * );
 * ```
 *
 * @author uhi
 * @package UXApp
 * @copyright 2017
 */

class L10nFile extends L10nBase {
	/** @var string $dir -- directory of translation files in form 'la.php', default is def/translations */
	public $dir;
	/** @var string|array $source -- Default source language, default is 'en'; or source languages for categories */
	public $source;

	/**
	 * {@inheritdoc}
	 * @throws Exception
	 */
	public function prepare() {
		parent::prepare();
		if($this->dir) {
			$this->dir = rtrim($this->dir, '/');
		}
		else {
			$this->dir = UXApp::$app->defPath.'/translations';
		}
		if(!Util::makeDir($this->dir,2)) throw new UXAppException('Configuration error: directory does not exists for translation class.', $this->dir);
	}

	/**
	 * Translates a text from source language to given language.
	 *
	 * If category directory is not found, returns source**.
	 * If translation is not found, returns source*.
	 *
	 * @param string $category -- message category, the framework itself uses 'uxapp'. Application default is 'app'
	 * @param string $source - source language text or text identifier
	 * @param array $params - replaces {$var} parameters
	 * @param integer $lang - target language code (ll or ll-LL) to translate into. Default is the language set in the framework
	 *
	 * @return string
	 */
	public function getText($category, $source, $params=NULL, $lang=null) {
		if($lang===null) $lang = $this->lang;

		$directory = $this->directory($category);
		if(!is_dir($directory)) return $source.'***'; // The category does not exist

		$text = static::getTextFile($category, $directory, $source, $lang);

		// substitute parameters
        if($params) {
        	if(!is_array($params)) $params = [$params];
        	$text = Util::substitute($text, $params);
       	}
		return $text;
	}

	/**
	 * Returns the directory for the category.
	 * If the directory is not found in the app,
	 * - 'uxapp' and 'uxapp/*' will refer to the framework directory,
	 * - 'vendor/lib/*' categories will refer to the corresponding library (~/def/translation/*)
	 *
	 * @param string $category
	 * @return string
	 */
	public function directory($category) {
		$category = str_replace('..', '.', $category); // prevent backstep in the directory tree
		$directory = $this->dir.'/'.$category;
		if(!is_dir($directory)) {
			$cats = explode('/', $category);
			if($cats[0]=='uxapp') {
				$directory = dirname(__DIR__).'/def/translations';
				array_shift($cats);
			}
			else {
				$directory = UXApp::$vendorDir . '/'. array_shift($cats).'/'.array_shift($cats).'/def/translations';
			}
			if(count($cats)>0) $directory .= '/'.implode('/', $cats);
		}
		return $directory;
	}
}
