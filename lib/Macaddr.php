<?php /** @noinspection PhpUnused */

namespace uhi67\uxapp;

/**
 * Macaddr -- represents a 48 bits Ethernet MAC address
 * 
 * @package UXApp
 * @author Peter Uherkovich
 * @copyright 2017
 */
class Macaddr extends Component {
	const VALID = "/^([\\dA-Fa-f]{2})[:.-]?([\\dA-Fa-f]{2})[:.-]?([\\dA-Fa-f]{2})[:.-]?([\\dA-Fa-f]{2})[:.-]?([\\dA-Fa-f]{2})[:.-]?([\\dA-Fa-f]{2})$/";
	
	/** @var string $value -- packed address */
	public $value = null;

	/** @noinspection PhpMissingParentConstructorInspection */

	/**
	 * Construct a Macaddr based on string representation
	 * 
	 * @param string|array $mac
	 *
	 * @return void
	 * @throws UXAppException on invalid input
	 */
	public function __construct($mac=[]) {
		if(($value = $this->isValid($mac, true))===false) throw new UXAppException('Invalid MAC address format');
		$this->value = hex2bin($value);
	}

	/**
	 * Checks $mac string if it is a valid MAC address
	 * Returns parsed string value (without separators) if valid
	 *
	 * @param string $mac -- value to check
	 * @param bool $returnvalue -- return parsed value instead of boolean
	 *
	 * @return string|boolean -- false on invalid
	 * @throws UXAppException
	 */
	public static function isValid($mac, $returnvalue=false) {
		Assertions::assertString($mac);
		$valid = preg_match(static::VALID, $mac, $mm);
		if(!$valid) return false;
		if(!$returnvalue) return true;
		$r = '';
		for($i=1;$i<=6;$i++) $r .= $mm[$i];
		return $r;
	}

	/**
	 * Returns a canonical text representation with :-s 
	 * 
	 * @return string
	 */
	public function toString() {
		$hex = bin2hex($this->value);
		$r = '';
		for($i=0; $i<6; $i++) {
			if($i>0) $r .= ':';
			$r .= substr($hex,$i*2,2);
		}
		return strtoupper($r);
	}
	
	public function __toString() {
		return '{'.$this->toString().'}';
	}

	/**
	 * MAC cím szabványos alakra hozása
	 * bájtonként :-tal elválasztott
	 *
	 * @param string $mac -- mac cím bármilyen formátumban
	 *
	 * @return string - szabványosított cím, vagy false, ha nem értelmezhető
	 * @throws UXAppException
	 */
	static function canonize($mac) {
		if($mac===null) return null;
		$valid = preg_match(static::VALID, $mac, $mm);
		if(!$valid) return false;
		$new = new Macaddr($mac);
		return $new->toString();
	}

	/**
	 * MAC részlet szabványos alakra hozása
	 * 
	 * @param string $mac -- mac cím részlet. null returns null
	 * @return string - szabványosított részlet, vagy false, ha nem értelmezhető
	 */
	static function canonize_part($mac) {
		if($mac===null) return null;
		$re = "/^(?:([\\dA-Fa-f]{2})[:.-]?){1,6}$/";
		if(!preg_match($re, $mac, $mm)) return false;
		#echo "<div>$mac=".implode('-', $mm)."</div>";
		$re = "/([\\dA-Fa-f]{2})[:.-]?/";
		preg_match_all($re, $mac, $mm);
		#echo "<div>$mac=".implode('-', $mm[0])."</div>";
		$r = '';
		$n = count($mm[1]);
		for($i=0;$i<$n;$i++) {
			if($i>0) $r .= ':';
			$r .= $mm[1][$i];
		}
		return strtoupper($r);
	}

}
