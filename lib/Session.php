<?php /** @noinspection PhpIllegalPsrClassPathInspection */

namespace uhi67\uxapp;

/**
 * # Class Session
 *
 * Default, file-version.
 * TODO: implement DatabaseSession
 *
 * ###Session configuration example for `config.php`
 *
 * ```php
 * 	'session' => [
 * 		'name' => 'sess_sample',
 * 		'save_path' => $datapath . '/session',
 * 		'lifetime' => 1800,
 * 		'cookie_path' => '',
 * 		'cookie_domain' => 'sample.hu',
 * 		'logfile' => $datapath.'/session.log',
 * 	],
 * ```
 */
class Session extends BaseSession {
	public $name = 'app';
	public $save_path;
	public $lifetime = 1800;
	public $cookie_path = '';
	public $cookie_domain;
	public $logfile;

	/**
	 * @throws UXAppException
	 */
	public function prepare() {
		if($this->cookie_domain === true) $this->cookie_domain = parse_url($this->parent->baseurl, PHP_URL_HOST);
		if(php_sapi_name() == "cli") return;
		if (session_status() == PHP_SESSION_NONE) {
			ini_set("session.gc_maxlifetime", $this->lifetime + 900);
			ini_set("session.lifetime", $this->lifetime);
			ini_set("session.gc_probability", "100");
			session_set_cookie_params(0, $this->cookie_path, $this->cookie_domain);
			session_name($this->name);
			session_cache_limiter('private_no_expire');
			if(headers_sent($file, $line)) throw new UXAppException("Cannot start session, headers already sent in $file:$line");
			if($this->save_path===null) $this->save_path = UXApp::$app->dataPath . '/session';
			if($this->save_path) {
				if(!file_exists($this->save_path)) mkdir($this->save_path, 0774);
				if(!file_exists($this->save_path)) throw new UXAppException("Cannot create session directory", $this->save_path);
				session_save_path($this->save_path);
			}
		}
		elseif(session_status()==PHP_SESSION_ACTIVE) {
			UXApp::trace('Session is already active');
		}
		else throw new UXAppException('Session already exist but not active.');

		if(!@session_start()) throw new UXAppException('Cannot start session: '.Util::objtostr(error_get_last()).'; Status: '.session_status());
	}

	/**
	 */
	function __destruct() {
		UXApp::trace('Destructing session', ['tags'=>'uxapp']);
		#$this->finish();
	}

	/**
	 */
	function finish() {
		if(session_status()==PHP_SESSION_ACTIVE) {
			session_write_close();
			//unset($_SESSION);
		}
	}
}
