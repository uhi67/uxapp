<?php /** @noinspection PhpIllegalPsrClassPathInspection */

namespace uhi67\uxapp;

use Exception;

/**
 * # Class BaseUrlFormatter
 *
 * Creates relative url from abstract url data and parses url to abstract structure.
 * Implements default behavior without compressing urls.
 *
 * Special keys of abstract url data arrays:
 *
 * - module
 * - page
 * - act
 * - id
 *
 * Key shortcuts:
 *
 * - first numeric indexed word is 'page', equivalent with 'page' index
 * - second numeric indexed word is 'act', equivalent with 'act' index
 * - third  numeric indexed word is 'id', equivalent with 'id' index
 * - all other indices are query parameters
 *
 * ## Example (the three abstract examples are equivalent, resulting the same full url)
 *
 * - ['page', 'act', 'id'=>23, 'p2'=>47] ==> '/index.php?page=page&act=act&id=23&p2=47'
 * - ['page', 'act', 23, 'p2'=>47] ==> '/index.php?page=page&act=act&id=23&p2=47'
 * - ['page'=>'page', 'act'=>'act', 'id'=>23, 'p2'=>47] ==> '/index.php?page=page&act=act&id=23&p2=47'
 *
 * @package app\vendor\uhi67\uxapp\lib
 */
class BaseUrlFormatter extends Component {
    /** @var string $baseUrl -- absolute base url, including dispatcher script, example: http://app.test/index.php */
    public $baseUrl;
    /** @var string $script -- dispatcher script relative to baseUrl, example: 'index.php' (optional) */
    public $script;

    public function prepare() {
        parent::prepare();
        if(!$this->baseUrl) {
            $this->baseUrl = UXApp::$app->baseurl;
        }
        $baseScript = $this->baseUrl ? parse_url($this->baseUrl, PHP_URL_PATH) : null;
        if(!$this->script && $baseScript > '/') $this->script = $baseScript;
        if(!$this->script) $this->script = '/index.php';
        if(!$baseScript) $this->baseUrl .= $this->script;
    }

    /**
     * Creates a relative URL string from an abstract url
     * Returns empty string on empty input
     *
     * The simple implementation returns canonical url
     *
     * @param array|string $to -- abstract url format (action ID + query)
     * @param bool $absolute -- return absolute url
     *
     * @return string -- relative or absolute url
     * @throws UXAppException
     * @noinspection PhpRedundantVariableDocTypeInspection
     */
    public function createUrl($to, bool $absolute=false): string {
        if(!$to) return '';
        if($absolute) return $this->absoluteUrl($to);
        if(!is_array($to)) {
            /** @var string $to */
            $fragment = parse_url($to, PHP_URL_FRAGMENT);
            $query = $fragment ? ['#' => $fragment] : [];
            parse_str(parse_url($to, PHP_URL_QUERY), $query);
            /** @var array $to */
            $to = $query;
        } else {
            $to = $this->canonize($to);
            $fragment = ArrayUtils::fetchValue($to, '#');
        }
        if(isset($to['path']) && is_array($to['path'])) {
            $to['path'] = implode('/', $to['path']);
        }
        return $this->script . '?' . http_build_query($to) . ($fragment ? '#' . $fragment : '');
    }

    /**
     * Returns canonized url array
     *
     * numeric keys converted into 'module', 'page', 'act', 'id', 'path' keys
     *
     * @param array $to
     *
     * @return array
     * @throws UXAppException
     * @throws Exception
     */
    public function canonize($to) {
        $query = [];
        $path = [];
        Assertions::assertArray($to);
        foreach($to as $key => $value) {
            if(is_int($key) && $value !== '') {
                if(!is_numeric($value) && !isset($query['page']) && !isset($to['page'])) $query['page'] = $value;
                elseif(!is_numeric($value) && !isset($query['act']) && !isset($to['act']) && !isset($query['id'])) $query['act'] = $value;
                elseif(!isset($query['id']) && !isset($to['id'])) $query['id'] = $value;
                else $path[] = $value;
            }
        }
        foreach($to as $key => $value) if(!is_int($key)) $query[$key] = $value;
        if($path) $query['path'] = implode('/', $path);
        return $query;
    }

    /**
     * Creates an absolute url from relative or abstract
     *
     * @param array|string $url -- relative url or abstract url data
     *
     * @return string -- absolute url
     * @throws UXAppException
     */
    public function absoluteUrl($url) {
        if(is_array($url)) $url = $this->createUrl($url);
        $data = parse_url($url, PHP_URL_QUERY);
        $fragment = parse_url($url, PHP_URL_FRAGMENT);
        return $this->baseUrl . '?' . http_build_query($data) . ($fragment ? '#' . $fragment : '');
    }

    /**
     * Returns canonical url array from a relative url
     *
     * @param string $url
     *
     * @return string[]
     * @throws Exception
     */
    public function parseUrl(string $url) {
        $url1 = Util::substring_after($url, $this->script, true);
        $path = parse_url($url1, PHP_URL_PATH) ?? '';
        $query = [];
        parse_str(parse_url($url1, PHP_URL_QUERY), $query);
        $fragment = parse_url($url1, PHP_URL_FRAGMENT);
        if($fragment) $query['#'] = $fragment;
        return $this->parsePath($path, $query);
    }

    /**
     * Modifies a given or current URL with new parameter values.
     * Does not use compression rules.
     *
     * @param string|null $url -- url to modify, default: current URL
     * @param array $params -- new parameter values
     *
     * @return string
     * @noinspection DuplicatedCode
     */
    public function modUrl(string $url = null, array $params = []) {
        if($url === null) $url = UXApp::$app->request->url;
        $fragment = '';
        if(isset($params['#'])) {
            $fragment = $params['#'];
            unset($params['#']);
        }

        $u = parse_url($url);
        $base = (isset($u['scheme']) ? $u['scheme'] . '://' : '') .
            ($u['user'] ?? '') .
            (isset($u['pass']) ? ':' . $u['pass'] : '') .
            ($u['host'] ?? '') .
            (isset($u['port']) ? ':' . $u['port'] : '') .
            ($u['path'] ?? '');
        $query = [];
        if(isset($u['query'])) parse_str($u['query'], $query);
        $params = array_filter(array_merge($query, $params), function($item) {
            return $item !== null;
        });
        return
            $base .
            (count($params) ? ('?' . http_build_query($params)) : '') .
            ($fragment ? ('#' . $fragment) : '');
    }

    /**
     * Decompress url element. The result is a canonical query containing all neccessary keys:
     *
     * - module (optional)
     * - page
     * - act (optional,default is "default")
     * - id (optional, numeric may be compressed only)
     * - path (remainder of the path, not recommended to use)
     *
     * This simples implementation decodes /page/act/id elements, or /"module"/module/page/act/id
     *
     * @param string|string[] $path --
     * @param string[] $query
     *
     * @return array|false
     */
    public function parsePath($path, array $query=[]): array {
        if(is_string($path)) $path = explode('/', $path);
        $page = array_shift($path);
        if($page=='module') {
            $module = $query['module'] = array_shift($path);
            if(!is_dir(UXApp::$app->rootPath.'/modules/'. $module)) {
                $query['module'] = $module . '/' . array_shift($path);
            }
            $page = array_shift($path);
        }
        if(is_numeric($page)) {
            $id = $page;
        }
        else {
            if($page && $page!='index.php') $query['page'] = $page;
            $act = array_shift($path);
            if(is_numeric($act)) $id = $act;
            else {
                if($act) $query['act'] = $act;
                $id = array_shift($path);
            }
        }
        if($id!==null) $query['id'] = $id;
        if($path) $query['path'] = implode('/', $path);
        if(isset($query['id']) && preg_match('/^\d+$/', $query['id'])) $query['id'] = (int)$query['id'];
        return $query;
    }
}