<?php /** @noinspection PhpUnused */

namespace uhi67\uxapp;

use DOMElement;
use ReflectionException;
use uhi67\uxapp\UXAppException;
use uhi67\uxml\UXMLElement;

/**
 * # Component Interfcae
 *
 * - Implements *property* features: magic getter and setter uses getProperty and setProperty methods
 * - Configurable: constructor accepts a configuration array containing values for public properties
 *
 * @package UXApp
 * @author Peter Uherkovich
 * @copyright 2017
 *
 * @property-read  UXMLElement $node -- first created node in the output DOM document
 */
Interface ComponentInterface {

	/**
	 * # Component constructor
	 * The default implementation does two things:
	 *
	 * - Initializes the object with the given configuration `$config`.
	 * - Calls prepare().
	 *
	 * If this method is overridden in a child class, it is recommended that
	 *
	 * - the last parameter of the constructor is a configuration array, like `$config` here.
	 * - call the parent implementation in the constructor.
	 *
	 * @param array $config name-value pairs that will be used to initialize the object properties
	 *
	 * @throws
	 */
    public function __construct($config = array());

    /**
     * Initializes the object.
     * This method is invoked at the end of the constructor after the object is initialized with the
     * given configuration. Default does nothing, override it if you want to use.
     */
    public function prepare();

	/**
	 * Returns the value of a component property.
	 *
	 * @param string $name the property name
	 *
	 * @return mixed the property value
	 * @throws UXAppException
	 * @see __set()
	 */
    public function __get($name);

    /**
     * Sets the value of a component property.
     *
     * @param string $name the property name
     * @param mixed $value the property value
     *
	 * @throws UXAppException if the property is not defined or the property is read-only.
     * @see __get()
     */
    public function __set($name, $value);

    /**
     * Checks if a property is set: defined and not null.
     * @param string $name the property name
     * @return bool whether the named property is set
     * @see http://php.net/manual/en/function.isset.php
     */
    public function __isset($name);

    /**
     * Sets a component property to be null.
     *
     * @param string $name the property name
	 *
	 * @throws UXAppException if the property is read only.
     * @see http://php.net/manual/en/function.unset.php
     */
    public function __unset($name);

    /**
     * Returns a value indicating whether a property is defined for this component.
     * @param string $name the property name
     * @param bool $checkVars whether to treat member variables as properties
     * @return bool whether the property is defined
     * @see canGetProperty()
     * @see canSetProperty()
     */
    public function hasProperty($name, $checkVars = true);

    /**
     * Returns a value indicating whether a property can be read.
     * @param string $name the property name
     * @param bool $checkVars whether to treat member variables as properties
     * @return bool whether the property can be read
     * @see canSetProperty()
     */
    public function canGetProperty($name, $checkVars = true);

    /**
     * Returns a value indicating whether a property can be set.
     * @param string $name the property name
     * @param bool $checkVars whether to treat member variables as properties
     * @return bool whether the property can be written
     * @see canGetProperty()
     */
    public function canSetProperty($name, $checkVars = true);

	/**
	 * Configures an object with the initial property values.
	 *
	 * @param Component $object the object to be configured
	 * @param array $properties the property initial values given in terms of name-value pairs.
	 *
	 * @return object the object itself
	 * @throws
	 */
    public static function configure($object, $properties=null);

	/**
	 * Creates a new Component using the given configuration.
	 *
	 * You may view this method as an enhanced version of the `new` operator.
	 * The method supports creating an object based on a class name, a configuration array or
	 * an anonymous function.
	 *
	 * Below are some usage examples:
	 *
	 * ```php
	 * // create an object using a class name
	 * $object = Component::create('Customer');
	 *
	 * // create an object using a configuration array
	 * $object = Component::create([
	 *     'class' => Customer::class,
	 *     'name' => 'Foo Baar',
	 * ]);
	 *
	 * // create an object with two constructor parameters
	 * $object = Component::create('Customer', [$param1, $param2]);
	 * ```
	 *
	 * @param string|array|callable $type the object type. This can be specified in one of the following forms:
	 *
	 * - a string: representing the class name of the object to be created
	 * - a configuration array: the array must contain a `class` element which is treated as the object class,
	 *   and the rest of the name-value pairs will be used to initialize the corresponding object properties
	 * - a configuration array: the array must contain a `0` element which is treated as the object class,
	 *   and the rest of the name-value pairs will be used to initialize the corresponding object properties
	 * - a PHP callable: either an anonymous function or an array representing a class method (`[$class or $object, $method]`).
	 *   The callable should return a new instance of the object being created.
	 *
	 * @param array $params the constructor parameters
	 *
	 * @return object the created object
	 * @throws UXAppException if the configuration is invalid.
	 * @throws ReflectionException
	 */
    public static function create($type, array $params = array());

	/**
	 * Must return the representative node of the component in the DOM.
	 * If the component is the document holder, the main content or document node should be returned.
	 *
	 * @return DOMElement
	 */
    public function getNode();
}
