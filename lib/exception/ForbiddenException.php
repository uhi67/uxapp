<?php

namespace uhi67\uxapp\exception;

use uhi67\uxapp\UXApp;

/**
 * ForbiddenException represents a "Forbidden" HTTP exception with status code 403.
 *
 * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.4
 */
class ForbiddenException extends UserException {
    /**
     * @param string|array $message -- message text or [message, params, info]
     * @param array $extra -- debug details as array($type, $object, $effective)
     */
    public function __construct($message = null, $extra=null, $goto=null) {
        parent::__construct($message, $extra, null, 403, $goto);
    }

    public function getTitle() {
		return 'Jogosultsági hiba';
	}

    public function getSubtitle() {
    	return UXApp::la('uxapp', 'You have no permission for this action / Nincs engedélye a keresett oldalhoz vagy a művelet végrehajtásához.');
    }
}
