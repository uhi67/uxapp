<?php

namespace uhi67\uxapp\exception;

use Exception;

/**
 * HandledException
 * 
 * Exception already rendered for user
 * Previous contains the original exception
 */
class HandledException extends Exception {
}
