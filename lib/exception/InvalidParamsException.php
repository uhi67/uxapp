<?php

namespace uhi67\uxapp\exception;

use Exception;
use uhi67\uxapp\UXApp;
use uhi67\uxapp\UXAppException;

class InvalidParamsException extends UXAppException {
    /**
     * @param string|array -- $message message text or [message, params, info]
     * @param array $extra -- debug details
     * @param string $goto -- clickable redirection for end user 
     * @param Exception $previous The previous exception used for the exception chaining.
     */
    public function __construct($message = null, $extra=null, $goto=null, $previous=null) {
        parent::__construct($message, $extra, $goto, 500, $previous);
    }
    
    public function getName() {
        return UXApp::la('uxapp', 'Invalid parameters');
    }
}
