<?php
namespace uhi67\uxapp\exception;

use Exception;
use uhi67\uxapp\UXApp;
use uhi67\uxapp\UXAppException;

/**
 * NotFoundException represents a "Not Found" HTTP exception with status code 404.
 */
class NotFoundException extends UserException {
    /**
     * @param string|array $message
     * @param mixed $extra -- debug details
     * @param string $goto -- clickable redirection for end user
     * @param Exception $previous The previous exception used for the exception chaining.
     */
    public function __construct($message = null, $extra=null, $goto=null, $previous=null) {
        parent::__construct($message, $extra, $previous, 404, $goto);
        $this->code = 404;
    }

    public function getTitle() {
        return UXApp::la('uxapp', 'Not found');
    }

    public function getSubtitle() {
        $message = 'The requested page or resource is not found / Az oldal vagy erőforrás nem található.';
        try {
            return UXApp::la('uxapp', $message);
        }
        catch(UXAppException $e) {
            return $message;
        }
    }
}
