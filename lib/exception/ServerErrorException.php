<?php

namespace uhi67\uxapp\exception;

use Exception;
use uhi67\uxapp\UXAppException;

/**
 * ServerErrorException represents an "Internal Server Error" HTTP exception with status code 500.
 */
class ServerErrorException extends UXAppException
{
    /**
     * Constructor.
     * @param string $message|array error message or array(message, params, info) where message contains %s style param references
     * @param string $goto
     * @param mixed $extra
     * @param Exception $previous The previous exception used for the exception chaining.
     */
    public function __construct($message = null, $extra=null, $goto=null, $previous = null)
    {
        parent::__construct($message, $extra, $previous, 500, $goto);
    }
}
