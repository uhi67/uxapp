<?php

namespace uhi67\uxapp\exception;

use uhi67\uxapp\UXAppException;

/**
 * UserException is a (HTTP) exception to display to end user without backtrace
 *
 * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.4
 */
class UserException extends UXAppException {

	public function getTitle() {
		return 'User Error / Felhasználói hiba';
	}
}
