<?php /** @noinspection PhpIllegalPsrClassPathInspection */
/** @noinspection PhpUnused */

namespace uhi67\uxapp;

use DOMDocument;
use DOMElement;
use Locale;
use Psr\Log\LoggerTrait;
use Psr\Log\LogLevel;
use ReflectionException;
use Throwable;
use DateTime;
use Exception;
use IntlDateFormatter;
use uhi67\uxapp\exception\HandledException;
use uhi67\uxapp\exception\NotFoundException;
use uhi67\uxml\FormatterInterface;

/**
 * doc * # UXApp -- the main application component
 *
 * Handles user request, loads and runs the controller and other basic utilities
 *
 * ### Mandatory or recommended configuration parameters:
 *
 * - **title** (string)
 * - **moduleName** -- module or application name (string)
 * - **components** (array) -- associative array of configurations for common components of the applications accessible as $app->comnponenName
 * - **defaultPage** (string) default is 'start'
 * - **design** -- design theme ('vendor/module:layout' or 'layout' or 'module:') -- default for application pages
 * - **baseurl** -- base url for accessing module assets or application base url
 * - **dataPath** -- Runtime data, logs, ignore from git (default is "$rootPath/runtime")
 *
 * ### Optional parameters:
 *
 * - ver (string)
 * - render (string)  -- default render mode: 'server' (default), 'client'
 * - environment -- must be 'development' or 'production'
 * - runtimeXslPath -- absolute path to runtime generated xsl renderer files. Default is "$dataPath/xsl"
 * - rootPath -- application's root path (default is root path of the project)
 * - defPath -- xml definitions, syntaxes, other data included in git (default is "$rootPath/def")
 * - basePath -- path of www assets (default is "$rootPath/www")
 * - xslurl -- base url for accessing xsl views of module or application (default is "$baseurl/xsl)
 *
 * @property-read L10nBase $l10n
 * @property-read DBX $database -- instance of 'database' component if exists, null otherwise
 * @property-read DBX $db -- alias of 'database' component
 * @property-read LoggerInterface $logger
 * @property-read BaseSession $session
 * @property-read CacheInterface $cache
 * @property-read DebugInterface $debug
 * @property-read RequestInterface $request
 * @property-read BaseUrlFormatter $urlFormatter
 * @property-read AssetManager $assetManager
 *
 * @property string $locale -- locale from l10n component
 * @property-read string $lang -- lang from l10n component
 * @property-read BaseUser $user -- user component, if exists or null
 * @property-read bool $isCLI
 */

class UXApp extends Module {
    const
        EXIT_STATUS_OK = 0,
        EXIT_STATUS_ERROR = 1,          // General error
        // User defined codes 2..99
        // UXApp common status codes
        EXIT_STATUS_ABORT = 101,          // Action was aborted by the user
        EXIT_STATUS_NOT_FOUND = 102,      // Command not found
        EXIT_STATUS_CONFIG_ERROR = 103,   // UXApp config error
        EXIT_STATUS_EXCEPTION = 104;      // Handled exception
        // Linux standard codes 126..255
    use LoggerTrait;
    /** @var string $title -- Title of the application. Default is copy of 'name' */
    public $title;
    /** @var string -- Version number of the application */
    public $ver;
    /** @var string $default -- Name of application's default page */
    public $defaultPage = 'start';
    /** @var string $render -- default render mode: server (default), client */
    public $render = 'server';
    /** @var string $design -- design theme (vendor/module:layout or layout or module:) -- default for application pages */
    public $design;
    /** @var string $environment -- must be 'development' or 'production' */
    public $environment;
    /** @var string $xslurl */
    public $xslurl;
    /** @var string $xslpath -- absolute path to application local xsl views */
    public $xslpath;
    /** @var string $runtimeXslPath -- absolute path to runtime generated xsl renderer files. Default is $dataPath/xsl */
    public $runtimeXslPath;
    /** @var string $modulesPath -- installed modules for this app. Don't confuse with modulePath */
    public $modulesPath;

    /** @var string[] $args -- named command-line arguments (indexed by names) */
    public $args;
    /** @var string[] $argn -- not named command-line arguments (numeric indexed) */
    public $argn;
    /** @var UXAppPage $page */
    public $page;
    /** @var string $ip -- client's ip */
    public $ip;
    /** @var FormatterInterface $formatter */
    public $formatter;
    /** @var string $timeZone */
    public $timeZone;
    /** @var string $sapi -- the interface between php and web server, default from php_sapi_name() */
    public $sapi;

    /** @var string */
    public static $vendorDir = '';
    /** @var UXApp $app -- single instance of UXApp */
    public static $app;

    /** @var string $_locale -- stored locale if no localization component present */
    private $_locale;

    /** @var array[] $_httpResponseCodes -- array of response names indexed by lang/code loaded from def file */
    private static $_httpResponseCodes;

    /**
     * @throws
     */
    function prepare() {
        if(!self::$vendorDir) self::$vendorDir = $GLOBALS['vendorDir']??getenv('COMPOSER_VENDOR_DIR');
        if(!self::$vendorDir) {
            $autoVendor = dirname(__DIR__,3);
            if(!is_dir($autoVendor.'/uhi67') && is_dir(dirname(__DIR__).'/vendor')) $autoVendor = dirname(__DIR__).'/vendor';
            self::$vendorDir =$autoVendor;
        }
        if(!$this->sapi) $this->sapi = php_sapi_name();
        if(!$this->rootPath) $this->rootPath = $this->rootPath();
        if(!$this->xslpath) $this->xslpath = $this->rootPath.'/xsl';
        if(!$this->modulesPath) $this->modulesPath = $this->rootPath.'/modules';
        if($this->locale) {
            setlocale(LC_ALL, $this->locale);
            Locale::setDefault($this->locale);
        }
        try {
            if(!$this->timeZone) $this->timeZone = date_default_timezone_get();
            date_default_timezone_set($this->timeZone);
            parent::prepare();
            static::$app = $this;

            if(!$this->ver && is_dir('.git')) {
                exec('git describe --tags --always', $ver);
                if($ver) $this->ver = trim($ver[0]);
            }
            if(!$this->title) $this->title = $this->moduleName;

            $this->ip = ArrayUtils::getValue($_SERVER, 'REMOTE_ADDR', getenv('REMOTE_ADDR'));

            if(isset($_SERVER['argv']) && isset($_SERVER['argc'])) $this->parseArgs($_SERVER['argc'], $_SERVER['argv']);

            if(!$this->runtimeXslPath) $this->runtimeXslPath = $this->dataPath . '/xsl/';
            if(substr($this->runtimeXslPath, -1) != '/') $this->runtimeXslPath .= '/';
            if(!is_dir($this->runtimeXslPath)) {
                if(!@mkdir($this->runtimeXslPath, 0774, true)) {
                    throw new Exception('Failed creating runtime XSL path `' . $this->runtimeXslPath . '`');
                }
            }

            // Default components
            if(!isset($this->components['logger'])) $this->components['logger'] = [
                'class' => UXAppLogger::class,
            ];
            if(!isset($this->components['urlFormatter'])) $this->components['urlFormatter'] = [
                'class' => BaseUrlFormatter::class,
            ];
            if(!isset($this->components['request'])) $this->components['request'] = [
                'class' => Request::class,
            ];
            if(!isset($this->components['assetManager'])) $this->components['assetManager'] = [
                'class' => AssetManager::class,
            ];
            if(!isset($this->components['session'])) $this->components['session'] = [
                'class' => Session::class,
            ];

            // Prepare components (in definition order)
            foreach($this->components as $name => $component) {
                try {
                    if($component === null) continue;
                    if(!is_array($component)) $component = [$component];
                    if(!isset($component['class']) && !isset($component[0])) throw new UXAppException("Configuration error: no class specified for component $name");
                    $className = $component['class'] ?? $component[0];
                    if(is_array($className)) {
                        // Multiple components specified
                        $components = [];
                        foreach($component as $i => $comp) {
                            $class = $comp['class'] ?? $comp[0];
                            if(!is_string($class) || !is_a($class, Component::class, true)) {
                                throw new UXAppException("Invalid component `$name [$i]`: " . $class . ' (class does not exist or not a Component)');
                            }
                            if(property_exists($class, 'parent') && !array_key_exists('parent', $comp)) $comp['parent'] = $this;
                            if(property_exists($class, 'componentName') && !array_key_exists('componentName', $comp)) $comp['componentName'] = $name . '_' . $i;
                            $components[] = Component::create($comp);
                        }
                        $this->components[$name] = $components;
                    } else {
                        if(!is_a($className, Component::class, true)) {
                            throw new UXAppException("Invalid component `$name`: " . $className . ' (class does not exist or not a Component)');
                        }
                        if(property_exists($className, 'parent') && !array_key_exists('parent', $component)) $component['parent'] = $this;
                        if(property_exists($className, 'componentName') && !array_key_exists('componentName', $component)) $component['componentName'] = $name;                    /** @var Module $component */
                    try {
                        $this->components[$name] = ($component === null) ? null : Component::create($component);
                    }
                    catch(Throwable $e) {
                        if(ENV_DEV) var_dump($component);
                        throw new Exception("Error creating component '$name'", 500, $e);
                    }
                }
                }
                catch(Throwable $e) {
                    throw new UXAppException("Error creating component '$name' ", $component, $e);
                }
            }

            // Init modules (in definition order)
            foreach($this->components as $name => $component) {
                if(is_array($component) && isset($component[0]) && is_object($component[0])) {
                    // Multiple components specified
                    foreach($component as $i => $comp) {

                        if(is_callable([$comp, 'init'])) call_user_func([$comp, 'init']);
                        UXApp::trace("Component '$name[$i]' initialized", ['tags' => 'uxapp']);
                    }
                }
                else {
                    if(is_callable([$component, 'init'])) call_user_func([$component, 'init']);
                    UXApp::trace("Component '$name' initialized", ['tags' => 'uxapp']);
                }
            }

            $this->formatter = ($formatter = $this->getComponent('formatter')) ?
                $formatter :
                new Formatter(['locale' => $this->locale]);
        }
        catch(Throwable $e) {
            static::showException($e);
            throw new Exception('Main configuration error', 500, $e);
        }
    }

    /**
     * Finds the named component.
     *
     * @param string $name
     *
     * @return Component|null -- returns null if not found
     * @throws UXAppException
     */
    public function __get($name) {
        if($this->hasComponent($name)) {
            return $this->components[$name];
        }
        return parent::__get($name);
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function hasComponent(string $name) {
        return isset($this->components[$name]) && (
                ($c = $this->components[$name]) instanceof Component ||
                is_array($c) && array_values($c)[0] instanceof Component
            );
    }

    /**
     * Returns a named component of the application.
     * If $name is already a Component, it's returned.
     * If $name is a configuration array, a new Component is created.
     *
     * Optionally, a default component name may be supplied.
     *
     * @param string|array|Component $name
     *
     * @param null $default
     *
     * @return Component|Module|null
     * @throws ReflectionException
     * @throws UXAppException
     */
    public function getComponent($name, $default = null) {
        if($name instanceof Component) return $name;
        if(is_string($name)) return ArrayUtils::getValue($this->components, $name, ArrayUtils::getValue($this->components, $default));
        if(is_array($name)) /** @noinspection PhpIncompatibleReturnTypeInspection */ return Component::create($name);
        if(is_null($name)) return ArrayUtils::getValue($this->components, $default);
        return null;
    }

    /**
     * Returns a cached value or computes it if not exists.
     * Uses given cahce object or default UXApp's configured one.
     *
     * @param string $key -- the name of the cached value
     * @param callable $compute -- the function retrieves the original value
     * @param int|null $ttl -- time to live in seconds (used in set only)
     * @param CacheInterface|null $cache -- the cache to use, default is UXApp::$app->cache
     *
     * @return mixed -- the cached value
     * @throws Exception
     */
    public static function cache(string $key, callable $compute, int $ttl = null, CacheInterface $cache = null) {
        if(!$cache) $cache = static::$app->getComponent('cache');
        if($cache) return $cache->cache($key, $compute, $ttl);
        UXApp::trace('No cache, computed');
        return $compute();
    }

    /**
     * Creates a URL relative to the baseurl
     * Returns null on empty input.
     *
     * @param string|array $to -- relative or abstract url
     * @param bool $absolute
     *
     * @return string|null
     * @throws UXAppException
     * @throws Exception
     * @see UrlHelper::createUrl()
     *
     */
    public function createUrl($to, bool $absolute = false) {
        if($to===null) return null;
        if(is_string($to)) $to = $this->urlFormatter->parseUrl($to);
        if(!is_array($to)) $to = [];
        try {
            return $this->urlFormatter->createUrl($to, $absolute);
        }
        catch(Throwable $e) {
            throw new UXAppException('Invalid URL data', $to, $e);
        }
    }

    /**
     * Application's root path
     *
     * @return string
     * @throws UXAppException
     */
    public function rootPath() {
        if(!$this->rootPath) throw new UXAppException('Config error: rootPath must be defined');
        return $this->rootPath;
    }

    /** @return true if debug is defined and is on */
    public static function debugIsOn() {
        return static::$app &&
            static::$app->hasComponent('debug') &&
            static::$app->debug instanceof DebugInterface &&
            static::$app->debug->isOn();
    }

    /**
     * Logs a trace level message and
     * Implicitly calls configured debug module if configured and swithed on.
     *
     * Options:
     *
     * - params ($vars to substitute into msg)
     * - depth (effective backtrace starting depth from trace call)
     * - tags (space spearated words, e.g. 'app', 'uxapp', 'sql')
     * - mt (microtime to display)
     * - color (message color)
     *
     * @param mixed $msg -- always converted to human-readable string
     * @param array $options
     */
    public static function trace($msg, array $options = ['tags' => 'app']) {
        if(!isset($options['depth'])) $options['depth'] = 4;
        if(!is_string($msg)) $msg = Util::objtostr($msg);
        if(static::$app) static::$app->debug($msg, $options);
    }

    /**
     * @param Exception $e
     * @param string[] $options
     */
    public static function traceException(Exception $e, array $options = ['tags' => 'app']) {
        if(!isset($options['depth'])) $options['depth'] = 4;
        if(static::$app) static::$app->debug(Util::backtraceException($e), $options);
    }

    /**
     * @param string $name
     * @param mixed $value
     * @param string|null $color
     *
     * @deprecated --- use UXApp::trace([$name, $value])
     */
    public static function tracex(string $name, $value, string $color=null) {
        static::trace([$name, $value], ['depth'=>4, 'tags' => 'app', 'color'=>$color]);
    }

    /**
     * Shows fatal error without autoloader
     *
     * @param mixed $code
     * @param string $subtitle
     * @param mixed|Exception $message
     * @param bool $trace -- force trace
     *
     * @return void
     */
    public static function fatal_error($code, string $subtitle = '', $message = '', bool $trace=false) {
        try {
            $title = static::getHttpResponseCode($code);
        }
        catch(Throwable $e) {
            $title = '';
        }
        if(!$title) $title = 'Fatal application error';
        if($message instanceof Throwable) {
            $e = $message;
            $message = $e->getMessage(). ' in '.$e->getFile(). ' on line '.$e->getLine();
            $trace = $e->getTraceAsString(); //isset($_SESSION['trace'])||$trace ? $e->getTraceAsString() : '';
        }
        if(in_array(php_sapi_name(), ["cli", 'cgi-fcgi'])) {
            echo "\nError $code $title \n";
            echo $subtitle, "\n";
            echo "$message \n";
            echo $trace;
        } else {
            $ex = '';
            header("Status: $code $title");
            try {
                if(static::$app && static::$app->hasComponent('assetManager')) {
                    $mainCss = static::$app->assetManager->single(['uhi67/uxapp', 'css/main.css']);
                }
                else $mainCss = '';
            }
            catch(Throwable $e) {
                $mainCss = '';
                $ex .= "AssetManager cannot load 'css/main.css'";
            }
            echo <<<EOT
<html lang="hu">
<head>
    <title>uxapp error</title>
    <link rel="stylesheet" type="text/css" href="$mainCss" />
</head>
<body>
    <div id="fatal">
        <div>$ex</div>
        <h2>$code $title</h2>
        <h3>$subtitle</h3>
        <pre>$message</pre>
        <pre>$trace</pre>
    </div>
</body>
</html>
EOT;
            $protocol = ($_SERVER['SERVER_PROTOCOL'] ?? 'HTTP/1.0');
            header($protocol . ' ' . $code . ' ' . $title);
        }
        try {
            if(UXApp::$app) {
                UXApp::$app->log(LogLevel::CRITICAL, $title . "\t" . $message, ['tags'=>'uxapp']);
            }
        }
        finally {
            exit($code);
        }
    }


    /**
     * Returns a http response code title in the given or default language
     *
     * @param int $code
     * @param string|null $lang
     *
     * @return string
     * @throws Exception
     */
    public static function getHttpResponseCode(int $code, string $lang=null) {
        if(!$lang && UXApp::$app) $lang = UXApp::$app->locale;
        if(!self::$_httpResponseCodes) {
            self::$_httpResponseCodes = Http::responseCodes();
        }

        // Find best language
        $la = $lang;
        if(!isset(self::$_httpResponseCodes[$la]) && $lang && strlen($lang)==5) {
            $la = substr($lang,0,2);
            if(!isset(self::$_httpResponseCodes[$la])) {
                $languages = array_keys(self::$_httpResponseCodes[$lang]);
                if(count($languages)==0) return static::la('uxapp', 'Missing http response definitions');
                $la = $languages[0];
            }
        }

        if(isset(self::$_httpResponseCodes[$la][$code])) return self::$_httpResponseCodes[$la][$code];
        return null;
    }

    /**
     * Starts the application
     *
     * @throws Exception
     */
    function run() {
        defined('ENV') or define('ENV', getenv('APPLICATION_ENV'));
        defined('ENV_DEV') or define('ENV_DEV', in_array(ENV, ['development', 'local']));
        if($this->isCLI) {
            return $this->runCli();
        }

        // web mode
        $page = $this->request->get('page');

        if($page == '') $page = $this->defaultPage;
        if(!headers_sent()) {
            header("HTTP/1.1 200 OK");
            header("X-Powered-By: uxapp framework");
        }

        if(file_exists($cv = $this->rootPath . '/version.php')) {
            $this->ver = trim(include($cv)); // maintain version of your application in this file
        }
        else if(file_exists($cv = $this->rootPath . '/version')) {
            $this->ver = trim(file_get_contents($cv)); // maintain version of your application in this file
        }

        // 1. module page
        $className = null;
        if(($moduleName = $this->request->req('module'))) {
            // 1.1 Internal module
            if(is_dir($path = $this->rootPath.'/modules/'.$moduleName)) {
                $className = 'app\\modules\\' . $moduleName. '\\';
            }
            // 2.2 Vendor module
            elseif(is_dir($path = UXApp::$vendorDir . '/' . $moduleName)) {
                $component = $this->findComponentByPath($path);
                $className = $component ? get_class($component) : '';
            }
            if(!$className) throw new NotFoundException("Module class at $path not found for `$moduleName`");

            $className = substr($className, 0, strrpos($className, '\\'));
            $pageclass = Util::camelize($page, true) . 'Page';
            if(class_exists($c = $className . '\\pages\\' . $pageclass)) $controller = $c;
            else throw new NotFoundException("Module controller class `$c` not found");

            // Running $controller class with $path parameters
            header("X-UXApp-Source: 2; $controller");
            $this->runPage($controller);
            $this->finish();
            return true;
        }

        // 2. application page controller
        $pageclass = Util::camelize($page, true) . 'Page';
        if(class_exists('app\pages\\' . $pageclass)) {
            if(!headers_sent()) header("X-UXApp-Source: 1; $page");
            $this->runPage('app\pages\\' . $pageclass);
            $this->finish();
            UXApp::$app = null;
            return true;
        }

        throw new NotFoundException("Module or page '$moduleName/$page' is not found (app\pages\\$pageclass)");
    }

    /**
     * Finds a registered component by $component->modulePath
     *
     * @param string $path
     *
     * @return Component|Module|null
     */
    public function findComponentByPath(string $path) {
        $path = str_replace('\\', '/', $path);
        foreach($this->components as $component) {
            if(is_array($component)) {
                /** @var Module $comp */
                foreach($component as $comp) {
                    if($comp instanceof Module && $comp->modulePath==$path) return $comp;
                }
            }
            else {
                $rootPath = $component instanceof Module ? str_replace('\\', '/', $component->modulePath) : '';
                if($component instanceof Module && $rootPath==$path) return $component;
            }
        }
        return null;
    }

    /**
     * Run application in command line
     *
     * @param bool $returnException
     * @return int -- command status, 0 = success
     * @throws NotFoundException
     * @throws Throwable
     * @throws UXAppException
     */
    public function runCli($returnException = false) {
        defined('ENV') or define('ENV', getenv('APPLICATION_ENV'));
        defined('ENV_DEV') or define('ENV_DEV', in_array(ENV, ['development', 'local']));
        $argv = $_SERVER['argv'];
        $command = $argv[1] ?? 'index';

        $commandClass = Util::camelize($command, true) . 'Command';

        try {
            if(class_exists($className = 'app\commands\\' . $commandClass) || class_exists($className = 'uhi67\uxapp\commands\\' . $commandClass)) {
                $result = $this->runPage($className, $returnException);
                if(is_string($result)) {
                    echo $result;
                    return UXApp::EXIT_STATUS_OK;
                }
                return $result;

            } else {
                echo "\nError: Class 'app\\commands\\$commandClass' not found\n";
                echo "Error: Class '$className' not found\n";
                return UXApp::EXIT_STATUS_NOT_FOUND;
            }
        }
        catch(Throwable $e) {
            if($returnException) throw $e;
            echo 'Error creating application object', PHP_EOL;
            if(ENV_DEV) static::showException($e);
            return self::EXIT_STATUS_CONFIG_ERROR;
        }
    }

    /**
     * Create and start a controller (WEB and CLI)
     *
     * @param UXAppPage|string $pageclass -- classname to use, must be an UAppPage
     * @param bool $returnException -- re-throws internal exceptions
     * @return int|string -- exit status or result string to the output
     * @throws NotFoundException
     * @throws Throwable
     * @throws UXAppException
     */
    function runPage($pageclass, $returnException=false) {
        $status = self::EXIT_STATUS_EXCEPTION;
        try {
            if(!class_exists($pageclass)) throw new NotFoundException("Page class `$pageclass` not found");
            $this->page = new $pageclass(['app' => $this]);
            $status = $this->page->go($returnException);
            if($status===null) throw new UXAppException("Controller '$pageclass' returned null");
            $this->page->finish();
            if($this->hasComponent('debug') && $this->debug) $this->debug->finish();
        }
            /** @noinspection PhpRedundantCatchClauseInspection */
        catch(HandledException $e) { // Codeception miatt kell
            // nothing to do
        }
        catch(Throwable $e) {
            if($returnException) throw $e;
            static::showException($e);
            return (int)$e->getCode() ?: self::EXIT_STATUS_EXCEPTION;
        }
        return $status;
    }

    /**
     * @throws
     */
    function finish() {
        if($this->debug) UXApp::trace('Finishing UXApp');
        // Finish components (in reverse definition order)
        foreach(array_reverse($this->components, true) as $component) {
            if(is_callable([$component, 'finish'])) $component->finish();
        }
    }

    /**
     * Displays an Exception on CLI or pure HTML output.
     *
     * @param Throwable $e
     * @param bool $d -- show details
     * @param string|null $title
     */
    static public function showException(Throwable $e, bool $d = true, $title=null) {
        if(!$title) $title = 'UXapp framework internal error';
        $status = $e instanceof UXAppException ? $e->statusCode : 500;
        try {
            $statusName = Http::responseTitle($status);
        }
        catch(Throwable $e1) {
            echo "An error occurred during handling exception: " . $e1->getMessage();
            $statusName = 'Internal Error';
        }
        if(!headers_sent()) {
            header("Content-Type: text/html; charset=UTF-8");
            header("HTTP/1.0 $status $statusName");
        }
        $debug_on = UXApp::debugIsOn();
        if(UXApp::$app && UXApp::$app->isCLI || !UXApp::$app && php_sapi_name()=='cli') {
            $msg = $title . ': '. $e->getMessage();
            echo Ansi::color("$msg\n", 'light red');
            echo 'in ' . $e->getFile() . ' on line ' . $e->getLine() . "\n";
            if($e instanceof UXAppException) {
                $info = $e->getExtra();
                if(!is_string($info)) $info = Util::objtostr($info);
                echo Ansi::color("$info\n", 'red');
            }
            if(true) {
                while($e) {
                    echo Ansi::color("\n" . $e->getMessage(), 'light purple') . "\n";
                    foreach($e->getTrace() as $i=>$t) {
                        $args = isset($t['args']) ? implode(', ', array_map(function($v) { return Util::objtostr($v, true, true, 0); }, $t['args'])) : '';
                        $class = isset($t['class']) ? $t['class'].$t['type'] : '';
                        $function = $t['function'] ?? '';
                        $file = isset($t['file']) ? 'in '.$t['file'] : '';
                        $line = isset($t['line']) ? 'on line '.$t['line'] : '';
                        if(preg_match('~\\\\uxapp\\\\~', $class)) echo Ansi::color("#$i $class$function($args) ", 'red');
                        else if(preg_match('~uhi67\\\\~', $class)) echo Ansi::color("#$i $class$function($args) ", 'green');
                        else echo "#$i $class$function($args) ";
                        echo "$file $line\n";
                    }
                    echo "\n";
                    $e = $e->getPrevious();
                }
            }
            return;
        }
        if($d && $debug_on) static::$app->debug->trace(['trace' => $e->getTrace()]);
        echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">', PHP_EOL;
        echo '<html lang="hu"><head><title>'.$title.'</title>', PHP_EOL;
        echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
        echo '<style>
            div.trace {font-family: "Consolas", courier, monospace; font-size: .9rem;}
            span.uxapp {color: #9a1c1c;}
            span.uxapp-lib {color: #831c9a;}
        </style>';
        echo '</head><body>', PHP_EOL;
        echo '<h1>'.$title.'</h1>', PHP_EOL;
        $details = sprintf(" in file '%s' at line '%d'", $e->getFile(), $e->getLine());
        /** @noinspection HtmlPresentationalElement */
        echo '<div><b>' . htmlspecialchars($e->getMessage()) . '</b>' . $details . '</div>';
        $extra = $e instanceof UXAppException ? $e->getExtra() : '';
        if($extra) {
            echo '<pre style="white-space: break-spaces">' . htmlspecialchars($extra) . '</pre>';
        }

        $trace = null;
        if($d || $debug_on || static::$app && in_array(static::$app->environment, ['development', 'local', 'dev', 'develop', 'test']) || $trace == 'on') {
            echo self::renderTraceColor($e);
            while(($e = $e->getPrevious())) {
                echo "<h2>Previous Exception</h2>\n";
                $details = sprintf(" in file '%s' at line '%d'", $e->getFile(), $e->getLine());
                /** @noinspection HtmlPresentationalElement */
                echo '<div><b>' . htmlspecialchars($e->getMessage()) . '</b>' . $details . '</div>';
                echo '<pre>' . htmlspecialchars($extra) . '</pre>';
                echo self::renderTraceColor($e);
            }
            echo '</pre>';
        }
        echo '</body>';
    }

        public static function renderTraceColor($e) {
            $result = '';
            $pattern1 = /** @lang RegExp */"~([\\\\/]vendor[\\\\/]uhi67[\\\\/])(uxapp[\\\\/][\w.-]+([\\\\/][\w-]+)*\\.php)\((\d+)\):~";
            $pattern2 = "~([\\\\/]vendor[\\\\/]uhi67[\\\\/])(uxapp-[\w.-]+[\\\\/][\w.-]+([\\\\/][\w-]+)*\\.php)\((\d+)\):~";
            $pattern3 = "~\((\d+)\):~";
            $trace = explode("\n", htmlspecialchars($e->getTraceAsString()));
            foreach($trace as $tr) {
                $tr = preg_replace($pattern1, '$1<span class="uxapp">$2 (</span><b>$4</b>):', $tr);
                $tr = preg_replace($pattern2, '$1<span class="uxapp-lib">$2 (</span><b>$4</b>):', $tr);
                $tr = preg_replace($pattern3, ' (<b>$1</b>):', $tr);
                $tr = preg_replace('~([\w-]+)\.php~', '<b>$1</b>.php:', $tr);
                $tr = preg_replace('~([\w-]+)\(~', '<b>$1</b>(:', $tr);
                $result .= "<div class='trace'>$tr</div>\n";
            }
            return $result;
        }

        /**
     * Translates a text using configured localization class.
     *
     * Category syntax
     * - uxapp -- framework texts, located in the /vendor/uhi67/uxapp/def/translations dir
     * - avendor/alib -- library texts, located in the /vendor/avendor/alib/def/translations dir
     * - avendor/alib/acat -- library category, located in the /vendor/avendor/alib/def/translations/acat dir
     * - any/other -- application categories, depending on current translator (e.g. located in the /def/translations/any/other dir of the application)
     * - app -- the default, if none specified; depending on current translator (e.g. located in the /def/translations/app dir of the application)
     *
     * @param string $category -- message category, UXApp uses 'uxapp'. Application default is 'app'
     * @param string $text -- message or textid to translate
     * @param array $params -- parameters to substitute into translated text at {$var} positions
     * @param string|null $locale -- language (ll)or locale (ll-LL) code to translate into.
     *
     * @return string -- the translated text with parameters substituted
     * @throws UXAppException
     */
    public static function la(string $category, string $text, array $params = [], string $locale = null) {
        if(!static::$app) throw new UXAppException('Application is not initialized');
        if(!static::$app->hasComponent('l10n')) {
            if($params) $text = Util::substitute($text, $params);
            return $text;
        }
        if(!$locale) $locale = static::$app->locale;

        /** @var L10nBase $l10n */
        if(UXApp::$app) $l10n = UXApp::$app->l10n; else $l10n = L10nBase::class;
        return $l10n->getText($category, $text, $params, $locale);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Formats a datetime using configured localization class
     *
     * - SHORT date is 2020.05.31 or 31/05/20
     * - SHORT time is 13:59 or 1:59pm
     *
     * @param DateTime $dt
     * @param int $datetype -- date format as IntlDateFormatter::NONE, type values are 'NONE', 'SHORT', 'MEDIUM', 'LONG', 'FULL'. Default is SHORT.
     * @param int $timetype -- time format as IntlDateFormatter::NONE, type values are 'NONE', 'SHORT', 'MEDIUM', 'LONG', 'FULL'. Default is NONE.
     * @param string|null $locale
     *
     * @return string -- the formatted date
     * @throws UXAppException
     */
    public static function df(DateTime $dt, int $datetype = IntlDateFormatter::SHORT, int $timetype = IntlDateFormatter::NONE, string $locale = null) {
        $dt = Util::createDateTime($dt);
        if(!$locale) $locale = UXApp::$app->locale;
        /** @var L10nBase $l10n */
        if(UXApp::$app && UXApp::$app->hasComponent('l10n') && UXApp::$app->l10n) {
            $l10n = UXApp::$app->l10n;
        }
        else {
            $l10n = new L10nBase(['locale'=>$locale]);
        }
        return $l10n->formatDateTime($dt, $datetype, $timetype, $locale);
    }

    /**
     * Gets current locale if known (example: "en-GB")
     *
     * @return string -- locale in ll-cc format (ISO 639-1 && ISO 3166-1) or empty string
     * @throws UXAppException
     */
    public function getLocale() {
        if(!$this->hasComponent('l10n')) return $this->_locale;
        Assertions::assertClass($this->l10n, L10nBase::class);
        return $this->l10n->locale;
    }

    /** @noinspection PhpUnused */

    /**
     * Gets current language if known (example: "en")
     *
     * @return string -- locale in ll format (ISO 639-1)
     * @throws UXAppException
     */
    public function getLang() {
        if(!$this->hasComponent('l10n')) return $this->_locale ? substr($this->_locale, 0, 2) : '';
        Assertions::assertClass($this->l10n, L10nBase::class);
        return $this->l10n->lang;
    }

    /**
     * @throws Exception
     */
    public function setLocale($locale) {
        if(!$this->hasComponent('l10n')) return $this->_locale = $locale;
        return $this->l10n->setUserLocale($locale);
    }

    /**
     * Stores command line arguments
     * - named values to args,
     * - other values to argn
     *
     * @param int $argc
     * @param array $argv
     *
     * @return void
     */
    function parseArgs(int $argc, array $argv) {
        $this->args = []; // Named argument option values name=>value
        $this->argn = []; // Not named values

        // Parsing args
        for($i = 2; $i < $argc; $i++) {
            if(preg_match('/^(--)?(\w+)=(.*)$/', $argv[$i], $m)) {
                $this->args[$m[2]] = $m[3];
            } else $this->argn[] = $argv[$i];
        }
    }

    /**
     * Returns a value from configuration parameters array
     *
     * @param string $key
     * @param mixed|null $default
     *
     * @return mixed
     * @throws UXAppException
     */
    public function getParam(string $key, $default = null) {
        return ArrayUtils::getConfigValue(UXApp::$app->params, $key, $default);
    }

    /**
     * Logs an application event using configured loggers.
     * The main config may contain a component named 'logger',
     * PSR-3 compatible (see {@see \Psr\Log\LoggerInterface})
     *
     * Context rules
     * - user: Includes user_id if exists;
     * - timestamp: default is auto-generated
     * - ip: default is auto-generated
     * - tags: Default tags is 'app'. Multiple tags may be specified.
     *
     * If debugger is configured, all message sent to debugger too.
     *
     * @param string $level -- [emergency, alert, critical, error, warning, notice, info, debug]
     * @param string $message -- may contain {name} placeholders
     * @param array $context -- (user, page, action, target, tags, timestamp, ip)
     */
    public function log($level, $message, array $context = []) {
        // Default context values
        if(!isset($context['timestamp'])) $context['timestamp'] = date(DATE_ATOM);
        if(!isset($context['user'])) {
            // Determine current user
            if($this->isCLI) {
                $_user_id = 'cli';
            } else {
                if(UXApp::$app && UXApp::$app->hasComponent('user')) {
                    $user = UXApp::$app->user;
                }
                else {
                    $user = null;
                }
                // User may not be created yet
                $_user_id = ($user instanceof BaseUser) ? $user->id : null; //$this->app->session->get('user_id');
            }
            $context['user'] = $_user_id;
        }
        if(!isset($context['ip'])) $context['ip'] = $this->ip;

        // Loggers, including debugger
        if($this->hasComponent('logger') && $this->logger) {
            $loggers = is_array($this->logger) ? $this->logger : [$this->logger];
            foreach($loggers as $i => $logger) {
                if(!($logger instanceof LoggerInterface)) {
                    var_dump($message);
                    var_dump($context);
                    $e = new UXAppException("Invalid logger $i");
                    UXApp::showException($e);
                    exit;
                }
                $logger->log($level, $message, $context);
            }
        }

        // Debug log
        if(static::debugIsOn()) {
            $debugContext = $context;
            $debugContext['level'] = $level;
            if($message instanceof DOMElement || $message instanceof DOMDocument) {
                static::$app->debug->trace_xml($message, $debugContext);
            } else static::$app->debug->log($level, $message, $debugContext);
        }
    }

    /** @noinspection PhpMethodNamingConventionInspection */
    /** @noinspection PhpUnused */

    /** @noinspection PhpUnused */
    public function getDb() {
        return ArrayUtils::getValue($this->components, 'database');
    }

    public static function mimeTypes() {
        return [
            'css' => 'text/css',
            'ico' => 'image/x-icon',
            'jpg' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'js' => 'text/javascript',
            'gif' => 'image/gif',
            'map' => 'application/octet-stream',
            'mp3' => 'audio/mpeg',
            'png' => 'image/png',
            'svg' => 'image/svg+xml',
            'tif' => 'image/tiff',
            'tiff' => 'image/tiff',
            'ttf' => 'font/ttf',
            'xsl' => 'text/xsl',
            'woff' => 'font/woff',
            'woff2' => 'font/woff2',
        ];
    }

    /**
     * @param string $ext -- extension without . or filename.ext
     * @return string -- the mimne type on null if not found
     */
    public static function mimetype(string $ext) {
        if(($p=strrpos($ext, '.'))!==false) $ext = substr($ext, $p+1);
        return ArrayUtils::getValue(static::mimeTypes(), $ext);
    }

    /**
     * @param array|null $config -- Application main config
     *
     * @return bool -- success
     */
    static public function go(array $config=null) {
        defined('ENV') or define('ENV', getenv('APPLICATION_ENV'));
        defined('ENV_DEV') or define('ENV_DEV', in_array(ENV, ['development', 'local']));
        try {
            if(!$config) {
                $dir = dirname(__DIR__, 4);
                $config = file_exists($dir.'/config/command.php') ? require $dir.'/config/command.php' : [UXApp::class];
            }
            /** @var UXApp $app */
            $app = Component::create($config);
            $result = $app->run();
            UXApp::$app = null;
            return $result;
        }
        catch(Throwable $e) {
            static::showException($e, true, 'Configuration error');
            return false;
        }
    }

    /**
     * Serves an asset file if request contains 'asset' query parameter
     *
     * Returns true if asset file is served and output is ready, false otherwise.
     *
     * @param $config -- main config (path definitions)
     *
     * @return bool -- true if asset is served
     * @throws NotFoundException
     * @deprecated - use AssetManager component to cache assets
     */
    public static function serveAsset($config) {
        if(!isset($_SERVER['REQUEST_METHOD']) || $_SERVER['REQUEST_METHOD']!='GET') return false;
        $vendorPath = static::$vendorDir;
        if(isset($_GET['asset'])) {
            // Canonical URL
            $path = $_GET['asset'];
            $path = explode('/', $path);
        }
        else {
            // Compressed URL
            $path = isset($_SERVER['PATH_INFO']) ? trim($_SERVER['PATH_INFO'], '/') : '';
            // PATH_INFO not always available
            if(!$path) $path = isset($_SERVER['REQUEST_URI']) ? trim($_SERVER['REQUEST_URI'], '/') : '';
            // query leválasztása
            $path = parse_url($path, PHP_URL_PATH);
            $path = explode('/', $path);
            if(array_shift($path) !== 'asset') return false;
        }
        $last = $path[count($path)-1];
        $ext = Util::ext($last);

        $msg = '';
        if($mimeType = static::mimeType($ext)) {
            $environment = $_SERVER['APPLICATION_ENV'] ?? 'production';
            header("Content-type: $mimeType");
            if($environment == 'production') {
                header("Cache-Control: max-age=25920");
            } else {
                header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
                header("Pragma: no-cache"); //HTTP 1.0
                header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
            }

            $rootpath = ArrayUtils::getValue($config, 'rootPath', dirname(__DIR__, 4));

            $file = '';
            $t='';

            // Application xsl
            if(($moduleName = array_shift($path))=='xsl') {
                $appxslpath = ArrayUtils::getValue($config, 'xslpath', $rootpath.'/xsl');
                $file = $appxslpath . '/' . implode('/', $path);
                $t=3;
            }
            // Generated xsl
            else if($moduleName=='_') {
                $dataPath = ArrayUtils::getValue($config, 'dataPath', $rootpath.'/runtime');
                $runtimeXslPath = ArrayUtils::getValue($config, 'runtimeXslPath', $dataPath . '/xsl/');
                $file = $runtimeXslPath . '/' . implode('/', $path);
                $t=2;
            }
            // Internal module
            else if(is_dir($module_dir = $rootpath . '/modules/' . $moduleName)) {
                if($ext!='xsl' && is_dir($module_dir . '/www')) {
                    $file = $module_dir . '/www/' . implode('/', $path);
                    $t=4;
                }
                else {
                    $file = $module_dir .'/'. implode('/', $path);
                    $t=5;
                }
            }
            // vendor module
            else if(is_dir($vendorPath . '/' . $moduleName)) {
                $moduleName .= '/' . array_shift($path);
                $module_dir = $vendorPath . '/'. $moduleName;
                if($ext!='xsl' && is_dir($module_dir . '/www')) {
                    $file = $module_dir . '/www/' . implode('/', $path);
                    $t=0;
                }
                else {
                    $file = $module_dir . '/' . implode('/', $path);
                    $t=1;
                }
            }
            else {
                $msg = "The module `$moduleName` does not exist";
            }

            if(file_exists($file)) readfile($file);
            else {
                header("Content-type: text/html");
                header("HTTP/1.1 404 Not found");
                if(!$msg) $msg = "The asset file '".implode('/', $path)."' is not found at `$file`. [$t]";
                throw new NotFoundException($msg);
            }
            return true;
        }
        return false;
    }

    public function getNode() {
        return ($this->page && $this->page->view) ? $this->page->view->node : null;
    }

    function getModulePath() {
        return dirname(__DIR__);
    }

    /**
     * Formats data using the configured or default formatter
     *
     * @param mixed $value
     * @param string $format -- format name supported by the formatter
     *
     * @return mixed
     */
    public function format($value, string $format) {
        return call_user_func([$this->formatter, 'format'], $value, $format);
    }

    /**
     * Returns a relative path of the file to app's root path if the file is under that.
     * If filePath is not an absolute path under the root, returns unchanged.
     *
     * @param string $filePath -- an absolute file path.
     *
     * @return string
     */
    public function relativePath(string $filePath) {
        return str_replace(str_replace('\\', '/', $this->rootPath), '', str_replace('\\', '/', $filePath));
    }

    public function getIsCLI() {
        return in_array($this->sapi, ["cli", 'cgi-fcgi', 'cli-server', 'fpm-fcgi', 'phpdbg']);
    }

    /**
     * Runs a CLI command without prepared UXApp environment.
     * Used in codecept initialization.
     *
     * @param array $config
     * @param array|string $argv CLI args (including the uxapp command itself) or space-separated command line string
     * @return UXApp -- the created instance
     * @throws NotFoundException
     * @throws ReflectionException
     * @throws Throwable -- all internal exceptions of the command are reflected without printed message or backtrace
     * @throws UXAppException
     */
    public static function runCommand($config, $argv) {
        if(is_string($argv)) $argv = explode(' ', $argv);
        $_SERVER['argv'] = $argv;
        $_SERVER['argc'] = count($_SERVER['argv']);
        $app = Component::create($config);
        /** @var UXApp $app */
        $app->runCli(true);
        return $app;
    }
}
