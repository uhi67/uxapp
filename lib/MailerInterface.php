<?php
namespace uhi67\uxapp;

interface MailerInterface {
	/**
	 * Sends a mail to the adress
	 * Must return true on success
	 *
	 * @param string $address
	 * @param string $subject
	 * @param string $message
	 * @param string $headers
	 * @return bool
	 */
	public function send($address, $subject, $message, $headers='');
}
