<?php /** @noinspection PhpIllegalPsrClassPathInspection */

namespace uhi67\uxapp;

/**
 * Class CompressedUrlFormatter
 *
 * Creates url from abstract url data and parses url to abstract structure.
 * Implements basic compression rules.
 *
 * If compression is disabled, falls back to SimpleUrl rules (no compression):
 *
 * - ['page', 'act', 'id'=>23, 'p2'=>47] ==> '/index.php?page=page&act=act&id=23&p2=47'
 *
 * If compression is enabled:
 *
 * - ['page', 'act', 'id'=>23, 'p2'=>47] ==> '/page/act/23?p2=47'
 * - ['module'=>mod, 'page'=>'p', 'id'=>17] ==> '/module/mod/p/17'
 *
 * @package app\vendor\uhi67\uxapp\lib
 */
class SimpleUrlFormatter extends BaseUrlFormatter {
    /** @var string $baseUrl -- absolute base url, without dispatcher script, example: 'http://app.test' */
    public $baseUrl=null;
    /** @var string $script -- dispatcher script relative to baseUrl, example: 'index.php' -- used only if url is not compressed */
    public $script;
    /** @var bool $compress -- enables url compression */
    public $compress;


    public function prepare() {
        $baseUrl = $this->baseUrl; // Save baseUrl, because parent assigns a wrong value for this class.
        parent::prepare();
        $this->baseUrl = $baseUrl;
        if($this->script===null) $this->script = '/index.php';
        if($this->baseUrl===null) $this->baseUrl = UXApp::$app->baseurl;
    }

    /**
     * Creates url from abstract url array. If enabled, creates compressed url
     *
     * - ['page', 'act', 'id'=>23, 'p2'=>47] ==> '/page/act/23?p2=47'
     * - ['module'=>mod, 'page'=>'p', 'id'=>17, '#'=>fr] ==> '/module/mod/p/17#fr'
     *
     * @param array $to -- 0=>page name and other query parameters
     * @param bool $absolute -- return absolute url (http://app.test/page/act/id?query
     *
     * @return string
     * @throws UXAppException
     */
    public function createUrl($to, bool $absolute=false) : string {
        if(!$this->compress) return parent::createUrl($to, $absolute);
        $to = $this->canonize($to);
        $url = '';
        $fragment = ArrayUtils::fetchValue($to, '#');
        if(isset($to['module'])) $url .= '/module/' . ArrayUtils::fetchValue($to, 'module');
        if(isset($to['page'])) $url .= '/' . ArrayUtils::fetchValue($to, 'page');
        if(isset($to['act'])) $url .= '/' . ArrayUtils::fetchValue($to, 'act');
        if(isset($to['id'])) $url .= '/' . ArrayUtils::fetchValue($to, 'id');
        $path = ArrayUtils::fetchValue($to, 'path');
        if($path) {
            if(is_array($path)) $path = implode('/', $path);
            $url .= '/' . $path;
        }
        if($to) $url .= '?' . http_build_query($to);
        if($fragment) $url .= '#' . $fragment;
        return $url;
    }

    public function absoluteUrl($url) : string {
        if(is_array($url)) $url = $this->createUrl($url);
        $path = parse_url($url, PHP_URL_PATH);
        $query = parse_url($url, PHP_URL_QUERY);
        $fragment = parse_url($url, PHP_URL_FRAGMENT);
        if($path && substr($path,0,1)!=='/') $path = '/'.$path;
        return $this->baseUrl.$path.'?'.$query . ($fragment ? '#'.$fragment : '');
    }
}
