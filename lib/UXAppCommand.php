<?php
/** @noinspection PhpIllegalPsrClassPathInspection */

namespace uhi67\uxapp;

use Exception;
use Throwable;

/**
 * UAppCommand
 *
 * Command line command base class (counterpart of UAppPage for html pages)
 * Override it in application level in AppCommand
 * All command classes in commands dir must extend AppCommand instead of this
 *
 * Using:
 *
 * Command line: $ php uxapp controller1 action1 parameters
 *   - runs actAction1 method in Controller1 class with unnamed parameters as arguments.
 * Default action is `default`
 * first numeric parameter is accessable as $this->id
 * all named parameters (passed in `name=value` form) are accessable as $this->param($name).
 * all non-named parameters are accessable via $params array which is passed as the first argument of the action method.
 *
 * Action method descriptions must be peresent in {@see descriptions()} and {@see params()}
 *
 * @author uhi
 * @copyright 2017
 * @package UXApp
 */
abstract class UXAppCommand extends Controller {
	public $user;
	public $h1;
	public $appname;
	/** @var bool the command is started interactively. */
	public $interactive = true;

	protected $title;
	protected $act = '';        // action word
	protected $id;
	protected $name;        // Page name for logging, etc.
	protected $path;

	private $logged;        // Naplózott művelet

	private static $colors = [
		'ok' => 'light green',
		'warning' => 'yellow',
		'error' => 'light red',
		'info' => 'light blue',
		'default' => 'light gray',
	];

	public function prepare() {
		parent::prepare();
		$this->logged = [];
		$this->appname = $this->app->moduleName;
		$this->user = null;

		$pagename = strtolower(preg_replace('/(\w+)Command/', '\1', get_class($this)));
		if(!$this->name) $this->setName($pagename);

		$app = UXApp::$app;
		if(isset($app->args['act'])) $this->act = $app->args['act'];
		else if(isset($app->argn[0]) && !is_numeric($app->argn[0])) $this->act = array_shift($app->argn);
		if($this->act == '') $this->act = 'default';

		if(isset($app->args['id'])) $this->id = $app->args['id'];
		else if(isset($app->argn[0]) && is_numeric($app->argn[0])) $this->id = array_shift($app->argn);

		$this->path = $app->argn;
	}

	/**
	 * Displays an exception in command line.
	 * Uses ANSI colors.
	 *
	 * @param Throwable|UXAppException $e
	 * @param string                   $title
	 *
	 * @return void
	 * @throws
	 */
	function showException(Throwable $e, string $title) {
		$msg = $e->getMessage();
		$subtitle = 'Váratlan belső hiba a parancs futtatásakor';
		$showtrace = true;

		if($e instanceof UXAppException) {
			$subtitle = $e->getSubtitle();
		}

		if($title) echo Ansi::color($title, 'light red', 'black'), "\n";
		if($subtitle) echo Ansi::color($subtitle, 'light red', 'black'), "\n";

		if(($e instanceof UXAppException) && ($extra = $e->getExtra())) {
			echo Ansi::color($extra, 'purple'), "\n";
			$this->addMessages($msg, ['class' => 'error main'], $extra);
		}

		/** @noinspection PhpConditionAlreadyCheckedInspection */
		if($showtrace) {
			echo Ansi::color("Részletek", 'light blue'), "\n";
			#echo Ansi::color('trace is '. Debug::$trace."\n", 'purple');
			if(($e instanceof UXAppException) && $e->extra) {
				if(is_array($e->extra)) array_walk($e->extra, function($v) {
					echo Ansi::color($v, 'green');
				}, $this);
				else echo Ansi::color(Util::objtostr($e->extra, false, true, 3), 'green');
			}
			$info = $e->getFile() . '(' . $e->getLine() . '): ' . $msg;
			$error = error_get_last();
			if($error) {
				echo Ansi::color(sprintf("[%s] %s(%d): %s", Util::friendlyErrorType($error['type']), $error['file'], $error['line'], $error['message']), null, 'gray'), "\n";
			}
			echo Ansi::color($info, 'light red'), "\n";
			$ee = $e;
			while(($ee = $ee->getPrevious())) {
				$info = get_class($ee) . ' in ' . $ee->getFile() . '(' . $ee->getLine() . '): ' . $ee->getMessage();
				echo Ansi::color('-- ' . $info, 'red', 'gray'), "\n";
			}
			$this->log($info, 'error');

			echo Ansi::color($e->getTraceAsString(), 'brown'), "\n";
			/** @var Exception|UXAppException $ee */
			$ee = $e;
			while(($ee = $ee->getPrevious())) {
				if(($ee instanceof UXAppException) && ($extra = $ee->getExtra())) echo Ansi::color($extra, 'purple'), "\n";
				$info = get_class($ee) . ' in ' . $ee->getFile() . '(' . $ee->getLine() . '): ' . $ee->getMessage();
				echo Ansi::color('-- Previous: ' . $info, 'red', 'gray'), "\n";
				echo Ansi::color($ee->getTraceAsString(), 'brown'), "\n";
			}

		}
	}

	function __destruct() {
		try {
			unset($this->user);
			#parent::__destruct();
		}
		catch(Throwable $e) {
			echo "Exception at " . __CLASS__ . " destruct: " . $e->getMessage(), "\n";
		}
	}

	/** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Execute the command defined in this controller instance
     *
     * @param bool $returnException
     * @return int
     * @throws Throwable
     */
	function go($returnException=false) {
		$result = UXApp::EXIT_STATUS_EXCEPTION;
		try {
			$result = $this->processActions(Util::camelize($this->act));
		}
		catch(Throwable $e) {
            if($returnException) throw $e;
			echo Ansi::color('Exception at UAppCommand::go', 'brown') . "\n";
			/** @var UXAppException $e */
			$title = 'Unexpected internal error / Váratlan hiba a művelet feldolgozása során';
			echo $title, PHP_EOL, $e->getMessage(), PHP_EOL;
			try {
				$this->showException($e, $title);
				echo "-----", $e->getMessage(), $e->getFile(), $e->getLine(), PHP_EOL;
			}
			catch(Throwable $e) {
				echo "Another error at shpowException: ", $e->getMessage(), $e->getFile(), $e->getLine(), PHP_EOL;
			}
		}
		$this->log(); // Végül mindenképpen logoljuk paraméterek nélkül, ha ezzel az akcióval még nem

		return $result;
	}

	/**
	 * @param $act
	 *
	 * @return int
	 */
	function processActions($act) {
		if($act == '' && is_callable([$this, $func = 'actDefault'])) {
			return call_user_func([$this, $func]);
		}
		$func = 'act' . ucfirst($act);
		if(is_callable([$this, $func])) {
			$params = $this->app->argn; //$GLOBALS['argv'];
			return call_user_func([$this, $func], $params);
		}
		$this->addMessages('Ismeretlen művelet: ' . $act, ['class' => 'error']);
		return UXApp::EXIT_STATUS_NOT_FOUND;
	}

	/**
	 * Gets a named command-line parameter
	 * in form of name=value
	 *
	 * @param string $name
	 * @param mixed  $default
	 *
	 * @return string -- null if parameter does not exist
	 */
	public function param(string $name, $default = null) {
		return $this->app->args[$name] ?? $default;
	}

	/**
	 * Sets the name of the page
	 * Used at logging, default view, etc.
	 *
	 * @param string $name
	 *
	 * @return string
	 */
	protected function setName(string $name) {
		return $this->name = $name;
	}

	/**
	 * adds one or more msg node to control
	 *
	 * @param mixed      $msg -- message or message array
	 * @param array|null $attr -- message attributes pl. array('class'=>'error')
	 * @param mixed      $extra
	 */
	function addMessages($msg, array $attr = null, $extra = null) {
		$types = explode(' ', ArrayUtils::getValue($attr, 'class', ''));
		$type = '';
		foreach($types as $t) {
			if(in_array($t, array_keys(self::$colors))) $type = $t;
		}
		if(is_array($msg)) {
			for($i = 0; $i < count($msg); $i++) {
				echo $type ? Ansi::color($type . ': ', self::msgcolor($types)) : '', $msg[$i], "\n";
			}
		}
		echo $type ? Ansi::color($type . ': ', self::msgcolor($type)) : '', $msg, "\n";
		if($extra) echo Util::objtostr($extra), "\n";
	}

	public static function msgcolor($msgtype) {
		if(is_array($msgtype)) {
			$color = '';
			foreach($msgtype as $t)
				$color .= ArrayUtils::getValue(self::$colors, $t, self::$colors['default']);
		} else
			$color = ArrayUtils::getValue(self::$colors, $msgtype, self::$colors['default']);
		return $color;
	}

	/**
	 * Tevékenységnapló fájlba
	 * Ha az adott action már naplózva lett, paraméter nélkül mégegyszer nem kell.
	 *
	 * @param string      $params
	 * @param string|null $action -- ha meg van adva, felülírja a page->act értékét
	 *
	 * @return void
	 * @throws
	 */
	function log(string $params = '', string $action = null) {
		UXApp::trace(['log ' . $action => $params]);
		$page = $this->name ?: get_class($this);
		$action = $action ?: $this->act;
		if(isset($this->logged[$action]) && !$params) return;
		$paramx = is_array($params) ? implode("\t", $params) : $params;
		$this->logged[$action] = true;
		$this->app->log('info', "$page\t$action\t$paramx");
	}

	/**
	 * Megjelenít egy üzenetlapot egy vagy több megjelenítendő üzenetsorral
	 *
	 * @param string       $title -- page title (not used here)
	 * @param string       $h1 -- main title
	 * @param string       $h2 -- secondary title
	 * @param string|array $msg -- message or messages
	 * @param string       $url -- not used here
	 * @param array        $attr -- message attributes, e.g. class (not used)
	 *
	 * @return void
	 */
	function showMessage($title, $h1, $h2, $msg, $url = null, $attr = null) {
		echo Ansi::color($msg, 'yellow'), "\n";
		$this->addMessages($msg, $attr);
		if($h1 != null) $this->setH1($h1);
		if($h2 != null) $this->setH2($h2);
	}

	function setH1($h2, /** @noinspection PhpUnusedParameterInspection */
				   $h2l = null) {
		echo Ansi::color("\n$h2", 'blue', 'white'), "\n";
	}

	function setH2($h2, /** @noinspection PhpUnusedParameterInspection */
				   $h2l = null) {
		echo Ansi::color("\n$h2", 'light blue', 'white'), "\n";
	}

	public function confirm($message, $default = false) {
		if($this->interactive) {
			return Console::confirm($message, $default);
		}
		return true;
	}

	public function prompt($text, $options = []) {
		if($this->interactive) {
			return Console::prompt($text, $options);
		}

		return $options['default'] ?? '';
	}

	/**
	 * Must return action decriptions
	 * @return string[]
	 */
	public static function descriptions() {
		return [];
	}

	/**
	 * Must return descriptions of action parameters.
	 *    - Numeric index: unnamed parameters. First whitespace seöparates name and description (mandatory)
	 *    - string index: named parameters (var=value form).
	 * [] may indicate optional parameters.
	 *
	 * @return array
	 */
	public static function params() {
		return [];
	}

	/**
	 * Default default action lists action names and descriptions
	 */
	public function actDefault() {
		$descriptions = $this->descriptions();
		$params = $this->params();

		echo "Available actions:\n";

		$methods = get_class_methods(static::class);
		foreach($methods as $method) {
			if(preg_match('/^act([A-Z]\w+)/', $method, $m) && !in_array($m[1], ['Default', 'Descriptions'])) {
				$action = Util::uncamelize($m[1], '-');
				$descr = ArrayUtils::getValue($descriptions, $action);
				$actionParams = ArrayUtils::getValue($params, $action);
				if($actionParams === null) $actionParams = [];
				if(!is_array($actionParams)) throw new UXAppException("params for action `$action` must be an array");

				// Paraméterösszefoglaló gyűjtése
				$pars = '';
				foreach($actionParams as $name => $item) {
					if(is_int($name)) {
						$dd = explode(' ', $item, 2);
						$pars .=  $dd[0];
					}
				}

				echo Ansi::color("- $action", 'green') . ' ' . Ansi::color($pars, 'blue') . "\n\t$descr\n";
				if($actionParams) {
					echo "\n";
					$n = 0;
					foreach($actionParams as $name => $actionParam) {
						if(is_int($name)) {
							$parts = explode(' ', $actionParam, 2);
							$par = $parts[0];
							$descr = $parts[1];
							echo Ansi::color("\t- " . sprintf('%-12s', $par), 'blue') . "\t-- " . $descr . "\n";
						} else $n++;
					}
					if($n) echo "\tOptions\n";
					foreach($actionParams as $name => $actionParam) {
						if(!is_int($name)) {
							echo Ansi::color("\t- " . sprintf('%-12s', $name), 'red') . "\t-- " . $actionParam . "\n";
						}
					}
				}
				echo "\n";
			}
		}
		return UXApp::EXIT_STATUS_OK;
	}
}
