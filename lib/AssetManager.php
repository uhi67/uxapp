<?php

namespace uhi67\uxapp;

use Exception;
use ReflectionException;

/**
 * Asset kezelés
 * =============
 *
 * Az AssetManager komponens az assetHandler osztállyal együttműködve kezeli a web-útvonalon el nem érhető erőforrásokat.
 * Az AssetManager-nek meg kell adni egy olyan assetHandler osztályt, mely a megadott fájlokat cacheli és az elérhető url-jét adja vissza.
 * Az AssetManager tehát egy factory, mely alatt elvileg többféle assetHandler osztály is lehet.
 *
 * ## Az **AssetManager** class használata
 *
 * Az AssetManager class komponensként telepítendő, de default mindenképpen települ.
 * Beállítások:
 *
 * - cacheDir -- ahová az asset fájlokat másolni kell. Webről elérhetőnek kell lennie. Default /www/assets
 * - cacheUrl -- a cache fájlok web-elérésének alap-url-je, default /www/assets
 * - itemCache -- TODO
 * - assetHandler -- default is Asset::class
 *
 * ### Egyedi erőforrások hivatkozása
 *
 *    `$assetManager->single('/vendor/bower-asset/bootstrap/dist/js/bootstrap.min.js')`
 *
 * Ez a megoldás az egyedi fájlt az Asset class használatával cacheli és az elérhető url-jét adja vissza.
 *
 * ### Csoportos erőforrások hivatkozása
 *
 * Erre azért van szükség, mert a cachelt erőforrások egymásra is hivatkozhatnak, ezért ezeket a cache-ben is együtt kell tartani.
 * A nézet fájlban az assetet először regisztrálni kell, a gyökér mappát és a relatív fájlnév minták megadásával (nem-XSl alapú példa):
 *
 * ```php
 * $languageswitcherAsset = $assetManager->register([
 *  'dir' => '/vendor/uhi67/languageswitcher/src/assets',
 *  'patterns' => [
 *      'languageswitcher.css',
 *      'flags/flags.min.css',
 *      'flags/flags.png',
 *      'flags/blank.gif'
 *  ]
 * ]);
 * ```
 *
 * A regisztráció egy Asset osztályú objektumot ad vissza, melyel a regisztrált asset elemeire a következőképpen kell hivatkozni:
 *
 * `$languageswitcherAsset->url('flags/flags.min.css')` vagy `$languageswitcherAsset->url().'/flags/flags.min.css'`
 *
 * Az XML outputban az asseteket a lefordított url-lel kell ellátni (path attributum):
 *
 * `<js module="uhi67/uxapp" type="..." path="/assets/ff/ffff-uxapp.js">js/uxapp.js</js>`
 *
 * XSL hivatkozások server side render esetén (nem változik):
 *
 * `<xsl path="../../vendor/uhi67/uxapp/xsl/main.xsl" module="uhi67/uxapp" order="0" module-path="D:\www\tk\vendor/uhi67/uxapp/xsl">xsl/main.xsl</xsl>`
 * `<include href="../../vendor/uhi67/uxapp/xsl/main.xsl"/>` -- a generált XSL-ben
 *
 * XSL hivatkozások client side render esetén:
 *
 * `<xsl path="/assets/ff/ffff-main.xsl" module="uhi67/uxapp" order="0" module-path="D:\www\tk\vendor/uhi67/uxapp/xsl">xsl/main.xsl</xsl>`
 * `<include href="/assets/ff/ffff-main.xsl"/>` -- a generált XSL-ben
 *
 * Ahol a '/assets/...' hivatkozásokat az Asset komponens állítja elő.
 *
 */
class AssetManager extends Component {
	// Configuration options
	/** @var string $cacheDir -- web-accessible directory to store asset files. E.g '/www/assets' */
	public $cacheDir;
	/** @var string $cacheDir -- url-path of the asset directory. E.g '/assets' */
	public $cacheUrl;
	/** @var CacheInterface -- cache to keep names and paths of registered assets */
	public $itemCache;
	/** @var string|Asset $assetHandler */
	public $assetHandler; // default is Asset::class
	/** @var int $ttl -- time to live in the www cache -- default is -1 = forever. Specify 0 to replace on every registration. */
	public $ttl;

	/**
	 * Asset constructor.
	 *
	 * Configuration options:
	 * - dir -- asset directory name (mandatory, relative to project root)
	 * - patterns -- filename patterns (mandatory) enclose in ~~ for RegEx patterns
	 *
	 * @throws UXAppException
	 * @throws ReflectionException
	 */
	public function prepare() {
		if(!$this->itemCache) {
			$mainCache = UXApp::$app->getComponent('cache');
			if($mainCache) {
				$this->itemCache = new SingleCache([
					'cache' => $mainCache,
					'name' => 'assetManagerCache',
				]);
			}
		}
		if(!$this->itemCache) $this->itemCache = new SessionCache([
			'name' => 'assetCache',
			'ttl' => 7200,                        // Default time-to-live in seconds
		]);

		if(!$this->assetHandler) $this->assetHandler = Asset::class;

		if(!$this->cacheUrl) $this->cacheUrl = '/assets';

		if(!$this->cacheDir) $this->cacheDir = UXApp::$app->basePath . $this->cacheUrl;
		if(!is_dir($this->cacheDir)) mkdir($this->cacheDir);

		if($this->ttl === null) $this->ttl = UXApp::$app->environment == 'development' ? 0 : 3600;
	}

	/**
	 * Returns an url path for a specific file from an asset.
	 * Throws an exception if the file does not exist
	 *
	 * @param string|array $filename -- absolute filename or [module, file]
	 *
	 * @return string -- cached url of the file
	 * @throws UXAppException -- if the file does not exist
	 */
	public function single($filename) {
		$file = is_array($filename) ? $this->moduleFile($filename) : $filename;
		if(!file_exists($file)) throw new UXAppException('Asset file not found', [$filename, $file]);
		$assetConfig = array_merge($this->config(), [
			'dir' => dirname($file),
			'patterns' => [$pattern = basename($file)],
		]);
		$assetHandler = $this->assetHandler;
		$asset = new $assetHandler($assetConfig);

		return $asset->url($pattern);
	}

	/**
	 * Registers a file.
	 * Throws an exception if the file does not exist
	 *
	 * @param UXPage       $view
	 * @param string|array $fileName -- absolute filename or [module, file]
	 *
	 * @throws UXAppException -- if the file does not exist
	 * @throws Exception
	 */
	public function registerFile($view, $fileName, $type = null) {
		$ext = Util::ext(is_array($fileName) ? $fileName[1] : $fileName);
		$url = $this->single($fileName);
		if(!$url) throw new UXAppException('Asset file not found', $fileName);
		$view->addAsset($ext, $url, $type);
	}

	/**
	 * Converts [module, filename] to absolute path
	 *
	 * @param array $name
	 *
	 * @return string
	 * @throws UXAppException
	 */
	public function moduleFile($name) {
		/** @var Module $module */
		$module = $name[0];    // Modulnév, indexeléshez
		$name = $name[1];    // baseurl-relatív fájlnév
		$dirName = '';        // module base-dir
		if(is_string($module)) {
			// Module name or classname
			if(is_a($module, Module::class, true)) {
				// FQN class-name
				/** @var Module $module */
				$module = new $module();
			} else if(
				// Short legacy module name (assets are in the root default) or
				is_dir($dirName = UXApp::$app->modulesPath . '/' . $module) ||
				// vendorname/modulename format (assets are in the root or /www)
				is_dir($dirName = UXApp::$vendorDir . '/' . $module)
			) {
				if(is_dir($dirName . '/www')) $dirName = $dirName . '/www';
				$dirName .= '/';
			} else throw new UXAppException("Invalid module '$module'");
		}
		if($module instanceof Module) {
			// Vendor module object
			$dirName = $module->basePath . '/';
		}
		return $dirName . $name;
	}

	/**
	 * Returns an asset cache url path for the specified asset file.
	 * Copies file into cache if necessary.
	 *
	 * Config options:
	 *
	 *  - dir        -- directory (from app's root) to the original files
	 *  - patterns  -- patterns or filenames to copy, may contain directories, *? patterns, ~RegEx~, /.../
	 *
	 * Depends on assetHandler class, e.g. {@see Asset}
	 *
	 * @param array $assetConfig
	 *
	 * @return string -- cached url path to serve
	 */
	public function register($assetConfig) {
		$assetConfig = array_merge($this->config(), $assetConfig);
		$assetHandler = $this->assetHandler;
		return new $assetHandler($assetConfig);
	}

	private function config() {
		return [
			'cacheDir' => $this->cacheDir,
			'cacheUrl' => $this->cacheUrl,
			'itemCache' => $this->itemCache,
			'ttl' => $this->ttl,
		];
	}
}
