<?php /** @noinspection PhpIllegalPsrClassPathInspection */

namespace uhi67\uxapp;
use DirectoryIterator;
use DOMElement;
use Exception;
use ReflectionException;
use Throwable;
use uhi67\uxml\UXMLDoc;
use uhi67\uxml\UXMLElement;
use Codeception\Util\Debug as CCDebug;

/**
 * # UXPage
 *
 * Represents an XSL view. Displays page content based on XML/XSL transformations.
 * In addition, provides some utilities for building page XML data.
 *
 * @property-read Request $request
 */
class UXPage extends Component {
    /** @var UXAppPage $controller */
    public $controller;
    /** @var string $render -- 'server' or 'client'. Default 'server'. Determined in constructor, may be ovveride on user page, used in the final rendering */
    public $render;

    /** @var UXMLDoc $xmldoc */
    public $xmldoc;
    /** @var UXMLElement $node_content -- /data/content -- main data block from SQL, etc */
    public $node_content;
    /** @var UXMLElement $node_control -- /data/control -- controller parameters */
    public $node_control;
    /** @var UXMLElement $node_data -- global /data node in page DOM */
    public $node_data;
    /** @var UXMLElement $node_head -- global /data/head node for page frame informations */
    public $node_head;
    /** @var UXMLElement $node_agent --  */
    public $node_agent;
    /** @var UXMLElement|null $node_agent --  */
    public $node_assets; // assets/module/file[@path, @url]
    /** @var string $xsl -- name of main renderer XSL (xsl name with optional module prefix e.g uxapp/name:, without extension) */
    public $xsl;
    public $formatter;
    public $locale;
    /** @var string $url -- complete url of current page */
    public $url;
    /** @var string $agent -- signature of browser agent */
    public $agent;
    /** @var AssetManager $assetManager */
    public $assetManager;

    /** @var bool $browser_xml -- client is capable of rendering xslt */
    protected $browser_xml = true;
    /** @var bool $secondary -- set true if you launch another page in the same request to avoid closing trace early */
    protected $secondary;
    /** @var array $assets -- registered assets */
    public $assets = [];

    /** @var RequestInterface $_request -- cached Request object */
    private $_request;

    private $_fileName;
    public $disposition;

    /**
     * # UXPage initializer
     *
     * ### useful config elements
     * - controller
     * - locale
     * - formatter (default is the app's one)
     * - assetManager (default is the app's one)
     * - render (default is auto-detect)
     * - agent (default is auto-detect)
     *
     * @throws
     */
    function prepare() {
        parent::prepare();

        $showxml = $this->request->req('showxml');
        if($showxml=='on') $_SESSION['showxml'] = 1;
        if($showxml=='off') $_SESSION['showxml'] = 0;

        $userrender = $this->request->req('render');
        if($userrender) $_SESSION['userrender'] = $userrender;

        if(!$this->xmldoc) {
            $this->xmldoc = new UXMLDoc('UTF-8');
        }

        $this->xmldoc->formatter = $this->formatter;
        $this->xmldoc->locale = $this->locale;

        $this->node_data = $this->xmldoc->documentElement;

        if(!$this->url) $this->url = $_SERVER['REQUEST_URI']??'';
        $this->node_data->setAttribute('url', $this->url);
        $this->node_data->setAttribute('session_id', session_id());
        $this->node_head = $this->xmldoc->createElement('head');
        $this->node_data->appendChild($this->node_head);
        $this->node_head->addNode( 'base', ['href'=>$this->controller->app->baseurl]);
        $this->node_content = $this->node_data->appendChild($this->xmldoc->createElement('content'));
        $this->node_control = $this->node_data->appendChild($this->xmldoc->createElement('control'));

        $node_params = $this->node_control->appendChild($this->xmldoc->createElement('params'));

        foreach($_GET as $i => $value)  {
            //$value = htmlspecialchars($value);
            if(is_array($value)) $value = '['.implode($value, ',').']';
            if(!is_int($i)) $node_params->setAttribute(Util::toNameID($i), $value);
        }

        if(UXApp::debugIsOn()) {
            $this->addControlNode('debug',null, UXApp::$app->debug->getId());
        }

        // Detect browser capabilities
        if(!$this->agent) $this->agent = $_SERVER['HTTP_USER_AGENT'] ?? '';

        $accept = $_SERVER['HTTP_ACCEPT'] ?? '';
        if(($this->browser_xml = $this->controller->app->session->get('browser_xml'))===null) {
            $this->browser_xml = self::certifyAgent($this->agent);
            if($this->browser_xml===null) $this->browser_xml = preg_match('~\bapplication/xml\b~', $accept);
            UXApp::trace(['browser-xml' => $this->browser_xml]);
            $this->controller->app->session->set('browser_xml', $this->browser_xml);
        }
        $this->node_agent = $this->node_control->addNode('agent', [
            'xml' => ($this->browser_xml?'Y':'N'),
            'name' => $this->agent,
            'accept' => $accept,
        ]);

        if(!$this->render) $this->render = $this->determineRender();
        UXApp::trace(['planned render' => $this->render]);

        if(!$this->assetManager) {
            if(!$this->controller->app) throw new \Exception('No associated app');
            if(!$this->controller->app->hasComponent('assetManager')) throw new \Exception('No assetManager initialized');
            $this->assetManager = $this->controller->app->assetManager;
        }
    }

    /**
     * ## UXPage user destructor
     * - Writes out the page DOM to the tracefile
     *
     */
    function finish() {
        if(UXApp::debugIsOn()) {
            // Writes out the page DOM to the tracefile
            UXApp::$app->debug->trace_xml($this->xmldoc, ['tags'=>'output']); // Don't remove it
            UXApp::$app->debug->finish();
        }
    }

    /**
     * Determines if given browser agent is capable of XSLT rendering
     * Uses $defpath/certified_user_agents.php definition file
     * Uses also optional user defined rules from `config/agents` path.
     * Returns null if no pattern matched
     *
     * @param string $agent -- signature from HTTP_USER_AGENT variable
     *
     * @return boolean
     * @throws UXAppException
     */
    public static function certifyAgent($agent) {
        // Load list of certified user agents
        $defpath = dirname(__DIR__).'/def';
        $cuafile = $defpath.'/certified_user_agents.php';
        /** @var array $certified -- array of [pattern, min, max, result] */
        $certified = include($cuafile);
        $userfile = UXApp::$app->getParam('agents');
        if($userfile && file_exists($userfile)) {
            // User defined additional or overlay data
            $userdata = include($userfile);
            if(is_array($userdata)) {
                $certified = array_merge($certified, $userdata);
            }
        }

        // Decides upon $cuafile patterns
        foreach($certified as $rule) {
            [$pattern, $min, $max, $result] = $rule;
            if(preg_match($pattern, $agent, $mm)) {
                $submatch = isset($mm[1]) ? $mm[1] : null;
                UXApp::trace(['agent' => $pattern . ' -- ' . $submatch], ['tags'=>'uxapp']);
                if($min!==null && ($submatch===null || $submatch < $min)) continue;
                if($max!==null && ($submatch===null || $submatch > $max)) continue;
                return $result;
            }
        }
        return null;
    }

    /**
     * @param $name
     * @param int $limit
     *
     * @return bool|DOMElement
     * @throws
     */
    function addXslCall($name, $limit=0) {
        if($limit) {
            $nodes = $this->node_control->selectNodes( "xsl-call[@name='$name']");
            $count = $nodes->length;
            if($count>=$limit) return false;
        }
        return $this->addControlNode('xsl-call', ['name'=>$name]);
    }

    /** @noinspection PhpUnused */

    /**
     * Content Node alá létrehoz egy node-ot a megadott attributumokkal és szöveges tartalommal
     *
     * A szöveges tartalom speciális karakterei kódolásra kerülnek.
     *
     * universal content data (UCD) may be
     *
     * - "text node" or ["text node", "text node", ...]
     * - [attribute => value, ...]
     * - [subnodename1 => [UCD, ...], subnodename2 => ], -- multiple subnodes with each name
     * - [[nodename, UCD], ...] -- multiple subnodes with individual specified names
     *
     * @param string $name
     * @param array $attributes -- universal content data (UCD)
     * @param string $content -- universal content data
     *
     * @return UXMLElement -- the added node
     * @throws
     */
    function addContentNode($name, $attributes=null, $content=null) {
        return $this->node_content->addNode($name, $attributes, $content);
    }

    function clearContent() {
        $this->node_control->removeChildren();
        $this->node_control->removeChildren();
    }

    /**
     * Hozzáad egy node-ot a /data/control fejezethez
     *
     * {@see UXMLElement::addNode()}
     *
     * @param string $nodeName
     * @param string $attributes
     * @param string $content
     *
     * @return UXMLElement -- the node added
     * @throws Exception
     */
    function addControlNode($nodeName, $attributes=null, $content=null) {
        return $this->node_control->addNode($nodeName, $attributes, $content);
    }

    /** @noinspection PhpUnused */

    /**
     * @param array $ba -- [value, name, descr, title]
     * @param string $nodeName
     * @param string $prefix -- prefix for attribute name (attr = prefix.name)
     *
     * @return UXMLElement
     * @throws Exception
     */
    function addBitArray($ba, $nodeName, $prefix='b_') {
        $node_ba = $this->addControlNode($nodeName);
        foreach($ba as $b) {
            $node_ba->addNode('bit', [
                'value' => $b[0],
                'attr' => $prefix.strtolower($b[2]),
                'name' => $b[2],
                'descr' => $b[3],
                'title' => $b[4]
            ]);
        }
        return $node_ba;
    }

    /**
     * Determines rendering mode (server or client)
     *
     * Priority:
     *    0. If cannot use server side library => client
     *  0. If browser is not certified for client render => server
     *  1. config/render setting is `$mode only` => $mode
     *  2. User request
     *  3. Session stored user request
     *  4. debug will prefer server render
     *  5. config/render setting
     *
     * Result of determineRender method may be overridden on the user page.
     * However, zero priority rules will be applied again just before rendering.
     *
     * @return string ('server' or 'client')
     * @throws Exception
     */
    function determineRender() {
        if(!$this->browser_xml) return 'server';
        if(!class_exists('XSLTProcessor')) return 'client';

        $renderconfig = UXApp::$app->render;

        if($renderconfig=='server only') return 'server';
        if($renderconfig=='client only') return 'client';

        $userrender = $this->controller->app->session->get('userrender');
        if($userrender=='server') return 'server';
        if($userrender=='client') return 'client';

        if(UXApp::debugIsOn()) return 'server';

        if($renderconfig=='server') return 'server';
        if($renderconfig=='client') return 'client';

        return 'client';
    }

    /** @noinspection PhpUnused */
    public static function isHTTPS() {
        if(!array_key_exists('HTTPS', $_SERVER)) {
            /* Not a https-request. */
            return FALSE;
        }
        if($_SERVER['HTTPS'] === 'off') {
            /* IIS with HTTPS off. */
            return FALSE;
        }
        /* Otherwise, HTTPS will be a non-empty string. */
        return $_SERVER['HTTPS'] !== '';
    }

    public static function getSelfHost() {
        if (array_key_exists('HTTP_HOST', $_SERVER)) {
            $currenthost = $_SERVER['HTTP_HOST'];
        } elseif (array_key_exists('SERVER_NAME', $_SERVER)) {
            $currenthost = $_SERVER['SERVER_NAME'];
        } else {
            /* Almost certainly not what you want, but ... */
            $currenthost = 'localhost';
        }
        if(strstr($currenthost, ":")) {
                $currenthostdecomposed = explode(":", $currenthost);
                $currenthost = $currenthostdecomposed[0];
        }
        return $currenthost;# . self::getFirstPathElement() ;
    }

    /**
     * @return string
     */
    public static function selfURLhost() {
        $currenthost = self::getSelfHost();

        $isHTTPS = strpos($_SERVER['REQUEST_URI'], 'https://') === 0;
        if ($isHTTPS) {
            $protocol = 'https';
        } else {
            $protocol = 'http';
        }
        $portnumber = $_SERVER["SERVER_PORT"];
        $port = ':' . $portnumber;
        if ($protocol == 'http') {
            if ($portnumber == '80') $port = '';
        } elseif ($protocol == 'https') {
            if ($portnumber == '443') $port = '';
        }
        return $protocol."://" . $currenthost . $port;
    }

    /** @noinspection PhpUnused */

    /**
     * @param int $withHost
     *
     * @return string
     */
    public static function selfURLNoQuery($withHost=0) {

        $selfURLhost = $withHost ? self::selfURLhost() : '';
        $selfURLhost .= $_SERVER['SCRIPT_NAME'];
        if (isset($_SERVER['PATH_INFO'])) {
            $selfURLhost .= $_SERVER['PATH_INFO'];
        }
        return $selfURLhost;

    }

    /**
     * A DOM <head> fejezetéhez ad egy szöveg tartalmú node-ot.
     *
     * @param string $name
     * @param string $content
     * @param array $attributes
     *
     * @return UXMLElement
     * @throws Exception
     */
    function addHead($name, $content='', $attributes=[]) {
        return $this->node_head->addNode($name, $attributes, (string)$content);
    }

    /**
     * Registers a single asset file on the view
     *
     * Assets are from the following sources:
     *
     * - local: use the relative asset filepath to www
     * - [Asset, filename] -- filename in an Asset object
     * - other array formats are deprecated
     *
     * @param string $fileType -- css or js
     * @param string|array $name -- filename (relative path to app base url or [Asset, filename])
     * @param string $type -- type specifier if not the default for the extension
     * @param array $attributes -- more attributes for the asset node, e.g. position
     *
     * @return DOMElement
     * @throws Exception
     */
    function addAsset($fileType, $name, $type=null, $attributes=[]) {
        if(!$type) $type = UXApp::mimetype($fileType);
        if(is_array($name) && $name[0] instanceof Asset) {
            $url = $name[0]->url($name[1]);
            if(!$url) throw new UXAppException("File {$name[1]} not found in asset {$name[0]->dir}");
            $name = $url;
        }
        if(is_array($name)) {
            // deprecated, use Asset
            /** @var Module $module */
            $module = $name[0];	// Modulnév, indexeléshez
            $name = $name[1];	// baseurl-relatív fájlnév
            throw new UXAppException("Array asset name is deprecated, use \$this->app->assetManager->registerFile(\$this->view, [".Util::shortName($module).", '$name'])");
        }
        elseif(!is_string($name)) {
            // invalid data type
            throw new UXAppException('Asset name must be a string or [Module, string], got '.gettype($name));
        }
        else if(preg_match('~vendor[/\\\\]([\w_-]+[/\\\\][\w_-]+)[/\\\\]~', $name, $mm)) {
            // Absolute path within a vendor module
            $module = $mm[1];
            $dirName = '';
        }
        else {
            // App asset
            $module = null;
            $ext = Util::ext($name);
            if($ext!=$fileType) $name = $name.'.'.$fileType;
            $dirName = $this->controller->app->basePath.'/';
        }

        if(array_key_exists($module.'/'.$name, $this->assets)) return $this->assets[$module.'/'.$name];
        if(!file_exists($dirName.$name)) $name = $fileType.'/'.$name;
        if(!file_exists($dirName.$name)) throw new UXAppException("Asset `$module:$name` was not found at `$dirName`.");

        $cachePath = $this->controller->app->assetManager->single($dirName.$name);

        $node = $this->addHead($fileType, $name, array_merge(['module'=>$module, 'type' => $type, 'path'=>$cachePath], $attributes));
        return $this->assets[$module.'/'.$name] = $node;
    }

    /**
     * Registers a css file on the view
     *
     * @param string|array $name -- filename (relative path to app base url or absolute path or [module, filename])
     * @param string $type -- type specifier if not 'text/css'
     *
     * @return DOMElement
     * @throws Exception
     */
    function addCss($name, $type=null) {
        if(!$type) $type = 'text/css';
        return $this->addAsset('css', $name, $type);
    }

    /**
     * Registers a js include to the page
     *
     * Specify file-path relative to application's js directory.
     * Specify [module, path] array if js file is a module asset.
     *
     * TODO: implement dependecy order
     *
     * @param string|array $name -- includes extension. Array: [module, filename]
     * @param string $type -- type specifier if not 'text/css'
     * @param string $position -- 'head'/'bottom', default is 'head'
     *
     * @return DOMElement
     * @throws Exception
     */
    function addJs($name, $type=null, $position='head') {
        if(!$type) $type = 'text/javascript';
        return $this->addAsset('js', $name, $type, ['position'=>$position]);
    }

    /**
     * ## Adds an XSL file to the page.
     *
     * `$name` cases:
     *
     * - [module, path]: module may be local module name, vendorname/modulename, FQN classname or Model object
     * - 'modulename:path': modulename may be local module name, vendorname/modulename or FQN classname
     * - local file name
     *
     * The xsl may be located in:
     *
     * - /xsl (use simple name)
     * - /modules/modulename (use modulename:path), a path in module's xsl dir
     * - /vendor/vendorname/modulename (use vendorname/modulename:path)
     *
     * Creates xsl node with attributes:
     *    - path: relative filepath to generated xsl file
     *    - module: module name (or vendorname/modulename) or empty
     *    - order: rendering order
     *    - _content: asset name within the module (relative path to module asset dir)
     *
     * @param string|array $name -- may be 'module:filename' without extension, or [module, name] or absolute path
     * @param int $order -- render include order
     * @return DOMElement node
     * @throws ReflectionException
     * @throws UXAppException
     */
    function addXsl($name, int $order=0) {
        $genxslpath = $this->controller->app->dataPath.'/xsl'; // Ahová a generált fájlok kerülnek

        $module = null;
        $assetName = $name;
        $moduleName = '';
        // Simple name
        if(is_array($name)) {
            [$module, $assetName] = $name;
        }
        else if(!is_string($name)) {
            throw new UXAppException('Asset must be array or string, got '.gettype($name));
        }
        else if(preg_match('~vendor[/\\\\]([\w_-]+[/\\\\][\w_-]+)[/\\\\]xsl[/\\\\]([\w_-]+)\.xsl$~', $name, $mm)) {
            $module = $mm[1];
            $assetName = $mm[2];
        }
        else if(strpos($name, ':')>1) {
            [$module, $assetName] = explode(':', $name);
        }

        if($module) {
            $path = $this->controller->getModulePath($module);
            $moduleName = Controller::getModuleName($path);

            if(is_dir($path.'/xsl')) {
                $path = $path.'/xsl';
            }
            if(!$path) throw new UXAppException(sprintf('Module `%s` not found', $module));
        }
        else {
            // Local XSL file
            $path = $this->controller->app->xslPath;
            // UXApp xsl (deprecated)
            if(!file_exists($path .'/'. $assetName) && !file_exists($path .'/'. $assetName.'.xsl')) $path = dirname(__DIR__) . '/xsl';
        }
        $fileName = $path.'/'.$assetName;

        // If extension is missing, try to complete
        if(!file_exists($fileName)) {
            $fileName = $fileName.'.xsl';
            $assetName = $assetName.'.xsl';
            if(!file_exists($fileName)) {
                throw new UXAppException(sprintf('XSL file `%s` not found (%s, %s)', substr($fileName,0,-4), $moduleName, $assetName));
            }
        }

        $url = $this->assetManager->single($fileName);
        return $this->addXslFile($url, [
            'path' => Util::getRelativePath($genxslpath, $path.'/'.$assetName),
            'module' => $moduleName,
            'order' => $order,
            'module-path' => $path,
        ]);
    }

    /** @noinspection PhpUnused */

    /**
     * Adds an user XSL file to the page.
     * The xsl may be located in:
     * - xsl (simply use it's name with extension')
     * - module (use modulename:path)
     * The difference to addXsl() is that this will include xsl in specified order, default AFTER design
     *
     * @param string $filename
     * @param int $order -- order of includes, default=11
     *
     * @return DOMElement
     * @throws Exception
     */
    function addUserXsl($filename, $order=11) {
        $node = $this->addXsl($filename);
        $node->setAttribute('order', $order);
        return $node;
    }

    /**
     * Simply creates an xsl node in the head with the given content
     * Do not use in user pages, because it does not find the location of the file
     * Use UXAppPage::addXsl() instead.
     *
     * Attributes:
     * - path: ../../vendor/* ../../www/* -- for server side render
     * - module:
     * - module-path: absolute FS path to module xsl dir
     * - order:
     *
     * @param string $filename -- relative url to application baseurl
     * @param array $attributes -- [path, module, order]
     *
     * @return DOMElement -- the added node
     * @throws Exception
     */
    function addXslFile($filename, $attributes=[]) {
        if(array_key_exists($filename, $this->assets)) return $this->assets[$filename];
        $node = $this->addHead('xsl', $filename, $attributes);
        return $this->assets[$filename] = $node;
    }

    /**
     * @param string $var
     * @param string $value
     * @param array|null $attr
     *
     * @return UXMLElement
     * @throws
     */
    function addControlVar($var, $value='', $attr=null) {
        $node = $this->node_control->addNode($var, null, (string)$value);
        if($attr) array_walk($attr,
            function($v, $k, $n) {
                /** @var DOMElement $n */
                $n->setAttribute($k, $v);
            }, $node
        );
        return $node;
    }

    /**
     * Adds default assets of the named page
     *
     * @param string $pageName
     *
     * @throws Exception
     */
    public function addDefaultAssets($pageName) {
        // Page további elemei
        if(file_exists($filename = "js/$pageName.js")) $this->addJs($filename);
        if(file_exists($filename = "css/$pageName.css")) $this->addCss($filename);
    }

    /**
     * Adds all assets from the directory (not recursive)
     *
     * - xsl: Creates xsl node with attributes:
     * 		- path: relative filepath to generated xsl file
     * 		- module: module name (or vendorname/modulename) or empty
     * 		- order: rendering order
     * 		- _content: asset name within the module (relative path to module asset dir)
     *
     * @param string $dirName -- directory to scan for assest (root only)
     * @param string $basePath -- xsl path to generate xsl's relative path to
     * @param string $module -- module name with vendor name
     * @param int $order -- xls include order (0: main, 100=layout, 200=view)
     *
     * @throws Exception
     */
    public function addDirAssets($dirName, $basePath=null, $module=null, $order=300) {
        if(!$basePath) $basePath = $this->controller->app->dataPath.'/xsl'; // Ahová a generált fájlok kerülnek
        $dir = new DirectoryIterator($dirName);
        foreach ($dir as $fileinfo) {
            $fileName = $fileinfo->getFilename();
            switch($fileinfo->getExtension()) {
                case 'xsl':
                    $url = $this->assetManager->single($fileName);
                    $this->addXslFile($url, [
                        'path' => Util::getRelativePath($basePath, $dirName).$fileName,
                        'module' => $module,
                        'order' => $order
                    ]);
                    break;
                case 'js':
                    $this->addJs($fileName);
                    break;
                case 'css':
                    $this->addCss($fileName);
            }
        }
    }

    /**
     * Generates HTML output from xmldoc data and xslt view
     *
     * - $this->xsl: the (generated) main xsl view with optional module: prefix
     *
     * @return bool - success
     * @throws Exception
     */
    function render(): bool {
        $app = $this->controller->app;
        $_showxml = $this->controller->app->session->get('showxml');

        $disposition = $this->disposition ? $this->disposition : 'inline'; // or attachment

        if($app->request->req('out')=='xml') {
            header("Content-Type: text/xml; charset=UTF-8");
            header("Cache-Control: no-cache, must-revalidate");  // HTTP/1.1
            header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
            if($this->_fileName) {
                header("Content-Disposition: $disposition; filename=\"$this->filename\"");
            }
            echo $this->xmldoc->saveXML();
            return true;
        }

        if($_showxml) $this->node_data->setAttribute('showxml', $_showxml);
        if($app->debug && $app->debug->isEnabled()) $this->node_data->setAttribute('trace', $app->debug->getId());

        $xslPath = $this->getXslPath($this->xsl, $app->runtimeXslPath);
        $this->addControlNode('xsl', [
            #'path' => $xslPath, // <-- Not public, enable for debug only
            'runtime-xsl-path' => $app->runtimeXslPath===null ? 'null' : $app->runtimeXslPath,
            'xsl-url' => $app->xslurl
        ], $this->xsl);
        if(!file_exists($xslPath)) {
            #throw new UXAppException("Xsl not found: $this->xsl -> $xslPath from $app->runtimeXslPath");
            UXApp::trace(['xsl' => $this->xsl, 'path'=>$xslPath], ['tags'=>'uxapp']);
            $this->xsl = 'uhi67/uxapp:xslnotfound';
            $xslPath = $this->getXslPath($this->xsl, $app->runtimeXslPath);
            if(!file_exists($xslPath)) throw new UXAppException('XSL not found: '.$xslPath);
        }

        if($this->render=='client' && !$this->_fileName) {
            // Client render
            UXApp::trace(['render-client' => $this->xsl], ['tags'=>'uxapp']);
            $xslurl = $this->assetManager->single($xslPath);
            UXApp::$app->log('info', $this->xsl.'-->'.$xslurl, ['tags'=>'uxapp']);
            $node_pi = $this->xmldoc->createProcessingInstruction('xml-stylesheet', 'type="text/xsl" href="'.$xslurl.'"');
            $this->xmldoc->insertBefore($node_pi, $this->node_data);
            $this->xmldoc->formatOutput = true;
            $output = $this->xmldoc->saveXML();
            UXApp::trace('UXPage::render', ['tags' => 'uxapp', 'xml'=>$output]);
            header("Content-Type: text/xml; charset=UTF-8");
            header("Cache-Control: no-cache, must-revalidate");  // HTTP/1.1
            header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
            echo $output;
        }
        else {
            // Server render
            UXApp::trace(['render-server' => $this->xsl, 'path'=>$xslPath], ['tags'=>'uxapp']);
            if(!headers_sent($filename, $line)) {
                header("Cache-Control:   no-cache, must-revalidate");  // HTTP/1.1
                header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
                header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
            }
            else {
                if(class_exists(CCDebug::class, false)) CCDebug::debug('# UXPage headers (!)');
                else echo "Header error, output is started at $filename #$line\n";
            }
            $c = $this->xmldoc->documentElement->getAttribute('content-type');

            if($c || mb_internal_encoding()=='UTF-8') {
                $content_type = $c ? $c : 'text/html';
                if(!headers_sent($filename, $line)) header("Content-Type: $content_type; charset=UTF-8");
                if($this->_fileName) {
                    header("Content-Disposition: $disposition; filename=\"$this->filename\"");
                }
                UXApp::trace(['$content_type' => $content_type], ['tags'=>'uxapp']);
                UXApp::trace($this->xmldoc->saveXML(), ['tags' => 'uxpage-xmldoc']); // Client side trace not needed, because debug is always in server mode.
                try {
                    $result = $this->xmldoc->process_xml($xslPath);
                }
                catch(Throwable $e) {
                    throw new UXAppException("Rendering error at $xslPath", $this->xmldoc->saveXML(), $e);
                }
                if($result) {
                    echo $result; //->saveXML();
                }
                else {
                    throw new UXAppException("rendering error at $xslPath");
                }
                if($_showxml) echo '<div style="clear: both"><h3>XML data of this page</h3><pre><code>'.htmlspecialchars($this->xmldoc->saveXML()).'</code></pre></div>';
            }
            else {
                UXApp::trace(['Opera render' => $c], ['tags'=>'uxapp']);
                // FireFox, Opera esetén csak html output működik!
                $result = $this->xmldoc->process_html($xslPath);
                echo str_replace('&#251;', 'ű', str_replace('&#245;', 'ő', $result));
            }
        }
        flush();
        return true;
    }

    /**
     * Returns real path of named xsl.
     *
     * Default names refers to application/xsl directory.
     * Module prefixes (vendor/module:) are translated to module/xsl directories.
     * uxapp: prefix is translated to framework xsl path.
     * _: prefix is translated to generated application xsl path.
     *
     * @param string $xslName
     * @param string $runtimeXslPath
     *
     * @return string
     */
    private function getXslPath($xslName, $runtimeXslPath) {
        $p = preg_match('~^([\w_-]+)[/|:]([\w_-]+)$~', $xslName, $mm);
        if($p) {
            $moduleName = $mm[1];
            $xslName = $mm[2];
            if($moduleName == '_') return $runtimeXslPath.$xslName.'.xsl';
            if($moduleName == 'uxapp') return dirname(__DIR__).'/xsl/'.$xslName.'.xsl';
        }
        $p = preg_match('~^([\w_-]+(/[\w_-]+)?)[/|:]([\w_-]+)$~', $xslName, $mm);
        if($p) {
            $moduleName = $mm[1];
            $xslName = $mm[3];
            return dirname(dirname(dirname(__DIR__))).'/'.$moduleName.'/xsl/'.$xslName.'.xsl';
        }
        else {
            return $this->controller->app->xslpath.'/'.$xslName.'.xsl';
        }
    }

    /**
     * Registers all asset files of given types in the directory of the module
     *
     * @param string|Module $module -- class or module name or Module instance
     * @param string $dirname -- absolute path
     * @param array|null $fileTypes -- file extensions to register. Default is null = all types.
     *
     * @throws Exception
     */
    public function registerAssets($module, $dirname, $fileTypes=null) {
        $name = $module instanceof Module ? $module->moduleName : $module;
        if(!($dir = @dir($dirname))) {
            $e = new Exception("Asset registration ($name): directory read error at path '$dirname'.");
            UXApp::showException($e);
            throw $e;

        }
        while(false !== ($entry = $dir->read())) {
            $eee = strrpos($entry, '.');
            $ext = $eee === false ? '' : substr($entry, $eee + 1);
            if(is_array($fileTypes) && !in_array($ext, $fileTypes)) continue;
            if($ext == 'js') $this->addJs([$module, $entry]);
            if($ext == 'css') $this->addCss([$module, $entry]);
            if($ext == 'xsl') $this->addXsl([$module, $entry]);
        }
    }

    public function getRequest() {
        if(!$this->_request) {
            if($this->controller && $this->controller->app && $this->controller->app->hasComponent('request')) {
                $this->_request = $this->controller->app->request;
            }
            else {
                $this->_request = new Request();
            }
        }
        return $this->_request;
    }

    public function getNode() {
        return $this->node_content;
    }

    public function setContentType(string $contentType) {
        $this->xmldoc->documentElement->setAttribute('content-type', $contentType);
    }

    public function setFileName($fileName, $disposition='inline') {
        $this->_fileName = $fileName;
        if($disposition!==null) $this->disposition = $disposition;
    }

    public function getFileName() {
        return $this->_fileName;
    }

    /**
     * Adds an asset file other than xsl, js or css
     * to head/assets node as module/name
     *
     * @param string $module -- module name without /, e.g. "modules-pte"
     * @param string $resource -- relative filename to the asset
     * @param string $url -- url of the resource file
     * @return void
     * @throws \DOMException
     */
    public function addAssetFile($module, $resource, $url) {
        $name = strtr($resource, '/', '`');
        if(!$this->node_assets) $this->node_assets = $this->node_head->appendChild($this->xmldoc->createElement('assets'));
        $node_module = $this->node_assets->selectSingleNode($module);
        if(!$node_module) $node_module = $this->node_assets->addNode($module);
        $node = $node_module->addNode($name, ['path'=>$resource], $url);
        $this->assets[$module.'/'.$name] = $node;
    }

}
