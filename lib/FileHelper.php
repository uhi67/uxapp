<?php

namespace uhi67\uxapp;

class FileHelper {
	/**
	 * Removes multiple files by RegEx pattern.
	 * Skips forbidden files silently.
	 *
	 * @param string $dir
	 * @param string $pattern
	 * @param bool $recurse -- search also in subdirectories
	 *
	 * @return int -- number of removed files, false on error (not existing dir)
	 */
	public static function unlinkFiles($dir, $pattern, $recurse = false) {
		if(!is_dir($dir)) return false;
		// Fájlnevek kigyűjtése
		$files = [];
		$dh = opendir($dir);
		while(false !== ($filename = readdir($dh))) {
			if(is_dir($dir . '/' . $filename)) {
				if($recurse) static::unlinkFiles($dir . '/' . $filename, $pattern, true);
			} else if(preg_match($pattern, $filename)) $files[] = $filename;
		}
		closedir($dh);
		// Fájlok törlése
		$c = 0;
		foreach($files as $fn) {
			UXApp::trace("unlink($dir/$fn)");
			$r = unlink($dir . '/' . $fn);
			if($r) $c++;
		}
		return $c;
	}

	/**
	 * @param $dir
	 * @param int $depth
	 *
	 * @return bool
	 */
	public static function makeDir($dir, $depth = 1) {
		if(!file_exists($dir)) {
			if($depth == 0) return false;
			static::makeDir(dirname($dir), $depth - 1);
			if(@!mkdir($dir, 0774)) {
				return false;
			}
		}
		return true;
	}

}