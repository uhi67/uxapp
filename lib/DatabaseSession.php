<?php /** @noinspection PhpUnused */

namespace uhi67\uxapp;
use ReflectionException;

/** @noinspection PhpUnused */

/**
 * # Class DatabaseSession
 *
 * ###Session configuration example for `config.php`
 *
 * ```php
 * 	'session' => [
 * 	'name' => 'sess_sample',
 * 	'lifetime' => 1800,
 * 	'cookie_path' => '',
 * 	'cookie_domain' => 'sample.hu',
 * 	'logfile' => $datapath.'/session.log',
 * 	'db' => $db, // A DBX connection object
 *	'tableName' => 'session',
 *  '' =>
 * ],
 * ```
 */
class DatabaseSession extends BaseSession {
	/** @var string */
	public $name = 'umxc_app';
	/** @var int */
	public $lifetime = 1800;
	/** @var string */
	public $cookie_path = '';
	/** @var string */
	public $cookie_domain;
	/** @var string */
	public $logfile;
	/** @var DBX|string|array -- config name, config array or connection object */
	public $db;
	/** @var string */
	public $tableName;

	/**
	 * @throws ReflectionException
	 */
	public function prepare() {
		if(php_sapi_name() == "cli") return false;
		if(!$this->tableName) $this->tableName = 'session';
		if(is_string($this->db)) $this->db = UXApp::$app->getComponent($this->db);
		if(is_array($this->db)) $this->db = Component::create($this->db);

		if(session_status()==PHP_SESSION_ACTIVE) throw new UXAppException('Session is already active.');

		if(!session_set_save_handler(
			[$this, "open"],
			[$this, "close"],
			[$this, "read"],
			[$this, "write"],
			[$this, "destroy"],
			[$this, "gc"]
		)) static::log('Error registering session handler');

		ini_set("session.gc_maxlifetime",$this->lifetime + 900);
		ini_set("session.lifetime", $this->lifetime);
		ini_set("session.gc_probability", "100");
		session_set_cookie_params(0, $this->cookie_path, $this->cookie_domain);
		session_name($this->name);
		session_cache_limiter('private_no_expire');
		if(headers_sent($file, $line)) throw new UXAppException("Cannot start session", "headers already sent in $file:$line");

		if(!static::is_started() && !session_start()) static::log('Error starting session');
		return true;
	}

	/**
	 * @return bool
	 */
	public function expired() {
		if(php_sapi_name() == "cli") return false;
		$id = session_id();
		$a = $this->db->selectvalue(/* @lang */"select floor(extract(epoch from (now() - modified))) as age from {$this->tableName} where id=$1", [$id]);
		return $a > $this->lifetime;
	}

	/**
	 * @noinspection PhpUnused
	 */
	public function open(/** @noinspection PhpUnusedParameterInspection */ $save_path, $session_name) {
		if(php_sapi_name() == "cli") return false;
		static::log("DatabaseSession::open\t".$_SERVER['REQUEST_METHOD']. "\t". $_SERVER['REQUEST_URI']."\t$session_name");
		return true;
	}

	/**
	 * @return bool
	 * @noinspection PhpUnused
	 */
	public function close() {
		if(php_sapi_name() == "cli") return false;
		$this->unlock();
		static::log("DatabaseSession::close\t$this->id");
		return true;
	}

	/**
	 * @param string $id
	 *
	 * @return array|bool|string
	 * @noinspection PhpUnused
	 */
	function read($id='') {
		if(php_sapi_name() == "cli") return false;
		static::log("DatabaseSession::read\t$id");
		if($id=='') return '';
		// Itt kell lockolni
		$this->lock();

		$rs = $this->db->query("select value from session where id=$1", [$id]);
		if($this->db->num_rows($rs)==0) {
			$sess_data = '';
			$rs = $this->db->query("insert into session (id, value, created, modified) 
				values ($1, $2, now(), now())", [$id, $sess_data]);
			if(!$rs) static::log("DatabaseSession::read: write failed\t$id");
			else static::log("DatabaseSession::read: written\t$id");
		}
		else
			$sess_data = $this->db->result($rs, 0, 'value');
		static::log("DatabaseSession::read OK\t$id\t$sess_data");
		return $sess_data;
	}

	/**
	 * @param string $id
	 * @param string $sess_data
	 *
	 * @return bool
	 * @noinspection PhpUnused
	 */
	public function write($id, $sess_data) {
		if(php_sapi_name() == "cli") return false;
		static::log("DatabaseSession::write...");
		if(!$this->db) {
			static::log("No database to write session into!\t$id");
			return false;
		}
		static::log("DatabaseSession::write\t$id\t$sess_data");
		$sql = "update session set value=$2, modified=now() where id=$1";
		//session_log("fav_session_write\t$sql\t$sess_data");
		if(!$this->db->query($sql, [$id, $sess_data])) {
			static::log("Error writing session into database!\t$id");
			return false;
		}
		return true;
	}

	/** @noinspection PhpUnused */
	/**
	 * @param $id
	 *
	 * @return resource|false
	 */
	public function destroy($id) {
		if(php_sapi_name() == "cli") return false;
		static::log("DatabaseSession::destroy\t$id");
		return $this->db->query("delete from session where id=$1", [$id]);
	}

	/**
	 * @param int $maxlifetime
	 *
	 * @return bool
	 */
	public function gc($maxlifetime=null) {
		if(php_sapi_name() == "cli") return false;
		static::log("DatabaseSession::gc\t$maxlifetime");
		$sql = "delete from session where modified + interval '$maxlifetime seconds' < now()";
		$this->db->query($sql);
		return true;
	}

	private function unlock() {
		if(php_sapi_name() == "cli") return false;
		if($this->db) $this->db->query("update session set lock=null where id=$1", [$this->id]);
		return true;
	}

	private function lock() {
		if(php_sapi_name() == "cli") return false;
		static::log("DatabaseSession::lock");
		return true;
	}
}
