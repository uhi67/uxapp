<?php
/**
 * Created by PhpStorm.
 * User: uhi
 * Date: 2018. 03. 29.
 * Time: 13:47
 */

namespace uhi67\uxapp;

class Html {
	/**
	 * @var string Regular expression used for attribute name validation.
	 */
	public static $attributeRegex = '/(^|.*\])([\w\.\+]+)(\[.*|$)/u';
	/**
	 * @var array list of void elements (element name => 1)
	 * @see http://www.w3.org/TR/html-markup/syntax.html#void-element
	 */
	public static $voidElements = [
		'area' => 1,
		'base' => 1,
		'br' => 1,
		'col' => 1,
		'command' => 1,
		'embed' => 1,
		'hr' => 1,
		'img' => 1,
		'input' => 1,
		'keygen' => 1,
		'link' => 1,
		'meta' => 1,
		'param' => 1,
		'source' => 1,
		'track' => 1,
		'wbr' => 1,
	];
	/**
	 * @var array the preferred order of attributes in a tag. This mainly affects the order of the attributes
	 * that are rendered by [[renderTagAttributes()]].
	 */
	public static $attributeOrder = [
		'type',
		'id',
		'class',
		'name',
		'value',

		'href',
		'src',
		'srcset',
		'form',
		'action',
		'method',

		'selected',
		'checked',
		'readonly',
		'disabled',
		'multiple',

		'size',
		'maxlength',
		'width',
		'height',
		'rows',
		'cols',

		'alt',
		'title',
		'rel',
		'media',
	];
	/**
	 * @var array list of tag attributes that should be specially handled when their values are of array type.
	 * In particular, if the value of the `data` attribute is `['name' => 'xyz', 'age' => 13]`, two attributes
	 * will be generated instead of one: `data-name="xyz" data-age="13"`.
	 */
	public static $dataAttributes = ['aria', 'data', 'data-ng', 'ng'];

	/**
	 * Visszaad egy 'fa' ikont `<i>` tagben
	 *
	 * @param string $name
	 * @param array $options
	 *
	 * @return string
	 */
	public static function fa($name, $options=[]) {
		$class = (!preg_match('~\bfa([rs])?\b~', $name) ? 'fa ' : '').$name;
		if(isset($options['class'])) $class .= ' ' . $options['class'];
		$options['class'] = $class;
		$options['aria-hidden'] = "true";
		return Html::tag('i', '', $options);
	}

	/**
	 * Generál egy div-et
	 *
	 * @param array $options -- see at {@see Html::tag()}
	 * @param string $content
	 * @return string
	 */
	public static function div($content='', $options=[]) {
		return static::tag('div', $content, $options);
	}

	public static function span($content='', $options=[]) {
		return static::tag('span', $content, $options);
	}

	public static function table($content='', $options=[]) {
		return static::tag('table', $content, $options);
	}

	public static function tag($name, $content = '', $options = [])
	{
		if ($name === null || $name === false) {
			return $content;
		}
		$html = "<$name" . static::renderTagAttributes($options) . '>';
		return isset(static::$voidElements[strtolower($name)]) ? $html : "$html$content</$name>";
	}

	/**
	 * Renders the HTML tag attributes.
	 *
	 * Attributes whose values are of boolean type will be treated as
	 * [boolean attributes](http://www.w3.org/TR/html5/infrastructure.html#boolean-attributes).
	 *
	 * Attributes whose values are null will not be rendered.
	 *
	 * The values of attributes will be HTML-encoded using [[encode()]].
	 *
	 * `aria` and `data` attributes get special handling when they are set to an array value. In these cases,
	 * the array will be "expanded" and a list of ARIA/data attributes will be rendered. For example,
	 * `'aria' => ['role' => 'checkbox', 'value' => 'true']` would be rendered as
	 * `aria-role="checkbox" aria-value="true"`.
	 *
	 * If a nested `data` value is set to an array, it will be JSON-encoded.
	 *
	 * @param array $attributes attributes to be rendered. The attribute values will be HTML-encoded using [[encode()]].
	 * @return string the rendering result. If the attributes are not empty, they will be rendered
	 * into a string with a leading white space (so that it can be directly appended to the tag name
	 * in a tag. If there is no attribute, an empty string will be returned.
	 */
	public static function renderTagAttributes($attributes)
	{
		if (count($attributes) > 1) {
			$sorted = [];
			foreach (static::$attributeOrder as $name) {
				if (isset($attributes[$name])) {
					$sorted[$name] = $attributes[$name];
				}
			}
			$attributes = array_merge($sorted, $attributes);
		}

		$html = '';
		foreach ($attributes as $name => $value) {
			if (is_bool($value)) {
				if ($value) {
					$html .= " $name";
				}
			} elseif (is_array($value)) {
				if (in_array($name, static::$dataAttributes)) {
					foreach ($value as $n => $v) {
						if (is_array($v)) {
							$html .= " $name-$n='" . json_encode($v) . "'";
						} elseif (is_bool($v)) {
							if ($v) {
								$html .= " $name-$n";
							}
						} elseif ($v !== null) {
							$html .= " $name-$n=\"" . static::encode($v) . '"';
						}
					}
				} elseif ($name === 'class') {
					if (empty($value)) {
						continue;
					}
					$html .= " $name=\"" . static::encode(implode(' ', $value)) . '"';
				} elseif ($name === 'style') {
					if (empty($value)) {
						continue;
					}
					$html .= " $name=\"" . static::encode(static::cssStyleFromArray($value)) . '"';
				} else {
					$html .= " $name='" . json_encode($value) . "'";
				}
			} elseif ($value !== null) {
				$html .= " $name=\"" . static::encode($value) . '"';
			}
		}

		return $html;
	}

	/**
	 * Generál egy vagy több (tömb) azonos taget
	 *
	 * @param string $name
	 * @param string|array $content -- tartalom, tagenként
	 * @param array $options -- tömbérték esetén közös
	 *
	 * @return string
	 */
	public static function tags($name, $content='', $options=[]) {
		$result = '';
		if(!is_array($content)) $content = [$content];
		foreach ($content as $i=>$c) $result .= static::tag($name, $c, $options);
		return $result;
	}

	/**
	 * Generál egy vagy több (tömb) tr sort
	 *
	 * @param string|array $content -- tartalom, soronként
	 * @param array $options -- tömbérték esetén közös
	 * @return string
	 */
	public static function tr($content='', $options=[]) {
		return static::tags('tr', $content, $options);
	}

	/**
	 * Generál egy vagy több (tömb) td oszlopot
	 *
	 * @param string|array $content -- tartalom, oszloponként
	 * @param array $options -- tömbérték esetén közös
	 * @return string
	 */
	public static function td($content='', $options=[]) {
		return static::tags('td', $content, $options);
	}

	public static function dl($items, $options) {
		$content = '';
		foreach($items as $key => $item) {
			$content .= static::tag('dt', $key, ['class'=>'list-group-item-heading']) .
				static::tag('dd', $item, ['class'=>'list-group-item-text']);
		}
		return static::tag('dl', $content, $options);
	}

	public static function deflist($items) {
		$content = '';
		foreach($items as $key => $item) {
			$content .= static::div(
				static::tag('p', $key, ['class'=>'list-group-item-heading']) .
				static::tag('p', $item, ['class'=>'list-group-item-text']),
				['class'=>'list-group-item']
			);
		}
		return static::tag('div', $content, ['class'=>'list-group']);
	}

	public static function row($contents) {
		if(!is_array($contents)) $contents = [$contents];
		return static::div(implode('', $contents), ['class'=>'row']);
	}

	public static function half($label, $value) {
		return
			Html::label($label, null, ['class'=>'col-xs-4']) .
			Html::div($value, ['class'=>'col-xs-8']);
	}

	/**
	 * label és div Bootstrap 2/10 arányú felosztással
	 *
	 * @param string $label -- címkeszöveg
	 * @param string $value -- érték-div tartalma
	 * @param string $name -- mezőnév (labelhez)
	 *
	 * @return string
	 */
	public static function full($label, $value, $name) {
		return
			Html::label($label, $name, ['class'=>'control-label col-xs-2']) .
			Html::div($value, ['class'=>'col-xs-10']);
	}

	/**
	 * Converts a CSS style array into a string representation.
	 *
	 * For example,
	 *
	 * ```php
	 * print_r(Html::cssStyleFromArray(['width' => '100px', 'height' => '200px']));
	 * // will display: 'width: 100px; height: 200px;'
	 * ```
	 *
	 * @param array $style the CSS style array. The array keys are the CSS property names,
	 * and the array values are the corresponding CSS property values.
	 * @return string the CSS style string. If the CSS style is empty, a null will be returned.
	 */
	public static function cssStyleFromArray(array $style)
	{
		$result = '';
		foreach ($style as $name => $value) {
			$result .= "$name: $value; ";
		}
		// return null if empty to avoid rendering the "style" attribute
		return $result === '' ? null : rtrim($result);
	}

	/**
	 * Generates a label tag.
	 * @param string $content label text. It will NOT be HTML-encoded. Therefore you can pass in HTML code
	 * such as an image tag. If this is is coming from end users, you should [[encode()]]
	 * it to prevent XSS attacks.
	 * @param string $for the ID of the HTML element that this label is associated with.
	 * If this is null, the "for" attribute will not be generated.
	 * @param array $options the tag options in terms of name-value pairs. These will be rendered as
	 * the attributes of the resulting tag. The values will be HTML-encoded using [[encode()]].
	 * If a value is null, the corresponding attribute will not be rendered.
	 * See [[renderTagAttributes()]] for details on how attributes are being rendered.
	 * @return string the generated label tag
	 */
	public static function label($content, $for = null, $options = [])
	{
		$options['for'] = $for;
		return static::tag('label', $content, $options);
	}

	/**
	 * Encodes special characters into HTML entities.
	 * @param string $content the content to be encoded
	 * @param bool $doubleEncode whether to encode HTML entities in `$content`. If false,
	 * HTML entities in `$content` will not be further encoded.
	 * @return string the encoded content
	 * @see decode()
	 * @see https://secure.php.net/manual/en/function.htmlspecialchars.php
	 */
	public static function encode($content, $doubleEncode = true)
	{
		return htmlspecialchars($content, ENT_QUOTES | ENT_SUBSTITUTE, 'UTF-8', $doubleEncode);
	}

	/**
	 * Decodes special HTML entities back to the corresponding characters.
	 * This is the opposite of [[encode()]].
	 * @param string $content the content to be decoded
	 * @return string the decoded content
	 * @see encode()
	 * @see https://secure.php.net/manual/en/function.htmlspecialchars-decode.php
	 */
	public static function decode($content)
	{
		return htmlspecialchars_decode($content, ENT_QUOTES);
	}

}
