<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.org/1999/xhtml">
<xsl:output method="xml" version="1.0" indent="yes"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/1999/XSL/Transform"
	omit-xml-declaration="no"
 />
 
<xsl:template match="/">
	<xsl:apply-templates select="/data/head" />
</xsl:template>

<xsl:template match="/data/head">
	<xsl:element name="stylesheet" namespace="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml"
		use-attribute-sets="namelist">
		<xsl:attribute name="version">1.0</xsl:attribute> 
		<xsl:element name="output" namespace="http://www.w3.org/1999/XSL/Transform">
			<xsl:attribute name="doctype-public">-//W3C//DTD XHTML 1.0 Transitional//EN</xsl:attribute>
			<xsl:attribute name="doctype-system">http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd</xsl:attribute>
			<xsl:attribute name="omit-xml-declaration">yes</xsl:attribute>
			<xsl:attribute name="method">xml</xsl:attribute> 
			<xsl:attribute name="version">1.0</xsl:attribute> 
			<xsl:attribute name="indent">yes</xsl:attribute>
		</xsl:element>
		
		<xsl:for-each select="xsl">
			<xsl:element name="include" namespace="http://www.w3.org/1999/XSL/Transform">
				<xsl:attribute name="href"><xsl:value-of select="." /></xsl:attribute>
			</xsl:element>
		</xsl:for-each>
		<xsl:comment>View</xsl:comment>
		<xsl:for-each select="view">
			<xsl:element name="include" namespace="http://www.w3.org/1999/XSL/Transform">
				<xsl:attribute name="href">xsl/<xsl:value-of select="." />.xsl</xsl:attribute>
			</xsl:element>
		</xsl:for-each>
		<xsl:comment>Design</xsl:comment>
		<xsl:element name="include" namespace="http://www.w3.org/1999/XSL/Transform">
			<xsl:choose>
				<xsl:when test="design">
					<xsl:attribute name="href"><xsl:value-of select="." />/design.xsl</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="href">xsl/design.xsl</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:element>
	</xsl:element>
</xsl:template>

</xsl:stylesheet>