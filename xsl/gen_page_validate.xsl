<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="xml" version="1.0" indent="yes"
		omit-xml-declaration="yes"
	 />

	<xsl:template match="/">
		<xsl:apply-templates select="/data/head" />
	</xsl:template>

	<xsl:template match="/data/head">
		<xsl:comment> Validation root for '<xsl:value-of select="name" />/<xsl:value-of select="view" />' view </xsl:comment>
		<xsl:element name="stylesheet" namespace="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml"
			use-attribute-sets="namelist">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:element name="output" namespace="http://www.w3.org/1999/XSL/Transform">
				<xsl:attribute name="doctype-public">-//W3C//DTD XHTML 1.0 Transitional//EN</xsl:attribute>
				<xsl:attribute name="doctype-system">http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd</xsl:attribute>
				<xsl:attribute name="omit-xml-declaration">yes</xsl:attribute>
				<xsl:attribute name="method">xml</xsl:attribute>
				<xsl:attribute name="version">1.0</xsl:attribute>
				<xsl:attribute name="indent">yes</xsl:attribute>
			</xsl:element>
			<xsl:comment>Modules</xsl:comment>
			<xsl:for-each select="xsl[@path and @module]">
				<xsl:sort select="@order" data-type="number" />
				<xsl:element name="include" namespace="http://www.w3.org/1999/XSL/Transform">
					<xsl:attribute name="href"><xsl:value-of select="@path" /></xsl:attribute>
				</xsl:element>
			</xsl:for-each>
			<xsl:comment>Includes</xsl:comment>
			<xsl:for-each select="xsl[@path and not(@module)]">
				<xsl:sort select="@order" data-type="number" />
				<xsl:element name="include" namespace="http://www.w3.org/1999/XSL/Transform">
					<!-- '../../xsl/../*' alakú path helyett '../../*' -->
					<xsl:attribute name="href">
						<xsl:choose>
							<xsl:when test="starts-with(@path, '../')">../<xsl:value-of select="substring-after(@path, '../')" /></xsl:when>
							<xsl:otherwise>../xsl/<xsl:value-of select="@path" /></xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
				</xsl:element>
			</xsl:for-each>
			<xsl:comment>View</xsl:comment>
			<xsl:for-each select="view[@path]">
				<xsl:sort select="@order" data-type="number" />
				<xsl:element name="include" namespace="http://www.w3.org/1999/XSL/Transform">
					<!-- '../../xsl/../*' alakú path helyett '../../*' -->
					<xsl:attribute name="href">
						<xsl:choose>
							<xsl:when test="starts-with(@path, '../')">../<xsl:value-of select="substring-after(@path, '../')" /></xsl:when>
							<xsl:otherwise>../xsl/<xsl:value-of select="@path" /></xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
				</xsl:element>
			</xsl:for-each>
	</xsl:element>
	</xsl:template>

	<xsl:template name="string-replace-all">
		<xsl:param name="text" />
		<xsl:param name="replace" />
		<xsl:param name="by" />
		<xsl:choose>
			<xsl:when test="$text = '' or $replace = '' or not($replace)" >
				<xsl:value-of select="$text" />
			</xsl:when>
			<xsl:when test="contains($text, $replace)">
				<xsl:value-of select="substring-before($text,$replace)" />
				<xsl:value-of select="$by" />
				<xsl:call-template name="string-replace-all">
					<xsl:with-param name="text" select="substring-after($text,$replace)" />
					<xsl:with-param name="replace" select="$replace" />
					<xsl:with-param name="by" select="$by" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$text" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
