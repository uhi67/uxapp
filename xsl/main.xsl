<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="html" version="1.0" indent="yes"
		doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
		doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
		omit-xml-declaration="yes"
	 />
	<!-- Callbacks (implement them in design or view files)

		<xsl:apply-templates select="/data" />
		<xsl:apply-templates select="/data/head" mode="user-head" />
		<xsl:apply-templates select="head/menu"/> ### served from menu module

	 -->

	<xsl:variable name="control" select="/data/control" />
	<xsl:variable name="params" select="/data/control/params" />
	<xsl:variable name="content" select="/data/content" />
	<xsl:variable name="head" select="/data/head" />
	<xsl:variable name="assets" select="/data/head/assets" />
	<xsl:variable name="script" select="/data/@script" />
	<xsl:variable name="act" select="$control/act" />
	<xsl:variable name="id" select="$control/id" />
	<xsl:variable name="user" select="/data/head/user" />
	<xsl:variable name="lang" select="/data/head/lang" />

	<xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyzáéíóöőúüű'" />
	<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÖŐÚÜŰ'" />

	<xsl:template match="head" mode="user-head">
		<!-- callback -->
	</xsl:template>

	<xsl:template match="data">
		<!-- callback - override in application template -->
		<h1><xsl:value-of select="head/h1" /></h1>
		<xsl:apply-templates select="head/menu"/>
		<xsl:apply-templates select="control/msg" />
		<xsl:apply-templates select="content" />
	</xsl:template>

	<!-- ================================================================================== -->
	<xsl:template match="/">
	<html lang="{$lang}" data-locale="{/data/head/locale}">
		<head>
			<base href="{/data/head/base/@href}/" />
			<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
			<meta name="Version" content="{/data/@ver}" />
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
			<meta name="ROBOTS" CONTENT="NOINDEX, NOFOLLOW" />
			<title>
				<xsl:value-of select="/data/head/title"/>
				<xsl:if test="/data/head/appname!='' and /data/head/title!=''"> - </xsl:if>
			    <xsl:value-of select="/data/head/appname"/>
			</title>
			<xsl:apply-templates select="/data/head/ico"/>
			<xsl:apply-templates select="/data/head/css" />
			<xsl:apply-templates select="/data/head/js[@position='head' or not(@position)]" />
			<xsl:apply-templates select="/data/head" mode="user-head" />
		</head>
		<body>
			<xsl:apply-templates select="/data/control/xsl-call" />
			<xsl:apply-templates select="/data" />
			<xsl:apply-templates select="/data/head/js[@position='bottom']" />
		</body>
	</html>
	</xsl:template>

	<xsl:template match="ico">
		<link rel="Shortcut Icon" href="{.}" type="image/x-icon" />
	</xsl:template>

	<xsl:template match="js|css" mode="asset-path">
		<xsl:choose>
			<xsl:when test="contains(. , '://')"/>
			<xsl:when test="@module">
				<xsl:value-of select="$control/url-base" />
				<xsl:text>/asset/</xsl:text>
				<xsl:value-of select="@module"/>
				<xsl:text>/</xsl:text>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="js">
		<xsl:variable name="path"><xsl:apply-templates select="." mode="asset-path" /></xsl:variable>
		<script type="text/javascript" src="{$path}{.}"/>
	</xsl:template>

	<xsl:template match="css">
		<xsl:variable name="path"><xsl:apply-templates select="." mode="asset-path" /></xsl:variable>
		<link rel="stylesheet" href="{$path}{.}" type="text/css"/>
	</xsl:template>

	<!-- Default data block, override it in the design -->
	<xsl:template match="data">
		<xsl:apply-templates select="/data/control/msg"/>
	</xsl:template>

	<!-- Default msg block, override it in the design -->
	<xsl:template match="msg">
		<div class="alert alert-danger">
			<xsl:if test="@class"><xsl:attribute name="class"><xsl:value-of select="@class" /></xsl:attribute></xsl:if>
			<xsl:if test="@style"><xsl:attribute name="style"><xsl:value-of select="@style" /></xsl:attribute></xsl:if>
			<xsl:choose>
				<xsl:when test="title"><h4 class="alert-heading"><xsl:value-of select="title" /></h4></xsl:when>
				<xsl:otherwise><xsl:value-of select="." /></xsl:otherwise>
			</xsl:choose>
			<xsl:if test="content">
				<div class="alert-content">
					<xsl:for-each select="content">
						<div><xsl:copy-of select="*" /></div>
					</xsl:for-each>
				</div>
			</xsl:if>
		</div>
	</xsl:template>

	<!-- ================================================================================================================== -->
	<xsl:template mode="copy-no-ns" match="*">
	  <xsl:element name="{name(.)}">
	    <xsl:copy-of select="@*" />
	    <xsl:apply-templates mode="copy-no-ns"/>
	  </xsl:element>
	</xsl:template>

	<xsl:template match="*" mode="copy">
		<xsl:for-each select="node()">
			<xsl:choose>
				<xsl:when test="name()=''"><xsl:value-of select="."/></xsl:when>
				<xsl:when test="name()='br'"><xsl:element name="br"/></xsl:when>
				<xsl:otherwise>
					<xsl:element namespace="http://www.w3.org/1999/xhtml" name="{name()}">
						<xsl:for-each select="@*"><xsl:attribute name="{name()}"><xsl:value-of select="."/></xsl:attribute></xsl:for-each>
						<xsl:apply-templates select="." mode="copy"/>
					</xsl:element></xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="text-match">
		<xsl:param name="text" />
		<xsl:param name="pattern" />
		<xsl:param name="pattern2" /> <!-- pontig illeszkedik a végére -->
		<xsl:param name="pattern3" /> <!-- ponttól az elejére -->
		<xsl:variable name="ltext" select="translate($text, $uppercase, $smallcase)" />
		<xsl:variable name="p2" select="substring-before($pattern2, '.')" />
		<xsl:variable name="p3" select="substring-after($pattern3, '.')" />
		<xsl:choose>
			<xsl:when test="$pattern!='' and contains($text, $pattern)">
				<xsl:value-of select="substring-before($text, $pattern)" />
				<u><xsl:value-of select="$pattern" /></u>
				<xsl:value-of select="substring-after($text, $pattern)" />
			</xsl:when>
			<xsl:when test="$pattern!='' and contains($ltext, $pattern)">
				<xsl:variable name="pre" select="string-length(substring-before($ltext, $pattern))" />
				<xsl:value-of select="substring($text, 1, $pre)" />
				<u class="i"><xsl:value-of select="substring($text, $pre+1, string-length($pattern))" /></u>
				<xsl:value-of select="substring($text, $pre+string-length($pattern)+1)" />
			</xsl:when>
			<xsl:when test="$p2!='' and contains($text, $p2) and substring-after($text, $p2)=''">
				<xsl:value-of select="substring-before($text, $p2)" />
				<u><xsl:value-of select="$p2" /></u>
			</xsl:when>
			<xsl:when test="$p3!='' and contains($text, $p3) and substring-before($text, $p3)=''">
				<u><xsl:value-of select="$p3" /></u>
				<xsl:value-of select="substring-after($text, $p3)" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$text" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
