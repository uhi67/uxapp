<?xml version="1.0" encoding="UTF-8"?>
<!-- message.xsl -->
<!-- Az egyszerű rendszerüzenet (pl. hibaüzenet) oldala -->
<!-- Szerkezetileg a page-xsl-eknek felel meg -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="xml" version="1.0" indent="yes"
		doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
		doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
		omit-xml-declaration="yes"
	 />

	<xsl:template match="content">
		<!-- A flash üzenetek a design.xsl-ben már remélhelőleg megjönnek -->
		<!-- Az exception üzenetet nem keverjük a flash-sel, azt mindenképpen megjelenítjük -->
		<xsl:apply-templates select="message" />

		<div class="content-block">
			<xsl:apply-templates select="/data/control/info" />
		</div>
		<xsl:apply-templates select="/data/control/url" />
	</xsl:template>

	<xsl:template match="h3">
		<h3><xsl:value-of select="text()" disable-output-escaping="yes"/></h3>
	</xsl:template>

	<xsl:template match="info">
		<p><xsl:value-of select="text()" disable-output-escaping="yes"/></p>
	</xsl:template>

	<xsl:template match="url">
		<ul class="nav">
			<li class="nav-item"><a class="nav-link btn btn-primary" href="{.}">Tovább</a></li>
		</ul>
	</xsl:template>

	<xsl:template match="message">
		<div class="{@class}">
			<h3><xsl:value-of select="title"/></h3>
			<xsl:for-each select="content">
				<div class="msg-content">
					<xsl:copy-of select="text()"/>
					<xsl:copy-of select="*"/>
				</div>
			</xsl:for-each>
		</div>
	</xsl:template>

</xsl:stylesheet>
