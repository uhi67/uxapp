﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" version="1.0" indent="yes"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
 /> 

<xsl:template match="*" mode="syntax" >
	<xsl:param name="level" select="1"/>
	<xsl:text>&lt;</xsl:text>
	<b><xsl:value-of select="name()" /></b>

	<!-- namespace -->
	<xsl:if test="namespace-uri()!=namespace-uri(..)">
		<xsl:variable name="prefix">
			<xsl:if test="contains(name(), ':')">:<xsl:value-of select="substring-before(name(), ':')" /></xsl:if>
		</xsl:variable>
		<xsl:text> </xsl:text>
  		xmlns<xsl:value-of select="$prefix" />="<i style="color:#93C"><xsl:value-of select="namespace-uri()" /></i>"
    	<xsl:text> </xsl:text>	
	</xsl:if>
	
	<xsl:for-each select="@*" >
		<xsl:text> </xsl:text>
		<span style="color:#f63"><xsl:value-of select="name()" /></span>="<span style="color:#00F"><xsl:value-of select="." /></span><xsl:text>"</xsl:text>
	</xsl:for-each>
	<!-- [<xsl:value-of select="count(*)"/>, <xsl:value-of select="count(text())"/>] -->
	<xsl:if test="count(*)=0 and count(text())=0">
		<xsl:text> /&gt;</xsl:text><br />
	</xsl:if>
	<xsl:if test="count(*)=0 and count(text())>0">
		<xsl:text>&gt;</xsl:text>
		<xsl:value-of select="text()"/>
		<span style="color:#999">
		<xsl:text>&lt;/</xsl:text>
		<xsl:value-of select="name()" />
		<xsl:text>&gt;</xsl:text><br />
		</span>
	</xsl:if>
	<xsl:if test="count(*)>0">
		<xsl:text>&gt;</xsl:text>
			<div class="indent"> <!-- style="margin-left:{$level * 10}px" -->
				<xsl:for-each select="* | text()">
					<xsl:apply-templates select="." mode="syntax">
						<xsl:with-param name="level" select="$level + 1" />
					</xsl:apply-templates>
				</xsl:for-each>
			</div>
		<span style="color:#999">
		<xsl:text>&lt;/</xsl:text>
		<xsl:value-of select="name()" />
		<xsl:text>&gt;</xsl:text><br />
		</span>
	</xsl:if>
</xsl:template>

<xsl:template match="text()" mode="syntax">
	<span style="color:#063"><xsl:value-of select="."/></span>
	<xsl:if test="normalize-space(.)!=''"><br /></xsl:if>
</xsl:template>

</xsl:stylesheet>
