﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns="http://www.w3.org/1999/xhtml">
<xsl:output method="xml" version="1.0" indent="yes"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
 />
<!-- viewnotfound.xsl -->

<xsl:template match="content">
	<div id="popup-content">
		<h2>Hiba</h2>
		<p class="error">A nézetfájl nem található. / View not found.</p>
		<p>(<xsl:value-of select="/data/head/original-view"/>)</p>
		<p>A keresett funkció még valószínűleg nem készült el, türelmedet kérem.</p>
	</div>
</xsl:template>

</xsl:stylesheet>
