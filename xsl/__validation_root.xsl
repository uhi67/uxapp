<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="html" version="1.0" indent="yes"
				doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
				doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
				omit-xml-declaration="yes"
	/>
	<!-- Validation root is to supress unresolved variable errors in code inspection -->

	<xsl:variable name="control" select="/data/control" />
	<xsl:variable name="params" select="/data/control/params" />
	<xsl:variable name="content" select="/data/content" />
	<xsl:variable name="head" select="/data/head" />
	<xsl:variable name="script" select="/data/@script" />
	<xsl:variable name="act" select="$control/act" />
	<xsl:variable name="id" select="$control/id" />
	<xsl:variable name="user" select="/data/head/user" />

	<xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyzáéíóöőúüű'" />
	<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÖŐÚÜŰ'" />

	<!-- Include here all affected files -->

	<xsl:include href="message.xsl" />

</xsl:stylesheet>