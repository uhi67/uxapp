﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" version="1.0" indent="yes"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
 /> 

<xsl:include href="copy.inc.xsl" />

<xsl:template match="/">
<html>
<body>
    <style>
        div.indent {margin-left:15px;}
    </style>
	<xsl:apply-templates select="*" mode="syntax"/>
</body>
</html> 
</xsl:template>

</xsl:stylesheet>
