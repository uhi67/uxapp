﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:include href="copy.inc.xsl" />

<xsl:template match="/">
	<html>
		<body>
			<style>
				div.indent {margin-left:15px;}
			</style>

			<div id="popup-content">
				<h2>Application error / Alkalmazás hiba</h2>
				<p class="error">xsl file not found: (<xsl:value-of select="/data/@xsl"/>)</p>
				<div>
					<code>
						<xsl:apply-templates select="data" mode="syntax" />
					</code>
				</div>
			</div>
		</body>
	</html>
</xsl:template>

</xsl:stylesheet>
