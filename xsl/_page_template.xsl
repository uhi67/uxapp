<?xml version="1.0" encoding="UTF-8"?>
<!-- _page_template.xsl -->
<!-- Minta a _page.xsl render xsl-ek generálásához -->
<!--suppress XmlPathReference -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.org/1999/xhtml">
<xsl:output method="xml" version="1.0" indent="yes"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
 />
 
<!-- regisztrált modulok és XSL-ek (/data/head/xsl alapján) -->
<xsl:include href="../../../UApp/modules/Main/main.xsl" />
<xsl:include href="../../../UApp/modules/UList/UList.xsl" />
<!-- Design (config/design alapján, design property felülbírálható  -> /data/head/design) -->
<xsl:include href="../modules/simpledesign/design.xsl" />
<!-- Nézet (view alapján -> /data/head/view) -->
<xsl:include href="index.xsl" />

</xsl:stylesheet>
