<?php
namespace uhi67\uxapp\commands;

use Exception;
use ReflectionException;
use uhi67\uxapp\Assertions;
use uhi67\uxapp\UXApp;
use uhi67\uxapp\UXAppCommand;
use uhi67\uxapp\Util;
use uhi67\uxapp\UXAppException;
use uhi67\uxml\UXMLDoc;

/** @noinspection PhpUnused */

/**
 * # Class CreateCommand
 *
 * Usage in CLI: `php uxapp create model tablename`
 *
 * @package uhi67\uxapp\commands
 */
class CreateCommand extends UXAppCommand {

	public static function descriptions() {
		return [
			'model' => 'Create model class from database table. Include o=1 option to overwrite existing model class file.',
			'app' => 'Initialize application creating minimum required directories and files.'
		];
	}

	/**
	 * @throws UXAppException
	 */
	function prepare() {
		parent::prepare();
		$this->db = $this->app->db;
		Assertions::assertClass($this->app, UXApp::class, false);
	}

	/** @noinspection PhpUnused */
	/**
	 * @param $params
	 * @return int
	 * @throws ReflectionException
	 * @throws UXAppException
	 * @throws Exception
	 */
	public function actModel($params) {
		if(!$this->app->db) throw new UXAppException('No database connection defined');
		if(!isset($params[0])) { echo 'Missing table name'; return UXApp::EXIT_STATUS_CONFIG_ERROR; }
		$tableName = strtolower($params[0]);
		$modelName = Util::camelize($tableName, true);
		$nameSpace = 'app\models';
		$fileName = $this->app->rootPath.'/models/'.$modelName.'.php';
		if(class_exists($nameSpace.'\\'.$modelName) && !$this->param('o')) {
			echo "Model $modelName already exists, skipping\n";
			return UXApp::EXIT_STATUS_ABORT;
		}

		echo "Filename is $fileName\n";
		
		$metadata = $this->db->getMetaData($tableName);
		$foreign_keys = $this->db->getForeignKeys($tableName);
		$referrers = $this->db->getReferrerKeys($tableName);
		$genPath = $this->getModulePath('uxapp').'/def/gen';

		$xmldoc = new UXMLDoc('UTF-8', 'data');
		$xmldoc->documentElement->setAttribute('date', date('Y-m-d'));
		$node_table = $xmldoc->documentElement->addNode('table', [
			'name' => $tableName,
			'model-class' => $modelName,
			'namespace' => $nameSpace,
		]);
		if(!is_array($metadata)) throw new UXAppException("Invalid metadata for '$tableName': ".gettype($metadata), $metadata);
		foreach($metadata as $column => $data) {
			$node_table->addNode('column', array_merge($data, [
				'php-name' => Util::camelize($column),
				'php-type' => $this->db->mapType($data['type']),
				'label' => ucwords(str_replace('_', ' ', $column)),
			]), $column);
		}
		if($foreign_keys) foreach($foreign_keys as $constraint => $data) {
			$column_name = $data['column_name'];
			$node_table->addNode('reference', array_merge($data, [
				'name' => substr($column_name, strlen($column_name)-3) =='_id' ? substr($column_name, 0, -3) : $column_name.'1',
				'foreign_model' => Util::camelize($data['foreign_table'], true),
				'foreign_field' => Util::camelize($data['foreign_column']),
			]), $constraint);
		}

		// referrers?
		if($referrers) foreach($referrers as $constraint => $data) {
			#$column_name = $data['column_name'];
			$node_table->addNode('referrer', array_merge($data, [
				'name' => Util::camelize($data['remote_table']),
				'remote_model' => Util::camelize($data['remote_table'], true),
				'remote_field' => Util::camelize($data['remote_column']),
			]), $constraint);
		}

		$result = $xmldoc->process($genPath.'/model.xsl', null, true);
		// TODO: <!DOCTYPE eltüntetése
		if(file_put_contents($fileName, $result)) {
			echo "Model $modelName created succesfully\n";
			return UXApp::EXIT_STATUS_OK;
		}
		echo "creating model $modelName failed\n";
		return UXApp::EXIT_STATUS_ERROR;
	}

	/**
	 * Initializes a new application from scratch
	 *
	 * Using: after installing `uhi67/uxapp` with composer, run `composer exec uxapp create app`
	 *
	 * Creates the most necessary directoriees and files using built-in templates
	 *
	 * @return int
	 */
	public function actApp() {
		$dir = dirname(__DIR__, 4);
		$files = [
			'runtime/',
			'www/assets/',
			'uxapp',
			'www/index.php',
			'www/.htaccess',
			'.gitignore',
			'config/command.php',
			'config/config.php',
			'xsl/basic.xsl',
			'pages/StartPage.php',
			'xsl/start.xsl',
			'sql/'
		];
		$source = dirname(__DIR__).'/template';
		foreach($files as $file) {
			$target = $dir.'/'.$file;
			if(!file_exists($target)) {
				if(substr($file, -1) == '/') {
					echo "Creating directory $dir\n";
					mkdir($target, 0774, true);
				}
				else {
					if(!file_exists(dirname($target))) mkdir(dirname($target), 0774, true);
					echo "Copying file $file\n";
					copy($source . '/' . $file, $target);
				}
			}
		}
		echo "Application is prepared.\n";
		return UXApp::EXIT_STATUS_OK;
	}
}
