<?php

namespace uhi67\uxapp\commands;

/**
 * indexcommand.php
 *
 * Implements the default command
 *
 * Using: php uxapp
 *
 * @package UXApp
 * @author Peter Uherkovich
 * @copyright 2019
 */

use Exception;
use ReflectionException;
use uhi67\uxapp\Ansi;
use uhi67\uxapp\ArrayUtils;
use uhi67\uxapp\UXApp;
use uhi67\uxapp\UXAppCommand;
use uhi67\uxapp\Util;
use uhi67\uxapp\UXAppException;


/**
 * # IndexCommand
 *
 * Lists available commands in UAppp and application commands dir.
 *
 * ## Usage in command line:
 * ```sh
 * $ ./uxapp
 * ```
 */
class IndexCommand extends UXAppCommand {
	/**
	 * Default action: List commands
	 *
	 * @return int
	 * @throws ReflectionException
	 * @throws UXAppException
	 * @throws Exception
	 */
	public function actDefault() {
		$name = Util::coalesce($this->app->title, 'UXApp');
		$ver = $this->app->ver;
		echo Ansi::color($name. ' version '.$ver, 'green')."\n";
		echo "Index of commands\n";
		$apppath = $this->app->rootPath;
		$uapppath = $this->getModulePath(UXApp::class);
		$commands = [];

		// UXApp commands
		$dir = $uapppath.'/commands';
		$dh = opendir($dir);
		if(!$dh) throw new Exception("Invalid dir $dir");
		while (($file = readdir($dh)) !== false) {
			if(filetype($dir.'/'.$file)=='file' && preg_match('/^(\w+)Command.php/', $file, $m)) {
				$command = Util::uncamelize($m[1]);
				$className = 'uhi67\uxapp\commands\\'.$m[1].'Command';
				$commands[$command] = $className;
			}
		}
		closedir($dh);

		// User commands (overwrites UXApp commands)
		$dir = $apppath.'/commands';
		if(is_dir($dir)) {
			$dh = opendir($dir);
			if(!$dh) throw new Exception("Invalid dir $dir");
			while (($file = readdir($dh)) !== false) {
				if(filetype($dir.'/'.$file)=='file' && preg_match('/^(\w+)Command.php/', $file, $m)) {
					$command = Util::uncamelize($m[1]);
					$className = 'app\commands\\'.$m[1].'Command';
					$commands[$command] = $className;
				}
			}
			closedir($dh);
		}

		foreach($commands as $command=>$className) {
			echo '- ', Ansi::color($command, 'blue')." \n\tActions:\n";
			$methods = get_class_methods($className);
			$descriptions = is_callable([$className, 'descriptions']) ? call_user_func([$className, 'descriptions']) : [];
			foreach($methods as $method) {
				if(preg_match('/^act([A-Z]\w+)/', $method, $m) && $m[1]!='Default') {
					$action = Util::uncamelize($m[1], '-');
					$description = ArrayUtils::getValue($descriptions, $action);
					echo Ansi::color("\t- ".sprintf('%-12s', $action), 'green')."\t$description\n";
				}
			}
		}
		return UXApp::EXIT_STATUS_OK;
	}
}
