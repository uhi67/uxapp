<?php

namespace uhi67\uxapp\commands;

use uhi67\uxapp\Ansi;
use uhi67\uxapp\Util;
use uhi67\uxapp\UXApp;
use uhi67\uxapp\UXAppCommand;

/**
 * EmptycacheCommand
 *
 * @package UApp
 * @author Peter Uherkovich
 * @copyright 2018
 */
class EmptycacheCommand extends UXAppCommand {
	function initUser() {}
	function loadData() {}

	/**
	 * Default action: help
	 *
	 * @return void
	 */
	public function actDefault() {
		echo Ansi::color("Emptycache command", 'green')."\n";
		echo "Using:
- emptycache xsl
";
        return UXApp::EXIT_STATUS_OK;
	}

	public static function descriptions() {
		return [
			'xsl' => 'Empty cache of generated xsl files.',
		];
	}

	/**
	 * @throws \Exception
	 */
	public function actXsl() {
		$genxslpath = $this->app->dataPath.'/xsl'; // Ahová a generált fájlok kerülnek
		echo Ansi::color("Emptying `$genxslpath`", 'green')."\n";

		if ($handle = opendir($genxslpath)) {
			$n = 0;
			while (false !== ($entry = readdir($handle))) {
				if ($entry != "." && $entry != "..") {
					$ext = Util::ext($entry);
					if($ext=='xsl') {
						unlink($genxslpath.'/'.$entry);
						echo Ansi::color("File `$entry` is deleted", 'light blue')."\n";
						$n++;
					}
				}
			}
			echo Ansi::color("$n files has been deleted", 'green')."\n";
			closedir($handle);
		}
        return UXApp::EXIT_STATUS_OK;
	}
}
