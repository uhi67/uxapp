<?php /** @noinspection PhpIllegalPsrClassPathInspection */
/** @noinspection PhpMultipleClassesDeclarationsInOneFile */

/** @noinspection PhpUnused */

namespace uhi67\uxapp\commands;

use Throwable;
use uhi67\uxapp\Migration;
use DateTime;
use Exception;
use ReflectionException;
use uhi67\uxapp\Ansi;
use uhi67\uxapp\ArrayUtils;
use uhi67\uxapp\Assertions;
use uhi67\uxapp\DBX;
use uhi67\uxapp\exception\NotFoundException;
use uhi67\uxapp\Model;
use uhi67\uxapp\Query;
use uhi67\uxapp\UXApp;
use uhi67\uxapp\UXAppCommand;
use uhi67\uxapp\Util;
use uhi67\uxapp\UXAppException;

/**
 * MigrateCommand
 *
 * Updates database using db_xxx.sql or php files in sql directory
 * Maintains database state in updatedb table (creates if not exists)
 *
 * Using: `php uxapp migrate action arguments`
 * Run `php uxapp migrate` for available actions
 *
 * Syntax of db_xxx files:
 * - standard SQL commands
 * - you may use `{$name}` params, they will be substituted from connection params.
 * - recommended to use `db_$creationdate_$purpose.sql` name convention for these files.
 *
 * Configuration:
 *
 * Migrate command uses 'updateDb' connection, if exists, default is 'db' connection.
 *
 * Configure the following parameters in database connection settings (in updateDb or db):
 * - initfile (default is db_update_2017_0101_updatedb.sql, located in UXApp/sql)
 * - tablename (default is 'updatedb')
 *
 * @package UXApp
 * @author Peter Uherkovich
 * @copyright 2019
 */
class MigrateCommand extends UXAppCommand {
	/** @var string $updateTable -- the name of database update tracking table */
	private $updateTable;
	/** @var string $sqlPath -- the  path of sql update files */
	private $sqlPath;
	/** @var string $initFile -- name of updatedb table initializing file */
	private $initFile;

	public static function descriptions() {
		return [
			'update' => 'Update database to last state.',
			'init' => 'Initialize migrate function, creates the table storing applied database updates - force deletes existing one.',
			'status' => 'Display update status',
			'admit' => 'Acknowledge updates without actually applying them (preg pattern)',
			'revoke' => 'Revoke updates from registry only, but not their effect!',
			'last' => 'List last n applied updates (optionally filtered by pattern)',
			'reset' => 'Reset (deletes) the whole database, all tables and routines',
		];
	}

	public static function params() {
		return [
			'init' => ['[force]' => 'Empty existing table'],
			'status' => [],
			'update' => ['[verbose]' => '1=show detailed informations'],
			'admit' => ['<patterns>|* preg pattern of migration file names or * for all'],
			'revoke' => ['<patterns>|* preg pattern of migration file names or * for all'],
			'last' => ['<n> number of updates to list', '[<pattern>] filter list'],
		];
	}

	/**
	 * @throws UXAppException
	 */
	function prepare() {
		parent::prepare();
		/** @noinspection PhpUndefinedFieldInspection */
		$this->db = $this->app->hasComponent('updateDb') ? $this->app->updateDb : $this->app->db;
		$params = ArrayUtils::getValue($this->db, 'params', []);

		$this->updateTable = ArrayUtils::getValue($params, 'updateTable', 'updatedb');
		$uxappPath = dirname(__DIR__);
		$this->initFile = ArrayUtils::getValue($params, 'initFile', $uxappPath.'/sql/db_update_2017_0101_updatedb.sql');
		Assertions::assertClass($this->app, UXApp::class, false);
		$this->sqlPath = $this->app->rootPath.'/sql';
		if(!$this->app->db) throw new UXAppException('No database connection defined');
	}

	function registerAssets() {}
	function loadData() {}

	/**
	 * Default action: help
	 *
	 * @return int
	 */
	public function actDefault() {
		echo "Migrate command keeps database changes in sync with source code.
Place plain SQL or PHP migration files into /sql/ directory named db_*.sql\n\n";

		parent::actDefault();

		echo "
Migrate is using database connection named 'updateDb' or the default 'db' connection.

Configure the following parameters in database connection settings (in updateDb or db):
- initfile (default is db_update_2017_0101_updatedb.sql, located in UXApp/sql)
- tablename (default is 'updatedb')

During running an update, migrate replaces all occurences of `{\$var}` to values found in database params. 
\n";
		return UXApp::EXIT_STATUS_OK;
	}

	/**
	 * @return int
	 * @throws
	 */
	public function actStatus() {
		echo Ansi::color("migrate status\n", 'green');
		if(!$this->db->tableExists($this->updateTable)) {
			echo Ansi::color("$this->updateTable table does not exist. Please run migrate init", 'red'), "\n";
			exit(1);
		}

		$query = Query::createSelect([
			'fields' => ['name', 'applied'],
			'from' => $this->updateTable,
			'orders' => [['applied', DBX::ORDER_DESC]],
			'connection' => $this->db,
		]);
		$last = $query->first(); //$this->db->select_assoc('select name, applied from '.$this->db->quoteIdentifier($this->updatetable).' order by applied desc limit 1');
		if($last) {
			/** @var DateTime $applied */
			$applied = $last['applied'];
			$appliedtime = $applied->format('Y-m-d H:i');
			echo "Last updated: {$last['name']} $appliedtime\n";
		}
		else echo "Update database is empty yet.\n";

		// Determine new files
		$dbparams = ArrayUtils::getValue($this->db, 'params', []);
		$pattern = ArrayUtils::getValue($dbparams, 'updatePattern', '/^db_(update_)?/');
		echo "SQL update directory is '$this->sqlPath'. Filename pattern is '$pattern'\n";

		$dh = opendir($this->sqlPath);
		if(!$dh) throw new UXAppException("Invalid dir '$this->sqlPath'");
		$new = [];
		while (($file = readdir($dh)) !== false) {
			if(filetype($this->sqlPath.'/'.$file)=='file' && preg_match($pattern, $file)) {
				$file = pathinfo($file, PATHINFO_FILENAME);

				/** @var DateTime $applied */
				$applied = $last ? Query::createSelect([
					'fields' => ['applied'],
					'from' => $this->updateTable,
					'condition' => ['name'=>'$1'],
					'orders' => [['applied', DBX::ORDER_DESC]],
					'params' => [$file],
					'connection' => $this->db,
				])->scalar() : false;
				//$applied = $this->db->selectvalue('select applied from '.$this->db->quoteIdentifier($this->updatetable).' where name=$1', array($file));

				if(!$applied) $new[] = $file;
			}
		}
		closedir($dh);
		if(empty($new)) {
			echo Ansi::color("No new updates.", 'green'), "\n";
		}
		else {
			sort($new);
			echo Ansi::color(sprintf("There are %d new updates:", count($new)), 'red'), "\n";
			foreach($new as $i=>$n) {
				$max = 4;
				$more = count($new)-$max*2;
				if($i<$max) echo $n."\n";
				else if($i==$max && $i<=count($new)-$max-1) {
					if($more>1) echo sprintf(" ... (%d more) ...\n", $more);
					else echo $n."\n";
				}
				else if($i>count($new)-$max-1) echo $n."\n";
			}
			echo "Use " . Ansi::color('migrate update', 'cyan') . " command to update.\n";
		}
		return UXApp::EXIT_STATUS_OK;
	}

	/**
	 * Admit action
	 * Registers given update(s) into database without actually applying them.
	 *
	 * @param array $params
	 *
	 * @return int
	 * @throws UXAppException|ReflectionException
	 */
	public function actAdmit(array $params) {
		$all = false;
		echo Ansi::color("migrate admit\n", 'green');
		foreach($params as $i=>$pattern) {
			if($pattern=='*') {
				$all=true;
				unset($params[$i]);
			}
		}

		// Determine new files mathcing patterns
		$dh = opendir($this->sqlPath);
		if(!$dh) throw new UXAppException("Invalid dir $this->sqlPath");
		$new = [];
		while (($file = readdir($dh)) !== false) {
			if(filetype($this->sqlPath.'/'.$file)=='file' && preg_match('/^db_(update_)?/', $file)) {
				$file = pathinfo($file, PATHINFO_FILENAME);
				$applied = Query::createSelect([
					'fields' => ['applied'],
					'from' => $this->updateTable,
					'condition' => ['name'=>'$1'],
					'params' => [$file],
					'connection' => $this->db,
				])->scalar();
					//$this->db->selectvalue('select applied from '.$this->db->quoteIdentifier($this->updatetable).' where name=$1', array($file));
				if(!$applied) {
					// check pattern match
					$m = $all;
					#$re = !(@preg_match($pattern, null)===false);
					foreach($params as $pattern) {
						$re = Util::is_valid_preg_pattern($pattern); //!(@preg_match($pattern, null)===false);
						$m = $m || ($re && preg_match($pattern, $file)) || (strpos($file, $pattern)!==false);
					}
					if($m) $new[] = $file;
				}
			}
		}
		closedir($dh);

		// Execute admit
		if(empty($new)) {
			echo Ansi::color("No new matching updates.", 'green'), "\n";
		}
		else {
			sort($new);
			echo Ansi::color(sprintf("There are %d matching new updates:", count($new)), 'brown'), "\n";
			foreach($new as $n) {
				$u = new Updatedb([
					'name' => $n,
					'applied' => new DateTime(),
				]);
				$u->db = $this->db;
				if(($u->save())) echo "Update ". $n." has been admitted.\n";
				else echo "Admitting ". $n." failed\n";
			}
		}
		return UXApp::EXIT_STATUS_OK;
	}

	/**
	 * This action revokes updates from registry.
	 * Howewer, it does not revoke the effect of the commited updates.
	 *
	 * @return int
	 * @throws UXAppException|ReflectionException
	 */
	public function actRevoke() {

		$params = UXApp::$app->argn;

		$all = false;
		$connection = $this->db;
		echo Ansi::color("migrate revoke\n", 'green');
		foreach($params as $i=>$pattern) {
			if($pattern=='*') {
				$all=true;
				unset($params[$i]);
			}
		}

		// Determine registered files mathcing patterns
		if($all) {
			Updatedb::deleteAll(['true']);
			echo Ansi::color("All updates has been revoked.\n", 'ligh purple');
			exit;
		}

		$condition = array_merge(['or'], array_map(function($p) use($connection) {
			return ['rlike', 'name', $connection->literal($p)];
		}, $params));
		$updates = Updatedb::all($condition);
		$n = count($updates); $c = 0;

		/** @var Updatedb $upd */
		foreach($updates as $upd) {
			echo $upd->name."\n";
			$upd->delete();
		}

		echo Ansi::color("$c/$n updates has been revoked.\n", 'light cyan');
		return UXApp::EXIT_STATUS_OK;
	}

	/**
	 * This action lists latest n applied updates from registry.
	 * Optionally filtered by a pattern
	 *
	 * @return int
	 * @throws UXAppException|ReflectionException
	 */
	public function actLast() {
		$params = UXApp::$app->argn;
		$n = array_shift($params);
		if(!$n || !is_numeric($n)) {
			$pattern = $n;
			$n = 12;
		}
		else $pattern = array_shift($params);

		$connection = $this->db;
		echo Ansi::color("migrate last $n $pattern\n", 'green');

		$condition = $pattern ? ['rlike', 'name', $connection->literal($pattern)] : null;
		$query = Query::createSelect('Updatedb', null, null, $condition, [['applied', 'desc']], $n, null, null, $connection);
		$updates = $query->all();
		foreach($updates as $upd) {
			echo $upd['name']."\n";
		}
		return UXApp::EXIT_STATUS_OK;
	}

	/**
	 * Update action
	 * Applies all new updates and registers them into database.
	 * Selective update is currently not available.
	 * Substitutes {$name} parameters from config/updatedb
	 *
	 * Options
	 * - verbose=0 -- silent (errors only)
	 * - verbose=1 -- display title and number of updates only + warnings
	 * - verbose=2 -- display applied updates (default)
	 * - verbose=3 -- display SQL commands
	 *
	 * @param array $params -- more command line parameters after action word, later may be considered as filename patterns
	 *
	 * @return int -- command status code, 0 = success
	 * @throws UXAppException|ReflectionException
	 */
	public function actUpdate(/** @noinspection PhpUnusedParameterInspection */ array $params) {
		$verbose = $this->param('verbose', 2);
		if($verbose) echo Ansi::color("migrate update\n", 'light blue');

		// Checking update table
		if(!$this->db->tableExists('updatedb')) {
			if ($result = $this->actInit([])) return $result;
		}

		// Determine new files
		$sqlpath = $this->db->migrationPath ?: $this->app->rootPath.'/sql';
		$dh = opendir($sqlpath);
		if(!$dh) throw new UXAppException("Invalid dir $sqlpath");
		$new = [];
		while (($file = readdir($dh)) !== false) {
			if(filetype($sqlpath.'/'.$file)=='file' && preg_match('/^db_(update_)?/', $file)) {
				$name = pathinfo($file, PATHINFO_FILENAME);
				$applied = Query::createSelect([
					'fields' => ['applied'],
					'from' => $this->updateTable,
					'condition' => ['name'=>'$1'],
					'params' => [$name],
					'connection' => $this->db,
				])->scalar();
				//$applied = $this->db->selectvalue('select applied from '.$this->db->quoteIdentifier($this->updatetable).' where name=$1', array($name));
				if(!$applied) $new[] = $file;
			}
		}
		closedir($dh);

		$dbparams = ArrayUtils::getValue($this->db, 'params');

		// Execute new updates
		if(empty($new)) {
			if($verbose) echo Ansi::color("Everything is up to date!", 'green'), "\n";
			return UXApp::EXIT_STATUS_OK;
		}
		sort($new);
		if($verbose) echo Ansi::color(sprintf("There are %d new updates:", count($new)), 'blue'), "\n";
		$n = 0;
		foreach($new as $file) {
			$name = pathinfo($file, PATHINFO_FILENAME);
			$ext = pathinfo($file, PATHINFO_EXTENSION);
			$filename = $sqlpath.'/'.$file;
			try {
				if($ext=='php') {
					if($verbose>1) echo Ansi::color("Applying $name from $filename\n", 'blue');
					$success = false;

					// Run php file
					try {
						/** @var Migration $className */
						$className = 'app\sql\\'.$name;
						/** Migration $migration */
						$migration = new $className(['app'=>$this->app, 'db'=>$this->app->db]);
						$migration->up();
						$success = true;
					}
					catch(Throwable $e) {
						if($verbose) echo sprintf('%s in file %s at line %s', $e->getMessage(), $e->getFile(), $e->getLine()), PHP_EOL;
                        if($e instanceof UXAppException) echo Ansi::color($e->getExtra(), 'purple'), PHP_EOL;
						if($verbose>1) debug_print_backtrace();
					}
					if($success) {
						$u = new Updatedb([
							'name' => $name,
							'applied' => new DateTime(),
						]);
						$u->db = $this->db;
						if(!($u->save())) echo Ansi::color("Registering update " . $n . " failed.", 'red') . "\n";
						else $n++;
					} else {
                        echo Ansi::color("Applying " . $name . " failed", 'red') . "\n";
                        return 2;
                    }
				}
				else {
					if($verbose>1) echo Ansi::color("Applying $name from $filename\n", 'cyan');
					$sql = file_get_contents($filename);
					$sql = Util::substitute($sql, $dbparams);
					if($verbose>2) echo Ansi::color("$sql\n", 'gray');
					$rs = $this->db->query($sql); //query($sql);
					if($rs) {
						if($verbose>1) echo Ansi::color("Update " . $name . " has been applied.", 'green') . "\n";
						$data = pg_fetch_assoc($rs);
						if($data) UXApp::trace(Util::objtostr($data));

						$u = new Updatedb([
							'name' => $name,
							'applied' => new DateTime(),
						]);
						$u->db = $this->db;
						if(!($u->save())) echo Ansi::color("Registering update " . $n . " failed.", 'red') . "\n";
						else $n++;
					} else echo Ansi::color("Applying " . $name . " failed", 'red') . "\n";
					$this->db->free($rs);
				}
			}
			catch(Throwable $e) {
				echo Ansi::color("Applying ". $name." failed", 'red'), PHP_EOL;
				echo Ansi::color($e->getMessage(), 'light purple'), PHP_EOL;
                if($verbose>2 && $e instanceof UXAppException) echo Ansi::color($e->getExtra(), 'purple'), PHP_EOL;
                return 1;
			}
		}
		if($n==0 && $verbose) echo Ansi::color("No updates applied", 'brown'), "\n";
		else if($verbose>1) echo Ansi::color("$n update(s) applied.", 'cyan'), "\n";
        return UXApp::EXIT_STATUS_OK;
	}

	/**
	 * @param $params -- all non-named parameters after action word (except options in form 'name=value')
	 * @return int
	 * @throws NotFoundException
	 * @throws UXAppException
	 */
	public function actInit($params) {
		echo Ansi::color("migrate init\n", 'green');
		$force = in_array('force', $params);
		if(!$this->db->tableExists($this->updateTable) || $force) {
			if($this->db->tableExists($this->updateTable)) {
				$sql = "drop table " . $this->db->quoteIdentifier($this->updateTable);
				$rs = $this->db->execute($sql);
				if(!$rs) {
					echo "Deleting '".$this->db->quoteIdentifier($this->updateTable)."' table failed.\n";
				}
				else {
					echo "'".$this->db->quoteIdentifier($this->updateTable)."' table deleted.\n";
				}
			}
			$initfile = preg_match('~[/\\\\]~', $this->initFile) ? $this->initFile : $this->sqlPath.'/'.$this->initFile;
			if(!file_exists($initfile)) throw new NotFoundException("Init file ($initfile) not found");
			$sql = file_get_contents($initfile);
			$params = ArrayUtils::getValue($this->db, 'params', []);
			if(!isset($params['user'])) $params['user'] = $this->db->user;
			if(!isset($params['user'])) throw new UXAppException('Please specify table-user in `config/updatedb/user`.');
			if(!isset($params['owner'])) $params['owner'] = $params['user'];
			if(!isset($params['tablename'])) $params['tablename'] = $this->updateTable;
			$sql = Util::substitute($sql, $params);
			$rs = $this->db->execute($sql);
			if(!$rs) {
				echo "Creating '".$this->db->quoteIdentifier($this->updateTable)."' table failed.\n";
                return 2;
			}
			else {
				echo "'".$this->db->quoteIdentifier($this->updateTable)."' table created successfully.\n";
                return UXApp::EXIT_STATUS_OK;
			}
		} else {
			echo "'".$this->db->quoteIdentifier($this->updateTable)."' table already exists.\n";
            return 3;
		}
	}

	/**
	 * Resets (deletes) the whole database, all tables and routines
	 *
	 * Options
	 * - verbose=0 -- silent (errors only)
	 * - verbose=1 -- display title and number of updates only + warnings
	 * - verbose=2 -- display applied updates (default)
	 * - verbose=3 -- display SQL commands
	 *
	 * - confirm=yes -- suppress confirmation input (auto-confirm)
	 *
	 * @return int
	 * @throws UXAppException
	 */
	public function actReset() {
		$verbose = $this->param('verbose', 2);
		if($this->param('confirm')!='yes') {
			if(!$this->confirm('Are you sure to delete the whole database?')) return UXApp::EXIT_STATUS_ABORT;
		}
		$name = $this->db->name;
		if($verbose) echo "Deleting database '$name'...\n";
		$this->truncateDatabase($verbose);
		return UXApp::EXIT_STATUS_OK;
	}

	/**
	 * @param int $verbose -- see actReset
	 *
	 * @throws UXAppException
	 */
	protected function truncateDatabase(int $verbose) {
		$db = $this->db;
		$tables = $db->getSchemaMetadata();
		if($verbose>2) echo 'Tables: ', json_encode(array_keys($tables)), PHP_EOL;

		// First drop all foreign keys,
		foreach ($tables as $tableName=>$schema) {
			$foreignKeys = $this->db->getForeignKeys($tableName);
			if($foreignKeys) {
				foreach ($foreignKeys as $name => $foreignKey) {
					$db->dropForeignKey($name, $tableName);
					if($verbose>1) echo "Foreign key $name dropped.\n";
				}
			}
		}

		// drop the tables:
		if($verbose>2) echo "Drop tables...\n";
		foreach ($tables as $tableName => $schema) {
			try {
				if($verbose>2) echo "Drop table '$tableName'...\n";
				$db->dropTable($tableName);
				if($verbose>1) echo "Table $tableName dropped.\n";
			} catch (Exception $e) {
				if ($this->isViewRelated($message = $e->getMessage())) {
					$db->dropView($tableName);
					if($verbose>1) echo "View $tableName dropped.\n";
				} else {
					echo "Cannot drop table '$tableName': $message .\n";
				}
			}
		}

		// Drop functions from public schema (exclude pg_catalog)
		$functions = $db->getRoutines();
		foreach($functions as $functionName) {
			if($verbose>1) echo "Dropping $functionName\n";
			if($db->dropRoutine($functionName)) {
				if($verbose>1) echo "$functionName dropped.\n";
			}
			else echo "Cannot drop '$functionName'.\n";
		}

		// TODO: drop triggers

		// Drop sequences
		$sequences = $db->getSequences();
		foreach($sequences as $sequenceName) {
			$db->dropSequence($sequenceName);
			if($verbose>1) echo "Sequence $sequenceName dropped.\n";
		}
	}

	/**
	 * Determines whether the error message is related to deleting a view or not
	 *
	 * @param string $errorMessage
	 * @return bool
	 */
	private function isViewRelated(string $errorMessage) {
		$dropViewErrors = [
			'DROP VIEW to delete view', // SQLite
			'SQLSTATE[42S02]', // MySQL
		];

		foreach ($dropViewErrors as $dropViewError) {
			if (strpos($errorMessage, $dropViewError) !== false) {
				return true;
			}
		}

		return false;
	}

}

/**
 * Updatedb table model
 *
 * @property int $id
 * @property string $name
 * @property DateTime $applied
 */
if(!class_exists(Updatedb::class)) {
	class Updatedb extends Model {
	}
}
