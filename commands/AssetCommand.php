<?php

namespace uhi67\uxapp\commands;

use Exception;
use uhi67\uxapp\Ansi;
use uhi67\uxapp\Util;
use uhi67\uxapp\UXApp;
use uhi67\uxapp\UXAppCommand;

/**
 * AssetCommand
 *
 * @package UXApp
 * @author Peter Uherkovich
 * @copyright 2018
 */
class AssetCommand extends UXAppCommand {
	function initUser() {}
	function loadData() {}

	public static function descriptions() {
		return [
			'clear' => 'Clear assets cache directory',
			'xsl' => 'Empty cache of generated xsl files.',
		];
	}

	/**
	 * Default action: help
	 *
	 * @return int
	 */
	public function actDefault() {
		echo Ansi::color("Asset command", 'green')."\n";
		$assetManager = $this->app->assetManager;
		if(!$assetManager) {
			echo Ansi::color("Warning: no assetManager configured.\n", 'yellow');
			return UXApp::EXIT_STATUS_CONFIG_ERROR;
		}
		$assetPath = $assetManager->cacheDir;
		echo "Using:
- asset clear -- clears all copied assets from $assetPath 
- asset xsl -- Empty cache of generated xsl files
";
		return UXApp::EXIT_STATUS_OK;
	}

	/**
	 * Clear assets cache directory
     *
     * Options:
     *  - verbose=n (0=silent, 1=summary (default), 2=detailed)
     *
	 * @return int
	 * @throws Exception
	 */
	public function actClear($params) {
        $verbose = $this->param('verbose', 1);
		$assetManager = $this->app->assetManager;
		if(!$assetManager) {
			echo Ansi::color("Warning: no assetManager configured.\n", 'yellow');
			return UXApp::EXIT_STATUS_CONFIG_ERROR;
		}
		$assetPath = $assetManager->cacheDir;
		if($verbose>1) echo Ansi::color("Emptying `$assetPath`", 'light blue')."\n";

		$n = $this->clearDir($assetPath);
        if($verbose>0) echo Ansi::color("$n files has been deleted", 'green')."\n";
		return UXApp::EXIT_STATUS_OK;
	}

	private function clearDir($assetPath) {
		if ($handle = opendir($assetPath)) {
			$n = 0;
			while($entry = readdir($handle)) {
				if ($entry == "." || $entry == "..")  continue;
				if(filetype($filename = $assetPath.'/'.$entry) == 'dir') $n += $this->clearDir($filename);
				if(filetype($filename) != 'file') continue;
				unlink($filename);
				echo Ansi::color("File `$entry` is deleted", 'light blue')."\n";
				$n++;
			}
			closedir($handle);
		}
		return $n;
	}

	/**
	 * Empty cache of generated xsl files.
	 *
	 * @return int
	 * @throws Exception
	 */
	public function actXsl() {
		$genxslpath = $this->app->dataPath.'/xsl'; // Ahová a generált fájlok kerülnek
		echo Ansi::color("Emptying `$genxslpath`", 'green')."\n";

		if ($handle = opendir($genxslpath)) {
			$n = 0;
			while (false !== ($entry = readdir($handle))) {
				if ($entry != "." && $entry != "..") {
					$ext = Util::ext($entry);
					if($ext=='xsl') {
						unlink($genxslpath.'/'.$entry);
						echo Ansi::color("File `$entry` is deleted", 'light blue')."\n";
						$n++;
					}
				}
			}
			echo Ansi::color("$n files has been deleted", 'green')."\n";
			closedir($handle);
		}
		return UXApp::EXIT_STATUS_OK;
	}
}
