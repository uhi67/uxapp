UXApp framework
===============

Version 1.9 -- 2023-12-10

A web-application framework implementig model-view-controller (MVC) architectural pattern, where the Views are -> XSLT stylesheets.

The key feauture is that the html pages can be rendered on server or client-side as well. 
If the browser supports the XSLT transformation, the client-side rendering is equivalent to the server-side rendering.

Prerequisites
-------------
- php ^7.2
- composer ^2.0 [see getcomposer.org](https://getcomposer.org/download/)

Check further prerequisites using `composer check` or `php composer.phar check` 

Installation
------------
The framework can be included into your project with composer. Run `composer require uhi67/uxapp:dev-master`

Modules
-------
UXApp supports modules, see configuration/components. The modules may inject xslt patterns into the view structure.

First steps
-----------
1. Create `composer.json` of your application, and include uh67/uxapp, e.g `composer init --name myname/myapp --require uhi67/uxapp:*`
2. run `composer update`
3. run `php vendor/uhi67/uxapp/uxapp create app`

For more details, see `getting-started.md`

Command line
------------
UXApp supports CLI. You may create your own commands. The built-in commands are:
 
- create 
- asset 
- index 
- migrate

Run commands as `php uxapp $command $action $parameters`

Testing with Codeception
------------------------
Globally installed codeception must be compatible with required (currently v3.1.2)

Change log
==========

## Version 1.9 -- 2023-12-10

- Query::map() function added
- Query without from -- fixed
- UXAppPage  $act, $id is public
-  unit tests and minor fixes
## Version 1.8.3 -- 2023-11-25

- php7 type declaration fixes
- MigrateCommand fixes
- AssetCommand/clear verbose option added
- Util::waitFor fix
- Camelize command action words
- Action method description fix
- DbLogger fix
- Support relocated vendor-dir
- Model::first() default not caching
- Model::firstCached() added
- UXApp::$rootPath config property is mandatory
- DBX::$migrationPath property added (MigrateCommand uses it)
- UXApp::runCommand() added; CLI exception returning
- Query::$last property removed

## Version 1.8.2 -- 2023-09-24

- urlFormatter completed
- Query: first AndWhere fixed;
  QueryDataSource: count query reset on search;
- asset/xsl comand (from the deleted emptycache command)
- BaseModel nodename accepts '.'
- Inet: prev(), next()
- L10n prepare fix (df bug)
- Union query
- Asset: register any file into head/assests node
- AssetCommand: verbose option added
- command exit status is always integer
- viewname priority is "view/act", "view/default", "view".
- UXAppException: parametrized (array) message
- showException  title  and cli fix
- js asset  position option added
- UXAppPage::createUrl() keepQuery parameter added
- UXAppPage::currentUrl added
- sqlBuilder  precedence bug fixed
- MigrateCommand save  and verbose fix
- other minor bugfixes

## Version 1.8.1 -- 2023-02-16

- uniqueValidate fixed
- json_response header fix
-  urlFormatter fixes
- Query nodename based on short class name
- xsl design path fix
- showException fix
- light php 8 compatibility
- MigrateCommand runs init  if necessary
- ConsoleLogger added
- scalar() call on sql type Query (throws Exception)
- Module translation support, using 'uxapp' category for framework
- deprecation fixes
- trace xml fixed
- json result cannot be null
- other minor fixes

## Version 1.8 -- 2022-03-02

- Minimum php version is 7.2
- BaseModel: createNode extra attr bug
- getInt() issue: getting 0 value fixed: In Request::req() only '' or null in $_GET involves $_POST
- Model: cache database attributes (fixed)

## Version 1.7 -- 2021-08-24

- updatedb command replaced by migrate
- Migration class added (base of php migrations)  
- Model::attributesExcept() added
- Model::clearCache() added
- Query: 'from' part may be null
- DBX::columnExists() added
- refactored: getTables, getForeignKeys, getReferrerKeys
- DBX codeception tests, codeception helpers
- several bugs

## Version 1.6 -- 2021-05-13

- Component::populate() added;
- BaseUser::loadSession(), saveSession() added;
- codeception helper enhancements (working)
- updatedb/reset displays database name

## Version 1.5.3 -- 2021-05-05

- updatedb/reset action: confirm=yes switch added, sequence drop added
- DBX/DBXPG: getSequences(), dropSequence() added, some bugs
- Query/DBX: Select building without FROM part
- BaseUser::getIsAuth() function instead of var

## Version 1.5.2 -- 2021-04-28

- "Last-Modified" headers added
- Uxapp.UConfirm may use url, translation bug.
- default favicon if user file exists

## Version 1.5.1 -- 2021-04-19

- Query::andWhere() bug
- Query::first() bug 
- Util::waitFor() added
- UXAppPage::log array; redirect location bug; trace color class names refactor

## Version 1.5 -- 2021-03-24

- Subview files (under view dir by actName) (optional)
- showMessage, showException uses 'message' node 
- UserException title
- Asset registering issues, resetxsl command deletes asset cache
- DatabaseSession class added;
- L10nFile class added;
- Filelogger: array message is valid;
- Module: session must not be created;
- Util::makeDir() added;
- UXApp: xslpath default;
- skip trace if app is not initialized yet;
- UXAppPage: /xsl/design/layout.xsl is valid;
- createXsl: corrected paths in "validation" xsl ;
- unset uxapp instance after run
- FileCache::deleteFiles() utility added;
- default ico is uxapp logo
- Action method must return true for normal render
- Model::getRefererQuery() added
- setHx accepts link as array
- UXApp::showException() enhancements and bugs
- Request::reqInt(), postXxx() added
- language setting issues
- Query::distinct, \['field'=>array] translated to 'in' op. ('is null' if empty array)
- Module::registerAssetsStatic() (instead of ambiguously callable registerAssets())
- UXPAge: fileName header with disposition, charset; setContentType(), setFileName(), getFileName()
- Component: camelize property names containing '-'
- UXApp::traceException() added

## Version 1.4 -- 2020-12-20

- `create app` command
- simplified launcher
- Command line action and parameter descriptions added and displaying 
- Using shim version of jquery; using a patched jqueryui
- Codeception is required only for dev

## Version 1.3 -- 2020-12-16

- BaseModel::createNode(): subnodeFields callback argument order is now (item, index) !
- client-render is OK

## Version 1.2 -- 2020-12-04 

- BaseModel::createNode(): extra option callback function's argument order is now (item, index) !
- Model: Valid foreign key value may be falsy (e.g 0, but not null)
- Model::createNode accepts any Component
- Model:: association node name issue
- Module::getNode (ComponentInterface requirement)
- validation message translations

## Version 1.1 -- 2020-11-27

- new url concept. Url formatting is only on php side by 
    - BaseUrlFormatter and derived components
    - UXApp::getUrlBase() removed
    - UXApp::scriptUrl() removed
    - UXApp::absoluteUrl() removed, use UXApp::$app->urlFormatter->absoluteUrl()
    - UXApp::$mod_rewrite, $urlFormat removed, configure urlFormatter component
    - UXAppPage::$script <data script...> removed
    - RequestInterface::createUrl() is removed.
    - script, url-base, script-go control vars are removed, compute urls on php side
    - UXAppPAge::decodeUrl() removed, use UXApp::$app->urlFormatter
    - UXAppPAge::urlFormatter property added
    - UXAppPage::createUrl() accepts 'page/act' and uses current urlFormatter
    - BaseUrlFormatter, SimpleUrlFormatter classes added
    - RequestInterface: createUrl removed, use urlFormatter; modUrl() deprecated, use to BaseUrlFormatter::modUrl()
- Asset management (asset cache and url)
    - Module::registerAssets() may be called statically, and uses AssetManager
    - AssetManager class introduced
    - Asset class introduced
    - AccetCommnd with "clear" action added
    - UXPage::addAsset(array) calling with array is deprecated, use AssetManager
- Cache changes
    - CacheInterface changed, set() added
    - SessionCache class added
    - SingleCache added
- Util::simpleClassName() renamed to shortName(); Util::ext() added
- ArrayUtils::array_find_key() now uses callback wit (item, key) argument order!
- UserException is introduced (End-user errors without backtrace)
- Component::simpleClassName() added
- UXApp::mimetype(file|ext) added
- UXAppPage:$noflash switch (supress retrieving flash messages on slave pages)
- BaseSession: set() method added
- Bugfixes
    - Query builder alias issue fixed
    - Codeception issues: downgraded to Codeception 3.
    - UXAppPage: validation xsl bugfix
- less stylesheet source    

Version 1.0.1 -- 2020-11-18
---------------------------
- updatedb command: verbose option added
- Util::simpleClassName() added
- UXAppPage::createUrl() accepts string
- UXPAge::addAsset() bugfix

Version 1.0 -- 2020-11-02
-------------------------
- first public release
