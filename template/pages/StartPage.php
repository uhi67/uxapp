<?php


namespace app\pages;


class StartPage extends \uhi67\uxapp\UXAppPage {
	public function actDefault() {
		$this->view->addContentNode('message', null, 'Hello World');
		return true;
	}
}
