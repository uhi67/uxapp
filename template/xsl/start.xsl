<?xml version="1.0" encoding="UTF-8"?>
<!-- example.xsl - view for ExamplePage.php controller -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="xml" version="1.0" indent="no"
				doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
				doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
				omit-xml-declaration="yes"
	/>

	<xsl:template match="content">
		<h2><xsl:value-of select="../head/h2"/></h2>
		<xsl:apply-templates select="message"/>
	</xsl:template>

	<xsl:template match="message">
		<p><xsl:value-of select="." /></p>
	</xsl:template>

</xsl:stylesheet>
