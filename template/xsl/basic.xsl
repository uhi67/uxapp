<?xml version="1.0" encoding="UTF-8"?>
<!-- basic layout - template -->
<!--suppress XmlDefaultAttributeValue -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="xml" version="1.0" indent="no"
				doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
				doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
				omit-xml-declaration="yes"
	/>

	<xsl:template match="data">
		<h1><xsl:value-of select="head/h1"/></h1>
		<xsl:apply-templates select="content"/>
	</xsl:template>

</xsl:stylesheet>
