<?php
/**
 * @author uhi
 * @copyright 2016
 * @abstract
 * This is the main router/dispatcher of UXApp framework.
 * It also responsible for including libraries and serving assets.
 * In the framework, this file can be found at /lib/_index.php
 * Please copy this file to the into application's web-root directory as 'index.php'
 * Redirect to this file in .htaccess if file not found
 */

use uhi67\uxapp\UXApp;

if(!file_exists($configFile = dirname(__DIR__).'/config/config.php')) {
	echo "Internal error: Main configuration file is missing.";
	exit;
}
$vendorDir = getenv('COMPOSER_VENDOR_DIR') ?: dirname(__DIR__) . '/vendor';
if(!file_exists($vendorload = $vendorDir. '/autoload.php')) echo "<h3>Composer install needed</h3>";
require $vendorload;
$config = require_once $configFile;	// path to config
UXApp::go($config);
