<?php /** @noinspection PhpUnhandledExceptionInspection */

/**
 * Application's 'global configuration file template: copy to `/config/config.php` and fill the real data
 *
 * @filesource	config-template.php
 * @package UXApp
 * @author uhi
 * @copyright 2020
 */

/**
 * @var array $config
 * Application's 'global configuration array
 */
return [
	'class' => \uhi67\uxapp\UXApp::class,

	/** Application basic setup parameters */
	'moduleName' => 'App',
	'timeZone' => 'Europe/Budapest',
	/** example: http://app.test */
	#'baseurl'=>	$baseurl = getenv('baseurl'),
	'render' => 'client',		// default render mode: server (default), client
	#'design' => 'appdesign',	// Module name contains design theme (may be in application or in UXApp library)
	'locale' => 'hu',

	'environment' => isset($_ENV['APPLICATION_ENV']) ? $_ENV['APPLICATION_ENV'] : 'production', // must be 'development' or 'production'
	#'message' => '', // Message to display on every page!

	/** Configurations of Application-specific modules */
	'components' => [
		/** Debugger */
		'debug' => [
			'class' => \uhi67\debug\Debug::class,
			'enabled' => true,
		],

		/** Default database connection configuration */
		'database' => [
			'class' => \uhi67\uxapp\DBXPG::class,
			'name' => 'app',
			'user' => 'app',
			'password' => getEnv('db_password'),
		],

		'urlFormatter' => [
			'class' => \uhi67\uxapp\SimpleUrlFormatter::class,
			'compress' => true,
		],

		// Localization
		'l10n' => [
			'class' => \uhi67\uxapp\L10nBase::class,	// Localization classname. Must provide getText(category, text, params, language) and formatDate(DateTime, datetype, timetype, language) for UXApp::la/UXApp:fd
			//'language' => 'hu',						// Default language with optional locale, may be changed by UXApp::setLang(lang/locale)
			'source' => 'en',							// Default source language, default is 'en' (not used in L10nBase)
			'locale' => 'hu'
		],
	],
];
