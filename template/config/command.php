<?php /** @noinspection PhpUnhandledExceptionInspection */

/**
 * Application's 'global configuration for CLI commands: copy to `/config/command.php` and fill the real data
 *
 * @filesource	command-template.php
 * @package UXApp
 * @author uhi
 * @copyright 2020
 */

/**
 * Application's 'global configuration array
 */
return [
	'class' => \uhi67\uxapp\UXApp::class,

	/** Application basic setup parameters */
	'timeZone' => 'Europe/Budapest',
	'locale' => 'hu',

	'environment' => isset($_ENV['APPLICATION_ENV']) ? $_ENV['APPLICATION_ENV'] : 'production', // must be 'development' or 'production'

	/** Configurations of Application-specific modules */
	'components' => [
		/** Default database connection configuration */
		'database' => [
			'class' => \uhi67\uxapp\DBXPG::class,
			'name' => 'app',
			'user' => 'app',
			'password' => getenv('db_password'),
		],

		'urlFormatter' => [
			'class' => \uhi67\uxapp\SimpleUrlFormatter::class,
			'compress' => true,
		],

		// Localization
		'l10n' => [
			'class' => \uhi67\uxapp\L10nBase::class,	// Localization classname. Must provide getText(category, text, params, language) and formatDate(DateTime, datetype, timetype, language) for UXApp::la/UXApp:fd
			//'language' => 'hu',						// Default language with optional locale, may be changed by UXApp::setLang(lang/locale)
			'source' => 'en',							// Default source language, default is 'en' (not used in L10nBase)
		],
	],
];
