create table "{$tablename}" (
	id serial,
	name text,
	applied timestamp
);

ALTER TABLE "{$tablename}" OWNER TO "{$owner}";
GRANT ALL ON TABLE "{$tablename}" TO "{$user}";
GRANT ALL ON SEQUENCE "{$tablename}_id_seq" TO "{$user}";
