/*
	Sample migration for creating systext table necessary for L10n class.
	PostgreSQL syntax.
	Copy into applicaton's /sql folder and apply in migrate command if you want to use it.
*/

CREATE TABLE systext (
    id serial primary key,
    name text,
    value text,
    title text,
    lang character varying(2) DEFAULT 'en'::character varying NOT NULL, -- may reference to language table if exists
    source integer references systext
);
ALTER TABLE systext OWNER TO "$owner";
COMMENT ON COLUMN systext.lang IS 'ISO-639-1 language code';
COMMENT ON COLUMN systext.source IS 'The original (reference) text of the translation. Null if this is the original.';
ALTER TABLE ONLY systext
    ADD CONSTRAINT systext_name_key UNIQUE (name, lang),
    ADD CONSTRAINT systext_source_key UNIQUE (source, lang);

GRANT ALL ON TABLE systext TO "$owner";
GRANT ALL ON TABLE systext TO "$user";
GRANT ALL ON SEQUENCE systext_id_seq TO "$owner";
GRANT SELECT,USAGE ON SEQUENCE systext_id_seq TO "$user";
