<?php
return [
	'Hiányzó nézetfájl `{$act}`',
	'Unknown action `{$act}`' => 'Ismeretlen művelet `{$act}`',
	'Not found' => 'Nem található',

	'is mandatory' => 'kötelező',
	'must be unique' => 'nem ismétlődhet',
	'érvénytelen formátumú',
	'hossza $1 és $2 között kell legyen',
	'értéke $1 és $2 között kell legyen',
];
