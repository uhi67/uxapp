Building new UXApp application
==============================

1. create composer.json with require `"uhi67/uxapp": "dev-master"` and repository `"url": "https://asset-packagist.org"`,
2. create runtime, www/assets directories with write access of web-server
3. copy `~/uxapp` to root, `~/www/index.php` to `/www/index.php` 
4. create `.htaccess`
5. create `readme.md` 
6. create .gitignore, `git init`, create git repository,
7. configure web server
8. create config / `command.php` and `config.php`

Buildig application details
---------------------------

### Application design

Create `/xsl/basic.xsl` stylesheet for basic application layout (use `uhi67/uxapp/template/xsl/basic.xsl` as template)

### Controllers and views

1. Create `/pages` dir and `/pages/StartPage.php` class as the first controller (use `uhi67/uxapp/template/pages/StartPage.php` as template)
2. Create `/xsl/start.xsl` view as the first view (use `uhi67/uxapp/template/xsl/example.xsl` as template)

### Database and models

1. Create database and configure in `config/config.php` and `config/command.php` (See `uhi67/uxapp/template/config/*`)
2. Create database-creation migration in `/sql/`
3. run `php uxapp migrate init`, `php uxapp migrate update` to create database tables
4. Create `/models` dir, run `php uxapp create model <tablename>` to create model class from a table

### Localization

